//
//  area.h
//  jsondata
//
//  Created by 王亚陆 on 16/5/18.
//  Copyright © 2016年 KevinLHP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface area : NSObject

- (NSMutableArray *)selectAreaWithFArea:(NSString*)FirstArea withTarea:(NSString*)TwoArea withFarea:(int)level;

@end
