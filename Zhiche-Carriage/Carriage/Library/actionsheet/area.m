//
//  area.m
//  jsondata
//
//  Created by 王亚陆 on 16/5/18.
//  Copyright © 2016年 KevinLHP. All rights reserved.
//
#import "area.h"



#define LOCAL_AREA_DB_NAME      @"area"
#define LOCAL_AREA_DB_PATH     [[NSBundle mainBundle] pathForResource:LOCAL_AREA_DB_NAME ofType:@"txt"]

@implementation area

- (NSMutableArray *)selectAreaWithFArea:(NSString*)FirstArea withTarea:(NSString*)TwoArea withFarea:(int)level{
    
    NSMutableArray * array = [[NSMutableArray alloc]init];
    NSString * path = LOCAL_AREA_DB_PATH;
    
    
    //《1》将文件的内容读取出来
    NSString*conterts=[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    //    NSString * path = [[NSBundle mainBundle] pathForResource:LOCAL_AREA_DB_NAME ofType:@"txt"];
    //    NSString *string = [NSString st];
    NSData*data=[conterts dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary*dic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    array = dic[@"data"];
    
    
    if (level==1) {
        
        return array;
    }
    if (level==2) {
        
        for (NSMutableDictionary*obj in array) {
            
            if ([obj[@"provinceName"] isEqualToString:FirstArea]) {
                array = obj[@"cityList"];
                return array;
            }
        }
    }
    
    if (level==3) {
        
        for (NSMutableDictionary * obj in array) {
            if ([obj[@"provinceName"] isEqualToString:FirstArea]) {
                
                NSMutableArray * arr = obj[@"cityList"];
                for (NSMutableDictionary * obj1 in arr) {
                    
                    if ([obj1[@"cityName"] isEqualToString:TwoArea]) {
                        array = obj1[@"countyList"];
                        return array;
                    }
                }
            }
        }
    }
    
    return array;
}
@end
