//
//  ImageWebVC.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ImageWebVC.h"
#import "RootViewController.h"

@interface ImageWebVC ()
{
    UIImageView * nav;
    RootViewController * TabBar;

}

@end

@implementation ImageWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"详情"];
    [self.view addSubview:nav];
    TabBar = [RootViewController defaultsTabBar];

    UIWebView * webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight-64)];
    NSString * path = nil;
    if (self.imageURL.length>0) {
        path = self.imageURL;
    }else{
        path = @"www.baidu.com";
    }
    NSURL * url = [NSURL URLWithString:path];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    [self.view addSubview:webView];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabBar setTabBarHidden:YES];
    
}



@end
