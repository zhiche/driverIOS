//
//  ImageWebVC.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NavViewController.h"

@interface ImageWebVC : NavViewController

@property (nonatomic,strong) NSString * imageURL;

@end
