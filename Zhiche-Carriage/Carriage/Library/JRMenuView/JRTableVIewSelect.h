//
//  JRTableVIewSelect.h
//  Uni-Desgin
//
//  Created by 深圳华尔兹科技 on 16/1/19.
//  Copyright © 2016年 华尔兹r_Jr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JRMenuCell.h"
@interface JRTableVIewSelect : UIView
/**
 *  创建一个弹出下拉控件
 *
 *  @param frame      尺寸
 *  @param selectData 选择控件的数据源
 *  @param action     点击回调方法
 *  @param animate    是否动画弹出
 */
+ (void)addJRTableVIewSelectWithWindowFrame:(CGRect)frame selectData:(NSArray *)selectData  action:(void(^)(NSInteger index))action animated:(BOOL)animate;
/**
 *  手动隐藏
 */
+ (void)hiden;
@end
