
//
//  JRMenuCell.m
//  Uni-Desgin
//
//  Created by 深圳华尔兹科技 on 16/1/19.
//  Copyright © 2016年 华尔兹r_Jr. All rights reserved.
//

#import "JRMenuCell.h"

@implementation JRMenuCell{
    UIView *sepView;
    UIImageView *imageView;
}

+(instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *cellID = @"JRMenuCell";
    JRMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[JRMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    return cell;
    
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
 
        //创建子控件
        [self setupChildViews];
        self.backgroundColor = UniColor(248, 248, 248);
    }
    return self;
    
}

//创建子控件
-(void)setupChildViews{
 
    UILabel *titile  = [UILabel new];
    _title = titile;
    _title.textColor = UniColor(102, 102, 102);
    _title.font = Font(14.0f);
    _title.textAlignment = 1;
    [self addSubview:titile];
    
//    sepView = [[UIView alloc] init];
//    sepView.backgroundColor = UniColor(182, 182, 182);
//    [self addSubview:sepView];
//
//    imageView = [[UIImageView alloc] init];
//    imageView.image = [UIImage imageNamed:@"diy_Btn_right"];
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
//    [self addSubview:imageView];
    
}
- (void)layoutSubviews {
    [super layoutSubviews];
    _title.frame = CGRectMake(0, 0, self.width ,self.height);
//    sepView.frame = CGRectMake(0, self.height - 1.0f, self.width, 1.0f);

//    imageView.frame = CGRectMake(self.width - kAdpatWidthIpone5(25.0f), self.height/2 - kAdpatHeightIpone5(10.0f), kAdpatWidthIpone5(25.0f), kAdpatHeightIpone5(20.0f));
}

@end
