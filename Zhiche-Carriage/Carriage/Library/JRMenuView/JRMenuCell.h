//
//  JRMenuCell.h
//  Uni-Desgin
//
//  Created by 深圳华尔兹科技 on 16/1/19.
//  Copyright © 2016年 华尔兹r_Jr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JRMenuCell : UITableViewCell
@property (nonatomic, weak) UILabel *title;

+(instancetype)cellWithTableView:(UITableView *)tableView;
 
@end
