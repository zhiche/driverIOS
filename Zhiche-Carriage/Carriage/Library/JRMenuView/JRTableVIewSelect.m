


//
//  JRTableVIewSelect.m
//  Uni-Desgin
//
//  Created by 深圳华尔兹科技 on 16/1/19.
//  Copyright © 2016年 华尔兹r_Jr. All rights reserved.
//

#import "JRTableVIewSelect.h"
#import "JRMenuCell.h"

@interface JRTableVIewSelect ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, copy) NSArray *selectData;
@property (nonatomic, copy) void(^action)(NSInteger index);


@end


JRTableVIewSelect *newBackgroundView;
UITableView *newTableView;




@implementation JRTableVIewSelect

/**
 *  创建一个弹出下拉控件
 *
 *  @param frame      尺寸
 *  @param selectData 选择控件的数据源
 *  @param action     点击回调方法
 *  @param animate    是否动画弹出
 */
+ (void)addJRTableVIewSelectWithWindowFrame:(CGRect)frame selectData:(NSArray *)selectData   action:(void(^)(NSInteger index))action animated:(BOOL)animate
{
    if (newBackgroundView != nil) {
        [JRTableVIewSelect hiden];
    }
    UIWindow *win = [[[UIApplication sharedApplication] windows] firstObject];
    
    newBackgroundView = [[JRTableVIewSelect alloc] initWithFrame:win.bounds];
    newBackgroundView.action = action;
    newBackgroundView.selectData = selectData;
    newBackgroundView.backgroundColor = [UIColor clearColor];
    [win addSubview:newBackgroundView];
    
  
     
    newTableView = [[UITableView alloc] initWithFrame:frame];
    newTableView.scrollEnabled = NO;
    newTableView.backgroundColor = [UIColor clearColor];
    newTableView.dataSource = newBackgroundView;
    newTableView.delegate = newBackgroundView;
    newTableView.showsVerticalScrollIndicator = NO;
    newTableView.rowHeight = frame.size.height/selectData.count;
    newTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    newTableView.tableHeaderView.backgroundColor =[UIColor clearColor];
    [win addSubview:newTableView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBackgroundClick)];
    [newBackgroundView addGestureRecognizer:tap];
    newBackgroundView.action = action;
    newBackgroundView.selectData = selectData;
    
    if (animate == YES) {
 
        newTableView.frame = CGRectMake(newTableView.x, newTableView.y, newTableView.width, 0);
        
        [UIView animateWithDuration:0.3 animations:^{
 
            newTableView.frame = CGRectMake(newTableView.x, newTableView.y, newTableView.width, frame.size.height);
            
        }];
    }
    
}
+ (void)tapBackgroundClick
{
    [JRTableVIewSelect hiden];
}
+ (void)hiden
{
    [UIView animateWithDuration:0.3 animations:^{
        
        newTableView.height = 0;
        
        [newBackgroundView removeFromSuperview];
        [newTableView removeFromSuperview];
        newTableView = nil;
        newBackgroundView = nil;
    }];
  
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.selectData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JRMenuCell *cell = [JRMenuCell cellWithTableView:tableView];

    if (!cell.selectedBackgroundView)
    {
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame] ;
         cell.selectedBackgroundView.backgroundColor = UniColor(234, 234, 234);
    }
   
    cell.title.text = self.selectData[indexPath.section];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.action) {
        self.action(indexPath.section);
        
    }
    [JRTableVIewSelect hiden];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.00000001f;
}
@end
