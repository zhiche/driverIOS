//
//  AppDelegate.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/6/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>

@interface AppDelegate : UIResponder
<
    UIApplicationDelegate,
    BMKMapViewDelegate,
    BMKLocationServiceDelegate,
    BMKGeoCodeSearchDelegate
>
{
    BMKMapManager * _mapManager;
    BMKLocationService * _locService;
    BMKGeoCodeSearch *_geoCode;
    Reachability *_hostReach;
}
@property (strong, nonatomic) UIWindow *window;

- (void)reachabilityChanged:(NSNotification *)note;

- (void)updateInterfaceWithReachability:(Reachability *)curReach;

@end

