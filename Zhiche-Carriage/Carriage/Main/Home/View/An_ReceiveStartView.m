//
//  An_ReceiveStartView.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/24.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import "An_ReceiveStartView.h"

#define BadgeW 14*kWidth
#define BadgeH 14*kHeight
#define BadegMarginRight 12*kWidth
#define BadegMarginTop BadgeH/2

#define Com_ViewW 80*kWidth
#define Com_ViewH 60*kHeight

#define Com_TitleW 35*kWidth
#define Com_TitleH 35*kHeight
#define Com_TitleX (Com_ViewW - Com_TitleW)/2
#define Com_TitleY (Com_ViewH - Com_TitleH)/2



@interface An_ReceiveStartView ()

@property (nonatomic, assign, readwrite) CGRect newsFrame;
@property (nonatomic, weak, readwrite)  UILabel *competitionCount_l;
@property (nonatomic, weak, readwrite) UIButton *competitionTitle;
@property (nonatomic, strong)  NSDictionary *attrs;
@end

@implementation An_ReceiveStartView{
    BOOL _show;
}
@synthesize competitionView;
- (NSDictionary *)attrs {
    if (!_attrs) {
        _attrs = @{NSFontAttributeName : [UIFont systemFontOfSize:9*kWidth]};
    }
    return _attrs;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.newsFrame = frame;
        _show = NO;
        [self setUpUI];
    }
    return self;
}



- (void)setUpUI {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    imageView.image = [UIImage imageNamed:@"backgroundImage"];
    [self addSubview:imageView];
 
    
    UIButton *centerBtn = [[UIButton alloc] initWithFrame:CGRectMake((self.newsFrame.size.width - 143*kWidth)/2, (self.newsFrame.size.height - 143*kHeight)/2, 143*kWidth, 143*kHeight)];
  
    
    centerBtn.tag = 100;
    [centerBtn setBackgroundImage:[UIImage imageNamed:@"startOrder"] forState:UIControlStateNormal];
    [centerBtn setImage:[UIImage imageNamed:@"pressStartOrder"] forState:UIControlStateSelected];
    centerBtn.titleLabel.font = Font(19);
    [centerBtn addTarget:self action:@selector(pressStarOrder) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:centerBtn];

}

- (void)pressStarOrder {
    
    [self.delegate startTheOrder];
    
}

@end
