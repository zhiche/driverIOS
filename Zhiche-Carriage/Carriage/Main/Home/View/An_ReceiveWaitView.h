//
//  An_ReceiveWaitView.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/24.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import <UIKit/UIKit.h>
@class An_ReceiveWaitView;

@protocol An_ReceiveWaitViewDelegate <NSObject>

- (void)waitingCancel;

- (void)waitingReload;

@end

@interface An_ReceiveWaitView : UIView

@property (nonatomic, strong) UILabel *orderNumber;//您的号码是10号

@property (nonatomic, strong) UILabel *waitNumber;//您的前面还有5位

@property (nonatomic, strong) UILabel *waitOrder;//待运单

- (void)waitReloadData:(NSDictionary *)dic;

@property (nonatomic, weak) id<An_ReceiveWaitViewDelegate> delegate;

@end
