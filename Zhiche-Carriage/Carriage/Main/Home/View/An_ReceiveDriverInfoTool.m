//
//  An_ReceiveDriverInfoTool.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/23.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import "An_ReceiveDriverInfoTool.h"
#import "UILabel+Category.h"

#define LocationTimeout 10

#define ReGeocodeTimeout 5

#define threeWidth  screenWidth/3
#define twoWidth  screenWidth/2

#define  SDCycleScrollViewH 164.0f

#define  DiverInfoToolH 70.0f

@interface An_ReceiveDriverInfoTool()

@property (nonatomic, weak, readwrite) NSArray *dataArray;

@property (nonatomic, assign, readwrite) CGRect newsframes;

@end

@implementation An_ReceiveDriverInfoTool

+ (instancetype)initWithFrame:(CGRect)frame dataArray:(NSArray *)dataArray {
    return [self initWithFrame:frame dataArray:dataArray];
}

- (instancetype)initWithFrame:(CGRect)frame dataArray:(NSArray *)dataArray {
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.newsframes = frame;
        self.dataArray = dataArray;
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
 
    CGRect frame = CGRectMake(0,  self.newsframes.size.height - 0.5, screenWidth,0.5);
    NSUInteger count = self.dataArray.count;
    
    
    ///Line
    UIImageView *imageline = [[UIImageView alloc] initWithFrame:frame];
    imageline.backgroundColor = AddCarNameBtnColor;
    [self addSubview:imageline];
 
    
    CGFloat space = screenWidth / self.dataArray.count - 30*kWidth;
    
    
    for (NSUInteger i = 0; i < count; i++)
    {
        
        /*
            这个按钮用作显示文字和图片
        */
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(50*kWidth+i*space, 10*kHeight, 70*kWidth, 21*kHeight)];
        

        
        if (i == 0)
        {
            [btn setImage:[UIImage imageNamed:@"billImage"] forState:UIControlStateNormal];
        }
        else
        {
            [btn setTitle:@"0" forState:UIControlStateNormal];
        }
    
        [btn setTitleColor:YellowColor forState:UIControlStateNormal];
        //btn.backgroundColor = UniRandomColor;
        btn.titleLabel.font = Font(FontOfSize12);
        
        btn.tag = 1101 + i;
       
        [self addSubview:btn];
        
        
        
        /*
            这个按钮用作点击
         */
        UIButton *btnClicked = [[UIButton alloc] initWithFrame:CGRectMake(twoWidth*i, 8*kHeight, twoWidth, 46*kHeight)];
        btnClicked.tag = 1001+i;
        
        [btnClicked addTarget:self
                      action:@selector(itemClick:)
            forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:btnClicked];
        

        /*
            这个Lable用作显示文字
         */
        
        
        UILabel * label = [UILabel createUIlabel:self.dataArray[i] andFont:FontOfSize12 andColor:AddressSCtitleColor];
     
        
        label.textAlignment = 1;
        //label.backgroundColor = UniRandomColor;
        label.x = btn.x;
        label.width = btn.width;
        label.y = CGRectGetMaxY(btn.frame) + kHeight*5;
        [self addSubview:label];
        
        
    }
 
}
- (void)itemClick:(UIButton *)itemClick{
    [self.delegate diverInfoToolClickItem:itemClick];
}




@end
