//
//  An_CompetitionOrderView.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/24.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import "An_CompetitionOrderView.h"
#import "UIView+jmCategory.h"

@interface An_CompetitionOrderView ()

@property (nonatomic, assign, readwrite) CGRect newsFrame;

@property (nonatomic, copy) CompentitionClick click;
@property (nonatomic, strong)  NSDictionary *attrs;
@property (nonatomic, weak, readwrite)  UILabel *competitionCount_l;
@property (nonatomic, weak, readwrite) UIButton *competitionTitle;

@end

@implementation An_CompetitionOrderView
{
    BOOL _show;
}

- (NSDictionary *)attrs {
    if (!_attrs) {
        _attrs = @{NSFontAttributeName : [UIFont systemFontOfSize:9*kWidth]};
    }
    return _attrs;
}
- (instancetype)initWithFrame:(CGRect)frame
            compentitionClick:(CompentitionClick)click {
    self = [super initWithFrame:frame];
    if (self) {
        self.newsFrame = frame;
        self.click = click;
        _show = NO;
        [self setUpUI];
    }
    return self;
}
#define BadgeW 14*kWidth
#define BadgeH 14*kHeight
#define BadegMarginRight 12*kWidth
#define BadegMarginTop BadgeH/2



///图片
#define Com_TitleW 35*kWidth
#define Com_TitleH 35*kHeight
#define Com_TitleX (Com_ViewW - Com_TitleW)/2
#define Com_TitleY (Com_ViewH - Com_TitleH)/2


#define popDuration 0

- (void)setUpUI {
    
    CGRect btnFrame = CGRectMake(Com_TitleX, Com_TitleY, Com_TitleW, Com_TitleH);
    
    UIButton *competitionTitle = [[UIButton alloc] initWithFrame:btnFrame];
    self.competitionTitle = competitionTitle;
    [competitionTitle setImage:[UIImage imageNamed:@"icon_grab_normal"]
                                forState:UIControlStateNormal];
    [competitionTitle setImage:[UIImage imageNamed:@"icon_grab_pressed"]
                                forState:UIControlStateSelected];
    competitionTitle.backgroundColor = [UIColor clearColor];
    [self addSubview:competitionTitle];
    
    
    
    UILabel *badgeLable = [[UILabel alloc] initWithFrame:CGRectMake((Com_TitleX + Com_TitleW - BadegMarginRight), Com_TitleY - BadgeH/2, BadgeW, BadgeH)];
    self.competitionCount_l = badgeLable;
    badgeLable.layer.masksToBounds = YES;
    badgeLable.textAlignment = 1;
    badgeLable.textColor = WhiteColor;
    badgeLable.backgroundColor = [UIColor redColor];
    badgeLable.text = @"0";
    badgeLable.font = Font(8.0)
    badgeLable.layer.cornerRadius = BadgeH/2;
    [self addSubview:badgeLable];
    
    
    
    
    [competitionTitle addTarget:self
                         action:@selector(competitionBtnClick)
               forControlEvents:UIControlEventTouchUpInside];
    
 
}
- (void)competitionBtnClick{
    self.click();
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}


- (void)setCompetitionCount:(NSUInteger)competitionCount {
    _competitionCount = competitionCount;
    
    
    CGSize size = [[NSString stringWithFormat:@"%ld", competitionCount] sizeWithAttributes:self.attrs];
    
    if (size.width  >= BadgeW)
    {
        self.competitionCount_l.width = size.width;
        self.competitionCount_l.x = (Com_TitleX + Com_TitleW - BadegMarginRight) - (size.width - BadgeW) ;
    }
    
    self.competitionCount_l.text = [NSString stringWithFormat:@"%ld", competitionCount];
    //NSLog(@"%f", size.width);
    
    
    
    if (competitionCount <= 0)
    {
        if (_show != NO)
        {
            self.competitionTitle.userInteractionEnabled = NO;
            
            [self.competitionTitle.layer removeAnimationForKey:@"competition"];
            
        
            [UIView animateWithDuration:popDuration animations:^{
                
                _show = NO;
                
                self.x = screenWidth;
            }];
        }
    }
    else
    {
        if (_show == NO)
        {
            
            [UIView animateWithDuration:popDuration animations:^{
                _show = YES;
                
                self.x = screenWidth - Com_ViewW;
                
                self.competitionTitle.userInteractionEnabled = YES;
                
            }];
            
         
        
            __weak typeof(self) weakSelf = self;
            
            double delayInSeconds = popDuration;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
               
                [weakSelf.competitionTitle.layer addAnimation:[weakSelf opacityForever_Animation:0.25] forKey:@"competition"];
                
            });
            
        }
    }
}



@end
