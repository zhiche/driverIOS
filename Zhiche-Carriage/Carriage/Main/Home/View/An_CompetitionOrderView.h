//
//  An_CompetitionOrderView.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/24.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import <UIKit/UIKit.h>

#define Com_ViewW 80*kWidth

#define Com_ViewH 60*kHeight

typedef void(^CompentitionClick)();

@interface An_CompetitionOrderView : UIView

 

- (instancetype)initWithFrame:(CGRect)frame
            compentitionClick:(CompentitionClick)click;

@property (nonatomic, assign, readwrite) NSUInteger competitionCount;

@end
