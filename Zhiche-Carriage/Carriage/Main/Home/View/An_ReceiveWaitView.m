//
//  An_ReceiveWaitView.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/24.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import "An_ReceiveWaitView.h"
#import "UILabel+Category.h"
#
@interface An_ReceiveWaitView ()

@property (nonatomic, assign, readwrite) CGRect newsFrame;




@end

@implementation An_ReceiveWaitView

@synthesize orderNumber, waitNumber, waitOrder;

- (void)waitReloadData:(NSDictionary *)dic {
    NSString * string = [NSString stringWithFormat:@"您的号码是%@号",[self backString:dic[@"rankNumber"]]];
    NSString * rankNumber = [NSString stringWithFormat:@"%@",[self backString:dic[@"rankNumber"]]];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    [str addAttribute:NSForegroundColorAttributeName value:YellowColor range:NSMakeRange(5,rankNumber.length)];//(5,2) 2是位数
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize19*kWidth] range:NSMakeRange(5, rankNumber.length)];
    self.orderNumber.attributedText = str;
    
    NSString * string1 = [NSString stringWithFormat:@"您前面还有%@位司机在等待",[self backString:dic[@"frontNumber"]]];
    NSString * frontNumber = [NSString stringWithFormat:@"%@",[self backString:dic[@"frontNumber"]]];
    NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:string1];
    [str1 addAttribute:NSForegroundColorAttributeName value:YellowColor range:NSMakeRange(5,frontNumber.length)];//(5,2) 2是位数
    [str1 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize19*kWidth] range:NSMakeRange(5, frontNumber.length)];
    self.waitNumber.attributedText = str1;
    NSString * number = [NSString stringWithFormat:@"待运单%@",[self backString:dic[@"orderNumber"]]];
    self.waitOrder.text = number;
}
- (NSString *)backString:(NSString *)string{
    //    NSString * str = [NSString stringWithFormat:@"%@",string];
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.newsFrame = frame;
        [self setUpUI];
    }
    return self;
}

- (void)setUpUI {
    [self createWaitView];
}

- (void)createWaitView {
    
  
    orderNumber = [UILabel createUIlabel:@"您的号码是0号" andFont:FontOfSize14 andColor:littleBlackColor];
    [self addSubview:orderNumber];
    [orderNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_top).with.offset(30*kHeight);
        make.centerX.mas_equalTo(self.mas_centerX);
    }];
    
    waitNumber = [UILabel createUIlabel:@"您前面还有0位司机在等待" andFont:FontOfSize12 andColor:littleBlackColor];
    [self addSubview:waitNumber];
    [waitNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(orderNumber.mas_centerY).with.offset(30*kHeight);
        make.centerX.mas_equalTo(self.mas_centerX);
    }];
    
    UIButton * btnFresh = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnFresh addTarget:self action:@selector(pressFreshBtn) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnFresh];
    [btnFresh mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.top.mas_equalTo(self.mas_top);
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.bottom.mas_equalTo(waitNumber.mas_bottom);
    }];
    
    UIButton * QXbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [QXbtn setTitle:@"取消排队" forState:UIControlStateNormal];
    QXbtn.titleLabel.font = Font(FontOfSize14);
    QXbtn.backgroundColor = GrayColor;
    [QXbtn setTitleColor:carScrollColor forState:UIControlStateNormal];
    QXbtn.layer.borderColor = RGBACOLOR(186, 186, 186, 1).CGColor;
    QXbtn.layer.borderWidth = 0.5;
    QXbtn.layer.cornerRadius = 5;
    [QXbtn addTarget:self action:@selector(pressQXbtn) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:QXbtn];
    [QXbtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(screenWidth*0.75, 40*kHeight));
        make.centerY.equalTo(self.mas_bottom).with.offset(-80*kHeight);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(QXbtn.mas_bottom).offset(30);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 60, 40));
    }];
    // label.text = @"小知实在无法帮您匹配到您的意向城市订单时，只能分配您其他方向订单，感谢您谅解小知。";
    label.font = Font(11);
    label.textColor = littleBlackColor;
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    
    
    
    NSString * number = [NSString stringWithFormat:@"待运单0"];
    waitOrder = [UILabel createUIlabel:number andFont:FontOfSize14 andColor:AddressSCtitleColor];
    [self addSubview:waitOrder];
    [waitOrder mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(QXbtn.mas_top).with.offset(-25*kHeight);
    }];
    
    UIImageView * ball1 = [[UIImageView alloc]init];
    ball1.layer.cornerRadius = 1.5*kHeight;
    ball1.backgroundColor = RGBACOLOR(154, 154, 154, 1);
    [self addSubview:ball1];
    [ball1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(waitOrder.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(3*kWidth, 3*kHeight));
        make.right.mas_equalTo(waitOrder.mas_left).with.offset(-7*kWidth);
    }];
    UIImageView * line1 = [[UIImageView alloc]init];
    line1.backgroundColor = RGBACOLOR(154, 154, 154, 1);
    [self addSubview:line1];
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(waitOrder.mas_centerY);
        make.left.mas_equalTo(self.mas_left).with.offset(24*kWidth);
        make.right.mas_equalTo(ball1.mas_left).with.offset(-4*kWidth);
        make.height.mas_equalTo(0.5);
    }];
    UIImageView * ball2 = [[UIImageView alloc]init];
    ball2.layer.cornerRadius = 1.5*kHeight;
    ball2.backgroundColor = RGBACOLOR(154, 154, 154, 1);
    [self addSubview:ball2];
    [ball2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(waitOrder.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(3*kWidth, 3*kHeight));
        make.left.mas_equalTo(waitOrder.mas_right).with.offset(7*kWidth);
    }];
    UIImageView * line2 = [[UIImageView alloc]init];
    line2.backgroundColor = RGBACOLOR(154, 154, 154, 1);
    [self addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(waitOrder.mas_centerY);
        make.right.mas_equalTo(self.mas_right).with.offset(-24*kWidth);
        make.left.mas_equalTo(ball2.mas_right).with.offset(4*kWidth);
        make.height.mas_equalTo(0.5);
    }];
}

#pragma mark - 按钮点击刷新数据
- (void)pressFreshBtn {
    [self.delegate waitingReload];
}

#pragma mark - 取消排队
- (void)pressQXbtn {
    [self.delegate waitingCancel];
}
@end
