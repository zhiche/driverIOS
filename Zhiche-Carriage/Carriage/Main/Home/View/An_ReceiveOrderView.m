//
//  An_ReceiveOrderView.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/24.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import "An_ReceiveOrderView.h"
#import "UILabel+Category.h"


@interface An_ReceiveOrderView ()

@property (nonatomic, assign, readwrite) CGRect newsFrame;

@end

@implementation An_ReceiveOrderView
@synthesize orderTitle;//今日订单
@synthesize timelabel;//今日时间
@synthesize statelabel;//订单状态
@synthesize starlabel;//开始地址
@synthesize endlabel;//结束地址
@synthesize pricelabel;//金额
@synthesize extraCostlabel;

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.newsFrame = frame;
        [self setUpUI];
    }
    return self;
}
- (void)orderViewReloadData:(NSDictionary *)dic {
    self.orderTitle.text = [NSString stringWithFormat:@"%@",dic[@"showWaybillDate"]];
    self.timelabel.text = [NSString stringWithFormat:@"%@",dic[@"showDate"]];
    self.statelabel.text = [NSString stringWithFormat:@"%@",dic[@"statusText"]];
    self.starlabel.text = [NSString stringWithFormat:@"%@",dic[@"departAddr"]];
    self.endlabel.text = [NSString stringWithFormat:@"%@",dic[@"arrivalAddr"]];
    
    if ([[dic objectForKey:@"waybillType"] integerValue] == 20)
    {
        self.extraCostlabel.text = [NSString stringWithFormat:@"+%@",dic[@"extraCost"]];
    }
    
    
    if (![dic[@"status"] isEqual:[NSNull null]])
    {
        if ([dic[@"status"] integerValue] == 5)
        {
            self.pricelabel.text = [NSString stringWithFormat:@"预估￥%.2f",[dic[@"orderPrice"] floatValue]];
            
        } else {
            self.pricelabel.text = [NSString stringWithFormat:@"￥%.2f",[dic[@"orderPrice"] floatValue]];
            
        }
        
    }
 
}
- (void)setUpUI {
    
    ///1.
    UIImageView * image0 = [[UIImageView alloc] initWithFrame:CGRectMake(14*kHeight, (35*kHeight - 16*kHeight) /2 , 13*kWidth, 16*kHeight)];
    image0.image = [UIImage imageNamed:@"orderOngoing"];
    //image0.backgroundColor = UniRandomColor;
    [self addSubview:image0];
    
    ///2.
    UILabel * label = [UILabel createUIlabel:@"进行中订单" andFont:FontOfSize14 andColor:littleBlackColor];
    //label.backgroundColor = [UIColor redColor];
    label.textAlignment = 0;
    label.frame = CGRectMake(35*kWidth, 0, screenWidth/2, 35*kHeight);
    [self addSubview:label];
   
    ///3.
    UIImageView * imageline = [[UIImageView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(label.frame), self.newsFrame.size.width, 0.5)];
    imageline.backgroundColor = AddCarNameBtnColor;
    [self addSubview:imageline];
    
    
    ///4.
    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(14*kHeight, CGRectGetMaxY(imageline.frame) + 22*kHeight, 2*kWidth, 15*kHeight)];
    image1.backgroundColor = YellowColor;
    [self addSubview:image1];
   

    ///5.标题
    orderTitle  = [UILabel createUIlabel:@"今日运单" andFont:FontOfSize12 andColor:littleBlackColor];
    orderTitle.frame = CGRectMake(CGRectGetMaxX(image1.frame) + 14*kHeight, image1.y, self.newsFrame.size.width - (CGRectGetMaxX(image1.frame) + 14*kHeight), image1.height);
    orderTitle.textAlignment = 0;
    [self addSubview:orderTitle];
  
    
    ///6.时间
    timelabel = [UILabel createUIlabel:@"今天 0:0" andFont:FontOfSize12 andColor:BtnTitleColor];
    timelabel.frame = CGRectMake(image1.x, CGRectGetMaxY(image1.frame) + 14*kHeight, 150*kWidth, 16*kHeight);
    //timelabel.backgroundColor = UniRandomColor;
    timelabel.textAlignment = 0;
    [self addSubview:timelabel];
    
    ///7.状态
    statelabel = [UILabel createUIlabel:@"派单中" andFont:FontOfSize12 andColor:YellowColor];
    statelabel.frame = CGRectMake(self.newsFrame.size.width - 80*kWidth  - 24*kWidth, timelabel.y, 80*kWidth, 16*kHeight);
    //statelabel.backgroundColor = UniRandomColor;
    statelabel.textAlignment = 2;
    [self addSubview:statelabel];
   

    ///8.开始图片
    UIImageView * starimage = [[UIImageView alloc] initWithFrame:CGRectMake(image1.x, 14*kHeight + CGRectGetMaxY(timelabel.frame), 16*kWidth, 21*kHeight)];
    starimage.image = [UIImage imageNamed:@"start"];
    [self addSubview:starimage];
 
    ///9.开始地址
    starlabel = [UILabel createUIlabel:@"江西-萍乡" andFont:FontOfSize12 andColor:BtnTitleColor];
    starlabel.frame = CGRectMake(19 * kWidth + CGRectGetMaxX(starimage.frame), starimage.y, self.newsFrame.size.width - (19 * kWidth + CGRectGetMaxY(starimage.frame)), starimage.height);
    starlabel.textAlignment = 0;
    //starlabel.backgroundColor = UniRandomColor;
    [self addSubview:starlabel];
    
    
    ///10. 结束地图
    UIImageView * endimage = [[UIImageView alloc] initWithFrame:CGRectMake(image1.x, CGRectGetMaxY(starimage.frame) + 10*kHeight, starimage.width, starimage.height)];
    endimage.image = [UIImage imageNamed:@"end"];
    [self addSubview:endimage];
    
    
    ///11. 结束地址
    endlabel = [UILabel createUIlabel:@"江西-萍乡" andFont:FontOfSize12 andColor:BtnTitleColor];
    endlabel.frame = CGRectMake(starlabel.x, endimage.y, starlabel.width, starlabel.height);
    endlabel.textAlignment = 0;
    [self addSubview:endlabel];
    
    
    ///12
    UIImageView * imageline2 = [[UIImageView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(endimage.frame) + 5.0*kHeight, screenWidth, .7f)];
    imageline2.backgroundColor = AddCarNameBtnColor;
    [self addSubview:imageline2];
   
    
    ///13.
    UIImageView * imageline1 = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(starimage.frame)+7*kWidth, starimage.y, 0.7f, CGRectGetMaxY(endimage.frame) - CGRectGetMaxY(starimage.frame) + endimage.height)];
    imageline1.backgroundColor = AddCarNameBtnColor;
    [self addSubview:imageline1];
    
    ///14
    pricelabel = [UILabel createUIlabel:@"￥0.00" andFont:FontOfSize14 andColor:YellowColor];

    pricelabel.frame = CGRectMake(self.newsFrame.size.width - endlabel.width  - 24*kWidth,starimage.y, endlabel.width, endlabel.height);
    //pricelabel.frame = CGRectMake(self.newsFrame.size.width - endlabel.width  - 24*kWidth, CGRectGetMaxY(starimage.frame) - endimage.height/2, endlabel.width, endlabel.height);
    pricelabel.textAlignment = 2;
    [self addSubview:pricelabel];
    
    ///15
    UIButton * DetailsBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.newsFrame.size.width - 124*kWidth, CGRectGetMaxY(imageline2.frame) + 15*kHeight, 100*kWidth,  30*kHeight)];
    [DetailsBtn setTitle:@"查看详情" forState:UIControlStateNormal];
    [DetailsBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    DetailsBtn.layer.cornerRadius = 5;
    DetailsBtn.layer.borderWidth = 0.5;
    DetailsBtn.layer.borderColor = YellowColor.CGColor;
    DetailsBtn.backgroundColor = YellowColor;
    [self addSubview:DetailsBtn];
    
    
    ///16
    UIButton * bigBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.newsFrame.size.width, self.newsFrame.size.height)];
    [bigBtn addTarget:self action:@selector(pressFindDetailsBtn) forControlEvents:UIControlEventTouchUpInside];
    bigBtn.backgroundColor = [UIColor clearColor];
    [self addSubview:bigBtn];
    
    ///17
    extraCostlabel = [UILabel createUIlabel:@"" andFont:FontOfSize14 andColor:YellowColor];
    extraCostlabel.frame = CGRectMake(pricelabel.x, endimage.y, pricelabel.width, pricelabel.height);
    extraCostlabel.textAlignment = 2;
    [self addSubview:extraCostlabel];
}
- (void)pressFindDetailsBtn {
    [self.delegate orderViewClick];
}
 
@end
