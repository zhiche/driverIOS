//
//  An_ReceiveOrderView.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/24.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import <UIKit/UIKit.h>
@class An_ReceiveOrderView;

@protocol An_ReceiveOrderViewDelegate <NSObject>

@required
- (void)orderViewClick;

@end

@interface An_ReceiveOrderView : UIView

@property (nonatomic, strong, readwrite) UILabel *orderTitle;//今日订单
@property (nonatomic, strong, readwrite) UILabel *timelabel;//今日时间
@property (nonatomic, strong, readwrite) UILabel *statelabel;//订单状态
@property (nonatomic, strong, readwrite) UILabel *starlabel;//开始地址
@property (nonatomic, strong, readwrite) UILabel *endlabel;//结束地址
@property (nonatomic, strong, readwrite) UILabel *pricelabel;//金额
@property (nonatomic, strong, readwrite) UILabel *extraCostlabel;//金额
@property (nonatomic, weak) id<An_ReceiveOrderViewDelegate>delegate;

- (void)orderViewReloadData:(NSDictionary *)dic;

@end
