//
//  An_ReceiveDriverInfoTool.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/23.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <UIKit/UIKit.h>

@class An_ReceiveDriverInfoTool;

@protocol An_ReceiveDriverInfoToolDelegate <NSObject>

- (void)diverInfoToolClickItem:(UIButton *)item ;

@end

@interface An_ReceiveDriverInfoTool : UIView

@property (nonatomic, weak) UIButton *titleBtn;

@property (nonatomic, weak) id<An_ReceiveDriverInfoToolDelegate>delegate;
 
- (instancetype)initWithFrame:(CGRect)frame dataArray:(NSArray *)dataArray;

@end
