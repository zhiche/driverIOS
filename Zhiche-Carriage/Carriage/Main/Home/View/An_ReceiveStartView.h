//
//  An_ReceiveStartView.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/24.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import <UIKit/UIKit.h>
@class An_ReceiveStartView;

@protocol An_ReceiveStartViewDelegate <NSObject>

@required

- (void)startTheOrder;
 

@end

@interface An_ReceiveStartView : UIView

@property (nonatomic, strong) UIView *competitionView;
@property (nonatomic, weak) id<An_ReceiveStartViewDelegate> delegate;

@end
