//
//  ReceiveViewController.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/6/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NavViewController.h"

#import "TopBackView.h"
/**
 Home , 首页
 */
@interface ReceiveViewController : NavViewController<UIScrollViewDelegate>

@property (nonatomic,copy) void (^callOnlineBack)(NSMutableDictionary * InforDic);

@end
