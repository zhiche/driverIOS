//
//  ReceiveViewController.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/6/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//


#import "ReceiveViewController.h"
#import "An_ReceiveDriverInfoTool.h"
#import "An_ReceiveStartView.h"
#import "An_ReceiveOrderView.h"
#import "An_ReceiveWaitView.h"
#import "An_CompetitionOrderView.h"

#import "SDCycleScrollView.h"       //首页轮播图
#import "RootViewController.h"      //根视图
#import "NewsVC.h"                  //消息Vc
#import "MyBillVC.h"                //我的账单
/*
#import "MyWalletViewController.h"  //账户余额
*/
#import "DrivingRecordVC.h"         //出车记录
#import "LoginViewController.h"     //登录Vc
#import "OrderdetailVC.h"           //运单详情
#import "An_CompetitionOrderVc.h"   //抢单
#import "UseCityVC.h"               //意向城市

#define  SDCycleScrollViewH 164.0f * kHeight
#define  DiverInfoToolH 60.0f  * kHeight

#define  FuncTop 5 * kHeight
#define  FuncBottom 5 * kHeight
#define  TabBarHeight 48 * kHeight
#define  FuncY (NaviStatusH + SDCycleScrollViewH + DiverInfoToolH + FuncTop)
///功能Height = 屏幕 - 滚动 - 工具 - Tabbar - 上下边距
#define  FucnHeight (screenHeight - SDCycleScrollViewH - DiverInfoToolH - TabBarHeight - NaviStatusH - FuncBottom - FuncTop)

#define TimeInterval 60
#define QueueInterval 60

@interface ReceiveViewController ()
<
    SDCycleScrollViewDelegate,
    An_ReceiveDriverInfoToolDelegate,
    An_ReceiveStartViewDelegate,
    An_ReceiveOrderViewDelegate,
    An_ReceiveWaitViewDelegate
>


@property (nonatomic, strong, readwrite) NSMutableArray *cycleScrollViewData;
@property (nonatomic, strong, readwrite) NSMutableArray *cycleScrollViewUrlData;

@property (nonatomic, strong, readwrite) An_ReceiveStartView *startView;           ///开始接单View
@property (nonatomic, strong, readwrite) An_ReceiveOrderView *orderNewView;        ///订单View
@property (nonatomic, strong, readwrite) An_ReceiveWaitView *waitsView;            ///订单等待View
@property (nonatomic, strong, readwrite) An_CompetitionOrderView *competionView;   ///抢单View

@property (nonatomic, strong, readwrite) NSMutableDictionary *dataQueueDic;

@property (nonatomic, strong) dispatch_source_t queueTimer;

@property (nonatomic, strong) dispatch_source_t freshTimer;

@end

@implementation ReceiveViewController {
    
    //分页控件的对象指针
    NSMutableDictionary *userDic;
 
    BOOL _isViewDidLoadSucess;
    TopBackView *_topBackView;                ///导航栏
    SDCycleScrollView *_cyleScrollView;       ///滚动视图
    An_ReceiveDriverInfoTool *_infoTool;       ///司机工具栏
  
}
#pragma mark -
#pragma mark -  Lazy
- (An_ReceiveStartView *)startView {
    if (!_startView) {
        
        _startView = [[An_ReceiveStartView alloc] initWithFrame:CGRectMake(0, FuncY, screenWidth, FucnHeight)];
        _startView.delegate = self;
        _startView.hidden = YES;
    }
    return _startView;
}
- (An_ReceiveOrderView *)orderNewView {
    if (!_orderNewView)
    {
        CGRect frame =  CGRectMake(0, FuncY, screenWidth, FucnHeight);
    
        _orderNewView = [[An_ReceiveOrderView alloc] initWithFrame:frame];
        _orderNewView.backgroundColor = [UIColor whiteColor];
        _orderNewView.hidden = YES;
        _orderNewView.delegate = self;
        
    }
    return _orderNewView;
}
- (An_ReceiveWaitView *)waitsView {
    if (!_waitsView) {
    
 
        
        _waitsView = [[An_ReceiveWaitView alloc] initWithFrame:CGRectMake(0, FuncY, screenWidth, FucnHeight)];
        
        _waitsView.backgroundColor = [UIColor whiteColor];
        _waitsView.hidden = YES;
        _waitsView.delegate = self;

    }
    return _waitsView;
}
- (An_CompetitionOrderView *)competionView {
    if (!_competionView) {
        CGRect frame = CGRectMake(screenWidth, FuncY + 10 * kHeight, Com_ViewW, Com_ViewH);
        __weak typeof(self) weakSelf = self;
        _competionView = [[An_CompetitionOrderView alloc] initWithFrame:frame
                                                      compentitionClick:^{
            
                                                          [weakSelf competitionOrder];
        }];
        _competionView.backgroundColor = [UIColor clearColor];
        

        
    }
    return _competionView;
}

- (NSMutableArray *)cycleScrollViewData {
    if (!_cycleScrollViewData) {
        _cycleScrollViewData = @[].mutableCopy;
    }
    return _cycleScrollViewData;
}
- (NSMutableArray *)cycleScrollViewUrlData {
    if (!_cycleScrollViewUrlData) {
        _cycleScrollViewUrlData = @[].mutableCopy;
    }
    return _cycleScrollViewUrlData;
}
- (NSMutableDictionary *)dataQueueDic {
    if (!_dataQueueDic) {
        _dataQueueDic = @{}.mutableCopy;
    }
    return _dataQueueDic;
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    ///设置UI
    [self setUpUI];

    
    ///获取轮播图数据, 刷新视图
    [self requsetTopScrollViewData];
    
    [self setFreshTimerStatus:YES];
    
    ///添加观察者
    [self addMangerObserver];
}


#pragma mark -
#pragma mark - 设置UI
- (void)setUpUI {
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.view.backgroundColor = GrayColor;

///1. 添加自定义导航栏
    _topBackView = [[TopBackView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, NaviStatusH) title:@"康舶司"];
    _topBackView.leftButton.hidden = YES;
    _topBackView.rightButton.hidden = NO;
    _topBackView.phoneImg.hidden = NO;
    _topBackView.rightButton.titleLabel.font = Font(13);
    
    [_topBackView.rightButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    
    [_topBackView.rightButton addTarget:self
                                 action:@selector(rightButtonAction)
                       forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_topBackView];
   
    ///4.
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.startView.frame];
    imageView.image = [UIImage imageNamed:@"backgroundImage"];
    [self.view addSubview:imageView];
    
    
    
    
///2. 添加滚动视图
    CGRect cyleFrame = CGRectMake(0, NaviStatusH, screenWidth, SDCycleScrollViewH);
    
    _cyleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:cyleFrame
                                                         delegate:self
                                                 placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    _cyleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    
    //自定义分页控件小圆标颜色
    _cyleScrollView.currentPageDotColor = [UIColor whiteColor];

    [self.view addSubview:_cyleScrollView];
    
    
///3. 添加司机工具栏
    CGRect toolFrame = CGRectMake(0, (NaviStatusH + SDCycleScrollViewH), screenWidth, DiverInfoToolH);
    
    _infoTool = [[An_ReceiveDriverInfoTool alloc] initWithFrame:toolFrame dataArray:@[@"我的账单",
                                                                                  @"出车记录"]];
    _infoTool.backgroundColor = [UIColor whiteColor];
    _infoTool.delegate = self;
    [self.view addSubview:_infoTool];
    
    

    
    [self.view addSubview:self.startView];
    
    [self.view addSubview:self.orderNewView];
    
    [self.view addSubview:self.waitsView];
    
    [self.view addSubview:self.competionView];
    
    
}



#pragma mark - 刷新界面数据
/**
 定时器
 */
- (void)addFreshTimer {

    if (!self.freshTimer)
    {
        
        //0.创建队列
        dispatch_queue_t queue = dispatch_get_main_queue();
        //1.创建GCD中的定时器
        /*
         第一个参数:创建source的类型 DISPATCH_SOURCE_TYPE_TIMER:定时器
         第二个参数:0
         第三个参数:0
         第四个参数:队列
         */
        dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
        
        //2.设置时间等
        /*
         第一个参数:定时器对象
         第二个参数:DISPATCH_TIME_NOW 表示从现在开始计时
         第三个参数:间隔时间 GCD里面的时间最小单位为 纳秒
         第四个参数:精准度(表示允许的误差,0表示绝对精准)
         */
        dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, TimeInterval * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
     
        //3.要调用的任务
        dispatch_source_set_event_handler(timer, ^{
            //NSLog(@"GCD-----%@",[NSThread currentThread]);
            [self reloadPageInfoData];
        });
        
        //4.开始执行
        dispatch_resume(timer);
        
        //
        self.freshTimer = timer;
        
    }
 
}
- (void)addQueueTimer {
    if (!self.queueTimer)
    {
        
        //0.创建队列
        dispatch_queue_t queue = dispatch_get_main_queue();
        //1.创建GCD中的定时器
        /*
         第一个参数:创建source的类型 DISPATCH_SOURCE_TYPE_TIMER:定时器
         第二个参数:0
         第三个参数:0
         第四个参数:队列
         */
        dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
        
        //2.设置时间等
        /*
         第一个参数:定时器对象
         第二个参数:DISPATCH_TIME_NOW 表示从现在开始计时
         第三个参数:间隔时间 GCD里面的时间最小单位为 纳秒
         第四个参数:精准度(表示允许的误差,0表示绝对精准)
         */
        dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, QueueInterval * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
        
        //3.要调用的任务
        dispatch_source_set_event_handler(timer, ^{
            //NSLog(@"GCD-----%@",[NSThread currentThread]);
            [self getQueuedSituation];
        });
        
        //4.开始执行
        dispatch_resume(timer);
        
        //
        self.queueTimer = timer;
        
    }
}
/**
 The timer status

 @param status 1 open 0 close
 */
- (void)setFreshTimerStatus:(BOOL)status {
    if (!status)
    {
        [self setFreshTimer:nil];
    }
    else
    {
        [self addFreshTimer];
    }
}
- (void)setQueueTimerStatus:(BOOL)status {
    if (!status)
    {
        [self setQueueTimer:nil];
    }
    else
    {
        [self addQueueTimer];
    }
}
/**
  Reload View Datas
 */
- (void)reloadPageInfoData {
    
   
     NSLog(@"刷新数据!!!");
    
    __weak typeof(self) weakSelf = self;
    
    [Common requestWithUrlString:waybill_pageinfo_Url
                     contentType:@"application/json"
                       errorShow:YES
                        finished:^(id responseObj){
        
        
    
        if ([responseObj[@"success"] boolValue] != NO)
        {
            
            userDic = responseObj[@"data"];
            
            /// -> topView模块 此参数判断是否存在新的消息
            NSInteger ishaveNoRead = [userDic[@"ishasNoRead"] intValue];
            
            if (ishaveNoRead == 0)
            {
                _topBackView.phoneImg.image = [UIImage imageNamed:@"noNews"];
            }
            else
            {
                _topBackView.phoneImg.image = [UIImage imageNamed:@"haveNews"];
            }
            
            ///钱包金额
            //NSString * balance = [NSString stringWithFormat:@"￥%.2f", [userDic[@"balance"] floatValue]];
            //UIButton * btnmoney = (UIButton*)[weakSelf.infoTool viewWithTag:1102];
            //[btnmoney setTitle:balance forState:UIControlStateNormal];
            
            ///出车记录
            NSString *doneCount = [NSString stringWithFormat:@"%@", userDic[@"doneCount"]];
            UIButton *btnnumber = (UIButton *)[_infoTool viewWithTag:1102];
            [btnnumber setTitle:doneCount forState:UIControlStateNormal];
            
////20:下线状态 10:上线状态 30:运行中
            NSInteger userStauts = [userDic[@"userStauts"] integerValue];
      

//NSLog(@"%d, %@", userStauts, userDic[@"userStauts"] );
            switch (userStauts){
//正在排队中
                case 10:
                {
///获取排队情况
                    [self setFreshTimerStatus:YES];
                    [self setQueueTimerStatus:YES];
                
                    break;
                }
                    break;
                case 30:
                {
//已接单
                    weakSelf.startView.hidden = YES;    //接单View
                    weakSelf.waitsView.hidden = YES;    //排队View
                    weakSelf.orderNewView.hidden = NO;  //订单View
                    
                    /// -> toolView模块 此参数判断是否存在新的消息
                    NSDictionary * showOrderVoDic = responseObj[@"data"][@"showOrderVo"];
                    
                    [RootViewController defaultsTabBar].orderType = showOrderVoDic[@"waybillType"];
                    ///订单View刷新
                    [self.orderNewView orderViewReloadData:showOrderVoDic];
                    
                    break;
                }
                    break;
                case 20:
                {
//未入队列 无订单 可以选择排队
                    
                    weakSelf.startView.hidden = NO;     //接单Viwq
                    weakSelf.waitsView.hidden = YES;    //排队View
                    weakSelf.orderNewView.hidden = YES; //订单View
                    
                    [self setFreshTimerStatus:YES];
                    
                    break;
                }
                    break;
                default:
                    break;
            }
            

            /*
             传递Data给开始抢单视图
             前提:  是否为抢单状态 && 抢单数量大于0且不为负数
             */
            NSUInteger competitionCount =  [userDic[@"quickAmunt"] integerValue];
            
            if (self.orderNewView.hidden != NO)
            {
                self.competionView.competitionCount = competitionCount;
            }
            else
            {
                self.competionView.competitionCount = 0;
            }
            
        }
        else
        {
            [WKProgressHUD popMessage:responseObj[@"message"]
                               inView:self.view
                             duration:1.5
                             animated:YES];
            
        }
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
}
/**
 获取排队情况
 */
- (void)getQueuedSituation{
    
    NSLog(@"获取排队情况");
    __weak typeof(self) weakSelf = self;
    
//    NSLog(@"获取排队情况");
    
    //获取排队情况
    [Common requestWithUrlString:driver_queue_Url
                     contentType:@"application/json"
                       errorShow:YES
                        finished:^(id responseObj) {
        
        if ([responseObj[@"success"] boolValue] != NO)
        {
            weakSelf.dataQueueDic = responseObj[@"data"];
            
            NSString * null = [NSString stringWithFormat:@"%@",weakSelf.dataQueueDic[@"order"]];
            
            //有订单
            if (![null isEqualToString:@"<null>"])
            {
                //刷新订单ui界面
                [weakSelf.orderNewView orderViewReloadData:weakSelf.dataQueueDic[@"order"]];
                weakSelf.orderNewView.hidden = NO;  //订单view
                weakSelf.waitsView.hidden = YES;    //订单等待view
                weakSelf.startView.hidden = YES;    //开始接单View
            }
            else
            {//正在排队
                //刷新排队ui界面
                ///如果返回排名和顺序值为null  调用下线接口
                if ([weakSelf.dataQueueDic[@"queue"][@"rankNumber"] isEqual:[NSNull null]] &&
                    [weakSelf.dataQueueDic[@"queue"][@"frontNumber"] isEqual:[NSNull null]])
                {
                    ////取消排队
                    [weakSelf sendCancelRequestQueueToServers];
                }
                else
                {
                    [weakSelf.waitsView waitReloadData:weakSelf.dataQueueDic[@"queue"]];
                    weakSelf.waitsView.hidden = NO;     //排队view
                    weakSelf.orderNewView.hidden = YES; //订单view
                    weakSelf.startView.hidden = YES;    //开始接单View
                }
            }
        }
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}


#pragma mark -
#pragma mark - 推出消息Vc
- (void)rightButtonAction {
    NewsVC * news = [[NewsVC alloc]init];
    [Common isLogin:self andPush:news];
}


#pragma mark -
#pragma mark - 轮播图
/**
 获取轮播图Data
 */
- (void)requsetTopScrollViewData {
    
    NSString * string = [NSString stringWithFormat:@"%@?clienttype=30&apptype=30",banner_list_Url];
    
    __weak typeof(self) weakSelf = self;
    
    [Common requestWithUrlString:string
                     contentType:@"application/json"
                       errorShow:NO
                        finished:^(id responseObj) {
        
        int success = [responseObj[@"success"] intValue];
        
        if (success == 1)
        {
            NSArray *scrollArr = responseObj[@"data"];
            
            [weakSelf.cycleScrollViewData removeAllObjects];
            
            [weakSelf.cycleScrollViewUrlData removeAllObjects];
            
            for (NSDictionary *dict in scrollArr)
            {
                NSString *url = dict[@"picKey"];
                
                NSString *clickUrl = dict[@"picHref"];
                
                if ([url isKindOfClass:[NSString class]] && url.length > 0) {
                    
                    [weakSelf.cycleScrollViewData addObject:url];
                }
                if ([clickUrl isKindOfClass:[NSString class]] && clickUrl.length > 0) {
                    
                    [weakSelf.cycleScrollViewUrlData addObject:clickUrl];
                }
            }
            
            ///创建轮播视图
            [weakSelf reloadCyleScorllView];
        }
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}
/**
 刷新轮播图
 */
- (void)reloadCyleScorllView {
    
    __weak typeof(self) weakSelf = self;
    
    _cyleScrollView.urlArr = weakSelf.cycleScrollViewUrlData;
    
    //--- 模拟加载延迟
    dispatch_queue_t queue = dispatch_get_main_queue();
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), queue, ^{
        
        _cyleScrollView.imageURLStringsGroup = weakSelf.cycleScrollViewData;
    });
}


#pragma mark -
#pragma mark - 观察者 与 VC 生命周期
- (void)addMangerObserver {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notice)
                                                 name:@"creatViews"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewWillIntoBackground)
                                                 name:AppIntoBackground
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewWillIntoForeground)
                                                 name:AppIntoForeground
                                               object:nil];
}
/**
 Vc销毁
 */
- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                     name:@"creatViews"
                                                   object:nil];
    
    [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                     name:AppIntoBackground
                                                   object:nil];
    
    [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                     name:AppIntoForeground
                                                   object:nil];
    
}
/**
 登陆成功
 */
- (void)notice{
    self.orderNewView.hidden = YES;
    self.waitsView.hidden = YES;
    self.startView.hidden = YES;

    [self requsetTopScrollViewData];
    [self viewWillIntoForeground];
}
/**
 程序进入前台
 */
- (void)viewWillIntoForeground {
  
    [self setFreshTimerStatus:YES];
}
/**
 程序进入后台
 */
- (void)viewWillIntoBackground {
    [self setFreshTimerStatus:NO];
    [self setQueueTimerStatus:NO];
}
/**
 视图即将展示
 */
- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *string = [userDefaults objectForKey:@"login"];
    
    if (string == nil )
    {
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        UINavigationController * loginNC = [[UINavigationController alloc]initWithRootViewController:loginVC];
        [self presentViewController:loginNC animated:YES completion:nil];
    }
    else
    {
        [self.navigationController setNavigationBarHidden:YES];
        
        RootViewController *rootVC = [RootViewController defaultsTabBar];
        
        [rootVC setTabBarHidden:NO];
        
        if (_isViewDidLoadSucess != NO)
        {
            [self viewWillIntoForeground];
        }
       
    }
}
/**
 视图即将消失
 */
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    _isViewDidLoadSucess = YES;
    
    [self viewWillIntoBackground];
}


#pragma mark -
#pragma mark - 协议
/**
 ToolInfo代理
 我的账单, 出车记录
 */
- (void)diverInfoToolClickItem:(UIButton *)item {
    switch (item.tag) {
        case 1001:
        {
            MyBillVC * mybill = [[MyBillVC alloc]init];
            [Common isLogin:self andPush:mybill];
        }
            break;
        case 1002:
        {
            DrivingRecordVC * Driving = [[DrivingRecordVC alloc]init];
            [Common isLogin:self andPush:Driving];
        }
            break;
            /*
             case 1102: ///需求 删除钱包
             {
             MyWalletViewController * wall = [[MyWalletViewController alloc]init];
             [Common isLogin:self andPush:wall];
             }
             break;
             */
        default:
            break;
    }
}
/**
 startView代理
 [开始接单]
 */
- (void)startTheOrder {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *string = [userDefaults objectForKey:@"login"];
    if (string == nil )
    {
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        UINavigationController * loginNC = [[UINavigationController alloc]initWithRootViewController:loginVC];
        [self presentViewController:loginNC animated:YES completion:nil];
    }
    else
    {
        
        
        
        UseCityVC * usecity = [[UseCityVC alloc]init];
        
        usecity.callOnlineBack = ^(NSMutableDictionary *dictionry){
            
            NSString * success = [NSString stringWithFormat:@"%@",dictionry[@"success"]];
            
            if ([success isEqualToString:@"1"]) {
                
                self.startView.hidden = YES;//开始按钮view
                self.orderNewView.hidden = YES;//订单view
                self.waitsView.hidden = NO;//订单等待view
                //                [WKProgressHUD popMessage:@"上线成功" inView:self.view duration:1.5 animated:YES];
                [self getQueuedSituation];
            }else{
                [WKProgressHUD popMessage:dictionry[@"message"] inView:self.view duration:1.5 animated:YES];
            }
        };
        
        
        
        [self.navigationController pushViewController:usecity animated:YES];
        
    }
    
}
/**
 competitionOrder代理
 开始抢单
 */
- (void)competitionOrder {
    An_CompetitionOrderVc *orderVc = [[An_CompetitionOrderVc alloc] init];
    
    [self.navigationController pushViewController:orderVc animated:YES];
}
/**
 orderView代理
 订单点击, 进入详情
 */
- (void)orderViewClick {
    
    OrderdetailVC * details = [[OrderdetailVC alloc]init];
    
    NSInteger  userStauts = [userDic[@"userStauts"] integerValue];
    
    if (userStauts == 30)
    {
        details.orderID = [NSString stringWithFormat:@"%@",userDic[@"showOrderVo"][@"id"]];
    }else{
        details.orderID = [NSString stringWithFormat:@"%@",self.dataQueueDic[@"order"][@"id"]];
    }
    
    details.waybillType = [userDic[@"showOrderVo"][@"waybillType"] integerValue];
    
    details.specialRequired = [NSString stringWithFormat:@"%@",userDic[@"showOrderVo"][@"specialRequired"]];
    
    details.status = [userDic[@"showOrderVo"][@"status"] integerValue];
    
    [self.navigationController pushViewController:details animated:YES];
}
/**
 waitView代理
 排队取消
 */
- (void)waitingCancel{
    __weak typeof(self) weakSelf = self;
    UIAlertController * alert = [Common zj_QueueAlertsControllerWithMessage:@"\n取消排队后,需重新排队,请慎重考虑!"
                                                              continueTitle:@"继续排队"
                                                                cancelTitle:@"放弃排队"
                                                               cancelAction:^(UIAlertAction *action) {
         [weakSelf sendCancelRequestQueueToServers];
    }];
    [self presentViewController:alert animated:YES completion:nil];
    
}
/**
 发送取消排队请求到服务器
 */
- (void)sendCancelRequestQueueToServers {
    
    NSLog(@"取消排队请求到服务器");
    
    [WKProgressHUD showInView:self.view withText:@"" animated:YES];
    
    [[Common shared] afPostRequestWithUrlString:user_offline_Url
                                          parms:nil
                                  finishedBlock:^(id responseObj)
    {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        NSString * userStatus = [NSString stringWithFormat:@"%@",dic[@"success"]];
        
        [WKProgressHUD dismissAll:YES];
        
        if ([userStatus isEqualToString:@"1"])
        {
            
            if ([dic[@"messageCode"] integerValue] == 200)
            {
                self.startView.hidden = NO;//开始按钮view
                self.orderNewView.hidden = YES;//订单view
                self.waitsView.hidden = YES;//订单等待view
                
                [self setQueueTimerStatus:NO];
         
            }
            else
            {
                /// 获取排队情况
                [self getQueuedSituation];
            }
        }
        else
        {
            [WKProgressHUD popMessage:dic[@"message"] inView:self.view duration:1.5 animated:YES];
        }
    }
    failedBlock:^(NSString *errorMsg)
    {
        [WKProgressHUD dismissAll:YES];
        
    }];
    
 
}

/**
    排队刷新
 */
- (void)waitingReload {
    [self reloadPageInfoData];
}




@end
