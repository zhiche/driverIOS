//
//  DetailComOrdersNotes.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import "DetailComOrdersNotes.h"

@implementation DetailComOrdersNotes

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (instancetype)cellWithTableView:(UITableView *)tableView
                  reuseIdentifier:(NSString *)reuseIdentifier{
    
    DetailComOrdersNotes *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell)
    {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"DetailComOrdersNotes" owner:nil options:nil] lastObject];
        
    }
    return cell;
}
@end
