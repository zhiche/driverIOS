//
//  CompitionOrdersDetailVc.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <UIKit/UIKit.h>

@class An_ComptitionOrdersModel;

@interface CompitionOrdersDetailVc : UIViewController

@property (nonatomic, copy, readwrite) An_ComptitionOrdersModel *orderModel;

@end
