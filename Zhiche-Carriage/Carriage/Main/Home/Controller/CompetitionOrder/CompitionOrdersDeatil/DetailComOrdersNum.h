//
//  DetailComOrdersNum.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailComOrdersNum : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *specialRequired;
@property (weak, nonatomic) IBOutlet UILabel *extraCost;
@property (weak, nonatomic) IBOutlet UILabel *orderCode;
+ (instancetype)cellWithTableView:(UITableView *)tableView
                  reuseIdentifier:(NSString *)reuseIdentifier;
@end
