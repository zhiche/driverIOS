//
//  DetailComOrdersAddress.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import "DetailComOrdersAddress.h"

@implementation DetailComOrdersAddress

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (instancetype)cellWithTableView:(UITableView *)tableView
                  reuseIdentifier:(NSString *)reuseIdentifier{
    
    DetailComOrdersAddress *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell)
    {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"DetailComOrdersAddress" owner:nil options:nil] lastObject];
        
    }
    return cell;
}
@end
