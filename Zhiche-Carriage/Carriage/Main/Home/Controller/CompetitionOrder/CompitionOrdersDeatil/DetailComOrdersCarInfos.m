//
//  DetailComOrdersCarInfos.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import "DetailComOrdersCarInfos.h"
@interface DetailComOrdersCarInfos ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLeft;

@end
@implementation DetailComOrdersCarInfos

- (void)awakeFromNib {
    [super awakeFromNib];
    NSLog(@"%f", kWidth);
    self.titleLeft.constant = 92*kHeight;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (instancetype)cellWithTableView:(UITableView *)tableView
                  reuseIdentifier:(NSString *)reuseIdentifier{
    
    DetailComOrdersCarInfos *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell)
    {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"DetailComOrdersCarInfos" owner:nil options:nil] lastObject];
        
    }
    return cell;
}
@end
