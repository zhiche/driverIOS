//
//  DetailComOrdersAddress.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailComOrdersAddress : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *departAddress;
@property (weak, nonatomic) IBOutlet UILabel *startAddress;
@property (weak, nonatomic) IBOutlet UILabel *endAddress;
@property (weak, nonatomic) IBOutlet UILabel *contactInfo;
@property (weak, nonatomic) IBOutlet UILabel *detailedAddress;
@property (weak, nonatomic) IBOutlet UILabel *mileage;
+ (instancetype)cellWithTableView:(UITableView *)tableView
                  reuseIdentifier:(NSString *)reuseIdentifier;
@end
