//
//  DetailComOrdersNotes.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailComOrdersNotes : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *customerRemark;
+ (instancetype)cellWithTableView:(UITableView *)tableView
                  reuseIdentifier:(NSString *)reuseIdentifier;
@end
