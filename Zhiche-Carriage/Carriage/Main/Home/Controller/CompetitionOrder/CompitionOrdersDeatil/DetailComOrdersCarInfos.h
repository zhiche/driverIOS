//
//  DetailComOrdersCarInfos.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailComOrdersCarInfos : UITableViewCell

///车型代码
@property (weak, nonatomic) IBOutlet UILabel *vehicleTypeCode;
 
///分类
@property (weak, nonatomic) IBOutlet UILabel *vehicleTypeName;


///底盘号
@property (weak, nonatomic) IBOutlet UILabel *vehicleVin;

///货物说明
@property (weak, nonatomic) IBOutlet UILabel *vehicleDesc;


+ (instancetype)cellWithTableView:(UITableView *)tableView
                  reuseIdentifier:(NSString *)reuseIdentifier;
@end
