//
//  CompitionOrdersDetailVc.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import "CompitionOrdersDetailVc.h"
#import "DetailComOrdersNum.h"
#import "DetailComOrdersAddress.h"
#import "DetailComOrdersCarInfos.h"
#import "DetailComOrdersNotes.h"
#import "TopBackView.h"

#import "An_ComptitionOrdersModel.h"
#import "VehicleInfoModel.h"

@interface CompitionOrdersDetailVc ()
<
UITableViewDelegate,
UITableViewDataSource,
TopBackViewDelegate
>
@property (nonatomic, strong, readwrite) UITableView *tableView;
@property (nonatomic, strong) TopBackView *topBackView;
@property (nonatomic, strong) NSMutableArray *tableViewHeight;

@end

@implementation CompitionOrdersDetailVc


- (NSMutableArray *)tableViewHeight {
    if (!_tableViewHeight)
    {
        _tableViewHeight = @[@100, @280, @240, @110].mutableCopy;
    }
    return _tableViewHeight;
}
- (void)viewDidLoad {
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self initData];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.topBackView];
    
}
- (void)initData {
    
 
    ///1
    NSUInteger index = 0;
    
    self.orderModel.specialRequired = [NSString stringWithFormat:@"特殊要求: %@", self.orderModel.specialRequired];
    
    
    CGFloat line1 = [self.tableViewHeight[index] floatValue];
    
    line1 += [Common contentText:self.orderModel.specialRequired
                           fonts:[UIFont systemFontOfSize:12*kWidth]
                        maxWidth:(screenWidth - 20) maxHeight:1000] - 17.0f;
    
    [self.tableViewHeight replaceObjectAtIndex:index
                                    withObject:[NSNumber numberWithFloat:line1]];
    
    
    ///2
    index = 2;
    
    line1 = [self.tableViewHeight[index] floatValue];
    
    line1 += [Common contentText:self.orderModel.dWaybillVehicles.vehicleDesc
                           fonts:[UIFont systemFontOfSize:12*kWidth]
                        maxWidth:(screenWidth - 92*kWidth - 20) maxHeight:1000] - 17.0f;
    [self.tableViewHeight replaceObjectAtIndex:index
                                    withObject:[NSNumber numberWithFloat:line1]];
    
    ///3
    index = 3;
    
    if (self.orderModel.customerRemark.length == 0)
    {
        self.orderModel.customerRemark = @"暂无信息";
    }
    line1 = [self.tableViewHeight[index] floatValue];
    
    line1 += [Common contentText:self.orderModel.customerRemark
                           fonts:[UIFont systemFontOfSize:12*kWidth]
                        maxWidth:(screenWidth - 60) maxHeight:1000] - 17;
    [self.tableViewHeight replaceObjectAtIndex:index
                                    withObject:[NSNumber numberWithFloat:line1]];
    
}
- (UITableView *)tableView {
    if (!_tableView)
    {
        
        CGRect frame = CGRectMake(0, 64 , screenWidth, screenHeight - 64);
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = GrayColor;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        _tableView.bounces = NO;
    }
    return _tableView;
}
- (TopBackView *)topBackView {
    if (!_topBackView)
    {
        _topBackView = [[TopBackView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)
                                                    title:@"急发单详情"];
        _topBackView.rightButton.hidden = YES;
        _topBackView.delegate = self;
        
    }
    return _topBackView;
}

/**
 导航栏左侧点击
 */
- (void)backViewPopViewController {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark - UITableView协议
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:
        {
            DetailComOrdersNum *cell = [DetailComOrdersNum cellWithTableView:tableView reuseIdentifier:@"DetailComOrdersNum"];
            cell.orderCode.text = [NSString stringWithFormat:@"运单号: %@", self.orderModel.orderCode];
            cell.specialRequired.text = self.orderModel.specialRequired;
            cell.extraCost.text = [NSString stringWithFormat:@"+ %.2f", [self.orderModel.extraCost floatValue]];
            return cell;
        }
            break;
        case 1:
        {
            DetailComOrdersAddress *cell = [DetailComOrdersAddress cellWithTableView:tableView reuseIdentifier:@"DetailComOrdersAddress"];
            cell.startAddress.text = [NSString stringWithFormat:@"%@-%@", self.orderModel.departProvince, self.orderModel.departCity];
            cell.endAddress.text = [NSString stringWithFormat:@"%@-%@", self.orderModel.destProvince, self.orderModel.destCity];
            cell.contactInfo.text = [NSString stringWithFormat:@"%@ %@", self.orderModel.arrivalContact, self.orderModel.arrivalPhone];
            cell.detailedAddress.text = self.orderModel.arrivalAddress;
            cell.mileage.text = [NSString stringWithFormat:@"公里数 %0.2fkm", [self.orderModel.distance floatValue]];
            cell.departAddress.text = self.orderModel.departAddress;
            return cell;
        }
            break;
         case 2:
        {
            DetailComOrdersCarInfos *cell = [DetailComOrdersCarInfos cellWithTableView:tableView reuseIdentifier:@"DetailComOrdersCarInfos"];
            cell.vehicleTypeCode.text = self.orderModel.dWaybillVehicles.vehicleTypeCode;
            cell.vehicleTypeName.text = self.orderModel.dWaybillVehicles.vehicleTypeName;
            cell.vehicleVin.text = self.orderModel.dWaybillVehicles.vehicleVin;
            cell.vehicleDesc.text = self.orderModel.dWaybillVehicles.vehicleDesc;
            return cell;
        }
            break;
        default:
        {
            DetailComOrdersNotes *cell = [DetailComOrdersNotes cellWithTableView:tableView reuseIdentifier:@"DetailComOrdersNotes"];
            cell.customerRemark.text = self.orderModel.customerRemark;
            return cell;
        }
            break;
    }
 
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section{
    return 5 * kHeight;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return [self.tableViewHeight[indexPath.section] floatValue];
    
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //[tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

- (void)dealloc {
    NSLog(@"控制器销毁了");
}
@end
