//
//  VehicleInfoModel.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 2017/6/7.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VehicleInfoModel : NSObject
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *waybillId;
@property (nonatomic, copy) NSString *vehicleTypeId;
///车型代码
@property (nonatomic, copy) NSString *vehicleTypeCode;
///分类
@property (nonatomic, copy) NSString *vehicleTypeName;
///底盘号
@property (nonatomic, copy) NSString *vehicleVin;
///货物说明
@property (nonatomic, copy) NSString *vehicleDesc;
@property (nonatomic, copy) NSString *vehicleNum;
@property (nonatomic, copy) NSString *vehicleUnit;
@property (nonatomic, copy) NSString *fuelStandard;
@property (nonatomic, copy) NSString *tankVolume;
@property (nonatomic, copy) NSString *vehicleClassifyId;
@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, copy) NSString *createTime;
@end
