//
//  An_ComptitionOrdersModel.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VehicleInfoModel;

@interface An_ComptitionOrdersModel : NSObject<NSCopying>


@property (nonatomic, copy) NSString *orderCode;//中联TMS订单编号
@property (nonatomic, copy) NSString *orderTime;//下单时间
@property (nonatomic, copy) NSString *departProvince;//发车省
@property (nonatomic, copy) NSString *departCity;//发车市



@property (nonatomic, copy) NSString *ID; //	6570
@property (nonatomic, copy) NSString *destProvince; //送达省
@property (nonatomic, copy) NSString *destCity; //	送达市



@property (nonatomic, copy) NSString *vehicleTypeName; //	撼路者v汽
@property (nonatomic, copy) NSString *waybillStatus; //	0
@property (nonatomic, copy) NSString *license; //	C1,B1,B2,A2,A1
@property (nonatomic, copy) NSString *extraCost; //	200.18
@property (nonatomic, copy) NSString *specialRequired; //	快点啊，哈哈哈

@property (nonatomic, copy) NSString *distance;
@property (nonatomic, copy) NSString *arrivalContact;
@property (nonatomic, copy) NSString *arrivalPhone;
@property (nonatomic, copy) NSString *arrivalAddress;

@property (nonatomic, copy) NSString *customerRemark;

@property (nonatomic, copy) NSString *departAddress;


@property (nonatomic, strong) VehicleInfoModel *dWaybillVehicles;

@end
