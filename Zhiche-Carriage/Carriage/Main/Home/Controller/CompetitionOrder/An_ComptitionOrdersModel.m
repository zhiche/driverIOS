//
//  An_ComptitionOrdersModel.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/6/5.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import "An_ComptitionOrdersModel.h"
#import "VehicleInfoModel.h"
#import <objc/runtime.h>

@implementation An_ComptitionOrdersModel
- (void)setValue:(id)value forUndefinedKey:(NSString *)key  {
    if([key isEqualToString:@"id"]){
        self.ID = value;
    }
    else if ([key isEqualToString:@"dWaybillVehicle"])
    {
        VehicleInfoModel *model = [[VehicleInfoModel alloc]init];
        if ([value isKindOfClass:[NSArray class]])
        {
            [model setValuesForKeysWithDictionary:[value[0] dictionaryByReplacingNullsWithStrings]];
        }
        self.dWaybillVehicles = model;
    }
    else{
        [super setValue:value forUndefinedKey:key];
    }
}

- (id)copyWithZone:(NSZone *)zone {
    An_ComptitionOrdersModel *cpyPerson = [[An_ComptitionOrdersModel allocWithZone:zone] init];
    
    NSDictionary *dict = [self getClassInfoWithClass:self];

    
    for (NSString * key in dict[@"keys"])
    {
        [cpyPerson setValue:[self valueForKey:key] forKey:key];
    }
    
    return cpyPerson;
}
- (NSDictionary *)getClassInfoWithClass:(id)class {
    
    NSMutableArray * keys = [NSMutableArray array];
    NSMutableArray * attributes = [NSMutableArray array];
    /*
     * 例子
     * name = value3 attribute = T@"NSString",C,N,V_value3
     * name = value4 attribute = T^i,N,V_value4
     */
    unsigned int outCount;
    objc_property_t * properties = class_copyPropertyList([class class], &outCount);
    for (int i = 0; i < outCount; i ++) {
        objc_property_t property = properties[i];
        //通过property_getName函数获得属性的名字
        NSString * propertyName = [NSString stringWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        [keys addObject:propertyName];
        //通过property_getAttributes函数可以获得属性的名字和@encode编码
        NSString * propertyAttribute = [NSString stringWithCString:property_getAttributes(property) encoding:NSUTF8StringEncoding];
        [attributes addObject:propertyAttribute];
    }
    //立即释放properties指向的内存
    free(properties);
    
    
    return @{@"keys":keys, @"attributes":attributes};
    
}
@end
