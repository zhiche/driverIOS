//
//  NewsCell.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/2/20.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsModel.h"
@interface NewsCell : UITableViewCell

@property(nonatomic,strong) UILabel * title;
@property(nonatomic,strong) UILabel * content;
@property(nonatomic,strong) UILabel * createTime;
@property(nonatomic,strong) UILabel * statu;

@property (nonatomic,strong) NewsModel *model;

-(void)showInfo:(NSMutableDictionary*)dic_info;


@end
