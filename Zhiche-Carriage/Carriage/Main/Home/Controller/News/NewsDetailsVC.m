//
//  NewsDetailsVC.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/2/28.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "NewsDetailsVC.h"
#import "UILabel+Category.h"

@interface NewsDetailsVC ()
{
    UIImageView * nav;

}

@property (nonatomic, strong) UILabel *TitleLabel;
@property (nonatomic, strong) UILabel *DetailLabel;
@property (nonatomic, strong) UILabel *DateLabel;
@property (nonatomic, strong) UILabel *SignLabel;
@property (nonatomic, strong) NSMutableDictionary * dicSouce;

@end

@implementation NewsDetailsVC

#pragma mark - Lazy
- (UILabel *)TitleLabel{
    
    
    if (!_TitleLabel) {
        _TitleLabel = [UILabel createUIlabel:@"通知" andFont:FontOfSize15 andColor:BlackTitleColor];
    }
    
    return _TitleLabel;
}

- (UILabel *)DateLabel{
    
    
    if (!_DateLabel) {
        _DateLabel = [UILabel createUIlabel:@"时间：2016-09-09 11：52：09" andFont:FontOfSize12 andColor:fontGrayColor];
    }
    
    return _DateLabel;
}

- (UILabel *)DetailLabel{
    
    
    if (!_DetailLabel) {
        
        
        _DetailLabel = [[UILabel alloc]init];
        _DetailLabel.frame = CGRectMake(20*kWidth, 130*kHeight, screenWidth-40*kWidth,37*kHeight);
        _DetailLabel.font = Font(FontOfSize12);
        _DetailLabel.textColor = fontGrayColor;
        _DetailLabel.numberOfLines = 0;
        _DetailLabel.textAlignment = NSTextAlignmentLeft;
        
    }
    
    return _DetailLabel;
}

- (UILabel *)SignLabel{
    
    
    if (!_SignLabel) {
        _SignLabel = [UILabel createUIlabel:@"中联感恩有您陪伴" andFont:FontOfSize12 andColor:fontGrayColor];
    }
    return _SignLabel;
    
}

- (NSMutableDictionary *)dicSouce{
    
    if (!_dicSouce) {
        _dicSouce = [[NSMutableDictionary alloc]init];
    }
    return _dicSouce;
    
}

#pragma mark - View初始化
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GrayColor;
    nav = [self createNav:@"消息详情"];
    [self.view addSubview:nav];

    [self initDate];
    
}

#pragma mark - 获取服务器数据
- (void)initDate{
    
    WKProgressHUD *hud = [WKProgressHUD showInView:self.view withText:@"加载中" animated:YES];
    NSString * string = [NSString stringWithFormat:@"%@%d",News_detail_Url,self.userId];
    [Common requestWithUrlString:string contentType:@"application/json" errorShow:NO finished:^(id responseObj){
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if ([responseObj[@"success"] boolValue]) {
                [hud dismiss:YES];
                self.dicSouce = responseObj[@"data"];
                [self createUI];
            }
        });
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [hud dismiss:YES];
            
        });
    }];

    
    

    
    
}

#pragma mark - 创建UI
- (void)createUI{
    
    
    
    
    [self.view addSubview:self.TitleLabel];
    [self.view addSubview:self.DateLabel];
    [self.view addSubview:self.SignLabel];
    self.TitleLabel.text = [NSString stringWithFormat:@"%@",self.dicSouce[@"title"]];
    self.DateLabel.text = [NSString stringWithFormat:@"时间：%@",self.dicSouce[@"createTime"]];
    self.SignLabel.text = [NSString stringWithFormat:@"%@",self.dicSouce[@"creator"]];
    

    [self.TitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.mas_equalTo(nav.mas_bottom).with.offset(26*kHeight);
    }];
    
    [self.DateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.TitleLabel.mas_centerX);
        make.top.mas_equalTo(self.TitleLabel.mas_bottom).with.offset(9*kHeight);
    }];
    

    //label 自适应高度
    NSString * str = [NSString stringWithFormat:@"%@",self.dicSouce[@"content"]];
    self.DetailLabel.text = str;
    CGSize size = [self.DetailLabel sizeThatFits:CGSizeMake(self.DetailLabel.frame.size.width, MAXFLOAT)];
    self.DetailLabel.frame = CGRectMake(20*kWidth, 130*kHeight, screenWidth-40*kWidth, size.height);
    [self.view addSubview:self.DetailLabel];
    [self.SignLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view.mas_right).with.offset(-20*kWidth);
        make.top.mas_equalTo(self.DetailLabel.mas_bottom).with.offset(9*kHeight);
    }];
    
    
    

    
    
}



@end
