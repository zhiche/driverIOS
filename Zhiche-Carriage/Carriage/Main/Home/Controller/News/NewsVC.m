//
//  NewsVC.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/2/20.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "NewsVC.h"

#import "NullDataView.h"

#import "RootViewController.h"

#import "NewsCell.h"

#import "NewsDetailsVC.h"
@interface NewsVC ()
{
    UIImageView * nav;
 
    RootViewController * TabBar;
    UITableView * table;
    NullDataView * nullView;
    
    NSMutableArray * dataArr;
    int page ;
    int totalPage;
    
}

@end

@implementation NewsVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = GrayColor;
    page = 1;
    totalPage = 1;
    
    nav = [self createNav:@"消息中心"];
    [self.view addSubview:nav];
    TabBar = [RootViewController defaultsTabBar];
    
    dataArr = [[NSMutableArray alloc]init];
    
    [self createTable];
    
    nullView = [[NullDataView  alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64) andTitle:@"暂无消息" andImageName:@"noMessage"];
    nullView.backgroundColor = GrayColor;
    
    
    
    [self createDate];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabBar setTabBarHidden:YES];
    [self createDate];
}

#pragma mark - 创建表视图
- (void)createTable{
    
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 43*kHeight) style:UITableViewStylePlain];
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table.showsVerticalScrollIndicator = NO;
    table.showsHorizontalScrollIndicator = NO;
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
    [table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.top.mas_equalTo(nav.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, Main_height-64));
    }];
    [table.mj_header beginRefreshing];
 

}

#pragma mark - 获取服务器数据
- (void)createDate{
    
    
    int pageSize = 10;
    WKProgressHUD *hud = [WKProgressHUD showInView:self.view withText:@"加载中" animated:YES];
    NSString * string = [NSString stringWithFormat:@"%@?pageNo=%d&pageSize=%d",News_List_Url,page,pageSize];
    [Common requestWithUrlString:string contentType:@"application/json" errorShow:YES finished:^(id responseObj){
        
        NSLog(@"%@",string);
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [hud dismiss:YES];
            dataArr = [NSMutableArray arrayWithArray:responseObj[@"data"][@"list"]];
            totalPage = [responseObj[@"data"][@"page"][@"totalPage"] intValue ];
            NSMutableArray * arr = [NSMutableArray arrayWithArray:responseObj[@"data"][@"dNoticeList"]];
            if ([arr count]>0) {
                if (page == 1) {
                    [dataArr removeAllObjects];
                    dataArr = arr;
                }else{
                    for (int i=0; i<[arr count]; i++) {
                        [dataArr addObject:arr[i]];
                    }
                }
                [nullView removeFromSuperview];
            }else{
                [dataArr removeAllObjects];
                [self.view addSubview:nullView];
            }
            [table reloadData];
        });
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [hud dismiss:YES];
           
        });
    }];
    
}


#pragma mark -
#pragma mark - 表协议
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *str=@"str";
    NewsCell  *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (cell == nil)
    {
        cell = [[NewsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
    [cell showInfo:dataArr[indexPath.row]];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    
    return [dataArr count];
//    return 5;
    
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60*kHeight;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
    
    
    NewsDetailsVC * detail = [[NewsDetailsVC alloc]init];
    detail.userId = [dataArr[indexPath.row][@"id"] intValue];
    [self.navigationController pushViewController:detail animated:YES];
    
    
}



/**
    MJRefresh
 */
- (void)addMJRefresh{
    table.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefresh)];
    table.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upRefresh)];
}
- (void)downRefresh{
    
    [table.mj_header endRefreshing];
    [table.mj_footer endRefreshing];
    page = 1;
    [self createDate];
    NSLog(@"刷新");
}
- (void)upRefresh{
    
    if (page!=totalPage) {
        page ++;
        [table.mj_footer endRefreshing];
        
        [self createDate];
        
    }else{
        table.mj_footer.state = MJRefreshStateNoMoreData;
    }
    [table.mj_header endRefreshing];
    NSLog(@"加载%d%d",page,totalPage);
}










@end
