//
//  NewsCell.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/2/20.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "NewsCell.h"
#import <Masonry.h>

@implementation NewsCell

- (void)awakeFromNib {
    [super awakeFromNib];

}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        self.title = [self createUIlabel:@"油价联动通知" andFont:FontOfSize12 andColor:BlackTitleColor];
        self.content = [self createUIlabel:@"知车平台将于2017年2月26日推出海南路线，全新" andFont:FontOfSize10 andColor:fontGrayColor];
        self.content.textAlignment = NSTextAlignmentLeft;
        self.statu = [self createUIlabel:@"已读" andFont:FontOfSize10 andColor:GreenColor];
        self.createTime = [self createUIlabel:@"2016-09-09" andFont:FontOfSize10 andColor:fontGrayColor];
        
//        UIView * line = [[UIView alloc]init];
//        line.backgroundColor = LineGrayColor;
//        [self.contentView addSubview:line];
        
        [self.contentView addSubview:self.title];
        [self.contentView addSubview:self.content];
        [self.contentView addSubview:self.statu];
        [self.contentView addSubview:self.createTime];
        
        [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(16*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_top).with.offset(20*kHeight);
        }];
        [self.content mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(16*kWidth);
            make.centerY.mas_equalTo(self.title.mas_bottom).with.offset(18*kHeight);
            make.width.mas_equalTo(Main_Width-70*kWidth);
        }];
//        [line mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.mas_equalTo(self.contentView.mas_left).with.offset(107*kWidth);
//            make.centerY.mas_equalTo(self.contentView.mas_centerY);
//            make.size.mas_equalTo(CGSizeMake(0.5, 38*kHeight));
//        }];
        
        
        [self.createTime mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).with.offset(-16*kWidth);
            make.centerY.mas_equalTo(self.title.mas_centerY);
        }];
        [self.statu mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.createTime.mas_left).with.offset(-10*kWidth);
            make.centerY.mas_equalTo(self.createTime.mas_centerY);
        }];
        
        UIView * viewline = [[UIView alloc]init];
        viewline.backgroundColor = LineGrayColor;
        [self.contentView addSubview:viewline];
        [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right);
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];
    }
    return self;
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}



-(void)setModel:(NewsModel *)model{
    
    NSLog(@"%@",model.content);
    
    self.title.text = [NSString stringWithFormat:@"%@",model.title];
    self.content.text = [NSString stringWithFormat:@"%@",model.content];
    self.createTime.text = [NSString stringWithFormat:@"%@",model.createTime];

//    BOOL isRead = model.isRead;
//    if (isRead) {
//        //已读
//        self.statu.text = @"已读";
//        self.statu.textColor = GreenColor;
//    }else{
//        self.statu.text = @"未读";
//        self.statu.textColor = RedColor;
//    }
}


-(void)showInfo:(NSMutableDictionary*)dic_info{

    /*     
     "id": 1,
     "title": "提示",
     "content": "11223",
     "status": 0,
     "creator": "中联",
     "createTime": "2017-03-01 11:15:38",
     "userId": 174,
     "isRead": false
*/

    self.title.text = [NSString stringWithFormat:@"%@",dic_info[@"title"]];
    self.content.text = [NSString stringWithFormat:@"%@",dic_info[@"content"]];
    self.createTime.text = [NSString stringWithFormat:@"%@",dic_info[@"createTime"]];
    
        BOOL isRead = [dic_info[@"isRead"] boolValue];
    //0未读  1是读了
        if (isRead) {
            //已读
            self.statu.text = @"已读";
            self.statu.textColor = GreenColor;
            NSLog(@"已读");
        }else{
            self.statu.text = @"未读";
            self.statu.textColor = RedColor;
        }

    
    

    


}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
