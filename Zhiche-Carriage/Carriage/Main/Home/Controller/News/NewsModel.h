//
//  NewsModel.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/3/1.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsModel : NSObject

@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * content;
//@property (nonatomic,assign) BOOL isRead;
@property (nonatomic,strong) NSString * creator;
@property (nonatomic,strong) NSString * createTime;




@end
