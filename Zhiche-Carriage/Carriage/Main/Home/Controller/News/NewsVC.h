//
//  NewsVC.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/2/20.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface NewsVC :  NavViewController <UITableViewDelegate,UITableViewDataSource>

@end
