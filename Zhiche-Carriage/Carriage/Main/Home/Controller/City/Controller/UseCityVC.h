//
//  UseCityVC.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/21.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 意向方向
 */
@interface UseCityVC : UIViewController
@property (nonatomic,copy) void (^callOnlineBack)(NSMutableDictionary * InforDic);

@end
