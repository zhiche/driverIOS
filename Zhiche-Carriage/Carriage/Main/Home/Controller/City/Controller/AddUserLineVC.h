//
//  AddUserLineVC.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/3.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddUserLineVC : UIViewController

@property (nonatomic,copy) NSString *startCityName;
@property (nonatomic,copy) NSString *startCityCode;


@property (nonatomic,copy) NSString *endCityName;
@property (nonatomic,copy) NSString *endCityCode;

@property (nonatomic,copy) NSString *orderId;

@end
