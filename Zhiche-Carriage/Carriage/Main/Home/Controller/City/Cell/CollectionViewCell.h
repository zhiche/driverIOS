//
//  CollectionViewCell.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/6/17.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) UILabel *label;

@end
