//
//  LineViewCell.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "LineViewCell.h"
#import <Masonry.h>

@implementation LineViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
    
    __weak typeof(self) weakSelf = self;
    self.startL = [[UILabel alloc]init];
    [self.contentView addSubview:self.startL];
    [self.startL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(60 * kHeight);
        make.width.mas_equalTo(85 * kWidth);
    
    }];
    
    self.startL.textColor = littleBlackColor;
    self.startL.font = Font(13);
    self.startL.numberOfLines = 0;
    
    UIImageView *imageV = [[UIImageView alloc]init];
    [self.contentView addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(47, 12.5));
        make.centerX.equalTo(weakSelf);
        make.centerY.equalTo(weakSelf.startL);
    }];
    imageV.image = [UIImage imageNamed:@"car_style1"];
    
    
    self.endL = [[UILabel alloc]init];
    [self.contentView addSubview:self.endL];
    [self.endL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-18);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(60 * kHeight);
        make.width.mas_equalTo(85 * kWidth);
        
    }];
    
    self.endL.textColor = littleBlackColor;
    self.endL.font = Font(13);
    self.endL.numberOfLines = 0;
    self.endL.textAlignment = NSTextAlignmentRight;

    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 60, screenWidth, 0.5)];
    label.backgroundColor = LineGrayColor;
    [self.contentView addSubview:label];

    
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:self.deleteButton];
    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.right.mas_equalTo(-18);
        make.top.mas_equalTo(label.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(88, 30 * kHeight));
        
    }];
    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteButton setBackgroundColor:Color_RGB(240, 240, 240, 1)];
    [self.deleteButton setTitleColor:carScrollColor forState:UIControlStateNormal];
    self.deleteButton.layer.cornerRadius = 5;
    self.deleteButton.titleLabel.font = Font(13);
    self.deleteButton.layer.borderWidth = 0.5;
    self.deleteButton.layer.borderColor = carScrollColor.CGColor;
    
    self.editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:self.editButton];
    [self.editButton mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.right.mas_equalTo(weakSelf.deleteButton.mas_left).offset(-10);
        make.top.mas_equalTo(label.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(88, 30 * kHeight));
 
        
    }];
    [self.editButton setTitle:@"编辑" forState:UIControlStateNormal];

    self.editButton.layer.borderColor = YellowColor.CGColor;
    self.editButton.layer.borderWidth = 0.5;
    self.editButton.layer.cornerRadius = 5;
    [self.editButton setTitleColor:YellowColor forState:UIControlStateNormal];
    self.editButton.backgroundColor = Color_RGB(248, 238, 226, 1);
    self.editButton.titleLabel.font = Font(13);

    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
