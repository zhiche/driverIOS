//
//  LineViewCell.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LineViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *startL;
@property (nonatomic,strong) UILabel *endL;
@property (nonatomic,strong) UIButton *deleteButton;
@property (nonatomic,strong) UIButton *editButton;

@end
