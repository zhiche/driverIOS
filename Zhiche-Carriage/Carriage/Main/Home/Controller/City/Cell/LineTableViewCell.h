//
//  LineTableViewCell.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/3.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LineTableViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *titleL;

@property (nonatomic,strong) UILabel *startL;

@end

