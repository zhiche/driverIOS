//
//  CollectionViewCell.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/6/17.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        [self initSubViews];
        
    }
    
    return self;
}


-(void)initSubViews

{
    self.label = [[UILabel alloc]initWithFrame:self.contentView.frame];
    [self.contentView addSubview:self.label];
    
    self.label.layer.cornerRadius = 5;
    self.label.font = Font(15);
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.backgroundColor = [UIColor clearColor];
}
@end
