//
//  LineTableViewCell.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/3.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "LineTableViewCell.h"
#import <Masonry.h>

@implementation LineTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
    
    __weak typeof(self) weakSelf = self;
    self.titleL = [[UILabel alloc]init];
    [self.contentView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(cellHeight);
        make.width.mas_equalTo(50 * kWidth);
        
    }];
    
    self.titleL.textColor = littleBlackColor;
    self.titleL.font = Font(13);
    
    
    self.startL = [[UILabel alloc]init];
    [self.contentView addSubview:self.startL];
    [self.startL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.titleL.mas_right).offset(10);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(cellHeight);
        make.width.mas_equalTo(285 * kWidth);
        
    }];
    
    self.startL.textColor = littleBlackColor;
    self.startL.font = Font(13);
    self.startL.numberOfLines = 0;
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
