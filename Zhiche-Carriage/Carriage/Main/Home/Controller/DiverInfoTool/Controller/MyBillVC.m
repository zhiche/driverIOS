//
//  MyBillVC.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/3/9.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "MyBillVC.h"



#import "RootViewController.h"


#import "MyBillCell.h"
#import "MyBillTimeCell.h"
#import "MyBillDetailVC.h"
#define TableTag 1000
#define TimeTableTag 2000


@interface MyBillVC ()
{
    UIImageView * nav;
    
    RootViewController * TabBar;
    
    UITableView * table;
    
    UITableView * TimeTabel;
 
    NSMutableArray * dataSouceArr;
    
    NSMutableArray * timeArr;
    
    int Index ;
}
@property (nonatomic,strong) UIView * backview;
@property (nonatomic,strong) UIView * allview;
@property (nonatomic,strong) UILabel * time1;
@property (nonatomic,strong) UILabel * time2;

@property (nonatomic,strong) UIButton * timeBtn;
@property (nonatomic,strong) UILabel * priceLabel;




@end

@implementation MyBillVC

- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"我的账单"];
    [self.view addSubview:nav];
    self.view.backgroundColor = WhiteColor;
    TabBar = [RootViewController defaultsTabBar];
    dataSouceArr = [[NSMutableArray alloc]init];
    timeArr = [[NSMutableArray alloc]init];
    
    Index = 1;
    [self createUI];
    [self createTable];
    [self createSMTable];
    [self createDate:@"" andEndDate:@""];

}



-(void)createUI{
    
    
    [self.view addSubview:self.backview];
    [self.backview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.top.mas_equalTo(nav.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 150*kHeight));
    }];
    
//    [self.backview addSubview:self.time1];
    [self.backview addSubview:self.time2];
    
//    [self.time1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.backview.mas_left).with.offset(16*kWidth);
//        make.centerY.mas_equalTo(self.backview.mas_top).with.offset(21.5*kHeight);
//    }];
    [self.time2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.backview.mas_left).with.offset(16*kWidth);
        make.centerY.mas_equalTo(self.backview.mas_top).with.offset(21.5*kHeight);    }];
    
    [self.backview addSubview:self.timeBtn];
    [self.timeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.backview.mas_right).with.offset(-10*kWidth);
        make.centerY.mas_equalTo(self.backview.mas_top).with.offset(21.5*kHeight);
        make.size.mas_equalTo(CGSizeMake(50*kWidth, 40*kHeight));
    }];
    
    
    [self.backview addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.backview.mas_centerY).with.offset(21.5*kHeight);
        make.centerX.mas_equalTo(self.backview.mas_centerX);
    }];

    
    
    UILabel * totlelabel  =[self createUIlabel:@"合计" andFont:FontOfSize11 andColor:BtnTitleColor];
    [self.backview addSubview:totlelabel];
    [totlelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.priceLabel.mas_left).with.offset(-10*kWidth);
        make.bottom.mas_equalTo(self.priceLabel.mas_bottom);
    }];

}


-(UIView*)backview{
    
    
    if (!_backview) {
        
        _backview = [[UIView alloc]init];
        _backview.backgroundColor = WhiteColor;
        UIView * line = [[UIView alloc]init];
        line.backgroundColor = LineGrayColor;
        [_backview addSubview:line];
        
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_backview.mas_left);
            make.centerY.mas_equalTo(_backview.mas_top).with.offset(43*kHeight);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5*kHeight));
        }];
        
        UIView * line1 = [[UIView alloc]init];
        line1.backgroundColor = LineGrayColor;
        [_backview addSubview:line1];
        
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_backview.mas_left);
            make.bottom.mas_equalTo(_backview.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 8*kHeight));
        }];

    }
    return _backview;
}




-(UILabel*)time1{
    if (!_time1) {
        
        _time1 = [self createUIlabel:@"上周" andFont:FontOfSize13 andColor:BlackTitleColor];
    }
    return _time1;

}
-(UILabel*)time2{
    
    if (!_time2) {
        _time2 = [self createUIlabel:@"2017年01日-07日" andFont:FontOfSize10 andColor:fontGrayColor];
        
    }
    return _time2;

}

-(UILabel*)priceLabel{
    
    if (!_priceLabel) {
        
        _priceLabel = [self createUIlabel:@"9000.00" andFont:FontOfSize15 andColor:YellowColor];
    }
    return _priceLabel;
    
}



-(UIButton*)timeBtn{
    
    
    if (!_timeBtn) {
        _timeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        _timeBtn.backgroundColor = [UIColor cyanColor];
        [_timeBtn setImage:[UIImage imageNamed:@"icon_infor_data_imges"] forState:UIControlStateNormal];
        [_timeBtn addTarget:self action:@selector(pressTimeBtn) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _timeBtn;
}



-(void)pressTimeBtn{
    
    if (self.allview.hidden) {
        self.allview.hidden = NO;
    }else{
        self.allview.hidden = YES;
    }
}


-(void)createSMTable{
    
    _allview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height)];
    _allview.backgroundColor = RGBACOLOR(0, 0, 0, 0.3);
    _allview.userInteractionEnabled = YES;
    _allview.hidden = YES;
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    // 添加到窗口
    [window addSubview:_allview];
    
    UITapGestureRecognizer * Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dataBackDiss)];
    Tap.delegate = self;
    [_allview addGestureRecognizer:Tap];
    
    UIImageView * image = [[UIImageView alloc]init];
    image.userInteractionEnabled = YES;
    image.tag = 1111;
    image.image = [UIImage imageNamed:@"icon_infor_drop-down_imges"];
    [_allview addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_allview.mas_right).with.offset(-8*kWidth);
        make.top.mas_equalTo(_allview.mas_top).with.offset(90*kHeight);
        make.size.mas_equalTo(CGSizeMake(100*kWidth, 44*kHeight*5));
    }];
    
    TimeTabel = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 43*kHeight) style:UITableViewStylePlain];
    [TimeTabel setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    TimeTabel.layer.cornerRadius = 5;
    TimeTabel.showsVerticalScrollIndicator = NO;
    TimeTabel.showsHorizontalScrollIndicator = NO;
    TimeTabel.tag = TimeTableTag;
    
    TimeTabel.delegate = self;
    TimeTabel.dataSource = self;
    [image addSubview:TimeTabel];//149 × 196
    [TimeTabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(image.mas_right).with.offset(1*kHeight);
        make.top.mas_equalTo(image.mas_top).with.offset(10*kHeight);
        make.size.mas_equalTo(CGSizeMake(100*kWidth, 43*kHeight*5));
    }];
}



-(void)dataBackDiss{
    
    self.allview.hidden = YES;
}

-(void)createTable{
    
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 43*kHeight) style:UITableViewStylePlain];
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table.showsVerticalScrollIndicator = NO;
    table.showsHorizontalScrollIndicator = NO;
    table.delegate = self;
    table.dataSource = self;
    table.tag = TableTag;
    [self.view addSubview:table];
    [table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.top.mas_equalTo(self.backview.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 180*kHeight));
    }];

}
-(void)createDate:(NSString*)beginDate andEndDate:(NSString*)endDate{
    
    //
    
    NSString * url = [NSString stringWithFormat:@"%@?beginDate=%@&endDate=%@",Find_summaryList_Url,beginDate,endDate];
    [Common requestWithUrlString:url contentType:@"application/json" errorShow:YES finished:^(id responseObj){
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            

            NSLog(@"%@",responseObj);
            
            if ([responseObj[@"success"] boolValue ]) {
                
                timeArr = responseObj[@"data"][@"weekList"];
                dataSouceArr = responseObj[@"data"][@"summaryList"];
                self.priceLabel.text = [NSString stringWithFormat:@"%@",responseObj[@"data"][@"amountPrice"]];
                self.time2.text = timeArr[0];
                [table reloadData];
                [TimeTabel reloadData];
                
            }else{
                
                [WKProgressHUD popMessage:responseObj[@"message"] inView:self.view duration:1.5 animated:YES];

            }
            

        });
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            
        });
    }];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *str=@"str";
    
    if (tableView.tag == TableTag) {
        MyBillCell  * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[MyBillCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //    NSMutableArray * arr = dataSouceArr[indexPath.section][@"list"];
        
        if (dataSouceArr.count >0) {
            
            [cell showInfo:dataSouceArr[indexPath.row]];

        }
        
        return cell;
  
    }else{
        
        MyBillTimeCell  * cell =  [tableView dequeueReusableCellWithIdentifier:str];
        if (cell ==nil) {
            cell = [[MyBillTimeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
//        cell.title.text = [NSString stringWithFormat:@"%@",BillListArr[indexPath.row][@"monthText"]];
//        if (indexPath.row == [BillListArr count]-1) {
//            cell.viewline.hidden = YES;
//        }

        if (timeArr.count >0) {
            
            cell.title.text = timeArr[indexPath.row];
 
        }
        
        return cell;
      
    }
    
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //    return [dataSouceArr count];
    
    return  tableView.tag == TableTag ? dataSouceArr.count:timeArr.count;
 
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    
    return  tableView.tag == TableTag ? 60*kHeight:43*kHeight;

    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //      int  b = [[string1 substringWithRange:NSMakeRange(0,2)] intValue];

    
    if (tableView.tag == TableTag) {
        MyBillDetailVC  * detail = [[MyBillDetailVC alloc]init];
        detail.orderCode = dataSouceArr[indexPath.row][@"orderCode"];
        [self.navigationController pushViewController:detail animated:YES];
    }else{
        NSLog(@"点击的是选择时间");
        
//03/13-03/19
        NSString * timeStr =  [timeArr[indexPath.row] stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
        NSString * begin = [timeStr substringWithRange:NSMakeRange(0, 5)];
        NSString * end = [timeStr substringWithRange:NSMakeRange(6, 5)];
        [self createDate:begin andEndDate:end];
        [self dataBackDiss];
        
        
    }
    
    
    
}


-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentLeft;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [TabBar setTabBarHidden:YES];
}

/* 解决触摸手势和cell点击的冲突 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([touch.view isEqual:self.allview]) {
        return YES;
    }
    return NO;
}





@end
