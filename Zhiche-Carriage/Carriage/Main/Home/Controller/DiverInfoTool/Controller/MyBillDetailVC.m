//
//  MyBillDetailVC.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/3/10.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "MyBillDetailVC.h"

#import "RootViewController.h"

#import "MyBillDetailCell.h"
@interface MyBillDetailVC ()
{
    UIImageView * nav;
 
    RootViewController * TabBar;
    UITableView * table;
    
    NSMutableArray * dataSouceArr;
    NSMutableDictionary * dataSouceDic;
    
}
@property (nonatomic,strong)UIView * backview;
@property (nonatomic,strong)UILabel * addressLabel;
@property (nonatomic,strong)UILabel * priceLabel;
@property (nonatomic,strong)UILabel * stateLabel;




@end

@implementation MyBillDetailVC




- (void)viewDidLoad {
    [super viewDidLoad];
    nav = [self createNav:@"账单详情"];
    [self.view addSubview:nav];
    self.view.backgroundColor = WhiteColor;
    TabBar = [RootViewController defaultsTabBar];
    dataSouceArr = [[NSMutableArray alloc]init];
    dataSouceDic = [[NSMutableDictionary alloc]init];
    
    
    [self createUI];
    [self createTable];
    [self createDate];

}

 
-(void)createDate{
    
    
//    WKProgressHUD *hud = [WKProgressHUD showInView:self.view withText:@"加载中" animated:YES];
    NSString * string = [NSString stringWithFormat:@"%@?orderCode=%@",Find_myBillDetail_Url,self.orderCode];
    [Common requestWithUrlString:string contentType:@"application/json" errorShow:YES finished:^(id responseObj){
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
//            [hud dismiss:YES];


            
            NSLog(@"%@",responseObj);
            dataSouceArr = responseObj[@"data"][@"billDetail"];
            dataSouceDic = responseObj[@"data"];
            
            self.priceLabel.text = [NSString stringWithFormat:@"%@",responseObj[@"data"][@"price"]];
            self.addressLabel.text = [NSString stringWithFormat:@"%@",responseObj[@"data"][@"line"]];
            
            
            
            [table mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.view.mas_left);
                make.top.mas_equalTo(self.backview.mas_bottom).with.offset(8*kHeight);
                make.size.mas_equalTo(CGSizeMake(Main_Width, 43*(dataSouceArr.count+2)*kHeight));
            }];
            
            
            [table reloadData];
        });
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
//            [hud dismiss:YES];
            
        });
    }];
    
}


-(void)createUI{
    
    [self.view addSubview:self.backview];
    
    [self.backview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.top.mas_equalTo(nav.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 150*kHeight));
    }];
    
    [self.backview addSubview:self.addressLabel];
    [self.backview addSubview:self.priceLabel];
    [self.backview addSubview:self.stateLabel];
    
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.backview);
    }];
    
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.priceLabel.mas_top).with.offset(-5*kHeight);
    }];
    [self.stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.mas_equalTo(self.priceLabel.mas_bottom).with.offset(5*kHeight);
    }];


    
}


-(void)createTable{
    
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 43*kHeight) style:UITableViewStylePlain];
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    table.showsVerticalScrollIndicator = NO;
    table.showsHorizontalScrollIndicator = NO;
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
    [table mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.top.mas_equalTo(self.backview.mas_bottom).with.offset(8*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 43*3*kHeight));
    }];
//    [table reloadData];

    
    
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *str=@"str";
    
    MyBillDetailCell  * cell =  [tableView dequeueReusableCellWithIdentifier:str];
    if (cell ==nil) {
        cell = [[MyBillDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //    NSMutableArray * arr = dataSouceArr[indexPath.section][@"list"];
    
    //    [cell showInfo:arr[indexPath.row]] ;
    
    if (dataSouceArr.count > 0) {
        
        if (indexPath.row == 0) {
            
            cell.titleLabel.text = @"运单号：";
            cell.detaillLabel.text = [NSString stringWithFormat:@"%@",dataSouceDic[@"orderCode"]];
            
        }else if (indexPath.row == 1){
            
            cell.titleLabel.text = @"车型：";
            cell.detaillLabel.text = [NSString stringWithFormat:@"%@",dataSouceDic[@"vehicle"]];
        }else{
            [cell showInfo:dataSouceArr[indexPath.row-2]];
        }

    }
    
    
    
    return cell;
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //    return [dataSouceArr count];
    
    
    
//    return   [dataSouceArr count]+2;
    return  dataSouceArr.count>0?dataSouceArr.count+2:dataSouceArr.count;
//    return 3;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    return  43*kHeight;
    
    
}



-(UILabel*)addressLabel{
    
    if (!_addressLabel) {
        _addressLabel = [self createUIlabel:@"江西省南昌市-江苏省无锡市" andFont:FontOfSize12 andColor:LineGrayColor];
    }
    return _addressLabel;
    
}

-(UILabel*)priceLabel{
    
    if (!_priceLabel) {
        _priceLabel = [self createUIlabel:@"3000.00" andFont:FontOfSize16 andColor:YellowColor];
    }
    return _priceLabel;
    
}

-(UILabel*)stateLabel{
    
    if (!_stateLabel) {
        _stateLabel = [self createUIlabel:@"已支付" andFont:FontOfSize10 andColor:BtnTitleColor];
        
    }
    return _stateLabel;
    
}


-(UIView*)backview{
    
    if (!_backview) {
        _backview = [[UIView alloc]init];
        _backview.backgroundColor = WhiteColor;
        
        UIView * line1 = [[UIView alloc]init];
        line1.backgroundColor = LineGrayColor;
        [_backview addSubview:line1];
        
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_backview.mas_left);
            make.bottom.mas_equalTo(_backview.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 8*kHeight));
        }];

    }
    return _backview;
    
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentLeft;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
