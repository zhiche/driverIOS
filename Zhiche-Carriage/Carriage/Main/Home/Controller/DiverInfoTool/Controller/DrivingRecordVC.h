//
//  DrivingRecordVC.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 16/11/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface DrivingRecordVC :NavViewController <UITableViewDelegate,UITableViewDataSource>

@end
