//
//  DrivingRecordVC.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 16/11/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "DrivingRecordVC.h"
#import "DrivingRecordCell.h"
#import "RootViewController.h"
#import "NullDataView.h"

 

@interface DrivingRecordVC ()
{
    UIImageView * nav;
   
    RootViewController * TabBar;
   
    NullDataView * nullView;
    
    NSMutableArray * dataArr;
    int page ;
    int totalPage;
    
}

@property (nonatomic, strong) UITableView *tableView;

@end


@implementation DrivingRecordVC

- (UITableView *)tableView {
    if (!_tableView)
    {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 43*kHeight) style:UITableViewStylePlain];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GrayColor;
    page = 1;
    totalPage = 1;
    
    nav = [self createNav:@"出车记录"];
    [self.view addSubview:nav];
    TabBar = [RootViewController defaultsTabBar];
    
    dataArr = [[NSMutableArray alloc]init];
    
    [self createTable];
    
    nullView = [[NullDataView  alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64) andTitle:@"暂无记录" andImageName:@"no_recode"];
    nullView.backgroundColor = GrayColor;

    
    [self createDate];
}


-(void)createTable{
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.top.mas_equalTo(nav.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(Main_Width, Main_height-64));
    }];
    


}




-(void)createDate{
    
    
    int pageSize = 10;
    WKProgressHUD *hud = [WKProgressHUD showInView:self.view withText:@"加载中" animated:YES];
    NSString * string = [NSString stringWithFormat:@"%@?pageNo=%d&pageSize=%d",waybill_done_Url,page,pageSize];
    [Common requestWithUrlString:string
                     contentType:@"application/json"
                       errorShow:YES
                        finished:^(id responseObj){
        
        NSLog(@"%@",string);
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [hud dismiss:YES];
            dataArr = [NSMutableArray arrayWithArray:responseObj[@"data"][@"list"]];
            totalPage = [responseObj[@"data"][@"page"][@"totalPage"] intValue ];
            NSMutableArray * arr = [NSMutableArray arrayWithArray:responseObj[@"data"][@"list"]];
            if ([arr count]>0) {
                if (page == 1) {
                    [dataArr removeAllObjects];
                    dataArr = arr;
                }else{
                    for (int i=0; i<[arr count]; i++) {
                        [dataArr addObject:arr[i]];
                    }
                }
                [nullView removeFromSuperview];
            }else{
                [dataArr removeAllObjects];
                [self.view addSubview:nullView];
            }
            [self.tableView reloadData];
        });
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [hud dismiss:YES];
            
        });
    }];
    
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *str=@"str";
    DrivingRecordCell  * cell =  [tableView dequeueReusableCellWithIdentifier:str];
    if (cell ==nil) {
        cell = [[DrivingRecordCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell showInfo:dataArr[indexPath.section][@"waybills"][indexPath.row]];
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [dataArr[section][@"waybills"] count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 90*kHeight;

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [dataArr count];
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 43*kHeight;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, 43*kHeight)];
    view.backgroundColor = WhiteColor;
    
    NSString * string = [NSString stringWithFormat:@"%@",dataArr[section][@"month"]];

    UILabel * timelabel = [self createUIlabel:string andFont:FontOfSize13 andColor:littleBlackColor];
    [view addSubview:timelabel];
    
    [timelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(view.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(view.mas_top).with.offset(21.5*kHeight);
    }];

    UIView * viewline = [[UIView alloc]init];
    viewline.backgroundColor = LineGrayColor;
    [view addSubview:viewline];
    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(view.mas_bottom);
        make.left.mas_equalTo(view.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    return view;
    
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TabBar setTabBarHidden:YES];
}

/**
    MJRefresh
 */
- (void)addMJRefresh {
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefresh)];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upRefresh)];
}
- (void)downRefresh {
    [self.tableView.mj_header endRefreshing];
    
    [self.tableView.mj_footer endRefreshing];
    
    page = 1;
    
    [self createDate];
    
    NSLog(@"刷新");
}
- (void)upRefresh{
    
    if (page!=totalPage) {
        page ++;
        [self.tableView.mj_footer endRefreshing];
        
        [self createDate];
        
    }else{
        self.tableView.mj_footer.state = MJRefreshStateNoMoreData;
    }
    [self.tableView.mj_header endRefreshing];
    NSLog(@"加载%d%d",page,totalPage);
}




@end
