//
//  MyBillDetailVC.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/3/10.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"

@interface MyBillDetailVC : NavViewController <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSString * orderCode;

@end
