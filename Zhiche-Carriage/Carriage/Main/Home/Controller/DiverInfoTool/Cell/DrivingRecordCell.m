//
//  DrivingRecordCell.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 16/11/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "DrivingRecordCell.h"
#import <Masonry.h>

@implementation DrivingRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        self.TimeTitle = [self createUIlabel:@"10-23" andFont:FontOfSize12 andColor:YellowColor];
        [self.contentView addSubview:self.TimeTitle];
        [self.TimeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(16*kWidth);
            make.centerY.mas_equalTo(self.contentView);
        }];
        
        self.addressTitle = [self createUIlabel:@"江西-萍乡--福建-厦门" andFont:FontOfSize12 andColor:BlackColor];
        [self.contentView addSubview:self.addressTitle];
        [self.addressTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.contentView.mas_top).with.offset(20*kHeight);
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(80*kWidth);

        }];
        self.carTitle = [self createUIlabel:@"宝典新超值版2016" andFont:FontOfSize12 andColor:BlackColor];
        [self.contentView addSubview:self.carTitle];
        [self.carTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.addressTitle.mas_centerY).with.offset(25*kHeight);
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(80*kWidth);
            
        }];
        self.distanceTitle = [self createUIlabel:@"行驶公里：1000km" andFont:FontOfSize12 andColor:BlackColor];
        [self.contentView addSubview:self.distanceTitle];
        [self.distanceTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.carTitle.mas_centerY).with.offset(25*kHeight);
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(80*kWidth);
        }];
        UIView * viewline = [[UIView alloc]init];
        viewline.backgroundColor = LineGrayColor;
        [self.contentView addSubview:viewline];
        [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.left.mas_equalTo(self.contentView.mas_left);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];
        
    }
    return self;
}


-(void)showInfo:(NSMutableDictionary *)dic_info {
    
    
//    arrivalCity = "\U6b66\U6c49\U5e02";
//    arrivalProvince = "\U6e56\U5317\U7701";
//    departCity = "";
//    departProvince = "";
//    distance = 20;
//    id = 117;
//    orderCode = "0171010096/JYGA2011";
//    orderTime = "2016-11-03 07:51:00";
//    vehicleTypeName = "\U5168\U987aV348\U77ed\U8f74v\U67f4";
//
    NSString * label11 = [NSString stringWithFormat:@"%@",dic_info[@"orderTime"]];
    _TimeTitle.text = [label11 substringWithRange:NSMakeRange(5, 5)];

    _addressTitle.text = [NSString stringWithFormat:@"%@--%@",dic_info[@"departAddr"],dic_info[@"arrivalAddr"]];
    _carTitle.text = [NSString stringWithFormat:@"%@",dic_info[@"vehicleTypeName"]];
    _distanceTitle.text = [NSString stringWithFormat:@"行驶公里：%.2fkm",[dic_info[@"distance"] floatValue]];
    NSLog(@"%@",dic_info);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}


@end
