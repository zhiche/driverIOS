//
//  MyBillTimeCell.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/3/9.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyBillTimeCell : UITableViewCell

@property (nonatomic,strong) UILabel * title;
@property (nonatomic,strong) UIView * viewline;


@end
