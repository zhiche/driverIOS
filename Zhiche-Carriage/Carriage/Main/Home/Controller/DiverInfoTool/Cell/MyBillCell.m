//
//  MyBillCell.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/3/9.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "MyBillCell.h"
#import "UILabel+Category.h"


#import <Masonry.h>

@implementation MyBillCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        self.dateLabel = [[UILabel alloc]init];
        self.dateLabel.textColor = fontGrayColor;
        self.dateLabel.font = Font(FontOfSize10);
        self.dateLabel.text = @"2017年2月1日";
        self.dateLabel.numberOfLines = 2;
        
        
        self.priceLabel = [UILabel createUIlabel:@"+3000" andFont:FontOfSize12 andColor:YellowColor];
        
        self.addressLabel = [UILabel createUIlabel:@"江西省南昌市-江苏省无锡市" andFont:FontOfSize12 andColor:fontGrayColor];
        
        //        UIView * line = [[UIView alloc]init];
        //        line.backgroundColor = LineGrayColor;
        //        [self.contentView addSubview:line];
        
        [self.contentView addSubview:self.dateLabel];
        [self.contentView addSubview:self.priceLabel];
        [self.contentView addSubview:self.addressLabel];
        
        [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).with.offset(16*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.width.mas_equalTo(35*kWidth);
        }];
        [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(100*kWidth);
            make.top.mas_equalTo(self.contentView.mas_top).with.offset(10*kHeight);
        }];
        //        [line mas_makeConstraints:^(MASConstraintMaker *make) {
        //            make.left.mas_equalTo(self.contentView.mas_left).with.offset(107*kWidth);
        //            make.centerY.mas_equalTo(self.contentView.mas_centerY);
        //            make.size.mas_equalTo(CGSizeMake(0.5, 38*kHeight));
        //        }];
        
        
        [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.priceLabel.mas_left);
            make.top.mas_equalTo(self.priceLabel.mas_bottom).with.offset(5*kHeight);
        }];

        
        UIView * viewline = [[UIView alloc]init];
        viewline.backgroundColor = LineGrayColor;
        [self.contentView addSubview:viewline];
        [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right);
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];
        //箭头
        UIImageView *arrowImg = [[UIImageView alloc]init];                                 
        arrowImg.image = [UIImage imageNamed:@"personal_arrow"];
        [self.contentView addSubview:arrowImg];
        [arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).with.offset(-26*kWidth);
            make.centerY.mas_equalTo(self.contentView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(6.5, 11));
        }];


        
        
    }
    return self;
}








-(void)showInfo:(NSMutableDictionary*)dic_info{
    

//    {
//        billDetailId = 136;
//        createTime = "2017-03-10 12:15:03";
//        destCity = "\U5b9c\U6625";
//        dtPrintDate = "2016-11-01 00:00:00";
//        dtUpdateDate = "2017-03-13 22:01:45";
//        id = 119;
//        updateDate = "2017-03-13 22:01:45";
//        userId = 915;
//        vcstyleNo = "X7\Uff08\U6c7d\Uff09";
//        waybillId = 2045;
//    },
    
    self.dateLabel.text = dic_info[@"recoveryTime"];
    self.priceLabel.text = dic_info[@"price"];
    self.addressLabel.text = dic_info[@"lines"];

    
    
    
    
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
