//
//  MyBillCell.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/3/9.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyBillCell : UITableViewCell


@property(nonatomic,strong) UILabel * dateLabel;
@property(nonatomic,strong) UILabel * priceLabel;
@property(nonatomic,strong) UILabel * addressLabel;



-(void)showInfo:(NSMutableDictionary*)dic_info;


@end
