//
//  MyBillDetailCell.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/3/10.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "MyBillDetailCell.h"
#import <Masonry.h>

@implementation MyBillDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
        self.titleLabel = [self createUIlabel:@"订单号：" andFont:FontOfSize11 andColor:BtnTitleColor];
        [self.contentView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView.mas_left).with.offset(16*kWidth);
            make.centerY.mas_equalTo(self.contentView);
        }];

        self.detaillLabel = [self createUIlabel:@"0171106429" andFont:FontOfSize11 andColor:BtnTitleColor];
        [self.contentView addSubview:self.detaillLabel];
        [self.detaillLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.titleLabel.mas_right).with.offset(5*kWidth);
            make.centerY.mas_equalTo(self.contentView);
        }];
        
        self.viewline = [[UIView alloc]init];
        self.viewline.backgroundColor = LineGrayColor;
        [self.contentView addSubview:self.viewline];
        [self.viewline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.contentView.mas_bottom);
            make.left.mas_equalTo(self.contentView.mas_left);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];
        
        
        
    }
    return self;
}
-(void)showInfo:(NSMutableDictionary*)dic_info{
    
    

    self.titleLabel.text = [NSString stringWithFormat:@"%@：",dic_info[@"name"]];
//    NSString * value = [NSString stringWithFormat:@"%.2f",[dic_info[@"value"] floatValue]];
    self.detaillLabel.text = [NSString stringWithFormat:@"%@",dic_info[@"value"]];

    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentRight;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}

@end
