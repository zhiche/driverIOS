//
//  DrivingRecordCell.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 16/11/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrivingRecordCell : UITableViewCell

@property (nonatomic,strong) UILabel * TimeTitle;
@property (nonatomic,strong) UILabel * addressTitle;
@property (nonatomic,strong) UILabel * carTitle;
@property (nonatomic,strong) UILabel * distanceTitle;

-(void)showInfo:(NSMutableDictionary *)dic_info  ;

@end
