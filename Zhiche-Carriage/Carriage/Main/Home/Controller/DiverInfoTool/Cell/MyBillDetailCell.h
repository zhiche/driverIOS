//
//  MyBillDetailCell.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/3/10.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyBillDetailCell : UITableViewCell


@property (nonatomic,strong) UILabel * titleLabel;
@property (nonatomic,strong) UILabel * detaillLabel;
@property (nonatomic,strong) UIView * viewline;

-(void)showInfo:(NSMutableDictionary*)dic_info;


@end
