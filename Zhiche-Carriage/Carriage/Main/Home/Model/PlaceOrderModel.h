//
//  PlaceOrderModel.h
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/5/17.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlaceOrderModel : NSObject

//address = Xiangxi;
//addressType = 1;
//cityCode = 130181;
//cityName = "\U8f9b\U96c6\U5e02";
//comment = "";
//contact = Wang;
//countyCode = 130183;
//countyName = "\U664b\U5dde\U5e02";
//createTime = 1463569315000;
//id = 6;
//isDefault = F;
//phone = 15011578178;
//provinceCode = 130000;
//provinceName = "\U6cb3\U5317\U7701";
//unitName = Shouche;
//updateTime = 1463569315000;
//userId = 4;
//
@property (nonatomic, copy) NSString * provinceName;
@property (nonatomic, copy) NSString * cityName;
@property (nonatomic, copy) NSString * countyName;
@property (nonatomic, copy) NSString * unitName;

@property (nonatomic, copy) NSString * address;
@property (nonatomic, copy) NSString * contact;
@property (nonatomic, copy) NSString * phone;

+(instancetype)ModelWithDic:(NSMutableDictionary*)dic;

@end


