//
//  DynamicCollectionViewCell.h
//  DymanicCollectionView
//
//  Created by LeeBruce on 16/11/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DynamicCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) UIImageView *imageView;

@end
