//
//  DynamicCollectionView.h
//  DymanicCollectionView
//
//  Created by LeeBruce on 16/11/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DynamicCollectionView : UIView
@property (nonatomic,copy) void (^callBack)(NSMutableArray *arr);
@end
