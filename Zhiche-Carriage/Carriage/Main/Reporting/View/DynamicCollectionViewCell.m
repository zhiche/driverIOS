//
//  DynamicCollectionViewCell.m
//  DymanicCollectionView
//
//  Created by LeeBruce on 16/11/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "DynamicCollectionViewCell.h"

@implementation DynamicCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        
        self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        
        [self.contentView addSubview:self.imageView];
        
    }
    
    return self;
}

@end
