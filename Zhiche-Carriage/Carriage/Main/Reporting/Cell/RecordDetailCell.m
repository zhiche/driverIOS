//
//  RecordDetailCell.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/16.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "RecordDetailCell.h"
#import "DynamicCollectionViewCell.h"
#import <UIImageView+WebCache.h>
#import "BigPhotoView.h"
#import "ScrollImageViewController.h"


@implementation RecordDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.imgArray = [NSMutableArray array];
        self.imageArray = [NSMutableArray array];


        [self initSubviews];
    }
    
    return  self;
}



-(void)postValueWith:(NSMutableArray *)arr
{
    if (arr.count >0) {
        
        self.imgArray = arr;
        
        [self.collectionView reloadData];

    }
}

-(void)initSubviews
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(80 * kHeight, 80 * kHeight);
    layout.sectionInset = UIEdgeInsetsMake(10, 20, 10, 20);
    layout.minimumLineSpacing = 10;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 100 * kHeight) collectionViewLayout:layout];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    [collectionView registerClass:[DynamicCollectionViewCell class] forCellWithReuseIdentifier:@"DynamicCollectionViewCell"];
    
    collectionView.backgroundColor = WhiteColor;
    _collectionView = collectionView;
    
    [self addSubview:_collectionView];
    
}


#pragma mark UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.imgArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DynamicCollectionViewCell *cell = (DynamicCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"DynamicCollectionViewCell" forIndexPath:indexPath];
    
    if (_imgArray.count > 0) {
        
        [cell.imageView sd_setImageWithURL:_imgArray[indexPath.row] placeholderImage:[UIImage imageNamed:@"car_style"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
            //        if (!activityIndicator)
            //        {
            //            activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            //            activityIndicator.center = images.center;
            //            //把更新UI放到主线程
            //            dispatch_async(dispatch_get_main_queue(), ^{
            //                [images addSubview:activityIndicator];
            //            });
            //
            //            [activityIndicator startAnimating];
            //        }
            
        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
//            [self.imageArray addObject:image];
            if (error) {
                cell.imageView.image = [UIImage imageNamed:@"bad_picture"];

            }
            
            
        }];
    }else{
        cell.imageView.image = [UIImage imageNamed:@"car_style"];
        
    }

    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ScrollImageViewController *scrollVC = [[ScrollImageViewController alloc]init];
    
    scrollVC.imageArr = _imgArray;
    
    [[self viewController].navigationController pushViewController:scrollVC animated:YES];
//    if (self.imgArray.count == self.imageArray.count) {
//        
//        BigPhotoView *bigPhotoView = [[BigPhotoView alloc]initWithFrame:[UIScreen mainScreen].bounds imageArray:self.imageArray selectPathIndex:indexPath collectionView:self.collectionView];
//        [self addSubview:bigPhotoView];
//    }
//
}

- (UIViewController *)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
