//
//  RecordDetailCell.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/16.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordDetailCell : UITableViewCell <UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *imgArray;
@property (nonatomic,strong) NSMutableArray *imageArray;

-(void)postValueWith:(NSMutableArray *)arr;

@end
