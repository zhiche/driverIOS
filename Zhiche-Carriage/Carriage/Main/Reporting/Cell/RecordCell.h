//
//  RecordCell.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordCell : UITableViewCell
@property (nonatomic,strong) UILabel *timeLabel;
@property (nonatomic,strong) UILabel *addressLabel;
@property (nonatomic,strong) UILabel *styleLabel;

@end
