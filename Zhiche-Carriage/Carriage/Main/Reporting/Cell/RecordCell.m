//
//  RecordCell.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "RecordCell.h"
#import <Masonry.h>

@implementation RecordCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
        
    }
    
    return  self;
}

-(void)initSubviews
{
    
    self.contentView.backgroundColor = WhiteColor;
    __weak typeof(self) weakSelf = self;
    self.timeLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.top.mas_equalTo(0);
        make.width.mas_equalTo(100 * kWidth);
        make.height.equalTo(weakSelf);
        
    }];
    
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.font = Font(13);
    self.timeLabel.textColor = carScrollColor;
    
    UILabel *label = [[UILabel alloc]init];
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf);
        make.size.mas_equalTo(CGSizeMake(0.5, 50 * kHeight));
        make.left.mas_equalTo(weakSelf.timeLabel.mas_right).offset(0);
        
    }];
    
    label.backgroundColor = LineGrayColor;
    
    
    
    
    self.addressLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.addressLabel];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(weakSelf.timeLabel.mas_right).offset(20);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(screenWidth - 100 * kWidth);
        make.height.mas_equalTo(weakSelf.mas_height).multipliedBy(0.5);

    }];
    self.addressLabel.font = Font(14);
    self.addressLabel.textColor = carScrollColor;

    
    
    self.styleLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.styleLabel];
    [self.styleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.width.equalTo(weakSelf.addressLabel);
        make.top.mas_equalTo(weakSelf.addressLabel.mas_bottom).offset(0);
        make.height.mas_equalTo(weakSelf.mas_height).multipliedBy(0.5);
        
    }];
    self.styleLabel.font = Font(14);
    self.styleLabel.textColor = carScrollColor;


    
}





- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
