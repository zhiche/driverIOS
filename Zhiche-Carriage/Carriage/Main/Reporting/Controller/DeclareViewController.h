//
//  DeclareViewController.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeclareViewController : UIViewController

@property (nonatomic,copy) NSString *orderId;

@property (nonatomic,copy) NSString *orderString;

@property (nonatomic,copy) void (^isSuccess)(BOOL b);//判断是否上传成功  更改返回重新刷新页面

//@property (nonatomic) NSInteger auditStatus;//审核状态 0待审核 1审核中 2完成  3审核未通过

@end
