//
//  ScanVC.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 事故申报
 */
@interface ScanVC : UIViewController

@property (nonatomic,strong) NSString *orderId;

@property (nonatomic) NSInteger statusId;

@property (nonatomic,copy) void (^isChange)(BOOL b);//判断图片是否更改  更改返回重新刷新页面

@property (nonatomic,strong) NSString *orderNumber;

@end
