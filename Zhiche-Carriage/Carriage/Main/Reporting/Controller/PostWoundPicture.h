//
//  PostWoundPicture.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 上传带伤照片
 */
@interface PostWoundPicture : UIViewController

@property (nonatomic,strong) NSString *orderId;
@end
