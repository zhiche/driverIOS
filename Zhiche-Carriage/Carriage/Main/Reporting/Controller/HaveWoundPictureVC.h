//
//  HaveWoundPictureVC.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/16.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 上传带伤照片
 */
@interface HaveWoundPictureVC : UIViewController
@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,copy) NSString *damageAudit;
@end
