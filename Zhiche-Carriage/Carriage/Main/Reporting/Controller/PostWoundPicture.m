//
//  PostWoundPicture.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "PostWoundPicture.h"
#import "TopBackView.h"
#import "QiniuSDK.h"

#import "RootViewController.h"
#import "DynamicCollectionView.h"

@interface PostWoundPicture ()

@property (nonatomic, strong) NSMutableArray *postArr;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) DynamicCollectionView *cynamicV;

@property (nonatomic, strong) TopBackView *topBackView;

@end

@implementation PostWoundPicture
#pragma mark -
#pragma mark - Lazy
- (TopBackView *)topBackView {
    if (!_topBackView) {
        CGRect frame = CGRectMake(0, 0, screenWidth, 64);
        
        _topBackView = [[TopBackView alloc] initWithFrame:frame
                                                    title:@"上传带伤照片"];
        
        
        _topBackView.rightButton.hidden = YES;
        
        [_topBackView.leftButton addTarget:self
                                    action:@selector(backAction)
                          forControlEvents:UIControlEventTouchUpInside];
    }
    return _topBackView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self.view addSubview:self.topBackView];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.postArr = [NSMutableArray array];
    
    [self initTopView];
}

- (void)initTopView{
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64)];
    [self.view addSubview:self.scrollView];
    self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 1);
    self.scrollView.backgroundColor = GrayColor;
    
    
    UIView *firstV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    firstV.backgroundColor = WhiteColor;
    [self.scrollView addSubview:firstV];
    
    
    UILabel *orderL = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 100 * kWidth, cellHeight)];
    orderL.text = @"带伤照片";
    orderL.textColor = littleBlackColor;
    orderL.font = Font(14);
    [firstV addSubview:orderL];
    
    [self initMiddleView];
    

}

- (void)initMiddleView{
    UIView *secondV = [[UIView alloc]initWithFrame:CGRectMake(0, cellHeight + 5* kHeight, screenWidth, 130 * kHeight)];
    secondV.backgroundColor = WhiteColor;
    [self.scrollView addSubview:secondV];
    
    
    UIImageView *imageV = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"noPicture"]];
    imageV.frame = CGRectMake((screenWidth - 40)/2.0, 35 * kHeight, 40, 35);
    [secondV addSubview:imageV];
    
    UILabel *firstL = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(imageV.frame) , screenWidth, 40 * kHeight)];
    firstL.text = @"您还没有提交带伤照片";
    firstL.textColor = carScrollColor;
    firstL.font = Font(20);
    firstL.textAlignment = NSTextAlignmentCenter;
    [secondV addSubview:firstL];
    
    UILabel *secondL = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(firstL.frame) , screenWidth, 20 * kHeight)];
    secondL.text = @"请到光线良好的地方拍照或者选择清晰的照片";
    secondL.font = Font(14);
    [secondV addSubview:secondL];
    secondL.textColor = carScrollColor;
    secondL.textAlignment = NSTextAlignmentCenter;
    
    [self initCollectionView];

}

- (void)initCollectionView{
    
    __weak typeof(self) weakSelf = self;
    self.cynamicV = [[DynamicCollectionView alloc]initWithFrame:CGRectMake(0, 135 * kHeight + cellHeight, screenWidth, 200 * kHeight)];
    self.cynamicV.backgroundColor = WhiteColor;
    
    
    [self.scrollView addSubview:self.cynamicV];
    
    self.cynamicV.callBack = ^(NSMutableArray *arr){
        
        weakSelf.postArr = arr;
                
    };
    
    [self initConfirmButtonWithView:weakSelf.cynamicV];

}

- (void)initConfirmButtonWithView:(UIView *)view{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(18 , CGRectGetMaxY(view.frame) + 30, screenWidth - 36, Button_height);
    button.backgroundColor = YellowColor;
    [button setTitle:@"确认提交" forState:UIControlStateNormal];
    [button setTitleColor:WhiteColor forState:UIControlStateNormal];
    [button addTarget:self action:@selector(confirmButton) forControlEvents:UIControlEventTouchUpInside];
    button.layer.cornerRadius = 5;
    button.tag = 400;
    
    //    self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(button.frame));
    
    [self.scrollView addSubview:button];
}

- (void)confirmButton{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"是否确认提交" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self postPicture];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:action1];
    [alert addAction:action2];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)postPicture{
//    Common *c = [[Common alloc]init];
//    NSString *urlString = [NSString stringWithFormat:@"%@attach/modify",Main_interface];
    
    if (self.postArr.count < 1)
    {
        [WKProgressHUD popMessage:@"请先上传照片" inView:self.view duration:1.5 animated:YES];
    }
    else
    {
    
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:self.postArr
                                                            options:NSJSONWritingPrettyPrinted
                                                              error:nil];
            
        NSString * jsonString = [[NSString alloc] initWithData:jsonData
                                                     encoding:NSUTF8StringEncoding];
        
        
        NSDictionary *dic = @{@"waybillId":self.orderId,@"picKeys":jsonString};
        
        [WKProgressHUD showInView:self.view withText:@"" animated:YES];
        
        [[Common shared] afPostRequestWithUrlString:attach_modify parms:dic finishedBlock:^(id responseObj) {
            
            NSDictionary *dictionay = [NSJSONSerialization JSONObjectWithData:responseObj
                                                                      options:NSJSONReadingMutableLeaves
                                                                        error:nil];
 
            
            if ([dictionay[@"success"] boolValue])
            {
                
          
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    [WKProgressHUD dismissInView:self.view animated:YES];
                    
                    [self backAction];
                    
                });
                
                
            }
            else
            {
                [WKProgressHUD dismissInView:self.view animated:YES];
            }
            
        } failedBlock:^(NSString *errorMsg) {
            NSLog(@"%@",errorMsg);
            [WKProgressHUD dismissInView:self.view animated:YES];
        }];

    }
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    
    [rootVC setTabBarHidden:YES];
}

- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

 

@end
