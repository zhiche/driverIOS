//
//  ScrollImageViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/21.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ScrollImageViewController.h"

@interface ScrollImageViewController ()<UIScrollViewDelegate>
{
    UIScrollView*Scroll;
    UILabel * Number;
    NSMutableArray * pageNumberArr;
    int number;
}
@end


@implementation ScrollImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatScroll];
}

- (void)creatScroll{
    
    pageNumberArr = [[NSMutableArray alloc]init];
    
    Scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,Main_Width,Main_height )];
    Scroll.backgroundColor = [UIColor blackColor];
    Scroll.contentSize = CGSizeMake(Main_Width*self.imageArr.count,screenHeight);
    Scroll.contentOffset = CGPointMake(self.indexNumber*Main_Width,0);
    Scroll.bounces = NO;
    Scroll.showsHorizontalScrollIndicator = NO;
    Scroll.showsVerticalScrollIndicator = NO;
    Scroll.pagingEnabled = YES;
    Scroll.delegate = self;
    
    
    
    for (int i = 0; i<self.imageArr.count; i++) {
        UIImageView * imgV =[[UIImageView alloc]initWithFrame:CGRectMake(screenWidth * i, 0, screenWidth, screenHeight)];
        imgV.contentMode = UIViewContentModeScaleAspectFit;
        
        __block UIActivityIndicatorView *activityIndicator;
        
        [imgV sd_setImageWithURL:self.imageArr[i] placeholderImage:nil options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
            if (!activityIndicator)
            {
                activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
                activityIndicator.center = imgV.center;
                //把更新UI放到主线程
                dispatch_async(dispatch_get_main_queue(), ^{
                    [imgV addSubview:activityIndicator];
                });
                
                [activityIndicator startAnimating];
            }
            
        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            if (error) {
            
                imgV.image = [UIImage imageNamed:@"bad_picture"];
            }
            
            [activityIndicator stopAnimating];
            
        }];
        
        [Scroll addSubview:imgV];
    
     }

        UITapGestureRecognizer * single = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction)];
        //设置手指根数
        single.numberOfTouchesRequired = 1;
        //设置点击次数
        single.numberOfTapsRequired = 1;
        
        
    [Scroll addGestureRecognizer:single];
    
    
    
    self.automaticallyAdjustsScrollViewInsets =NO;
    [self.view addSubview:Scroll];
    
    
    UIButton * backBtn =[UIButton buttonWithType:UIButtonTypeCustom];

    [backBtn setImage:[UIImage imageNamed:@"picture_back"] forState:UIControlStateNormal];
    backBtn.frame = CGRectMake(0, 20, 50, 50);
    [backBtn addTarget:self action:@selector(pressBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    

    
    Number = [[UILabel alloc]initWithFrame:CGRectMake(0, screenHeight - 60, screenWidth, 20)];
    
    Number.text = [NSString stringWithFormat:@"1/%d",(int)self.imageArr.count];
    
    Number.textColor = [UIColor whiteColor];
    [self.view addSubview:Number];
    Number.textAlignment = NSTextAlignmentCenter;

   
}

- (void)tapAction{
    [self.navigationController popViewControllerAnimated:YES];

   
}

- (void)pressBtn{
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
 
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGPoint point = scrollView.contentOffset;
    
    Number.text = [NSString stringWithFormat:@"%.0f/%d",point.x/self.view.frame.size.width + 1,(int)self.imageArr.count];
    
}


@end
