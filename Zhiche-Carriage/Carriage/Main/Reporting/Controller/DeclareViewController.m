//
//  DeclareViewController.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/14.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "DeclareViewController.h"
#import "RootViewController.h"
#import "TopBackView.h"
#import "DynamicCollectionView.h"



@interface DeclareViewController ()<UITextViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UILabel *orderLabel;
@property (nonatomic, strong) UILabel *placeholdLabel;
@property (nonatomic, strong) NSMutableArray *postArr;
@property (nonatomic, assign) BOOL success;

@property (nonatomic, strong) TopBackView *topBackView;
@end

@implementation DeclareViewController
#pragma mark -
#pragma mark - Lazy 
- (TopBackView *)topBackView {
    if (!_topBackView) {
       _topBackView = [[TopBackView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)
                                                    title:@"事故申报"];
        
        _topBackView.rightButton.hidden = YES;
        
        [_topBackView.leftButton addTarget:self
                                    action:@selector(backAction)
                          forControlEvents:UIControlEventTouchUpInside];
    }
    return _topBackView;
}
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64 )];
        _scrollView.backgroundColor = GrayColor;
        _scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
       _scrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 1);
        
    }
    return _scrollView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.topBackView];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    [self.view addSubview:self.scrollView];
    
    self.postArr = @[].mutableCopy;
    
    _success = NO;
    
    [self initSubViews];
    
}



- (void)initSubViews{
    

    UIView *firstView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 50 * kHeight)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(18, 0, 70, 50 * kHeight)];
    [firstView addSubview:label];
    label.text = @"运单号:";
    label.font = Font(12);
    label.textColor = littleBlackColor;
    
    self.orderLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(label.frame), 0, 250, 50 * kHeight)];
    self.orderLabel.text = self.orderString;
    self.orderLabel.font = Font(12);
    self.orderLabel.textColor = littleBlackColor;
    
    [firstView addSubview:self.orderLabel];
    firstView.backgroundColor = WhiteColor;
    [self.scrollView addSubview:firstView];

    
    [self initSecondView];
    
}

- (void)initSecondView{
    UIView *secondView = [[UIView alloc]initWithFrame:CGRectMake(0, 55 * kHeight, screenWidth, 170 * kHeight)];
    secondView.backgroundColor = WhiteColor;
    [self.scrollView addSubview:secondView];
    
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(10 * kHeight, 26 * kHeight, screenWidth - 20 * kWidth, 120 * kHeight)];
    [secondView addSubview:self.textView];
    self.textView.layer.cornerRadius = 5;
    self.textView.layer.borderColor = LineGrayColor.CGColor;
    self.textView.layer.borderWidth = 0.5;
    self.textView.delegate = self;
    self.textView.font = Font(12);
    self.textView.textColor = littleBlackColor;
    
    self.placeholdLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 10, 200, 30)];
    self.placeholdLabel.font = Font(12);
    self.placeholdLabel.textColor = Color_RGB(181, 181, 184, 1);
    self.placeholdLabel.text = @"请输入时间、地点、事故描述";
    [self.placeholdLabel sizeToFit];
    self.placeholdLabel.tag = 506;
    [self.textView addSubview:self.placeholdLabel];


    self.textView.returnKeyType = UIReturnKeyDone;
    
    
    [self initCollectionView];

}

- (void)initCollectionView{
    DynamicCollectionView *cynamicV = [[DynamicCollectionView alloc]initWithFrame:CGRectMake(0, 225 * kHeight, screenWidth, 200 * kHeight)];
    cynamicV.backgroundColor = WhiteColor;
    
    
    cynamicV.callBack = ^(NSMutableArray *arr){
      
        self.postArr = arr;
        
    };
    
    [self.scrollView addSubview:cynamicV];
    
    [self initConfirmButton];
}

- (void)initConfirmButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(18 , 425 * kHeight + 30, screenWidth - 36, Button_height);
    button.backgroundColor = YellowColor;
    [button setTitle:@"确认提交" forState:UIControlStateNormal];
    [button setTitleColor:WhiteColor forState:UIControlStateNormal];
    [button addTarget:self action:@selector(confirmButton) forControlEvents:UIControlEventTouchUpInside];
    button.layer.cornerRadius = 5;
    
//    self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(button.frame));
    
    [self.scrollView addSubview:button];
}

- (void)confirmButton {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"是否确认提交" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self postPicture];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:action1];
    [alert addAction:action2];
    
    [self presentViewController:alert animated:YES completion:nil];

}


#pragma mark 确认提交
- (void)postPicture{
 
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:self.postArr options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *dic = @{@"waybillId":self.orderId,@"description":self.textView.text,@"picKeys":jsonString};
 
    [[Common shared] afPostRequestWithUrlString:accident_modify parms:dic finishedBlock:^(id responseObj) {
        NSDictionary *dictionay = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        
        [WKProgressHUD popMessage:dictionay[@"message"] inView:self.view duration:1.5 animated:YES];
        
        if ([dictionay[@"success"] boolValue]) {
            
            //延迟执行
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                _success = YES;
                [self backAction];
                
            });
            
        }
        
    } failedBlock:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}




#pragma mark -
#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView
shouldChangeTextInRange:(NSRange)range
  replacementText:(NSString *)text {
    
     if ([@"\n" isEqualToString:text])
     {
         [textView resignFirstResponder];
     
         return  NO;
     }
     else
     {
         return YES;
     }
 }

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self.placeholdLabel removeFromSuperview];
}
 
- (void)textViewDidEndEditing:(UITextView *)textView {
 
     [UIView animateWithDuration:0.35 animations:^{
  
         if (textView.text.length == 0)
         {
             [self.textView addSubview:self.placeholdLabel];
         }
     
     }];
 }
 
 
 

#pragma mark -
#pragma mark - 导航栏
- (void)backAction{
    if (self.isSuccess) {
        self.isSuccess(_success);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}



@end
