//
//  ScrollImageViewController.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/21.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollImageViewController : UIViewController

@property (nonatomic, strong) NSMutableArray * imageArr;

@property (nonatomic, assign) int indexNumber;

@end
