//
//  AccidentsList.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/17.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccidentsList : NSObject

@property (nonatomic, copy) NSString *arrivalAddress;
@property (nonatomic, copy) NSString *arrivalCity;
@property (nonatomic, copy) NSString *arrivalCounty;
@property (nonatomic, copy) NSString *arrivalProvince;
@property (nonatomic, copy) NSString *attachVos;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *departAddr;
@property (nonatomic, copy) NSString *departCity;
@property (nonatomic, copy) NSString *departCounty;
@property (nonatomic, copy) NSString *departProvince;
@property (nonatomic, copy) NSString *a_description;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *orderCode;
@property (nonatomic, copy) NSString *processResult;
@property (nonatomic, copy) NSString *showDate;
@property (nonatomic, copy) NSString *status;;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *vehicleTypeName;
@property (nonatomic, copy) NSString *waybillId;
@property (nonatomic, copy) NSString *waybillStatus;


@end
