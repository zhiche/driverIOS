//
//  DriverScoreModel.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DriverScoreModel : NSObject
@property(nonatomic,copy) NSString *userId;
@property(nonatomic,strong) NSString *userName;
@property(nonatomic,strong) NSString *UserType;
@property(nonatomic,strong) NSString *score;
@property(nonatomic,strong) NSString *businessType;
@property(nonatomic,strong) NSString *businessTypeText;
@property(nonatomic,strong) NSString *businessId;
@property(nonatomic,strong) NSString *createTime;
@property(nonatomic,strong) NSString *directionText;

@property (nonatomic,strong) NSString *contact;

@end
