//
//  TradeModel.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TradeModel : NSObject


@property (nonatomic,copy) NSString *accountId;
@property (nonatomic,copy) NSString *accountName;
@property (nonatomic,copy) NSString *userTypes;
@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,copy) NSString *orderPrice;
@property (nonatomic,copy) NSString *orderTotal;
@property (nonatomic,copy) NSString *feetype;
@property (nonatomic,copy) NSString *bno;
@property (nonatomic,copy) NSString *createTime;
@property (nonatomic,copy) NSString *updateTime;
@property (nonatomic,copy) NSString *feeTypeDesc;
@property (nonatomic,copy) NSString *strOrderTotal;
@property (nonatomic,copy) NSString *ID;
@property (nonatomic,copy) NSString *orderCode;


//
//batchNo = "<null>";
//billId = "<null>";
//createTime = "2016-07-21 08:47:14";
//id = 26;
//isRt = "<null>";
//orderActPay = "0.2";
//orderFee = 1;
//orderId = 13;
//payType = 10;
//updateTime = "2016-07-21 08:47:14";
//userId = 14;
//userName = "\U674e\U56db";
//userType = 10;



@end
