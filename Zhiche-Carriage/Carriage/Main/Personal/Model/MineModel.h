//
//  MineModel.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/18.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MineModel : NSObject

@property (nonatomic, assign) int ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *birthday;
@property (nonatomic, copy) NSString *realName;
@property (nonatomic, copy) NSString *openId;
@property (nonatomic, copy) NSString *enable;
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSString *picKey;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, assign) int userTypes;
@property (nonatomic, assign) int userStatus;
@property (nonatomic, copy) NSString *driverStatus;
@property (nonatomic, copy) NSString *companycode;
@property (nonatomic, copy) NSString *invitedcode;
@property (nonatomic, assign) int userLevel;
@property (nonatomic, copy) NSString *levelName;
@property (nonatomic, copy) NSString *scores;
@property (nonatomic, copy) NSString *companyName;

 
@end
