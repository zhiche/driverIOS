//
//  TradeModel.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "TradeModel.h"

@implementation TradeModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key  {
    if([key isEqualToString:@"id"]) {
        self.ID = value;
    }
    
    if ([key isEqualToString:@"userType"]) {
        self.userTypes = value;
    }
    
}

@end
