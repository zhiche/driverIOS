//
//  ScoreCell.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DriverScoreModel.h"

@interface ScoreCell : UITableViewCell

//@property (nonatomic,strong) UIImageView *backImg;
//@property (nonatomic,strong) UILabel *numberLabel;
//@property (nonatomic,strong) UILabel *firstLabel;
//@property (nonatomic,strong) UILabel *secondLabel;
//@property (nonatomic,strong) UIImageView *img;


@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UILabel *scoreLabel;

@property (nonatomic,strong) DriverScoreModel *model;

@end
