//
//  MineCell.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/6/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineModel.h"

@interface MineCell : UITableViewCell

@property (nonatomic, strong) MineModel* mineModel;

@property (nonatomic,strong) UIImageView *ImageView;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *phoneLabel;
@property (nonatomic,strong) UILabel *levelLabel;

@end
