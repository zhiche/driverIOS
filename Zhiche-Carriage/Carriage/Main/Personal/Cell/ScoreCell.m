//
//  ScoreCell.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ScoreCell.h"
#import <Masonry.h>

@implementation ScoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
    __weak typeof(self) weakSelf = self;
    
    self.backgroundColor = WhiteColor;
    
    
    self.titleLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(18);
        make.top.mas_equalTo(5);
        make.size.mas_equalTo(CGSizeMake(200, cellHeight/2.0 + 5));
        
    }];
    
    self.titleLabel.textColor = littleBlackColor;
    self.titleLabel.font = Font(13);
    
    self.dateLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(weakSelf.titleLabel.mas_bottom).offset(-5);
        make.left.equalTo(weakSelf.titleLabel);
        make.size.mas_equalTo(CGSizeMake(200, cellHeight/2.0 ));
        
    }];
    
    self.dateLabel.textColor = carScrollColor;
    self.dateLabel.font = Font(12);
    
    self.scoreLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.scoreLabel];
    [self.scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-18);
        make.centerY.equalTo(weakSelf);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        
    }];
    
    self.scoreLabel.textColor = carScrollColor;
    self.scoreLabel.textAlignment = NSTextAlignmentRight;
    
    /*
    self.backImg = [[UIImageView alloc]init];
    [self.contentView addSubview:self.backImg];
    [self.backImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(27);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 54, 41));
        
    }];
    
    self.numberLabel = [[UILabel alloc]init];
    [self.backImg addSubview:self.numberLabel];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.and.bottom.and.top.mas_equalTo(0);
        make.width.mas_equalTo(60 * kWidth);
        
    }];
    self.numberLabel.textColor = YellowColor;
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.font = Font(15);
    
    
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = YellowColor;
    [self.backImg addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.equalTo(weakSelf.numberLabel);
        make.left.mas_equalTo(weakSelf.numberLabel.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(1, 23));
    }];
    
    self.firstLabel = [[UILabel alloc]init];
    [self.backImg addSubview:self.firstLabel];
    [self.firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(label.mas_right).offset(10);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(5);
//        make.height.mas_equalTo(13);
//        make.bottom.mas_equalTo(weakSelf.secondLabel.mas_top).offset(-6);
        
    }];
    self.firstLabel.textColor = BlackTitleColor;
    self.firstLabel.font = Font(11);
    
    
    
    
    
    self.secondLabel = [[UILabel alloc]init];
    [self.backImg addSubview:self.secondLabel];
    [self.secondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(weakSelf.firstLabel.mas_bottom).offset(0);
        make.left.equalTo(weakSelf.firstLabel);
        make.bottom.and.right.mas_equalTo(-5);
        
    }];
    
    self.secondLabel.textColor = littleBlackColor;
    self.secondLabel.font = Font(9);
    
    self.img = [[UIImageView alloc]init];
    [self.backImg addSubview:self.img];
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.right.mas_equalTo(-10);
        make.centerY.equalTo(weakSelf.numberLabel);
        make.size.mas_equalTo(CGSizeMake(6.5, 11));
        
    }];
     
     */

}

-(void)setModel:(DriverScoreModel *)model
{
    if (model != _model) {
        //赋值
        
        self.titleLabel.text = [NSString stringWithFormat:@"%@",model.businessTypeText];
        self.dateLabel.text = [NSString stringWithFormat:@"%@",model.createTime];
        
        if ([model.directionText isEqualToString:@"收入"]) {
            self.scoreLabel.text = [NSString stringWithFormat:@"+%.1f",[model.score floatValue]];
        } else {
            self.scoreLabel.text = [NSString stringWithFormat:@"-%.1f",[model.score floatValue]];

        }
        
        
    }
}






- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
