//
//  MineCell.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/6/15.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "MineCell.h"

@implementation MineCell

- (void)setMineModel:(MineModel *)mineModel {
    _mineModel = mineModel;
    
    NSString *string = [NSString stringWithFormat:@"%@",mineModel.driverStatus];
  
    if ([string isEqualToString:@"0"]) {
        
        self.nameLabel.text = @"未认证";
        self.nameLabel.textColor = littleBlackColor;
        self.nameLabel.layer.borderColor = littleBlackColor.CGColor;
        self.nameLabel.layer.borderWidth = 0.5;
    }
    
    if ([string isEqualToString:@"10"]) {
        
        self.nameLabel.text = @"认证中";
        self.nameLabel.textColor = Color_RGB(38, 206, 121, 1);
        self.nameLabel.layer.borderColor = Color_RGB(38, 206, 121, 1).CGColor;
        self.nameLabel.layer.borderWidth = 0.5;
        
    }
    
    if ([string isEqualToString:@"20"]) {
        
        self.nameLabel.text = @"认证未通过";
        self.nameLabel.textColor = Color_RGB(154, 154, 154, 1);
        self.nameLabel.layer.borderColor = Color_RGB(154, 154, 154, 1).CGColor;
        self.nameLabel.layer.borderWidth = 0.5;
        
    }
    
    if ([string isEqualToString:@"30"]) {
        
        self.nameLabel.text = @"已认证";
        self.nameLabel.textColor = Color_RGB(235, 130, 19, 1);
        self.nameLabel.layer.borderColor = Color_RGB(235, 130, 19, 1).CGColor;
        self.nameLabel.layer.borderWidth = 0.5;
        
    }
    
    
    self.phoneLabel.text = [NSString stringWithFormat:@"%@", mineModel.phone];
    
    

    
  
    
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubviews];
    }
    
    return self;
}

-(void)initSubviews
{
    
    self.ImageView = [[UIImageView alloc]initWithFrame:CGRectMake(14, 14 * kWidth, 50 * kWidth,50 * kHeight)];
    self.ImageView.layer.cornerRadius = 50 * kWidth /2.0;
    self.ImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.ImageView];
    
    
    self.phoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.ImageView.frame) + 13.5, CGRectGetMinY(self.ImageView.frame) + 7.5 * kHeight, 200, 15 * kHeight)];
    [self.contentView addSubview:self.phoneLabel];
    self.phoneLabel.font = Font(14);
    self.phoneLabel.textColor = littleBlackColor;
    self.phoneLabel.text = @"";

    
//    self.levelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.ImageView.frame) + 14.5, CGRectGetMaxY(self.phoneLabel.frame) + 7 * kHeight, 20 * kWidth, 13 * kHeight)];
//    self.levelLabel.font = Font(10);
//    self.levelLabel.textColor = littleBlackColor;
//    [self.contentView addSubview:self.levelLabel];
    
    self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.phoneLabel.frame) , CGRectGetMaxY(self.phoneLabel.frame) + 7 * kHeight, 50, 13 * kHeight)];
    
    [self.contentView addSubview:self.nameLabel];
    self.nameLabel.font = Font(10);
    self.nameLabel.textColor = littleBlackColor;
    self.nameLabel.text = @"";
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
//        self.nameLabel.backgroundColor = [UIColor redColor];
    
  
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
