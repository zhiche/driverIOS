//
//  MineDetailVC.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "MineDetailVC.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import "LoginViewController.h"


#import "QiniuSDK.h"
#import "ZHPickView.h"
#import <AssetsLibrary/AssetsLibrary.h>//相册权限
#import <AVFoundation/AVCaptureDevice.h>//相机权限
#import <AVFoundation/AVMediaFormat.h>

@interface MineDetailVC ()
<
    UITextFieldDelegate,
    UINavigationControllerDelegate,
    UIImagePickerControllerDelegate,
    UIGestureRecognizerDelegate,
    ZHPickViewDelegate
>
///电话号码(不能修改)
@property (nonatomic, strong) UITextField *phoneNum_tf;

@property (nonatomic, strong) UIScrollView *scrollView;


@property (nonatomic, strong) UIImageView *imagev;

@property (nonatomic, strong) ZHPickView *pickV;


@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *genderTextField;
@property (nonatomic, strong) UIButton *dateButton;
@property (nonatomic, strong) NSMutableArray *textFieldArray;
@property (nonatomic, strong) UIView *dateView;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, copy) NSString *valueString;

@property (nonatomic, assign) int currentTextFieldIndex;

@property (nonatomic, strong) NSMutableDictionary *dataDictionary;


@property (nonatomic,copy) NSString *token;
@property (nonatomic,copy) NSString *qiNiuKey;

@property (nonatomic, strong) UIImageView *imageV1;
@property (nonatomic, strong) UIImageView *imageV2;
@property (nonatomic, copy) NSString *sexString;


@property (nonatomic, strong) TopBackView *topBackView;
@end

@implementation MineDetailVC

#pragma mark -
#pragma mark - Lazy
- (TopBackView *)topBackView {
    if (!_topBackView) {
        
        CGRect frame = CGRectMake(0, 0, screenWidth, 64);
        
        _topBackView = [[TopBackView alloc]initWithFrame:frame title:@"账户管理"];
        
        [_topBackView.rightButton setTitle:@"保存" forState:UIControlStateNormal];
        
        _topBackView.rightButton.titleLabel.font = Font(14);
       
        [_topBackView.rightButton setTitleColor:WhiteColor forState:UIControlStateNormal];
        
    
        [_topBackView.rightButton addTarget:self
                                     action:@selector(naviRightItemClick)
                           forControlEvents:UIControlEventTouchUpInside];
        
        
        [_topBackView.leftButton addTarget:self
                                    action:@selector(naviLeftItemClick)
                          forControlEvents:UIControlEventTouchUpInside];

    }
    return _topBackView;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
       _scrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 1);
    }
    return _scrollView;
}



#pragma mark -
#pragma mark - 初始化视图
- (void)viewDidLoad {
    [super viewDidLoad];
    
    ///设置本地数据配置
    [self setUpLocalData];
    
    ///设置UI
    [self setUpUI];
   
    ///获取服务器数据
    [self getServerData];
 
    [self initSubviews];
    

    
    ///添加观察者
    [self addMangerObserver];
}

#pragma mark - 设置UI
- (void)setUpUI {
 
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.topBackView];
    
    [self.view addSubview:self.scrollView];
}

#pragma mark - 设置本地数据配置
- (void)setUpLocalData {
    self.qiNiuKey = @"";
    self.valueString = @"";
    self.textFieldArray = [NSMutableArray array];
    self.dataDictionary = [NSMutableDictionary dictionary];
    
}

- (void)initSubviews{
    
    
    /// 头像视图
    UIView *pictureView = [self backViewWithFrame:CGRectMake(0, 0, screenWidth, 78) andString:@"头像"];
 
    self.imagev = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth - 9 - 10 - 14 - 50, 14, 50, 50)];
    self.imagev.layer.cornerRadius = 25;
    self.imagev.layer.masksToBounds = YES;
    [pictureView addSubview:self.imagev];
    
    [self.scrollView addSubview:pictureView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(photoOptionsCards)];
    [pictureView addGestureRecognizer:tap];
    
    
    UIImageView *arrowImg = [[UIImageView alloc]initWithFrame:CGRectMake( screenWidth - 10 - 14, 32, 10, 14)];
    arrowImg.image = [UIImage imageNamed:@"personal_arrow"];
    [pictureView addSubview:arrowImg];
    
    
    // 用户名
    
    UIView *nameView = [self backViewWithFrame:CGRectMake(0, CGRectGetMaxY(pictureView.frame) + 10, screenWidth, cellHeight) andString:@"手机号"];
    [self.scrollView addSubview:nameView];
    self.scrollView.backgroundColor = GrayColor;
    
    self.phoneNum_tf = [[UITextField alloc]initWithFrame:CGRectMake(screenWidth -  14 - 250, 5, 250, cellHeight)];
    self.phoneNum_tf.delegate = self;
    self.phoneNum_tf.font = Font(12);
    self.phoneNum_tf.userInteractionEnabled = NO;
    self.phoneNum_tf.textAlignment = NSTextAlignmentRight;
    [nameView addSubview:self.phoneNum_tf];
    self.phoneNum_tf.textColor = fontGrayColor;
    

    //性别

    UIView *genderView = [self backViewWithFrame:CGRectMake(0, CGRectGetMaxY(nameView.frame), screenWidth, cellHeight) andString:@"性别"];
    [self.scrollView addSubview:genderView];
    
    UIButton *buttonF = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonF.frame = CGRectMake(screenWidth - 40 - 15, 0, 40, cellHeight);
    [genderView addSubview:buttonF];
    [buttonF addTarget:self action:@selector(buttonF) forControlEvents:UIControlEventTouchUpInside];
    
    self.imageV2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sex_unselect"]];
    self.imageV2.frame = CGRectMake(0, CGRectGetMidY(buttonF.frame) - 6.5, 13, 13);
    [buttonF addSubview:self.imageV2];
    
    UILabel *labelF = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imageV2.frame) + 8, 0, 20, cellHeight)];
    labelF.text = @"女";
    labelF.textColor = fontGrayColor;
    labelF.font = Font(12);
    [buttonF addSubview:labelF];
    
    UIButton *buttonM = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonM.frame = CGRectMake(screenWidth - 40 - 15 - 22 - 40, 0, 40, cellHeight);
    [genderView addSubview:buttonM];
    [buttonM addTarget:self action:@selector(buttonM) forControlEvents:UIControlEventTouchUpInside];
    
    self.imageV1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sex_unselect"]];
    self.imageV1.frame = CGRectMake(0, CGRectGetMidY(buttonF.frame) - 6.5, 13, 13);
    [buttonM addSubview:self.imageV1];
    
    UILabel *labelM = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imageV1.frame) + 8, 0, 20, cellHeight)];
    labelM.text = @"男";
    labelM.textColor = fontGrayColor;
    labelM.font = Font(12);
    [buttonM addSubview:labelM];
    
    
    //出生日期
    UIView *dateView = [self backViewWithFrame:CGRectMake(0, CGRectGetMaxY(genderView.frame), screenWidth, cellHeight) andString:@"出生日期"];
    [self.scrollView addSubview:dateView];
    
    self.dateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _dateButton.frame = CGRectMake(screenWidth - 268, 0, 250, cellHeight);
    _dateButton.layer.cornerRadius = 5;
    self.dateButton.titleLabel.font = Font(12);
    self.dateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    [_dateButton addTarget:self action:@selector(dateButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [_dateButton setTintColor:fontGrayColor];
    
    
    [dateView addSubview:self.dateButton];
 
    UIButton *logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    logoutButton.backgroundColor = WhiteColor;
    logoutButton.frame = CGRectMake(0, CGRectGetMaxY(dateView.frame) + 10, screenWidth - 0, cellHeight);
    [logoutButton setTitle:@"退出登录" forState:UIControlStateNormal];
    //    logoutButton.layer.cornerRadius = 5;
    logoutButton.titleLabel.font = Font(13);
    [logoutButton addTarget:self action:@selector(logoutButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [logoutButton setTitleColor:littleBlackColor forState:UIControlStateNormal];
    
    
    [self.scrollView addSubview:logoutButton];
    

}

#pragma mark - 获取服务器数据
- (void)getServerData{
    
    [WKProgressHUD showInView:self.view withText:@"" animated:YES];
    
    __weak typeof(self) weakSelf = self;
    
    [Common requestWithUrlString:user_info contentType:application_json errorShow:YES finished:^(id responseObj) {
        
       [WKProgressHUD dismissInView:weakSelf.view animated:YES];

        if ([responseObj[@"success"] boolValue]) {
            
            self.dataDictionary = responseObj[@"data"];
            //赋值
            [self getValueString];
        }
        else
        {
            [WKProgressHUD popMessage:responseObj[@"message"] inView:weakSelf.view duration:1.5 animated:YES];

        }
        
    } failed:^(NSString *errorMsg) {
        
        [WKProgressHUD dismissInView:weakSelf.view animated:YES];
        
        [WKProgressHUD popMessage:@"请求超时" inView:weakSelf.view duration:1.5 animated:YES];
        
        NSLog(@"%@",errorMsg);
    }];
}

- (void)getValueString{
    if ([self.dataDictionary[@"pic"] isEqual:[NSNull null]])
    {
        self.imagev.image = [UIImage imageNamed:@"home_portrait"];
    }
    else
    {
        //拼接图片url
        NSString *urlString = [NSString stringWithFormat:@"%@",self.dataDictionary[@"pic"]];
        
        [self.imagev sd_setImageWithURL:[NSURL URLWithString:urlString]
                       placeholderImage:[UIImage imageNamed:@"home_portrait"]];
    }
    if ([self.dataDictionary[@"gender"] isEqual:[NSNull null]])
    {
        
        self.imageV1.image = [UIImage imageNamed:@"sex_unselect"];
        self.imageV2.image = [UIImage imageNamed:@"sex_unselect"];
        _sexString = @"";

    }
    else
    {
        
        if ([self.dataDictionary[@"gender"] isEqualToString:@"M"])
        {
            self.imageV2.image = [UIImage imageNamed:@"sex_unselect"];
            self.imageV1.image = [UIImage imageNamed:@"sex_select"];
            
            _sexString = @"男";
            
        }
        else if([self.dataDictionary[@"gender"] isEqualToString:@"F"])
        {
            self.imageV1.image = [UIImage imageNamed:@"sex_unselect"];
            self.imageV2.image = [UIImage imageNamed:@"sex_select"];
            _sexString = @"女";
            
        }
    }

    self.phoneNum_tf.text = [NSString stringWithFormat:@"%@",self.dataDictionary[@"phone"]];
    
    NSString *birthday ;
    if ([self.dataDictionary[@"birthday"] isEqual:[NSNull null]])
    {
        birthday = @"";
    }
    else
    {
        birthday = [NSString stringWithFormat:@"%@",self.dataDictionary[@"birthday"]];
        
    }
    [self.dateButton setTitle:birthday forState:UIControlStateNormal];
    [self.dateButton setTitleColor:fontGrayColor forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    
    [rootVC setTabBarHidden:YES];
}



//性别
- (void)buttonF{
    self.imageV2.image = [UIImage imageNamed:@"sex_select"];
    self.imageV1.image = [UIImage imageNamed:@"sex_unselect"];
    _sexString = @"女";
}

- (void)buttonM{
    self.imageV1.image = [UIImage imageNamed:@"sex_select"];
    self.imageV2.image = [UIImage imageNamed:@"sex_unselect"];
    _sexString = @"男";
    
}

- (void)sexButton{
    _currentTextFieldIndex = 2;
    
    [self.view endEditing:YES];
    //    [self.dateView removeFromSuperview];
    [self.pickV removeFromSuperview];
    
    _pickV=[[ZHPickView alloc] initPickviewWithPlistName:@"一组数据" isHaveNavControler:NO];
    
    _pickV.delegate=self;
    
    [_pickV show];
}




#pragma mark 退出登录
- (void)logoutButtonAction{
   
    [WKProgressHUD showInView:self.view withText:@"" animated:YES];
    
    __weak typeof(self) weakSelf = self;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [WKProgressHUD dismissInView:weakSelf.view animated:YES];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        LoginViewController *loginVC = [[LoginViewController alloc]init];
        UINavigationController *loginNC = [[UINavigationController alloc]initWithRootViewController:loginVC];
        [weakSelf presentViewController:loginNC animated:YES completion:nil];
        
        [userDefaults setObject:nil forKey:@"login"];
        [userDefaults setObject:nil forKey:@"phone"];
        [userDefaults setObject:nil forKey:login_token];
        [userDefaults setObject:nil forKey:@"userType"];
        
    });
    
  

}





#pragma mark 从七牛获取Token
- (void)getTokenFromQN{
    
    [Common requestWithUrlString:picture_qiniu_token contentType:application_json  errorShow:YES finished:^(id responseObj) {
        if ([responseObj[@"success"] boolValue]) {
            
            self.token = responseObj[@"data"];
            [self uploadImageToQNWithData:[self getImageWith:self.imagev.image]];
        }
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
    
}

- (void)uploadImageToQNWithData:(NSData *)data{
    
    QNUploadManager *upManager = [[QNUploadManager alloc]init];
    
    QNUploadOption *uploadOption = [[QNUploadOption alloc]initWithMime:nil progressHandler:^(NSString *key, float percent) {
        
        
    } params:nil checkCrc:NO cancellationSignal:nil];
    
    [upManager putData:data key:nil token:self.token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        
       [WKProgressHUD dismissInView:self.view animated:YES];
        
        self.qiNiuKey = resp[@"key"];
        
        
    } option:uploadOption];
}

- (NSData *)getImageWith:(UIImage *)image{
    NSData *data = nil;
    if (UIImagePNGRepresentation(image)) {
        
        data = UIImageJPEGRepresentation(image, 0.2);
        
        
    } else {
        data = UIImagePNGRepresentation(image);
        
        
    }
    
    return data;
}




#pragma mark -
#pragma mark - 照片选项卡
#pragma mark -
- (void)photoOptionsCards {
    __weak typeof(self) weakSelf = self;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        [weakSelf getPictureFromCamera];
        
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"从相册选取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [weakSelf getPictureFromLibrary];
        
    }];
    
    [alert addAction:action1];
    [alert addAction:action2];
    [alert addAction:action3];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - 拍照
- (void)getPictureFromCamera {
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark  - 相册
- (void)getPictureFromLibrary {
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}


#pragma mark  - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        
        self.imagev.image = image;
        
        [WKProgressHUD showInView:self.view withText:@"" animated:YES];
        
        //上传服务器
        [self getTokenFromQN];
        
    }];
}




#pragma mark - 选择出生日期
- (void)dateButtonAction{
    [self.pickV removeFromSuperview];
    
    _currentTextFieldIndex = 3;
    
    [self.view endEditing:YES];
    
    NSDate *date = [NSDate date];
   
    _pickV = [[ZHPickView alloc] initDatePickWithDate:date
                                       datePickerMode:UIDatePickerModeDate
                                   isHaveNavControler:NO];
    _pickV.delegate = self;
    
    [_pickV show];
}


#pragma mark -
#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
    } completion:^(BOOL finished) {
        
    }];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    //    [self.dateView removeFromSuperview];
    [self.pickV removeFromSuperview];
    
    if (textField == self.phoneNum_tf) {
        _currentTextFieldIndex = 0;
    }
    
    if (textField == self.emailTextField) {
        _currentTextFieldIndex = 1;
    }
    
    if (textField == self.genderTextField) {
        _currentTextFieldIndex = 2;
        [self sexButton];
        
        
    }
    
    
}


#pragma mark - ZhpickVIewDelegate
-(void)toobarDonBtnHaveClick:(ZHPickView *)pickView resultString:(NSString *)resultString{
    
    if (_currentTextFieldIndex == 2) {
        
        self.genderTextField.text=resultString;
        
    } else {
        
        NSString *string = [NSString stringWithFormat:@"%@", resultString];
        NSArray *array = [string componentsSeparatedByString:@" "];
        _valueString = array[0];
        
        [self.dateButton setTitle:_valueString forState:UIControlStateNormal];
    }
    
}

#pragma mark -
#pragma mark - 添加观察者
- (void)addMangerObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popSelf) name:@"dismiss" object:nil];
    
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismiss" object:nil];
}
- (void)popSelf{
    [self .navigationController popViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark - 导航栏点击
#pragma mark -

#pragma mark - 返回
- (void)naviLeftItemClick {
    
    [self.pickV removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - 保存
- (void)naviRightItemClick {
    [self changePersonInfo];
}

#pragma mark - 保存提交(用户修改个人数据)
- (void)changePersonInfo{
   
 
    
    if (self.phoneNum_tf.text.length <= 0 )
    {
        self.phoneNum_tf.text = @"";
    }
    
    
    if (self.emailTextField.text.length <= 0 ) {
        self.emailTextField.text = @"";
    }
    
    
    NSString *genderString = [NSString string];
    if (_sexString >= 0) {
        
        if ([_sexString isEqualToString:@"男"]) {
            genderString = @"M";
        } else if ([_sexString isEqualToString:@"女"]) {
            genderString = @"F";
        }
    } else {
        
        if ([self.dataDictionary[@"gender"] isEqual:[NSNull null]]) {
            genderString = @"";
            
        } else {
            genderString = self.dataDictionary[@"gender"];
            
            
        }
    }
    
    
    if (self.valueString.length <= 0 ) {
        
        if (self.dateButton.titleLabel.text.length <= 0) {
            self.valueString = @"";
        } else {
            self.valueString = self.dateButton.titleLabel.text;
        }
    }
    
    
    if (self.qiNiuKey.length > 0) {
        
    } else {
        
        if ( [[NSString stringWithFormat:@"%@",self.dataDictionary[@"picKey"]] isEqual:[NSNull null]]) {
            self.qiNiuKey = @"";
            
            
        } else {
            
            self.qiNiuKey = self.dataDictionary[@"picKey"];
            
        }
    }
    
    
    
    NSDictionary *dictionay = @{@"name":self.phoneNum_tf.text,
                                @"gender":genderString,
                                @"birthday":_valueString,
                  @"pic":self.qiNiuKey,
                  };
    
    
    __weak typeof(self) weakSelf = self;
 
    [[Common shared] afPostRequestWithUrlString:user_modify parms:dictionay finishedBlock:^(id responseObj) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        
        if ([dic[@"success"] boolValue]) {
            
            [WKProgressHUD popMessage:dic[@"message"] inView:weakSelf.view duration:1.5 animated:YES];
            
            //延迟执行
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                
                [weakSelf naviLeftItemClick];
                [weakSelf.delegate mineDetailSaveNewInfo];
            });
            
            
        } else {
            [WKProgressHUD popMessage:dic[@"message"] inView:weakSelf.view duration:1.5 animated:YES];
        }
        
    } failedBlock:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    
}


- (UIView *)backViewWithFrame:(CGRect)frame andString:(NSString *)string{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    view.backgroundColor = WhiteColor;
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, frame.size.height/2 - 10, 100, 20)];
    label.text = string;
    label.font = Font(13);
    label.textColor = littleBlackColor;
    
    
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0,frame.size.height - 0.5, self.view.frame.size.width , 0.5)];
    label1.backgroundColor = [UIColor colorWithRed:205/255.0 green:205/255.0 blue:205/255.0 alpha:1];
    [view addSubview:label1];
    
    //    [view addSubview:imageV];
    [view addSubview:label];
    
    
    
    return  view;
    
}

@end
