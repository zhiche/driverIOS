//
//  ShowViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ShowViewController.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import <WebKit/WebKit.h>


@interface ShowViewController ()
<
    WKUIDelegate,
    WKNavigationDelegate
>
@property (nonatomic, strong) TopBackView *topBackView;
@end

@implementation ShowViewController
#pragma mark -
#pragma mark - Lazy
- (TopBackView *)topBackView {
    if (!_topBackView) {
        
        CGRect frame = CGRectMake(0, 0, screenWidth, 64);
        
        _topBackView = [[TopBackView alloc] initWithFrame:frame
                                                    title:@"运费说明"];
        _topBackView.rightButton.hidden = YES;
        
        [_topBackView.leftButton addTarget:self
                                    action:@selector(backAction)
                          forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _topBackView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    
    self.view.backgroundColor = WhiteColor;
    
    [self.view addSubview:self.topBackView];
    
    [self initWebViews];

}

- (void)initWebViews{
    
    
    NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
    
    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
   
    [wkUController addUserScript:wkUScript];
    
    WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
    
    wkWebConfig.userContentController = wkUController;
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64) configuration:wkWebConfig];
    
    NSURL *url = [NSURL URLWithString:price_show];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    webView.navigationDelegate = self;
  
    webView.backgroundColor = [UIColor whiteColor];
  
    [webView loadRequest:request];
    
    [self.view addSubview:webView];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
   
    [rootVC setTabBarHidden:YES];
}

- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

 

@end
