//
//  MineDetailVC.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/22.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MineDetailVC;
@protocol MineDetailVCDelegate <NSObject>
@required
- (void)mineDetailSaveNewInfo;

@end
/**
 用户详情Vc
 */
@interface MineDetailVC : UIViewController
@property (nonatomic, weak) id<MineDetailVCDelegate>delegate;
@end
