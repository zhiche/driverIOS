//
//  MyScoreViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/8/18.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "MyScoreViewController.h"
#import "TopBackView.h"
#import "RootViewController.h"

#import "ScoreCell.h"
#import "ShowViewController.h"



#define UmengAppkey @"53290df956240b6b4a0084b3"

#import "RLNetworkHelper.h"//网络判断

#import "NullView.h"

#define defaultShow 10



@interface MyScoreViewController ()
<
    UITableViewDelegate,
    UITableViewDataSource
>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UILabel *scoreLabel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableDictionary *dataDictionary;


@property (nonatomic, assign) int pageNo;
@property (nonatomic, strong) UIView *netView;
@property (nonatomic, strong) WKProgressHUD *hud;
@property (nonatomic, strong) NullView *nullView;
@property (nonatomic, assign) NSInteger pageTotal;//最大分页数

@property (nonatomic, strong) TopBackView *topBackView;

@end

@implementation MyScoreViewController
#pragma mark -
#pragma mark - Lazy
- (TopBackView *)topBackView {
    if (!_topBackView) {
        
        CGRect frame = CGRectMake(0, 0, screenWidth, 64);
        
        _topBackView = [[TopBackView alloc] initWithFrame:frame title:@"我的积分"];
        
        [_topBackView.rightButton setTitle:@"积分规则" forState:UIControlStateNormal];
        
        _topBackView.rightButton.titleLabel.font = Font(13);
        
        [_topBackView.rightButton setTintColor:WhiteColor];
        
        [_topBackView.rightButton addTarget:self
                                     action:@selector(shareMyApp)
                           forControlEvents:UIControlEventTouchUpInside];
        
        [_topBackView.leftButton addTarget:self
                                    action:@selector(backAction)
                          forControlEvents:UIControlEventTouchUpInside];
    }
    return _topBackView;
}
- (NullView *)nullView {
    if (!_nullView) {
        CGRect frame = CGRectMake(0, 0, screenWidth, screenHeight  - 45 * kHeight);
        
        _nullView = [[NullView  alloc] initWithFrame:frame
                                            andTitle:@""
                                            andImage:[UIImage imageNamed:@"no_order"]];
        _nullView.label.text = @"暂无更多积分详情";
        
        _nullView.backgroundColor = GrayColor;
    }
    return _nullView;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.view setBackgroundColor:GrayColor];
    
    [self.view addSubview:self.topBackView];
    
    ///判断是否有网
    [self judgeNetWork];
    
    [self initSubViews];
    
}

#pragma mark -
#pragma mark - 判断是否有网
//判断是否有网
- (void)judgeNetWork{
    if (![RLNetworkHelper isConnectedToNetwork])
    {
        [self.tableView addSubview:self.netView];
        
        [WKProgressHUD showInView:self.view withText:@"" animated:YES];
    }
    else
    {
        [self.netView removeFromSuperview];
        [self getServerData];
    }
}

- (void)getServerData {
    

    NSString *URL = [NSString stringWithFormat:@"%@?pageNo=%d&pageSize=%d",
                     score_details,
                     self.pageNo,
                     defaultShow];

    
    [Common requestWithUrlString:URL contentType:application_json errorShow:YES finished:^(id responseObj) {
       
        
        self.pageTotal = [responseObj[@"data"][@"page"][@"totalPage"] integerValue];
        
        self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
        
        if ([self.dataDictionary[@"turnover"] isEqual:[NSNull null]])
        {
            [self.tableView addSubview:self.nullView];

        } else {
            
            self.dataArray = [NSMutableArray arrayWithArray:self.dataDictionary[@"turnover"]];
           
            [self.nullView removeFromSuperview];
            
            [self.tableView reloadData];
            
        }
        
        if ([self.dataDictionary[@"scores"] isEqual:[NSNull null]]) {
            
        } else {
            self.scoreLabel.text = [NSString stringWithFormat:@"%.2f",[self.dataDictionary[@"scores"] floatValue]];
        }
 
    } failed:^(NSString *errorMsg) {
        [self.hud dismiss:YES];
        
        NSLog(@"%@",errorMsg);
    }];

    
}

- (void)initSubViews {
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    [self.view addSubview:self.scrollView];
    
    UIView *firstView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 140 * kHeight)];
    
    firstView.backgroundColor = Color_RGB(252, 161, 87, 1);
    self.scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 106 * kHeight, 106 * kHeight)];
    self.scoreLabel.center = firstView.center;
    self.scoreLabel.textAlignment = NSTextAlignmentCenter;
    self.scoreLabel.layer.cornerRadius = 53 * kHeight;
    self.scoreLabel.layer.borderColor = WhiteColor.CGColor;
    self.scoreLabel.layer.borderWidth = 1;
    self.scoreLabel.adjustsFontSizeToFitWidth = YES;
    self.scoreLabel.font = Font(34);
    self.scoreLabel.textColor = WhiteColor;
//    self.scoreLabel.text = @"12400";
    [firstView addSubview:self.scoreLabel];
    
    UILabel *scoreL = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 200, cellHeight)];
    [firstView addSubview:scoreL];
    scoreL.textColor = WhiteColor;
//    scoreL.text = @"当前积分为:";
    scoreL.font = Font(15);
    
    [self.scrollView addSubview:firstView];
    

//    [self initButtonWithView:firstView];
    [self initTableViewWithView:firstView];
}


- (void)initTableViewWithView:(UIView *)view{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(view.frame) , screenWidth, screenHeight - CGRectGetMaxY(view.frame)) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]init];
    
    //下拉加载
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefresh)];
    
    //上拉加载
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upRefresh)];
    
    //无网络状态
    [self createNetWorkAndStatue];

    [self.scrollView addSubview:self.tableView];
      
}


- (void)createNetWorkAndStatue{
    
    //判断是否有网
    [self judgeNetWork];
    //创建加载状态
}



//无网络状态
- (void)NoNetPressBtn{
    if ([RLNetworkHelper isConnectedToNetwork]) {
        [self.netView removeFromSuperview];
        [self getServerData];
    } else {
        [self.hud dismiss:YES];
    }
}

- (void)upRefresh{
    
    if (self.pageNo != (int)self.pageTotal) {
        self.pageNo ++;
        [self judgeNetWork];
        [self.tableView.mj_footer endRefreshing];
    }else{
        self.tableView.mj_footer.state = MJRefreshStateNoMoreData;
    }
    [self.tableView.mj_header endRefreshing];
}

- (void)downRefresh{
    if (self.pageNo != 1) {
        self.pageNo --;
        [self judgeNetWork];
        [self.tableView.mj_header endRefreshing];
        
    } else {
        self.tableView.mj_header.state = MJRefreshStateNoMoreData;
        
    }
    [self.tableView.mj_header endRefreshing];
    
    
}

#pragma mark -
#pragma mark - 表代理
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return (self.dataArray.count > 0)? self.dataArray.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *string = @"scroeCell";
//    
    ScoreCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    if (!cell) {
        cell = [[ScoreCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.dataArray.count > 0) {
        
        cell.titleLabel.text = self.dataArray[indexPath.row][@"businessTypeText"];
        cell.dateLabel.text = self.dataArray[indexPath.row][@"createTime"];
        
        if ([self.dataArray[indexPath.row][@"score"] integerValue] > 0) {
            
            cell.scoreLabel.text = [NSString stringWithFormat:@"+%@",self.dataArray[indexPath.row][@"score"] ];
            
        } else {
            
            cell.scoreLabel.text = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"score"] ];
        }
        
        
    }
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
     return cellHeight + 10;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    //    return 50 * kHeight;
    return cellHeight;
}

- (UIView *)tableView:(UITableView *)tableView
viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]init];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/2.0 - 30 * kWidth, cellHeight/2.0 - 10, 60 * kWidth, 20)];
    label.text = @"积分明细";
    label.textColor = carScrollColor;
    label.font = Font(14);
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(35, cellHeight/2.0, CGRectGetMinX(label.frame) - 55, 0.5)];
    
    line1.backgroundColor = LineGrayColor;
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(label.frame) + 20, cellHeight/2.0, CGRectGetWidth(line1.frame), 0.5)];
    line2.backgroundColor = LineGrayColor;
    [view addSubview:line1];
    [view addSubview:line2];
    view.backgroundColor = WhiteColor;
    
    return view;
}

#pragma mark - 分享
- (void)shareMyApp{
    ShowViewController *showVC = [[ShowViewController alloc]init];
    [self.navigationController pushViewController:showVC animated:YES];
 
}

#pragma mark - 返回

- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}







@end
