//
//  NewAboutViewController.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/9/28.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "NewAboutViewController.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import "HTTPHeader.h"
@interface NewAboutViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArray;//网络数据

@end

@implementation NewAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    TopBackView *topBackView= [[TopBackView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 64) title:@"客服中心"];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    
    [self initDataSource];
    
    
    [self initSubViews];
}

-(void)initDataSource
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@?clientType=30&appType=30",about_contact];
    
    
    
    [Common requestWithUrlString:urlString contentType:application_json errorShow:YES finished:^(id responseObj) {
        
        
        if ([responseObj[@"success"] boolValue]) {
            
            self.dataArray = responseObj[@"data"];
            
            [self.tableView reloadData];
            
        }
        
    } failed:^(NSString *errorMsg) {
        
        NSLog(@"%@",errorMsg);
    }];
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *callString = @"callCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:callString];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:callString];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (self.dataArray.count > 0) {
        
        cell.textLabel.text =  [NSString stringWithFormat:@"%@  %@  %@", self.dataArray[indexPath.row][@"positionDescribe"],self.dataArray[indexPath.row][@"contactPerson"],self.dataArray[indexPath.row][@"contactPhone"]];
        
    }
    
    
    cell.textLabel.font = Font(13);
    cell.textLabel.textColor = littleBlackColor;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //    if (indexPath.row == 0) {
    //运输说明
    //        AboutViewController *aboutVC = [[AboutViewController alloc]init];
    //        [self.navigationController pushViewController:aboutVC animated:YES];
    
    NSString *str1 = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"contactPhone"]];
    
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",str1];
    //            NSLog(@"str======%@",str);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    //    } else {
    //        NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"13510818604"];
    //        //            NSLog(@"str======%@",str);
    //        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    //    }
    
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return cellHeight;
    
}



-(void)initSubViews
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64 ) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = GrayColor;
    [self.view addSubview:self.tableView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, screenHeight - 150, screenWidth, 20)];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    // app名称
    //    NSString *app_Name = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    // app版本
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    // app build版本
    //    NSString *app_build = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    label.text = [NSString stringWithFormat:@"当前版本号:%@",app_Version];
    
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font = Font(14);
    [self.tableView addSubview:label];
    label.textColor = littleBlackColor;
    
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
    
    
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
