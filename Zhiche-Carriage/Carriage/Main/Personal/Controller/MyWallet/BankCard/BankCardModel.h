//
//  BankCardModel.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankCardModel : NSObject
@property (nonatomic,copy) NSString *Id;
@property (nonatomic,copy) NSString *card;
@property (nonatomic,copy) NSString *bankName;
@property (nonatomic,copy) NSString *bankLogo;
@property (nonatomic,copy) NSString *bankLogoBg;
@property (nonatomic,copy) NSString *bankWhiteLogo;
@property (nonatomic,copy) NSString *bankLogoColor;
@end
