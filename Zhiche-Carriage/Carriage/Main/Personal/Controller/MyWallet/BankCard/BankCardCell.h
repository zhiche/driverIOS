//
//  BankCardCell.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankCardModel.h"

@interface BankCardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *littleLogo;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *styleLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bigLogo;

@property (nonatomic,strong) BankCardModel *model;

@end
