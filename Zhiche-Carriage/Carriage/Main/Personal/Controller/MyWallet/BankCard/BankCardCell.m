//
//  BankCardCell.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/24.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "BankCardCell.h"
#import <UIImageView+WebCache.h>
#import "UIColor+Hex.h"

@implementation BankCardCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.littleLogo.layer.cornerRadius = 17;
    self.littleLogo.layer.masksToBounds = YES;
    
    self.backView.layer.cornerRadius = 5;

}

- (void)setModel:(BankCardModel *)model{
    if (_model != model) {
        //背景色
        self.backView.backgroundColor = [UIColor colorWithHexString:model.bankLogoColor];
        //logo
        [self.littleLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.bankWhiteLogo]]];
        //name
        self.nameLabel.text = [NSString stringWithFormat:@"%@",model.bankName];
        
        //style
        self.styleLabel.text = [NSString stringWithFormat:@"储蓄卡"];
        [self.styleLabel sizeToFit];
        
        self.styleLabel.frame = CGRectMake(self.styleLabel.frame.origin.x, self.styleLabel.frame.origin.y, self.styleLabel.frame.size.width + 10, 13);
        self.styleLabel.layer.borderColor = WhiteColor.CGColor;
        self.styleLabel.layer.borderWidth = 0.5;
        self.styleLabel.layer.cornerRadius = 2;
        self.styleLabel.textAlignment = NSTextAlignmentCenter;
        
        
        self.numberLabel.text = [NSString stringWithFormat:@"%@",model.card];
        //
        [self.bigLogo sd_setImageWithURL:[NSURL URLWithString:model.bankLogoBg]];

        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
