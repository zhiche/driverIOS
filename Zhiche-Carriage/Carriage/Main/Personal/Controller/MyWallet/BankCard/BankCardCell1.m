//
//  BankCardCell1.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/25.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "BankCardCell1.h"
#import <Masonry.h>

@implementation BankCardCell1
- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    
        [self initSubViews];
        
    }
    
    return  self;
}

-(void)initSubViews
{
    __weak typeof(self) weakSelf = self;
    self.ImageView = [[UIImageView alloc]init];
    [self.contentView addSubview:self.ImageView];
    [self.ImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(weakSelf);
        make.left.mas_equalTo(14);
        make.size.mas_equalTo(CGSizeMake(35, 35));
        
    }];
    
    
    self.nameLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(weakSelf.ImageView.mas_right).offset(8);
        make.centerY.equalTo(weakSelf);
        make.size.mas_equalTo(CGSizeMake(200, 20));
        
    }];
    self.nameLabel.textColor = littleBlackColor;
    self.nameLabel.font = Font(13);
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
