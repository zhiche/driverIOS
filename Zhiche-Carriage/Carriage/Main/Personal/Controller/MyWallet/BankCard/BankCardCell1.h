//
//  BankCardCell1.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/25.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankCardCell1 : UITableViewCell
@property (nonatomic,strong) UIImageView *ImageView;
@property (nonatomic,strong) UILabel *nameLabel;
@end
