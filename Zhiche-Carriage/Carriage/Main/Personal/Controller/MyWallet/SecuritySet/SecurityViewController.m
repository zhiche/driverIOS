//
//  SecurityViewController.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "SecurityViewController.h"
#import "TopBackView.h"
#import "PasswordView.h"
#import "Common.h"
#import "WKProgressHUD.h"
#import "SetPasswordVC.h"


@interface SecurityViewController ()<UITableViewDelegate,UITableViewDataSource,PasswordViewDelegate>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) UIView *alphaView;

@property (nonatomic,strong) NSString *checkString;
@property (nonatomic,strong) NSString *updateString;
@property (nonatomic) NSInteger integer;

@property (nonatomic,strong) WKProgressHUD *hud;


@end

@implementation SecurityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    TopBackView *topBackView= [[TopBackView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 64) title:@"安全设置"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    self.view.backgroundColor = GrayColor;
    self.alphaView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];

    self.dataArray = [NSMutableArray arrayWithObjects:@"修改提现密码",@"忘记提现密码", nil];
    
    [self initSubview];
}

-(void)initSubview
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.view addSubview:self.tableView];
}

#pragma mark -
#pragma mark - 表代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *string = @"tradeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:string];
    }
    
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.textLabel.font = Font(14);
    cell.textLabel.textColor = littleBlackColor;
    cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"personal_arrow"]];

    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if (indexPath.row == 0) {
//        
//        self.integer = 1;
//        [self initAlphaViewAndString:@"请输入原提现密码"];
//        
//    } else {
//        SetPasswordVC *setVC = [[SetPasswordVC alloc]init];
//        
//        [self.navigationController pushViewController:setVC animated:YES];
//    }
    
    
    [Common requestWithUrlString:exist_pwd contentType:application_json errorShow:YES finished:^(id responseObj) {
        
        
        if ([responseObj[@"data"] boolValue]) {
            
            if (indexPath.row == 0) {
                
                self.integer = 1;
                [self initAlphaViewAndString:@"请输入原提现密码"];
                
            } else {
                SetPasswordVC *setVC = [[SetPasswordVC alloc]init];
                
                [self.navigationController pushViewController:setVC animated:YES];
            }
            
            
        } else {
            
            //未设置密码 先去设置密码
            [WKProgressHUD popMessage:responseObj[@"message"] inView:self.view duration:1.5 animated:YES];
            //延迟执行
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                SetPasswordVC *set = [[SetPasswordVC alloc]init];
                [self.navigationController pushViewController:set animated:YES];
                
            });
            
            
            
        }
        
    } failed:^(NSString *errorMsg) {
        
        //        [self.hud dismiss:YES];
        NSLog(@"%@",errorMsg);
    }];

    
    
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}



- (void)initAlphaViewAndString:(NSString *)string{

    self.alphaView.backgroundColor = RGBACOLOR(0, 0, 0, 0.3);
    self.alphaView.alpha = 1;
//    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
//    [window addSubview:self.alphaView];
    
    [self.view addSubview:self.alphaView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToDismiss)];
    [self.alphaView addGestureRecognizer:tap];
    
    UIView *whiteV = [[UIView alloc]initWithFrame:CGRectMake(20, 110 * kHeight, screenWidth - 40, 140)];
    whiteV.backgroundColor = WhiteColor;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 25, screenWidth - 80, 15)];
    label.textColor = YellowColor;
    label.text = string;
    label.font = Font(12);
    [whiteV addSubview:label];
    
    PasswordView *password = [[PasswordView alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(label.frame) + 20, screenWidth - 80, cellHeight)];
    [whiteV addSubview:password];
    password.layer.borderColor = LineGrayColor.CGColor;
    password.layer.borderWidth = 0.5;
    password.delegate = self;
    password.backgroundColor = WhiteColor;
    
    [self.alphaView addSubview:whiteV];
    
    
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.5;
    
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.8, 0.8, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [whiteV.layer addAnimation:animation forKey:nil];
    

}

- (void)tapToDismiss{
    
    [UIView animateWithDuration:0.2 animations:^{
        [self.alphaView endEditing:YES];
        
        self.alphaView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.alphaView removeFromSuperview];
    }];
}

#pragma mark PasswordView Delegate
- (void)passWordDidChange:(PasswordView *)password{
    NSLog(@"改变%@",password.textStore);
}

- (void)passWordBeginInput:(PasswordView *)password{
    NSLog(@"开始输入%@",password.textStore);
}

- (void)passWordCompleteInput:(PasswordView *)password{
    NSLog(@"输入完成%@",password.textStore);
    
    if (password.textStore.length == 6 && self.integer == 1) {
        
        self.checkString = password.textStore;
        //延迟执行
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            self.hud = [WKProgressHUD showInView:self.view withText:@"" animated:YES];
            
            [self checkPassword];
            
            
        });
    }
    
    
    if (password.textStore.length == 6 && self.integer == 2) {
        
        self.updateString = password.textStore;
        //延迟执行
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self updatePassword];
            
            
        });
    }

}

//验证原始密码
- (void)checkPassword{
    __weak typeof (self) weakSelf = self;
    
   
    [[Common shared] afPostRequestWithUrlString:check_pwd parms:@{@"pwd":self.checkString} finishedBlock:^(id responseObj) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        
        [self.hud dismiss:YES];

        
        if ([dic[@"success"] boolValue]) {
            
                weakSelf.integer = 2;
                [weakSelf initAlphaViewAndString:@"请输入新提现密码"];

        } else {
            
            [WKProgressHUD popMessage:dic[@"message"] inView:self.view duration:1.5 animated:YES];

            [weakSelf initAlphaViewAndString:@"请输入原提现密码"];
            
        }
        
        
    } failedBlock:^(NSString *errorMsg) {
        [self.hud dismiss:YES];

        
        NSLog(@"%@",errorMsg);
    }];
}


//更改密码
- (void)updatePassword{
    __weak typeof (self) weakSelf = self;
    
   
    [[Common shared] afPostRequestWithUrlString:update_pwd parms:@{@"pwd":self.updateString} finishedBlock:^(id responseObj) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        
        [WKProgressHUD popMessage:dic[@"message"] inView:self.view duration:1.5 animated:YES];
        
        if ([dic[@"success"] boolValue]) {
            
            [weakSelf tapToDismiss];
            
            
            
        } else {
            
            
        }
        
    } failedBlock:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
}

#pragma mark - 导航栏
- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
