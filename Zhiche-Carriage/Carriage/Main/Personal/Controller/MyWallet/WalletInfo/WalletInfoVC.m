//
//  WalletInfoVC.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "WalletInfoVC.h"
#import "TopBackView.h"
#import "RootViewController.h"
#import <WebKit/WebKit.h>


@interface WalletInfoVC ()<WKUIDelegate,WKNavigationDelegate>

@end

@implementation WalletInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    TopBackView *topBackView= [[TopBackView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 64) title:@"钱包说明"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    self.view.backgroundColor = WhiteColor;
    
//    self.dataArray = [NSMutableArray arrayWithObjects:@"当天的收入，什么时候可以提现？",@"提现是否收取手续费",@"提现的限制额度和限制次数？",@"提现什么时候到账？",@"提现目前支持哪些银行卡", nil];
    
    [self initWebViews];
}

- (void)initWebViews{
    
    
    NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
    
    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    [wkUController addUserScript:wkUScript];
    
    WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
    wkWebConfig.userContentController = wkUController;
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64) configuration:wkWebConfig];
    NSURL  *url = [NSURL URLWithString:wallet_explain];
    
//    WKWebView *webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64)];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    webView.navigationDelegate = self;
    webView.backgroundColor = [UIColor whiteColor];
    
    [webView loadRequest:request];
    
    [self.view addSubview:webView];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
}

- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
