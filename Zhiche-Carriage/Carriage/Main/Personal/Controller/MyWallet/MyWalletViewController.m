//
//  MyWalletViewController.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/4.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "MyWalletViewController.h"
#import "TopBackView.h"

#import "RootViewController.h"
#import "MyBalanceVC.h"
#import "TradeViewController.h"//交易记录
#import "SecurityViewController.h"//安全设置
#import "WalletInfoVC.h"//钱包说明
#import "BankCardVC.h"//银行卡

@interface MyWalletViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UILabel *monkeyLabel;
@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableDictionary *dataDictionary;

@property (nonatomic,strong) NSMutableArray *imageArray;
@end

@implementation MyWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataDictionary = [NSMutableDictionary dictionary];
    self.imageArray = [NSMutableArray arrayWithObjects:@"wallet_icon_bankcard",@"wallet_icon_record",@"wallet_icon_password",@"wallet_icon_explain", nil];
    TopBackView *topBackView= [[TopBackView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 64) title:@"我的钱包"];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    self.view.backgroundColor = GrayColor;
    
    [self initDataSource];
    
    self.dataArray = [NSMutableArray arrayWithObjects:@"银行卡",@"交易记录",@"安全设置",@"钱包说明", nil];

    [self initSubViews];
    
    
}

- (void)initDataSource{
    [Common requestWithUrlString:user_balances contentType:application_json  errorShow:YES finished:^(id responseObj) {
        
        
                if ([responseObj[@"success"] boolValue]) {
                
                    self.dataDictionary = [NSMutableDictionary dictionaryWithDictionary:responseObj[@"data"]];
                    
                    [self getValueString];
                    
                }
        
    } failed:^(NSString *errorMsg) {
        
        NSLog(@"%@",errorMsg);
    }];

}

- (void)getValueString{
    
    if ([self.dataDictionary[@"balance"] integerValue] == 0) {
        
        self.monkeyLabel.text = @"0.00";
    } else {
        
        self.monkeyLabel.text = [NSString stringWithFormat:@"%.2f",[self.dataDictionary[@"balance"] floatValue] ];
 
    }
    

}

- (void)initSubViews{
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(0, 64, screenWidth, 96 * kHeight);
    nextButton.backgroundColor = YellowColor;
    [nextButton addTarget:self action:@selector(nextButton) forControlEvents:UIControlEventTouchUpInside];
    
//    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 0.5)];
//    label.backgroundColor = WhiteColor;
//    [nextButton addSubview:label];
    
    //余额
    UILabel *yueLabel = [self backLabelWithFont:12 andFrame:CGRectMake(18, 20 * kHeight, 200, 20 * kHeight) andString:@"余额(元)"];
    [nextButton addSubview:yueLabel];
    
    self.monkeyLabel = [self backLabelWithFont:30 andFrame:CGRectMake(18, CGRectGetMaxY(yueLabel.frame) + 5, 200, 40 * kHeight) andString:@"0.00"];
    [nextButton addSubview:self.monkeyLabel];
    
    UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth - 24, (CGRectGetHeight(nextButton.frame)/2.0 - 7), 10, 14)];
    imageV.image = [UIImage imageNamed:@"wallet_icon_arrow_right"];
    [nextButton addSubview:imageV];
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(nextButton.frame) + 10, screenWidth, cellHeight * 4 - 2) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:nextButton];
    [self.view addSubview:self.tableView];
    self.tableView.scrollEnabled = NO;
    
    
}

- (void)nextButton{
    MyBalanceVC *balanceVC = [[MyBalanceVC alloc]init];
    NSString *string ;
    
    if ([self.dataDictionary[@"withdrawalBalance"] integerValue] == 0) {
        
        string = @"0.00";
    } else {
        
       string = [NSString stringWithFormat:@"%@",self.dataDictionary[@"withdrawalBalance"]];
        
    }

    
    balanceVC.moneyString = self.monkeyLabel.text;
    balanceVC.drawString = string;
    
    [self.navigationController pushViewController:balanceVC animated:YES];
}

#pragma mark -
#pragma mark - 表代理
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSString *string = @"wallet";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:string];
    }
    
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.textLabel.font = Font(13);
    cell.textLabel.textColor = littleBlackColor;
    cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"personal_arrow"]];
    cell.imageView.image = [UIImage imageNamed:self.imageArray[indexPath.row]];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        //银行卡
        BankCardVC *bancVC = [[BankCardVC alloc]init];
        [self.navigationController pushViewController:bancVC animated:YES];
    }
    
    if (indexPath.row == 1) {
        //交易记录

        TradeViewController *tradeVC = [[TradeViewController alloc]init];
        [self.navigationController pushViewController:tradeVC animated:YES];
    }
    
    if (indexPath.row == 2) {
        //安全设置

        SecurityViewController *secury = [[SecurityViewController alloc]init];
        [self.navigationController pushViewController:secury animated:YES];
    }
    
    if (indexPath.row == 3 ) {
        //钱包说明

        WalletInfoVC *wallet = [[WalletInfoVC alloc]init];
        [self.navigationController pushViewController:wallet animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}




- (UILabel *)backLabelWithFont:(NSInteger)integet
                      andFrame:(CGRect)frame andString:(NSString *)string{
    
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.font = Font(integet);
    label.textColor = WhiteColor;
    label.text = string;
    
    return  label;
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    [rootVC setTabBarHidden:YES];
    
    //提现后重新请求数据
    [self initDataSource];
}


#pragma mark - 导航栏返回
- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}




@end
