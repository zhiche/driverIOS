//
//  TradeCell.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TradeModel.h"

@interface TradeCell : UITableViewCell
@property (nonatomic,strong) UILabel *moneyLabel;
@property (nonatomic,strong) UILabel *dateLabel;

@property (nonatomic,strong) UILabel *orderLabel;
@property (nonatomic,strong) UILabel *detailLabel;
@property (nonatomic,strong) TradeModel *model;

+(CGFloat)calCellHeight:(NSString *)str;
+(CGFloat)updateCellHeightWithStr:(NSString *)str;


@end
