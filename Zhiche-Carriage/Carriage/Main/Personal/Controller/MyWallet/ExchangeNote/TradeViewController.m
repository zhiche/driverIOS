//
//  TradeViewController.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "TradeViewController.h"

#import "UIView+jmCategory.h"

#import "RLNetworkHelper.h"//网络判断

#import "TopBackView.h"

#import "NullView.h"//没有记录加载

#import "TradeModel.h"

#import "TradeCell.h"

#define defaultShow 10


@interface TradeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic) NSInteger pageNo;
@property (nonatomic,strong) NSMutableArray *dataArray;

//@property (nonatomic) int pageNo;
@property (nonatomic) NSInteger pageTotal;
@property (nonatomic) int pageSize;


@property (nonatomic,strong) UIView *netView;
@property (nonatomic,strong) WKProgressHUD *hud;

@property (nonatomic,strong) NullView *nullView;

@end

@implementation TradeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    
    TopBackView *topBackView= [[TopBackView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 64) title:@"交易记录"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    self.view.backgroundColor = GrayColor;
    
    self.pageNo = 1;
    self.pageSize = defaultShow;
    
    self.nullView = [[NullView  alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight  - 45 * kHeight) andTitle:@"暂时没有交易记录" andImage:[UIImage imageNamed:@"no_trade"]];
    self.nullView.backgroundColor = GrayColor;
    
    
    //创建无网络状态view
    self.netView = [UIView noInternet:@"网络连接失败，请检查网络设备" and:self action:@selector(NoNetPressBtn) andCGreck:CGRectMake(0, 0, Main_Width, Main_height-64)];

    
    
//    [self initDataSource];
    [self initSubview];
}

- (void)initSubview{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.view addSubview:self.tableView];
    
    //mj
    [self.tableView.mj_header beginRefreshing];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefresh)];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upRefresh)];
    
    //无网络状态
    [self judgeNetWork];
}



#pragma mark -
#pragma mark - 判断是否有网
- (void)judgeNetWork{
    if (![RLNetworkHelper isConnectedToNetwork]) {
        [self.tableView addSubview:self.netView];
        self.hud = [WKProgressHUD showInView:self.view withText:@"加载中" animated:YES];
        
    }
    else{
        [self.netView removeFromSuperview];
        [self getDataToServer];
    }
}
- (void)NoNetPressBtn{
    if ([RLNetworkHelper isConnectedToNetwork]) {
        [self.netView removeFromSuperview];
        [self getDataToServer];
    } else {
        [self.hud dismiss:YES];
    }
}
- (void)downRefresh{
    
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    self.pageNo = 1;
    
    [self judgeNetWork];
}
- (void)upRefresh{
    
    if (self.pageNo != (int)self.pageTotal) {
        self.pageNo ++;
        [self judgeNetWork];
        [self.tableView.mj_footer endRefreshing];
    }else{
        self.tableView.mj_footer.state = MJRefreshStateNoMoreData;
    }
    [self.tableView.mj_header endRefreshing];
}


- (void)getDataToServer{
    
    NSString *string = [NSString stringWithFormat:@"%@?pageNo=%d&pageSize=10",user_trade,(int)self.pageNo];
    
    [Common requestWithUrlString:string contentType:application_json errorShow:YES finished:^(id responseObj) {
        
        [self.hud dismiss: YES];

            if (self.pageNo == 1) {
            [self.dataArray removeAllObjects];
        }
        
        
        if ([responseObj[@"data"] isEqual:[NSNull null]]) {
            
            [WKProgressHUD popMessage:responseObj[@"message"] inView:self.view duration:1.5 animated:YES];
            
            
        } else {

        
        if ([responseObj[@"data"][@"details"] count] > 0) {
            
            self.pageTotal = [responseObj[@"data"][@"page"][@"totalPage"] integerValue];

            
            for (NSDictionary *dic in responseObj[@"data"][@"details"]) {
                
                TradeModel *model = [[TradeModel alloc]init];

                [model setValuesForKeysWithDictionary:dic];
                
                [self.dataArray addObject:model];
            }
            
            
        }
        
        
        if (self.dataArray.count >0) {
            
            [self.nullView removeFromSuperview];
            
        } else {
            
            [self.tableView addSubview:self.nullView];
        }
        
        [self.tableView reloadData];

        }
    } failed:^(NSString *errorMsg) {
        
        [self.hud dismiss: YES];

        NSLog(@"%@",errorMsg);
    }];
}

#pragma mark -
#pragma mark - 表代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (self.dataArray.count > 0) ? self.dataArray.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *string = @"tradeCell";
    TradeCell *cell = [tableView dequeueReusableCellWithIdentifier:string];
    if (!cell) {
        cell = [[TradeCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:string];
    }
    
    if (self.dataArray.count > 0) {
        
        cell.model = self.dataArray[indexPath.row];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
        
    if (self.dataArray.count > 0) {
        
        TradeModel *model = self.dataArray[indexPath.row];
        
      return  [TradeCell updateCellHeightWithStr:model.feeTypeDesc];

        
    } else {
    
    return 68 * kHeight;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

#pragma mark - 返回
- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
