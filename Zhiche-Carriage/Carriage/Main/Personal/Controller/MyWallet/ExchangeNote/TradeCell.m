//
//  TradeCell.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "TradeCell.h"
#import <Masonry/Masonry.h>

@implementation TradeCell

- (id)initWithStyle:(UITableViewCellStyle)style
   reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    
    return self;
}

- (void)initSubviews{
    __weak typeof(self) weakSelf = self;
    self.moneyLabel = [self backLabelAndColor:YellowColor];
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(18);
        make.top.mas_equalTo(14 * kHeight);
        make.size.mas_equalTo(CGSizeMake(75 * kWidth, 17 * kHeight));
    }];
    
    self.moneyLabel.adjustsFontSizeToFitWidth = YES;
    
//    self.moneyLabel.text = @"1000";
    
    
    self.dateLabel = [self backLabelAndColor:fontGrayColor];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.moneyLabel.mas_bottom).offset(5 * kHeight);
        make.size.equalTo(weakSelf.moneyLabel);
        make.left.mas_equalTo(18);
    }];
    
//    self.dateLabel.text = @"7月1日";
    
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = GrayColor;
    [self.contentView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(weakSelf.dateLabel.mas_right).offset(10);
        make.size.mas_equalTo(CGSizeMake(0.5,  48 * kHeight));
        make.top.mas_equalTo(10 * kHeight);
    }];
    
    
    self.orderLabel = [self backLabelAndColor:littleBlackColor];
    
    [self.orderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(label.mas_right).offset(20);
        make.top.mas_equalTo(14 * kHeight);
        make.size.mas_equalTo(CGSizeMake(200 * kHeight, 17 * kHeight));
    }];
//    self.orderLabel.text = @"adfasfasdfasdfasdf";
    
    self.detailLabel = [self backLabelAndColor:fontGrayColor];
    self.detailLabel.numberOfLines = 0;
    
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(weakSelf.orderLabel.mas_bottom).offset(5 * kHeight);
//        make.size.equalTo(weakSelf.orderLabel);
        make.size.mas_equalTo(CGSizeMake(200 * kHeight, 17 * kHeight));

        make.left.mas_equalTo(label.mas_right).offset(20);
    }];
//    self.detailLabel.text = @"定金";
    
    
}

- (UILabel *)backLabelAndColor:(UIColor *)color{
    UILabel *label = [[UILabel alloc]init];
    label.textColor = color;
    label.font = Font(13);
    
    [self.contentView addSubview:label];
    
    return label;
    
}

- (void)setModel:(TradeModel *)model{
    if (_model != model) {
        
        NSString *string = [self backString: model.createTime];
        NSArray *array = [string componentsSeparatedByString:@" "];
//        string = [array[0] substringFromIndex:5];
        
        
        self.moneyLabel.text = [self backString:model.strOrderTotal];
        self.dateLabel.text = array[0];
        
        
        if ([model.orderCode isEqualToString:@"null"]) {
            
            self.orderLabel.text = @"";

        } else {
            
            self.orderLabel.text = [self backString:model.orderCode];

        }
        
        self.detailLabel.text = [self backString:model.feeTypeDesc];
        
       CGFloat h = [TradeCell calCellHeight:self.detailLabel.text];
        
        if (h > 17 * kHeight) {
        
            [self.detailLabel mas_updateConstraints:^(MASConstraintMaker *make) {
              
                make.height.mas_equalTo(h);
                
            }];
            
        }
    
    }
}

+ (CGFloat)calCellHeight:(NSString *)str{
    NSDictionary *dic = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:13 * kHeight] forKey:NSFontAttributeName];
    
    CGRect rect =[str boundingRectWithSize:CGSizeMake(200 * kHeight, 2000) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
    

    return rect.size.height;
    
}

+ (CGFloat)updateCellHeightWithStr:(NSString *)str{
    
    CGFloat height = [TradeCell calCellHeight:str];

    if (height > 17 * kHeight) {
        
        return 68 * kHeight + (height - 17 * kHeight);
        
    } else {
        return 68 * kHeight;
    }
}

- (NSString *)backString:(NSString *)string{
    
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
