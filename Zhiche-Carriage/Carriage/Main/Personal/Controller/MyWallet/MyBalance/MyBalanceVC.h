//
//  MyBalanceVC.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyBalanceVC : UIViewController

@property (nonatomic,copy) NSString *moneyString;

@property (nonatomic,copy) NSString *drawString;
@end
