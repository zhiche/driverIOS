//
//  WithDrawVC.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "WithDrawVC.h"
#import "TopBackView.h"
#import "PasswordView.h"
#import "Common.h"
#import "AddBankCardVC.h"
#import <UIImageView+WebCache.h>
#import "BankCardCell1.h"
#import "WKProgressHUD.h"
#import "Common.h"
#import "BankCardVC.h"

#define leftMar 18

@interface WithDrawVC ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>
@property (nonatomic,strong) UITextField *textField;
@property (nonatomic,strong) UIView *alphaView;
@property (nonatomic,strong) UIButton *choiceButton;

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArray;

@property (nonatomic,copy) NSString *checkString;

@property (nonatomic,strong) NSMutableDictionary *postDic;

@property (nonatomic,strong) WKProgressHUD *hud;

@property (nonatomic,strong) UIView *bankView;

@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UISwitch *fastSwitch;
@property (nonatomic,strong) UISwitch *scoreSwitch;
@property (nonatomic,strong) UILabel *scoreLabel;
@property (nonatomic,strong) UIView *thirdView;
@property (nonatomic,strong) UIView *fourView;
@property (nonatomic,copy) NSString *textString;
@property (nonatomic,strong) UILabel *showLabel;
@property (nonatomic,strong) NSDictionary *dic;

@end

@implementation WithDrawVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    self.postDic = [NSMutableDictionary dictionary];
    self.dic = [NSDictionary dictionary];
    TopBackView *topBackView= [[TopBackView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 64) title:@"提现"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    self.view.backgroundColor = GrayColor;
    
    self.alphaView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.bankView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];

    [self.postDic setObject:@"" forKey:@"cardId"];
    [self.postDic setObject:@"" forKey:@"cashPwd"];
    [self.postDic setObject:@"0" forKey:@"isUrgency"];
    [self.postDic setObject:self.moneyS forKey:@"amount"];
    [self.postDic setObject:@(0) forKey:@"score"];
    [self.postDic setObject:@(0) forKey:@"quickcash"];

    
    [self initDataSource];
    [self initSubViews];
}


-(void)initSubViews
{
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight)];
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = GrayColor;
    
    UIView *firstView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight * 2)];
    firstView.backgroundColor = WhiteColor;
    [self.scrollView addSubview:firstView];
    
    UILabel *firstL = [self backLabelWithY:CGRectMake(leftMar, 0, screenWidth, cellHeight) andInteger:14 andColor:littleBlackColor];
    firstL.text = @"到账账户";
    [firstView addSubview:firstL];
    
    UILabel *lineL1 = [self backLabelWithY:cellHeight AndView:firstView];
    [firstView addSubview:lineL1];
    
    UILabel *secondL = [self backLabelWithY:CGRectMake(leftMar, cellHeight, 100, cellHeight) andInteger:12 andColor:fontGrayColor];
//    secondL.text = @"";
    [firstView addSubview:secondL];
    
    self.choiceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.choiceButton setFrame:CGRectMake(leftMar, cellHeight, screenWidth - leftMar, cellHeight)];
    [self.choiceButton setTitle:@"选择到账账号" forState:UIControlStateNormal];
    [self.choiceButton setTitleColor:fontGrayColor forState:UIControlStateNormal];
    self.choiceButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [firstView addSubview:self.choiceButton];
    self.choiceButton.titleLabel.font = Font(12);
    [self.choiceButton addTarget:self action:@selector(choiceButton:) forControlEvents:UIControlEventTouchUpInside];

    
    
    UIView *secondView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(firstView.frame) + 10, screenWidth, cellHeight * 2)];
    secondView.tag = 800;
    secondView.backgroundColor = WhiteColor;
    [self.scrollView addSubview:secondView];
    
    UILabel *fourthL = [self backLabelWithY:CGRectMake(leftMar, 0, screenWidth, cellHeight) andInteger:14 andColor:littleBlackColor];
    [secondView addSubview:fourthL];
    fourthL.text = @"提现金额";
    
    UILabel *lineL2 = [self backLabelWithY:cellHeight AndView:secondView];
    [secondView addSubview:lineL2];
    
    
    UILabel *sixL = [self backLabelWithY:CGRectMake(leftMar, cellHeight , 70 * kWidth, cellHeight) andInteger:12 andColor:fontGrayColor];
    sixL.text = [NSString stringWithFormat:@"可提现金额"];
    [secondView addSubview:sixL];
    
    
    UILabel *moneyL = [self backLabelWithY:CGRectMake(screenWidth - 100 - 18, cellHeight , 100, cellHeight) andInteger:12 andColor:littleBlackColor];
    moneyL.text = [NSString stringWithFormat:@"￥%@",self.moneyS];
    moneyL.textColor = fontGrayColor;
    [secondView addSubview:moneyL];
    moneyL.textAlignment = NSTextAlignmentRight;
    moneyL.textColor = YellowColor;
    moneyL.tag = 500;

    //快速提现
    
    UILabel *lineL4 = [self backLabelWithY:cellHeight * 2 AndView:secondView];
    [secondView addSubview:lineL4];
    
    UILabel *fastL = [self backLabelWithY:CGRectMake(leftMar, cellHeight * 2, 100, cellHeight) andInteger:12 andColor:littleBlackColor];
    fastL.text = @"快速提现";
//    [secondView addSubview:fastL];
    
    self.fastSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(screenWidth - 18 - 40, CGRectGetMidY(fastL.frame) - 13.5, 40, 27)];
//    [secondView addSubview:self.fastSwitch];
    self.fastSwitch.onTintColor = YellowColor;
    [self.fastSwitch addTarget:self action:@selector(fastSwitchAction:) forControlEvents:UIControlEventValueChanged];
    
    //控件不能设置frame，只能缩放比例
        self.fastSwitch.transform = CGAffineTransformMakeScale(0.8, 0.8);

//    [self initThirdViewWithView:secondView];
    [self initButtonWithView:secondView];

}

-(void)initThirdViewWithView:(UIView *)view
{
   
    self.thirdView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(view.frame), screenWidth, cellHeight )];
    self.thirdView.backgroundColor = WhiteColor;
    [self.scrollView addSubview:_thirdView];
    
    UILabel *lineL = [self backLabelWithY:0  AndView:_thirdView];
    [_thirdView addSubview:lineL];
    
    
    //手续费

    UILabel *feeL = [self backLabelWithY:CGRectMake(leftMar, 0, 100, cellHeight) andInteger:12 andColor:littleBlackColor];
    feeL.text = @"手续费";
    [_thirdView addSubview:feeL];
    
    self.scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 100 - 18, 0, 100, cellHeight)];
    self.scoreLabel.textColor = YellowColor;
    self.scoreLabel.font = Font(12);
    [_thirdView addSubview:self.scoreLabel];
    self.scoreLabel.textAlignment = NSTextAlignmentRight;
//    self.scoreLabel.text = @"￥3.00";
    
    
    //积分
    UILabel *lineL1 = [self backLabelWithY:cellHeight  AndView:_thirdView];
    [_thirdView addSubview:lineL1];
    
    UILabel *scoreL = [self backLabelWithY:CGRectMake(leftMar, cellHeight , screenWidth - 60 - leftMar, cellHeight) andInteger:12 andColor:littleBlackColor];
//    scoreL.text = @"积分共1000积分，本次使用100积分抵扣10.00";
    scoreL.tag = 801;
    scoreL.adjustsFontSizeToFitWidth = YES;
//    [_thirdView addSubview:scoreL];
    
    self.scoreSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(screenWidth - 18 - 40, CGRectGetMidY(scoreL.frame) - 13.5, 40, 27)];
//    [_thirdView addSubview:self.scoreSwitch];
    self.scoreSwitch.onTintColor = YellowColor;
    [self.scoreSwitch addTarget:self action:@selector(scoreSwitchAction:) forControlEvents:UIControlEventValueChanged];
    //控件不能设置frame，只能缩放比例
    self.scoreSwitch.transform = CGAffineTransformMakeScale(0.8, 0.8);
    

}

//快速提现
-(void)fastSwitchAction:(UISwitch *)sender
{
 
    //快速提现
    if (sender.isOn) {
        [UIView animateWithDuration:0.2 animations:^{
            
            UIView *view = [self.view viewWithTag:800];
            [self initThirdViewWithView:view];
            
            self.fourView.frame = CGRectMake(0, CGRectGetMaxY(self.thirdView.frame), screenWidth, 250);
            
            self.scrollView.contentSize = CGSizeMake(screenWidth, CGRectGetMaxY(self.fourView.frame) + 20);

        }];
        
        

        [[Common shared] afPostRequestWithUrlString:with_drawfee parms:@{@"amount":self.fastMoney} finishedBlock:^(id responseObj) {
            
            self.dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
            
            
            if ([self.dic[@"data"] allKeys] > 0) {
                
                self.scoreLabel.text = [NSString stringWithFormat:@"￥%.2f",[self.dic[@"data"][@"fee"] floatValue]];
                UILabel *label = [self.view viewWithTag:801];
                label.text = [NSString stringWithFormat:@"%@",self.dic[@"data"][@"scoresText"]];
                
                //消费积分
//                [self.postDic setObject:dic[@"data"][@"scores"] forKey:@"scores"];
                _textString = self.dic[@"data"][@"scores"];
                //手续费
                [self.postDic setObject:@(1) forKey:@"quickcash"];
                [self.postDic setObject:self.fastMoney forKey:@"amount"];

            
                //提现金额  （全部）
                UILabel *fastL = [self.view viewWithTag:500];
                fastL.text = [NSString stringWithFormat:@"￥%.2f",[self.fastMoney floatValue]];

                
            }
            
        } failedBlock:^(NSString *errorMsg) {
            NSLog(@"%@",errorMsg);
        }];
       
        
    } else {
        
        [UIView animateWithDuration:0.2 animations:^{
            
            [self.thirdView removeFromSuperview];
            UIView *view = [self.view viewWithTag:800];
            //消费积分
//            [self.postDic setObject:@(0) forKey:@"score"];
            //快速提现
//            [self.postDic setObject:@"" forKey:@"fee"];
            [self.postDic setObject:@(0) forKey:@"quickcash"];

            [self.postDic setObject:self.moneyS forKey:@"amount"];

            //提x金额（普通）
            UILabel *fastL = [self.view viewWithTag:500];
            fastL.text = [NSString stringWithFormat:@"￥%.2f",[self.moneyS floatValue]];
            
            self.fourView.frame = CGRectMake(0, CGRectGetMaxY(view.frame), screenWidth, 250);
            
            self.scrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 1);

        }];
        

    }
   
}

//积分
-(void)scoreSwitchAction:(UISwitch *)sender{
    if (sender.isOn) {
        
        [self.postDic setObject:@(1) forKey:@"score"];
        self.scoreLabel.text = [NSString stringWithFormat:@"￥%.2f",[self.dic[@"data"][@"fee"] floatValue] - [self.dic[@"data"][@"subfee"] floatValue]];


    } else {
        
        [self.postDic setObject:@(0) forKey:@"score"];
        self.scoreLabel.text = [NSString stringWithFormat:@"￥%.2f",[self.dic[@"data"][@"fee"] floatValue]];

    }
    
    
}

//全部提现
- (void)allButton{
//    UILabel *label = (UILabel *)[self.view viewWithTag:500];
//    NSString *string = [label.text substringFromIndex:1];
//    self.textField.text = self.moneyS;
    
//    [self.postDic setObject:self.textField.text forKey:@"amount"];
}

//选择到账账户
- (void)choiceButton:(UIButton *)sender{
    
    [self.view endEditing:YES];
    
    if (self.dataArray.count <= 0) {
        
        BankCardVC *addVC = [[BankCardVC alloc]init];
        
        [self.navigationController pushViewController:addVC animated:YES];
        
        
    } else {
        
       [UIView animateWithDuration:0.3 animations:^{
          
           UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissBankView)];
           [self.bankView addGestureRecognizer:tap];
           tap.delegate = self;
           
           [self.view addSubview:self.bankView];
           self.bankView.backgroundColor = Color_RGB(0, 0, 0, 0.5);
           self.bankView.alpha = 1;
           [self.bankView addSubview:self.tableView];
           self.tableView.frame = CGRectMake(0, screenHeight - cellHeight *(self.dataArray.count + 1), screenWidth, cellHeight *(self.dataArray.count + 1));
           
       }];
    }
    
    
}

- (void)initDataSource{
    [Common requestWithUrlString:bankcard_list contentType:application_json errorShow:YES finished:^(id responseObj) {
        
        if ([responseObj[@"success"] boolValue] ) {
            
            if ([responseObj[@"data"] count] > 0) {
            
            self.dataArray = [NSMutableArray arrayWithArray:responseObj[@"data"]];
                
        
                //默认显示第一张银行卡
                if (self.dataArray.count > 0) {
                    
                    NSString *string = self.dataArray[0][@"card"];
                    NSArray *arr = [string componentsSeparatedByString:@" "];
                    
                    for (int i = 0; i < arr.count; i ++) {
                    
                    [self.choiceButton setTitle:[NSString stringWithFormat:@"%@(%@)",self.dataArray[0][@"bankName"],arr[arr.count - 1]] forState:UIControlStateNormal];
                    
                    [self.postDic setObject:self.dataArray[0][@"id"] forKey:@"cardId"];
                        
                }
                }
                
//            [self.tableView reloadData];
                
                [self initTableV];
                
            }
            
        }
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
    

    
    /*
    //查询手续费
    Common *c = [[Common alloc]init];
    [c afPostRequestWithUrlString:with_drawfee parms:@{@"amount":self.moneyS} finishedBlock:^(id responseObj) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        
        if ([dic[@"data"] allKeys] > 0) {
            
//            self.showLabel.text = [NSString stringWithFormat: @"每周四，0:00-23:59为提现日，其他时间请选择快速提现\n快速提现需要支付手续费（%@，不封顶）\n可用积分冲抵，单次最多冲抵手续费的%@并且\n不超过%.2f元",dic[@"data"][@"rate"],dic[@"data"][@"maxrate"],[dic[@"data"][@"maxscores"] floatValue]];
            
//            self.showLabel.text = [NSString stringWithFormat: @"每周四，0:00-23:59为提现日，其他时间请选择快速提现\n快速提现需要支付手续费（%@，不封顶）",dic[@"data"][@"rate"]];
            
             self.showLabel.text = [NSString stringWithFormat: @"每周四，0:00-23:59为提现日。"];

        }
        
    } failedBlock:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];
     
     */

}

- (void)initTableV{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, screenHeight , screenWidth, cellHeight * (self.dataArray.count + 1)) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.bankView addSubview:self.tableView];
}


#pragma mark -
#pragma mark - 表代理
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return (self.dataArray.count >0) ? self.dataArray.count : 0;
//    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellString = @"bankCell";
    BankCardCell1 *cell = [tableView dequeueReusableCellWithIdentifier:cellString];
    if (!cell) {
        cell = [[BankCardCell1 alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellString];
    }
    
    if (self.dataArray.count > 0) {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"bankLogo"]]];
        
        [cell.ImageView sd_setImageWithURL:url];
        
        NSString *string = self.dataArray[indexPath.row][@"card"];
       NSArray *arr = [string componentsSeparatedByString:@" "];
        
        for (int i = 0; i < arr.count; i ++) {
            cell.nameLabel.text = [NSString stringWithFormat:@"%@(%@)",self.dataArray[indexPath.row][@"bankName"],arr[arr.count - 1]];
        }
        
        
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.textField becomeFirstResponder];
    
    BankCardCell1 *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    [self.choiceButton setTitle:cell.nameLabel.text forState:UIControlStateNormal];
    
    [self.postDic setObject:self.dataArray[indexPath.row][@"id"] forKey:@"cardId"];
    
    
    [self dismissBankView];
   
}

- (void)dismissBankView{
    //先移到屏幕底部 然后消失
    [UIView animateWithDuration:0.3 animations:^{
        
        self.bankView.alpha = 0.5;
        
        self.tableView.frame = CGRectMake(0, screenHeight , screenWidth, cellHeight *(self.dataArray.count + 1));
    } completion:^(BOOL finished) {
        [self.bankView removeFromSuperview];
        
    }];
    

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch {
    //Tip:我们可以通过打印touch.view来看看具体点击的view是具体是什么名称,像点击UITableViewCell时响应的View则是UITableViewCellContentView.
    if ([NSStringFromClass([touch.view class])    isEqualToString:@"UITableViewCellContentView"]) {
        //返回为NO则屏蔽手势事件
        return NO;
    } else {
        return  YES;
    }
//    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}

- (UIView *)tableView:(UITableView *)tableView
viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, cellHeight)];
    view.backgroundColor = WhiteColor;
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(14, 0, 140, cellHeight)];
    label.textColor = YellowColor;
    label.font = Font(14);
    label.text = @"到账银行卡";
    [view addSubview:label];
    
    UILabel *l = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
    l.backgroundColor = LineGrayColor;
    [view addSubview:l];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    return cellHeight;
}


- (UILabel *)backLabelWithY:(CGFloat)y AndView:(UIView *)view{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, y, screenWidth, 0.5)];
    label.backgroundColor = GrayColor;
//    [view addSubview:label];
    return label;
}

- (UILabel *)backLabelWithY:(CGRect)frame andInteger:(NSInteger)integer andColor:(UIColor *)color{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.textColor = color;
    label.font = Font(integer);
    
    
    return label;
}


- (void)initButtonWithView:(UIView *)view{
    self.fourView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(view.frame), screenWidth, 250)];
    self.fourView.backgroundColor = GrayColor;
    [self.scrollView addSubview:self.fourView];
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(18, 15, 20, 20)];
    img.image = [UIImage imageNamed:@"prompt"];
    [self.fourView addSubview:img];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(img.frame), CGRectGetMidY(img.frame) - 7, 100, 20)];
    label.text = @"温馨提示";
    label.textColor = YellowColor;
    label.font = Font(12);
    [self.fourView addSubview:label];
    
    
    self.showLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(img.frame) + 10, CGRectGetMaxY(img.frame) , screenWidth - 50, 40 * kHeight)];
    self.showLabel.textColor = YellowColor;
    self.showLabel.numberOfLines = 0;
    self.showLabel.font = Font(12);
    self.showLabel.text = [NSString stringWithFormat: @"每周四，0:00-23:59为提现日。"];

    [self.fourView addSubview:self.showLabel];
    
    
    UIButton *tixianButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tixianButton.frame = CGRectMake(18, CGRectGetMaxY(self.showLabel.frame) + 30, screenWidth - 36, Button_height);
    [tixianButton setTitle:@"提现" forState:UIControlStateNormal];
    tixianButton.backgroundColor = YellowColor;
    tixianButton.layer.cornerRadius = 5;
    [tixianButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    [tixianButton addTarget:self action:@selector(tixianButton) forControlEvents:UIControlEventTouchUpInside];
    [self.fourView addSubview:tixianButton];
}

- (void)tixianButton{
    
//    提现金额为0不让提
    if ([self.postDic[@"amount"] integerValue] == 0) {
        
        [WKProgressHUD popMessage:@"提现金额须大于0" inView:self.view duration:1.5 animated:YES];
        
    } else {
    
    //弹出支付密码框
    
    if (self.dataArray.count <= 0) {
        
        [WKProgressHUD popMessage:@"未绑定银行卡，请先绑定银行卡" inView:self.view duration:1.5 animated:YES];

        //延迟执行
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            AddBankCardVC *addVC = [[AddBankCardVC alloc]init];
            
            [self.navigationController pushViewController:addVC animated:YES];

        });
        

        
    } else if ([self.postDic[@"cardId"] isEqualToString:@""] || [self.postDic[@"amount"] isEqualToString:@""])
    {
        
        [WKProgressHUD popMessage:@"请将信息填写完整" inView:self.view duration:1.5 animated:YES];
    }

    else {
    
        [self initAlphaViewAndString:@"为了您的账户安全，请输入提现密码"];

    }
        
  }

}


- (void)initAlphaViewAndString:(NSString *)string{
    self.alphaView.backgroundColor = RGBACOLOR(0, 0, 0, 0.3);
    self.alphaView.alpha = 1;
//    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    [self.view addSubview:self.alphaView];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToDismiss)];
    [self.alphaView addGestureRecognizer:tap];
    
    UIView *whiteV = [[UIView alloc]initWithFrame:CGRectMake(20, 110 * kHeight, screenWidth - 40, 140)];
    whiteV.backgroundColor = WhiteColor;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 25, screenWidth - 80, 15)];
    label.textColor = YellowColor;
    label.text = string;
    label.font = Font(12);
    [whiteV addSubview:label];
    
    PasswordView *password = [[PasswordView alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(label.frame) + 20, screenWidth - 80, cellHeight)];
    [whiteV addSubview:password];
    password.layer.borderColor = LineGrayColor.CGColor;
    password.layer.borderWidth = 0.5;
    password.delegate = self;
    password.backgroundColor = WhiteColor;
    
    [self.alphaView addSubview:whiteV];
    
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.5;
    
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.8, 0.8, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [whiteV.layer addAnimation:animation forKey:nil];

    

}

- (void)tapToDismiss{
    
    
    [UIView animateWithDuration:0.2 animations:^{
        [self.alphaView endEditing:YES];
        
        self.alphaView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.alphaView removeFromSuperview];
    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self.textField addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.postDic setObject:textField.text forKey:@"amount"];
}

- (void)textChange:(UITextField *)textField{
    [self.postDic setObject:textField.text forKey:@"amount"];

}

- (void)tap{

    [self.textField resignFirstResponder];

}


#pragma mark PasswordView Delegate
- (void)passWordDidChange:(PasswordView *)password{
    NSLog(@"改变%@",password.textStore);
}

- (void)passWordBeginInput:(PasswordView *)password{
    NSLog(@"开始输入%@",password.textStore);
}

- (void)passWordCompleteInput:(PasswordView *)password{
    NSLog(@"输入完成%@",password.textStore);
    
    if (password.textStore.length == 6 ) {
        
        self.checkString = password.textStore;
        //延迟执行
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            self.hud = [WKProgressHUD showInView:self.view withText:@"" animated:YES];
            
            [self checkPassword];
            
        });

   }
}

//验证原始密码
- (void)checkPassword{
    __weak typeof (self) weakSelf = self;
    
    
    [[Common shared] afPostRequestWithUrlString:check_pwd parms:@{@"pwd":self.checkString} finishedBlock:^(id responseObj) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        
        [self.hud dismiss:YES];
        
        if ([dic[@"success"] boolValue]) {
         
            [self.postDic setObject:self.checkString forKey:@"cashPwd"];
            
            [self tapToDismiss];
            
            [self gotoCostCash];
                        
            
        } else {
            

            
            [WKProgressHUD popMessage:dic[@"message"] inView:self.view duration:1.5 animated:YES];
            
            [weakSelf initAlphaViewAndString:@"请输入提现密码"];
            
        }
        

        
    } failedBlock:^(NSString *errorMsg) {
        
        [self.hud dismiss:YES];
        NSLog(@"%@",errorMsg);
    }];
}

- (void)gotoCostCash{
   
    
    
    
//    self.hud = [WKProgressHUD showInView:self.view withText:@"" animated:YES];

    [[Common shared] afPostRequestWithUrlString:cost_cash parms:self.postDic finishedBlock:^(id responseObj) {
        
//        [self.hud dismiss:YES];

        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableLeaves error:nil];
        [WKProgressHUD popMessage:dic[@"message"] inView:self.view duration:1.5 animated:YES];

        if ([dic[@"success"] boolValue]) {
            
            //延迟执行
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                if (self.callBack) {
                    NSString *str = [NSString stringWithFormat:@"%.2f",[self.fastMoney floatValue] - [self.postDic[@"amount"] floatValue]];
                    self.callBack(str);
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }
            });

            
        }
                           
        
    } failedBlock:^(NSString *errorMsg) {
        
//        [self.hud dismiss:YES];

        NSLog(@"%@",errorMsg);
    }];
    
}





#pragma mark - 导航栏返回
- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
