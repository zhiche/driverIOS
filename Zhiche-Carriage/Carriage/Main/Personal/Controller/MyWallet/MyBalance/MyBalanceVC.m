//
//  MyBalanceVC.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "MyBalanceVC.h"
#import "TopBackView.h"
#import "WithDrawVC.h"
#import "Common.h"
#import "SetPasswordVC.h"
#import "WKProgressHUD.h"

@interface MyBalanceVC ()

@end

@implementation MyBalanceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    TopBackView *topBackView= [[TopBackView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 64) title:@"余额"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topBackView];
    self.view.backgroundColor = GrayColor;
    
    
    [self initSubViews];
}

- (void)initSubViews{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, 175 * kHeight)];
    view.backgroundColor = WhiteColor;
    [self.view addSubview:view];
    
    UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake((screenWidth - 60)/2.0, 30 * kHeight, 60 , 60)];
    imageV.backgroundColor = [UIColor redColor];
    imageV.layer.cornerRadius = 30;
    imageV.layer.masksToBounds = YES;
    imageV.image = [UIImage imageNamed:@"wallet_icon_money"];
    [view addSubview:imageV];
    
    UILabel *label  = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth - 100)/2.0, CGRectGetMaxY(imageV.frame) + 20, 100, 20)];
    label.textColor = fontGrayColor;
    label.font = Font(13);
    [view addSubview:label];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"余额(元)";
    
    UILabel *balanceLabel = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth - 200)/2.0, CGRectGetMaxY(label.frame) + 5, 200, 40)];
    balanceLabel.textAlignment = NSTextAlignmentCenter;
    balanceLabel.textColor = YellowColor;
    balanceLabel.text = self.moneyString;
    balanceLabel.font = Font(30);
    balanceLabel.tag = 606;
    [view addSubview:balanceLabel];
    [self initButtonWithView:view];
}

- (void)initButtonWithView:(UIView *)view{
    UIButton *tixianButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tixianButton.frame = CGRectMake(18, CGRectGetMaxY(view.frame) + 30, screenWidth - 36, Button_height);
    [tixianButton setTitle:@"提现" forState:UIControlStateNormal];
    tixianButton.backgroundColor = YellowColor;
    tixianButton.layer.cornerRadius = 5;
    [tixianButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    [tixianButton addTarget:self action:@selector(tixianButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:tixianButton];
}

- (void)tixianButton{
    
    
    [Common requestWithUrlString:exist_pwd contentType:application_json errorShow:YES finished:^(id responseObj) {
        
        
        if ([responseObj[@"data"] boolValue]) {
            
            WithDrawVC *with = [[WithDrawVC alloc]init];
            with.moneyS = self.drawString;
            with.fastMoney = self.moneyString;
            with.callBack = ^(NSString *str){
                
                UILabel *label = [self.view viewWithTag:606];
                label.text = str;
                
            };
            
            [self.navigationController pushViewController:with animated:YES];
            
//            [self initAlphaViewAndString:@"为了您的帐户安全，请输入您的提现密码"];
            
        } else {
            
            
            //未设置密码 先去设置密码
            [WKProgressHUD popMessage:responseObj[@"message"] inView:self.view duration:1.5 animated:YES];
            //延迟执行
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                SetPasswordVC *set = [[SetPasswordVC alloc]init];
                [self.navigationController pushViewController:set animated:YES];
                
            });
            
            
            
        }
        
    } failed:^(NSString *errorMsg) {
        
//        [self.hud dismiss:YES];
        NSLog(@"%@",errorMsg);
    }];

    
    
    
    
    
    
    
    
}

- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
