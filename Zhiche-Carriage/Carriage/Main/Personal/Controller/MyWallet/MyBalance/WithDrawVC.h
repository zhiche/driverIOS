//
//  WithDrawVC.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/7.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PasswordView.h"

@interface WithDrawVC : UIViewController<PasswordViewDelegate>

@property (nonatomic,copy) NSString *moneyS;
@property (nonatomic,copy) NSString *fastMoney;//快速提现金额

//提现成功后刷新上个页面的纸
@property (nonatomic,copy) void (^callBack)(NSString *str);


@end
