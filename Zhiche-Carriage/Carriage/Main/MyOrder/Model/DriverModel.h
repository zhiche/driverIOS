//
//  DriverModel.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/3.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DriverModel : NSObject

@property (nonatomic, copy) NSString *orderCode;//中联TMS订单编号
@property (nonatomic, copy) NSString *orderTime;//下单时间
@property (nonatomic, copy) NSString *departProvince;//发车省
@property (nonatomic, copy) NSString *departCity;//发车市

@property (nonatomic, copy) NSString *status;//订单状态

@property (nonatomic, copy) NSString *departCounty;//发车区县
@property (nonatomic, copy) NSString *arrivalProvince;//送达省
@property (nonatomic, copy) NSString *arrivalCity;// 送达市
@property (nonatomic, copy) NSString *arrivalCounty;//送达县
@property (nonatomic, copy) NSString *arrivalAddress;//送达地址
@property (nonatomic, copy) NSString *orderPrice;//订单价格
@property (nonatomic, assign) NSUInteger extraCost;  ///加价
@property (nonatomic, assign) NSUInteger waybillType; ///订单类型
@property (nonatomic, copy) NSString *specialRequired; ///特殊要求

@property (nonatomic, copy) NSString *departAddr;//起运地
@property (nonatomic, copy) NSString *arrivalAddr;//到达地


@property (nonatomic, copy) NSString *statusText;
@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *damageAudit;//是否审核通过
@property (nonatomic, assign) BOOL isCheckLocalPic;//可否取相册照片

@end
