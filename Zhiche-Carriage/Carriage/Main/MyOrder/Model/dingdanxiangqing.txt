{
"success": true,
"data": {
"id": 92,
"waybillCode": "WB777703745058553856",
"departProvinceName": "安徽省",
"departCityName": "淮南市",
"departCountyName": "固原县",
"departAddr": "54354thteres",
"departContact": "李某",
"departPhone": "18500000000",
"receiptProvinceName": "宁夏",
"receiptCityName": "固原市",
"receiptCountyName": "固原县",
"receiptAddr": "543qhyetrwghtrter",
"receiptContact": "张某",
"receiptPhone":"18500000000",
"shipmentDate": "2016-09-19",
"arriveDate": "2016-09-21",
"status": 90,
"statusText": "已完成",
"payStatus": 10,
"payStatusText": "未付款",
"cost": 54346.00,
"vehicles": [
{
"id": 106,
"brandId": 3,
"brandName": "奥迪",
"vehicleId": 11,
"vehicleName": "奥迪100",
"vin": "11112121111111111",
"brandLogo": "http://qiniu.logo.huiyunche.001/QtUjNjc0="
}
],
"postTimeRemaining": 0,
"detail": "车辆总数 1辆",
"bidderType": null,
"bidderTypeText": "",
"sendPics": null,
"driverInfo": null,
"deliverPics": null
},
"messageCode": null,
"message": "查询成功！"
}