//
//  DriverOrderCell.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/1.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "DriverOrderCell.h"
#import "DriverModel.h"
#import "An_ComptitionOrdersModel.h"

@interface DriverOrderCell ()

@property (nonatomic, strong, readwrite) UILabel *orderCode;//订单号
@property (nonatomic, strong, readwrite) UILabel *timeLabel;//时间
@property (nonatomic, strong, readwrite) UILabel *statusLabel;//状态
@property (nonatomic, strong, readwrite) UILabel *startAddressLabel;//起始地
@property (nonatomic, strong, readwrite) UILabel *endAddressLabel;//目的地
@property (nonatomic, strong, readwrite) UILabel *moneyLabel;//价格
@property (nonatomic, strong, readwrite) UIButton *detailButton;//查看详情
@property (nonatomic, strong, readwrite) UIButton *giveButton;//确认交车
@property (nonatomic, strong, readwrite) UIButton *woundButton;//带伤上传
@property (nonatomic, strong, readwrite) UIButton *competitionButton;//抢单
@property (nonatomic, strong, readwrite) UILabel *addMoneyLabel;

@property (nonatomic, assign, readwrite) DriverOrderCellType modelCellType;

@end

@implementation DriverOrderCell
@synthesize orderCode;//订单号
@synthesize timeLabel;//时间
@synthesize statusLabel;//状态
@synthesize startAddressLabel;//起始地
@synthesize endAddressLabel;//目的地
@synthesize moneyLabel;//价格
@synthesize detailButton;//查看详情
@synthesize giveButton;//确认交车
@synthesize woundButton;//带伤上传
@synthesize competitionButton;


/**
    給Cell赋值
 */
- (void)setModel:(DriverModel *)model{
    _model = model;
    
  
    NSArray *arr = [model.orderTime componentsSeparatedByString:@" "];
    
    NSString * ordercode = [NSString stringWithFormat:@"订单号：%@",[self backString:model.orderCode]];
    
    //订单号
    self.orderCode.text = ordercode;

    //时间
    self.timeLabel.text = [self backString:arr[0]];
  
    //状态
    self.statusLabel.text = [self backString:model.statusText];
    
    //起始地
    self.startAddressLabel.text = [NSString stringWithFormat:@"%@",[self backString:model.departAddr]];
  
    //目的地
    self.endAddressLabel.text = [NSString stringWithFormat:@"%@",[self backString:model.arrivalAddr]];
    
    

    //价格
    if ([model.status integerValue] == 5)
    {
        self.moneyLabel.text = [NSString stringWithFormat:@"预估￥%.2f",[model.orderPrice floatValue]];
    }
    else
    {
        self.moneyLabel.text = [NSString stringWithFormat:@"￥%.2f",[model.orderPrice floatValue]];
    }
 
    // 5 待发车
    //INTRANSIT(10, "在途"),
    //DEAL_CAR(20, "已交车"),
    //    COMPLETED(30, "已完成");
    // 40 " "
    
    
    switch (self.modelCellType) {
        case DriverOrderCellTypeNomal:
        {
            [self nomalCellSettingWithModel:model];
        }
            break;
        case DriverOrderCellTypeHurry:
        {
            [self nomalCellSettingWithModel:model];
            
            ///加价
            self.addMoneyLabel.text = [NSString stringWithFormat:@"+%ld", model.extraCost];

        }
            break;
            
        case DriverOrderCellTypeOrdersCompetition:
        {
            ///加价
            self.addMoneyLabel.text = [NSString stringWithFormat:@"+%ld", model.extraCost];

        }
            break;
    }
}

- (void)setComModel:(An_ComptitionOrdersModel *)comModel {
    _comModel = comModel;
    
    NSArray *arr = [comModel.orderTime componentsSeparatedByString:@" "];
    
    NSString * ordercode = [NSString stringWithFormat:@"订单号：%@",[self backString:comModel.orderCode]];
    
    //订单号
    self.orderCode.text = ordercode;
    
    //时间
    self.timeLabel.text = [self backString:arr[0]];
    
    //状态
    self.statusLabel.text = @"待分配";
  
  
    //起始地
    self.startAddressLabel.text = [NSString stringWithFormat:@"%@ - %@",[self backString:comModel.departProvince], [self backString:comModel.departCity]];
    
    //目的地
    self.endAddressLabel.text = [NSString stringWithFormat:@"%@ - %@",[self backString:comModel.destProvince], [self backString:comModel.destCity]];

    ///加价
    self.addMoneyLabel.text = [NSString stringWithFormat:@"+ %0.2f", [comModel.extraCost floatValue]];
}

- (void)nomalCellSettingWithModel:(DriverModel *)model {
    
    switch ([model.status integerValue])
    {
        case 5:
        {
            ///待发车不能交车
            self.giveButton.hidden = YES;
            self.woundButton.hidden = NO;
        }
            break;
        case 10:
        {
            ///在途不能带伤发车
            self.woundButton.hidden = YES;
            self.giveButton.hidden = NO;
            
        }
            break;
        default:
        {
            self.giveButton.hidden = YES;
            self.woundButton.hidden = YES;
        }
            break;
    }
    
    if ([model.damageAudit integerValue] == 2)
    {
        self.woundButton.hidden = YES;
    }
    
}

/**
    非空判断
 */
- (NSString *)backString:(NSString *)string{
    
    if ([string isEqual:[NSNull null]]) {
        return @"";
    } else {
        return string;
    }
}

#pragma mark -
#pragma mark - Method
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier
                       cellType:(DriverOrderCellType)cellType {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self setUpUI];
        self.modelCellType = cellType;
        switch (cellType)
        {
            //普通单
            case DriverOrderCellTypeNomal:
            {

                 [self nomalOrderSubView];
            }
                break;
            //急发单
            case DriverOrderCellTypeHurry:
            {
                
                [self hurryWithNomalIntersectionView];
                [self hurryWithCompetitionOrdersViews];
            }
                break;
            //抢单
            default:
            {
                 [self competitionOrderSubView];
            }
                break;
        }
    
        [self addTargets:cellType];
    }
    return self;
}
/**
 普通Cell
 */
+ (instancetype)cellNomalOrdersWithTableView:(UITableView *)tableView
                             reuseIdentifier:(NSString *)reuseIdentifier {
    DriverOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell) {
        cell = [[DriverOrderCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:reuseIdentifier
                                               cellType:DriverOrderCellTypeNomal];
    }
    return cell;
}
/**
 急发单Cell
 */
+ (instancetype)cellHurryOrdersWithTableView:(UITableView *)tableView
                                   reuseIdentifier:(NSString *)reuseIdentifier {
    DriverOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell) {
        cell = [[DriverOrderCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:reuseIdentifier
                                               cellType:DriverOrderCellTypeHurry];
    }
     return cell;
}
/**
 抢单Cell
 */
+ (instancetype)competitionCellWithTableView:(UITableView *)tableView
                             reuseIdentifier:(NSString *)reuseIdentifier {
    DriverOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell)
    {
        cell = [[DriverOrderCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:reuseIdentifier
                                               cellType:DriverOrderCellTypeOrdersCompetition];
    }
    return cell;
}
/*
    1. 普通Cell 和 急发单Cell 区别只有一个 就是多了 加价
  
    2. 普通Cell 和 抢单Cell 区别 按钮不同，多了加价
*/

/**
    三种布局Cell共同部分
*/
- (void)setUpUI {
    
    ///1.
    orderCode = [[UILabel alloc]init];
    orderCode.font = Font(12);
    self.orderCode.textColor = AddressSCtitleColor;
    [self.contentView addSubview:self.orderCode];
    [self.orderCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(200, 40 * kHeight));
    }];
    
    ///2.
    UIView *lineL0 = [[UIView alloc]init];
    [self.contentView addSubview:lineL0];
    lineL0.backgroundColor = LineGrayColor;
    [lineL0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.equalTo(self.orderCode.mas_bottom).offset(0);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 10, 0.5 ));
    }];
    
    
    
    
    //时间
    self.timeLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.timeLabel];
    self.timeLabel.textAlignment = NSTextAlignmentRight;
    self.timeLabel.font = Font(14);
    self.timeLabel.textColor = Color_RGB(51, 51, 51, 1);
    
    
    
    //起始地
    UIImageView *startImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"start"]];
    [self.contentView addSubview:startImg];
    [startImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.orderCode.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(15, 22));
        
    }];
    
    
    UILabel *rowL = [[UILabel alloc]init];
    [self.contentView addSubview:rowL];
    rowL.backgroundColor = LineGrayColor;
    [rowL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(startImg.mas_right).offset(10);
        make.top.equalTo(startImg);
        make.size.mas_equalTo(CGSizeMake(0.5, 50 ));
        
    }];
    
    self.startAddressLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.startAddressLabel];
    [self.startAddressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(rowL.mas_right).offset(10);
        make.top.equalTo(rowL);
        make.size.mas_equalTo(CGSizeMake(200, 22));
        
    }];
    self.startAddressLabel.font = Font(12);
    self.startAddressLabel.textColor = AddressSCtitleColor;
    
    
    
    
    //目的地
    UIImageView *endImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"end"]];
    [self.contentView addSubview:endImg];
    [endImg mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(startImg.mas_bottom).offset(8);
        make.size.mas_equalTo(CGSizeMake(15, 22));
        
    }];
    
    self.endAddressLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.endAddressLabel];
    [self.endAddressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(rowL.mas_right).offset(10);
        make.top.equalTo(endImg);
        make.size.mas_equalTo(CGSizeMake(200, 22));
        
    }];
    self.endAddressLabel.font = Font(12);
    self.endAddressLabel.textColor = AddressSCtitleColor;
    
    
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(-10);
        make.centerY.equalTo(self.endAddressLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(200, 40 * kHeight));
    }];
    
    
    
    
    
    
    UIView *lineL = [[UIView alloc]init];
    [self.contentView addSubview:lineL];
    lineL.backgroundColor = LineGrayColor;
    [lineL mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(10);
        make.top.equalTo(endImg.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 10, 0.5 ));
        
    }];
    
    
    //查看详情
    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:self.detailButton];
    [self.detailButton setTitle:@"查看详情" forState:UIControlStateNormal];
    self.detailButton.backgroundColor = YellowColor;
    [self.detailButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    [self.detailButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(lineL.mas_bottom).offset(10* kHeight);
        make.size.mas_equalTo(CGSizeMake(100, 30 * kHeight));
        
    }];
    self.detailButton.layer.cornerRadius = 5;
    self.detailButton.titleLabel.font = Font(14);
    
    
    //状态
    
    self.statusLabel = [[UILabel alloc]init];
    [self.contentView addSubview:self.statusLabel];
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(-10);
        make.size.mas_equalTo(CGSizeMake(100, 40 * kHeight));
        
    }];
    self.statusLabel.textAlignment = NSTextAlignmentRight;
    self.statusLabel.textColor = YellowColor;
    self.statusLabel.font = Font(10);
    
    
}

/**
     默认订单(nomal) View
 */
- (void)nomalOrderSubView {
    
    
    [self hurryWithNomalIntersectionView];
    
    //价格
    self.moneyLabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.moneyLabel];
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(-10);
        make.centerY.equalTo(self.startAddressLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(250, 50));
    }];
    
    self.moneyLabel.textAlignment = NSTextAlignmentRight;
    self.moneyLabel.textColor = YellowColor;
    self.moneyLabel.font = Font(14);
    
}

/**
     抢单(competition) View
 */
- (void)competitionOrderSubView {
    
    statusLabel.text = @"待分配";
    
    //抢单
    competitionButton = [[UIButton alloc] init];
    [self.contentView addSubview:competitionButton];
    [competitionButton setTitle:@"抢单" forState:UIControlStateNormal];
    competitionButton.backgroundColor = YellowColor;
    [competitionButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    [competitionButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(self.detailButton.mas_left).offset(-10);
        make.top.equalTo(self.detailButton);
        make.size.equalTo(self.detailButton);
        
    }];
    competitionButton.layer.cornerRadius = 5;
    competitionButton.titleLabel.font = Font(14);
    
    
    [self hurryWithCompetitionOrdersViews];
    
}

/**
    急发单和抢单订单 公共
 */
- (void)hurryWithCompetitionOrdersViews {
    
    //加钱
    self.addMoneyLabel = [[UILabel alloc] init];
    self.addMoneyLabel.text = @"+1000";
    //self.addMoneyLabel.backgroundColor = [UIColor blueColor];
    [self.contentView addSubview:self.addMoneyLabel];
    [self.addMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerY.equalTo(self.startAddressLabel.mas_centerY);
    }];
    self.addMoneyLabel.textAlignment = NSTextAlignmentRight;
    self.addMoneyLabel.textColor = YellowColor;
    self.addMoneyLabel.font = Font(14);
    
    //价格
    self.moneyLabel = [[UILabel alloc]init];
    //self.moneyLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:self.moneyLabel];
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.addMoneyLabel.mas_left);
        make.centerY.equalTo(self.startAddressLabel.mas_centerY);
    }];
    
    self.moneyLabel.textAlignment = NSTextAlignmentRight;
    self.moneyLabel.textColor = UniColor(119, 119, 119);
    self.moneyLabel.font = Font(12);
    
}

/**
    急发单和普通订单 公共
 */
- (void)hurryWithNomalIntersectionView {
    //确认交车
    self.giveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:self.giveButton];
    [self.giveButton setTitle:@"确认交车" forState:UIControlStateNormal];
    self.giveButton.backgroundColor = YellowColor;
    [self.giveButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    [self.giveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(self.detailButton.mas_left).offset(-10);
        make.top.equalTo(self.detailButton);
        make.size.equalTo(self.detailButton);
        
    }];
    self.giveButton.layer.cornerRadius = 5;
    self.giveButton.titleLabel.font = Font(14);
    
    
    //带伤发运
    self.woundButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:self.woundButton];
    [self.woundButton setTitle:@"带伤发运" forState:UIControlStateNormal];
    self.woundButton.backgroundColor = YellowColor;
    [self.woundButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    [self.woundButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(self.detailButton.mas_left).offset(-10);
        make.top.equalTo(self.detailButton);
        make.size.equalTo(self.detailButton);
        
    }];
    self.woundButton.layer.cornerRadius = 5;
    self.woundButton.titleLabel.font = Font(14);
    
}

/**
    添加点击
 */
- (void)addTargets:(DriverOrderCellType)CellType {
    
    [detailButton addTarget:self
                     action:@selector(diverOrderCellClick:)
           forControlEvents:UIControlEventTouchUpInside];
    detailButton.tag = DriverOrderStateDeatail;
    
    switch (CellType)
    {
        case DriverOrderCellTypeOrdersCompetition:
        {
            [competitionButton addTarget:self
                                  action:@selector(diverOrderCellClick:)
                        forControlEvents:UIControlEventTouchUpInside];
            competitionButton.tag = DriverOrderStateCompetition;
            return;
        }
            break;
            
        default:
        {
            [giveButton addTarget:self
                           action:@selector(diverOrderCellClick:)
                 forControlEvents:UIControlEventTouchUpInside];
            giveButton.tag = DriverOrderStateGiveCar;
            
            [woundButton addTarget:self
                            action:@selector(diverOrderCellClick:)
                  forControlEvents:UIControlEventTouchUpInside];
            woundButton.tag = DriverOrderStateCarWound;
        }
            break;
    }
    
    
    
}

/**
    点击执行
 */
- (void)diverOrderCellClick:(UIButton *)btn {
    [self.delegate driverOrderCell:self
                        orderState:btn.tag];
}





@end
