//
//  DriverOrderCell.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/11/1.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
 
@class DriverModel, An_ComptitionOrdersModel;


typedef NS_ENUM(NSUInteger, DriverOrderState){
    DriverOrderStateDeatail = 111, ///详情
    DriverOrderStateGiveCar, ///交车
    DriverOrderStateCarWound, ///带伤发运
    DriverOrderStateCompetition ///抢单
};
typedef NS_ENUM(NSUInteger, DriverOrderCellType) {
    DriverOrderCellTypeNomal = 10,               //普通单
    DriverOrderCellTypeHurry = 20,               //急发单
    DriverOrderCellTypeOrdersCompetition = 30    //抢单
};
@class DriverOrderCell;

@protocol DriverOrderCellDelegate <NSObject>

@required

/**
 订单Cell点击

 @param cell 当前订单Cell
 @param state 根据state状态，执行代理
 */
- (void)driverOrderCell:(DriverOrderCell *)cell
             orderState:(DriverOrderState)state;


@end

@interface DriverOrderCell : UITableViewCell

@property (nonatomic, weak) id <DriverOrderCellDelegate> delegate;

@property (nonatomic, strong, readwrite) DriverModel *model;

@property (nonatomic, strong, readwrite) An_ComptitionOrdersModel *comModel;
/**
    普通Cell
 */
+ (instancetype)cellNomalOrdersWithTableView:(UITableView *)tableView
                  reuseIdentifier:(NSString *)reuseIdentifier;

/**
    急发单Cell
 */
+ (instancetype)cellHurryOrdersWithTableView:(UITableView *)tableView
                  reuseIdentifier:(NSString *)reuseIdentifier;

/**
    抢单Cell
 */
+ (instancetype)competitionCellWithTableView:(UITableView *)tableView
                  reuseIdentifier:(NSString *)reuseIdentifier;


@end
