//
//  PostPictureVC.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/9/20.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>

/**
 上传照片Vc
 */
@interface PostPictureVC : UIViewController<BMKMapViewDelegate,BMKLocationServiceDelegate,BMKGeoCodeSearchDelegate>
{
//    BMKMapManager* _mapManager;
    BMKLocationService* _locService;
    
}
@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,copy) NSString *orderNumber;
@property (nonatomic,copy) NSString *titleString;
@property (nonatomic,assign) BOOL  isCheckLocalPic;
@end
