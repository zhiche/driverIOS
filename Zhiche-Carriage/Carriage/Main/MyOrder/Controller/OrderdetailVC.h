//
//  OrderdetailVC.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 16/11/1.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NavViewController.h"
/**
 运单详情
 */
@interface OrderdetailVC : NavViewController<UIScrollViewDelegate>

@property (nonatomic, strong, nonnull, readwrite) NSString * orderID;

@property (nonatomic, assign) BOOL isChange;//scan页面修改图片 刷新页面

@property (nonatomic, assign) BOOL isSuccess;//事故申报成功 刷新页面

@property (nonatomic, assign) NSInteger status;//判断能否事故上报

@property (nonatomic, strong, nullable, readwrite) NSString *specialRequired;

@property (nonatomic, assign, readwrite) NSUInteger waybillType;

@end
