//
//  OrderdetailVC.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 16/11/1.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderdetailVC.h"
#import "UILabel+Category.h"

#import "RootViewController.h"
#import "PicCollectionViewCell.h"
#import "BigPhotoView.h"

#import "DeclareViewController.h"
#import "RecordDetailCell.h"
#import "ScanVC.h"

@interface OrderdetailVC ()<UITableViewDelegate,UITableViewDataSource>
{
   
    UIImageView * nav;
    UIScrollView * scroll;
    RootViewController * TabBar;

}
@property (nonatomic, strong) UIView *BackView;
@property (nonatomic, strong) UIView *BackView1;
@property (nonatomic, strong) UIView *BackView2;
@property (nonatomic, strong) UIView *BackView3;
@property (nonatomic, strong) UIView *BackView4;
@property (nonatomic, strong) UIView *BackView5;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *imageArray;//图片数组
@property (nonatomic, strong) NSMutableArray *imgArray;
@property (nonatomic, strong) NSMutableDictionary *countDictionry;
@property (nonatomic, copy) NSString *orderCode;
@property (nonatomic, strong) NSMutableDictionary *dic;

@end
@implementation OrderdetailVC {
    NSString *newsSpecialRequired;
    float labHeight;
    float contentSizeRatio;
    float isWaybillType;
    BOOL orderTimeYesOrNo;
}

 
- (void)viewDidLoad {
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if (self.waybillType == 20 && self.specialRequired.length > 0)
    {
        isWaybillType = 0.1 - 0.04*kHeight;
        
        newsSpecialRequired = [NSString stringWithFormat:@"特殊要求：%@",self.specialRequired];
        
        labHeight = [Common contentText:newsSpecialRequired fonts:[UIFont systemFontOfSize:13]
                               maxWidth:(screenWidth - 2*18*kWidth)
                              maxHeight:1000];
    }
    else{
        isWaybillType = 0.003f;
    }
    
    [self createNav];
    
    TabBar = [RootViewController defaultsTabBar];

    [TabBar setTabBarHidden:YES];

    self.imageArray = [NSMutableArray array];
    self.imgArray = [NSMutableArray array];
    self.countDictionry = [NSMutableDictionary dictionary];
    self.dic = [NSMutableDictionary dictionary];
    _isChange = NO;

    [self createScroll];

    [self initDataSource];

}

- (void)initDataSource{
    NSString *string = [NSString stringWithFormat:@"%@%@",mission_detail,self.orderID];
    
    
    [Common requestWithUrlString:string contentType:application_json errorShow:YES finished:^(id responseObj) {
        
        if ([responseObj[@"success"] boolValue]) {
            
            if ([[self.countDictionry allKeys] count] > 0) {
                [self.countDictionry removeAllObjects];
            }
            
            NSMutableDictionary * dic = responseObj[@"data"];
            self.dic = dic;
            
            self.orderCode = dic[@"orderCode"];
            
            if (self.status == 10) {
            //只有在途状态才能事故申报
                [self initRightButtonWithDic:dic];

            }
            
            [self createBackView1:dic];
            [self createBackView2:dic];
            [self createBackView3:dic];
            [self createBackView4:dic];
            [self createBackView5:dic];
 
            
            ///单据照片
            if (![dic[@"deliverPics"] isEqual:[NSNull null]] && [dic[@"deliverPics"] count] > 0)
            {
                for (int i = 0; i < [dic[@"deliverPics"] count]; i ++) {
                    [self.imageArray addObject:dic[@"deliverPics"][i][@"picKey"]];
                }
                [self.countDictionry setObject:dic[@"deliverPics"] forKey:@"单据照片"];
            }
        
            ///带伤照片
            if (![dic[@"damageAttach"] isEqual:[NSNull null]] && [dic[@"damageAttach"] count] > 0)
            {
                [self.countDictionry setObject:dic[@"damageAttach"] forKey:@"带伤照片"];
            }
            
            ///事故照片
            if (![dic[@"accidentPics"] isEqual:[NSNull null]] && [dic[@"accidentPics"] count] > 0)
            {
                 [self.countDictionry setObject:dic[@"accidentPics"] forKey:@"事故照片"];
            }
           
            
            if ([[self.countDictionry allKeys] count] > 0) {
                
                contentSizeRatio = 1.475 + isWaybillType;
                
                scroll.contentSize = CGSizeMake(Main_Width, (Main_height  + [[self.countDictionry allKeys] count] * 148 * kHeight) * contentSizeRatio );
                
                _BackView.frame = CGRectMake(0, 0, Main_Width, Main_height * contentSizeRatio + [[self.countDictionry allKeys] count] * 148 * kHeight);
                
                [self initTableView];

            }
            
            if ([[dic allKeys] containsObject:@"description"]) {
            
            if ([dic[@"description"] isEqual:[NSNull null]] || [dic[@"description"] isEqualToString:@""]) {
                
            } else {
                
                contentSizeRatio = 1.78 + isWaybillType;
                
                scroll.contentSize = CGSizeMake(Main_Width, Main_height*contentSizeRatio + [[self.countDictionry allKeys] count] * 148 *kHeight);
                _BackView.frame = CGRectMake(0, 0, Main_Width, Main_height*contentSizeRatio + [[self.countDictionry allKeys] count] * 148 *kHeight);
                
                [self createBackView6:dic];
            }
                
        }

        } else {
            //获取失败信息
            [WKProgressHUD popMessage:responseObj[@"message"] inView:self.view duration:1.5 animated:YES];
        }
        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
        
    }];

}

- (void)createNav{
    
    nav = [self createNav:@"运单详情"];
    [self.view addSubview:nav];

}


- (void)createScroll{
    
    scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, Main_Width,Main_height-64*kHeight)];
    scroll.backgroundColor = GrayColor;
    scroll.contentOffset = CGPointMake(0, 0);
    contentSizeRatio = 1.4 + isWaybillType;
    scroll.contentSize = CGSizeMake(Main_Width, Main_height*contentSizeRatio);
    self.automaticallyAdjustsScrollViewInsets =NO;
    scroll.bounces = NO;
    //隐藏横向、纵向的滚动条
    scroll.showsHorizontalScrollIndicator = NO;
    scroll.showsVerticalScrollIndicator = NO;
    scroll.delegate = self;
    scroll.minimumZoomScale = 1.0;
    scroll.maximumZoomScale = 3.0;
    [self.view addSubview:scroll];
    
    [scroll mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.top.equalTo(nav.mas_bottom).with.offset(0*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, Main_height-64));
    }];
    //设置scrollView的内容视图
    _BackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height*1.4)];
    _BackView.backgroundColor = GrayColor;
    [scroll addSubview:_BackView];
    
    contentSizeRatio = 1.55 + isWaybillType;
    
    scroll.contentSize = CGSizeMake(Main_Width, Main_height  * contentSizeRatio);
   
    _BackView.frame = CGRectMake(0, 0, Main_Width, Main_height * contentSizeRatio);
    
}

- (void)createBackView1:(NSMutableDictionary*)dic{
    
    CGFloat view1Height = 2 * 5*kHeight +  10*kHeight + 16.0f;
    
    _BackView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 5*kHeight, Main_Width, view1Height)];
    _BackView1.backgroundColor = WhiteColor;
    [_BackView addSubview:_BackView1];
    
    NSString * ordercode = [NSString stringWithFormat:@"运单号：%@",dic[@"orderCode"]];
    UILabel * label = [UILabel createUIlabel:ordercode andFont:FontOfSize12 andColor:littleBlackColor];
    label.textAlignment = 0;
    label.frame = CGRectMake(18*kWidth, 10*kHeight, (screenWidth - 2*18*kWidth), 16.0f);
    [_BackView1 addSubview:label];
    
    if (labHeight > 0)
    {
        view1Height += labHeight;
        
        view1Height += 10*kHeight;
        
        _BackView1.height = view1Height;
        
        
        ////-------
        UILabel *specialRequiredLab = [[UILabel alloc] initWithFrame:CGRectMake(label.x, CGRectGetMaxY(label.frame) + 10*kHeight, label.width, labHeight)];
        specialRequiredLab.text = newsSpecialRequired;
        specialRequiredLab.font = [UIFont systemFontOfSize:13];
        specialRequiredLab.textColor = YellowColor;
        specialRequiredLab.textAlignment = 0;
        specialRequiredLab.numberOfLines = 0;
        //specialRequiredLab.backgroundColor = UniRandomColor;
        [_BackView1 addSubview:specialRequiredLab];
    }

}

- (void)createBackView2:(NSMutableDictionary*)dic{
    
    
    _BackView2 = [[UIView alloc]init];
    _BackView2.backgroundColor = WhiteColor;
    [_BackView addSubview:_BackView2];
    [_BackView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView.mas_left);
        make.top.mas_equalTo(CGRectGetMaxY(_BackView1.frame) + 5*kHeight) ;
        make.size.mas_equalTo(CGSizeMake(Main_Width, 290*kHeight));
    }];
    
    
    
    UILabel * CarModel = [UILabel createUIlabel:@"地址" andFont:FontOfSize14 andColor:BlackTitleColor];
    [_BackView2 addSubview:CarModel];
    [CarModel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView2.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_BackView2.mas_top).with.offset(21.5*kHeight);
    }];
    
    UIView * viewline = [[UIView alloc]init];
    viewline.backgroundColor = LineGrayColor;
    [_BackView2 addSubview:viewline];
    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_BackView2.mas_top).with.offset(43*kHeight);
        make.left.mas_equalTo(_BackView2.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    UIView * viewSline = [[UIView alloc]init];
    viewSline.backgroundColor = LineGrayColor;
    [_BackView2 addSubview:viewSline];
    
    
    
    
    UILabel * cityName = [UILabel createUIlabel:[NSString stringWithFormat:@"%@",dic[@"departAddr"]] andFont:FontOfSize12 andColor:littleBlackColor];
    UILabel * address = [UILabel createUIlabel:[NSString stringWithFormat:@"%@",dic[@"departAddress"]] andFont:FontOfSize12 andColor:littleBlackColor];
    UIImageView * SmallImage = [[UIImageView alloc]init];
    SmallImage.image = [UIImage imageNamed:@"Address_Cell_fache"];
    
    [_BackView2 addSubview:cityName];
    [_BackView2 addSubview:address];
    [_BackView2 addSubview:SmallImage];
    [cityName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(SmallImage.mas_right).with.offset(16*kWidth);
        make.centerY.mas_equalTo(viewline.mas_top).with.offset(21.5*kHeight);
    }];
    [address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(SmallImage.mas_right).with.offset(16*kWidth);
        make.centerY.mas_equalTo(cityName.mas_centerY).with.offset(21.5*kHeight);
    }];
    [viewSline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cityName.mas_top);
        make.left.mas_equalTo(_BackView2.mas_left).with.offset(40*kWidth);
        make.width.mas_equalTo(0.5*kHeight);
        make.bottom.mas_equalTo(address.mas_bottom);
    }];
    [SmallImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView2.mas_left).with.offset(20*kWidth);
        make.centerY.mas_equalTo(viewSline.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(12*kWidth, 16*kHeight));
    }];
    
    
    UIImageView * viewline0 = [[UIImageView alloc]init];
    
    viewline0.backgroundColor = LineGrayColor;
    [_BackView2 addSubview:viewline0];
    [viewline0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(viewSline.mas_bottom).with.offset(19*kHeight);
        make.left.mas_equalTo(_BackView2.mas_left).with.offset(20*kWidth);
        make.size.mas_equalTo(CGSizeMake(Main_Width-40*kWidth, 0.5*kHeight));
    }];
    
    
    UIView * viewSline1 = [[UIView alloc]init];
    viewSline1.backgroundColor = LineGrayColor;
    [_BackView2 addSubview:viewSline1];
    
    
    
    UILabel * cityName1 = [UILabel createUIlabel:[NSString stringWithFormat:@"%@",dic[@"arrivalAddr"]] andFont:FontOfSize12 andColor:littleBlackColor];
    UILabel * address1 = [UILabel createUIlabel:[NSString stringWithFormat:@"%@",dic[@"arrivalAddress"]] andFont:FontOfSize12 andColor:littleBlackColor];
    address1.textAlignment = NSTextAlignmentLeft;
    address1.numberOfLines = 2;
    UILabel * iPhone1 = [UILabel createUIlabel:[NSString stringWithFormat:@"%@ %@  ",dic[@"arrivalContact"],dic[@"arrivalPhone"]] andFont:FontOfSize12 andColor:littleBlackColor];
    iPhone1.textAlignment = NSTextAlignmentLeft;
    UIImageView * SmallImage1 = [[UIImageView alloc]init];
    //    SmallImage1.backgroundColor = [UIColor cyanColor];
    SmallImage1.image = [UIImage imageNamed:@"Address_Cell_shouche"];
    [_BackView2 addSubview:cityName1];
    [_BackView2 addSubview:address1];
    [_BackView2 addSubview:SmallImage1];
    [_BackView2 addSubview:iPhone1];
    [cityName1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(SmallImage1.mas_right).with.offset(16*kWidth);
        make.centerY.mas_equalTo(viewline0.mas_top).with.offset(21.5*kHeight);
    }];
    [address1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(SmallImage1.mas_right).with.offset(16*kWidth);
        make.centerY.mas_equalTo(cityName1.mas_centerY).with.offset(21.5*kHeight);
        make.width.mas_equalTo(Main_Width*0.75);
        
    }];
    [iPhone1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(SmallImage1.mas_right).with.offset(16*kWidth);
        make.centerY.mas_equalTo(address1.mas_centerY).with.offset(21.5*kHeight);
    }];
    
    
    [viewSline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(cityName1.mas_top);
        make.left.mas_equalTo(_BackView2.mas_left).with.offset(40*kWidth);
        make.width.mas_equalTo(0.5*kHeight);
        make.bottom.mas_equalTo(iPhone1.mas_bottom);
    }];
    
    [SmallImage1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView2.mas_left).with.offset(20*kWidth);
        make.centerY.mas_equalTo(viewSline1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(12*kWidth, 16*kHeight));
    }];
    
    UIView * viewline1 = [[UIView alloc]init];
    viewline1.backgroundColor = LineGrayColor;
    [_BackView2 addSubview:viewline1];
    [viewline1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(viewSline1.mas_bottom).with.offset(14*kHeight);
        make.left.mas_equalTo(_BackView2.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    UIImageView * image1 = [[UIImageView alloc]init];
    image1.image = [UIImage imageNamed:@"orderTime"];
    [_BackView2 addSubview:image1];
    [image1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(viewline1.mas_bottom).with.offset(15*kHeight);
        make.left.equalTo(_BackView2.mas_left).with.offset(20*kWidth);
    }];
    
    UILabel * order1 = [UILabel createUIlabel:@"派单时间" andFont:FontOfSize12 andColor:littleBlackColor];
    [_BackView2 addSubview:order1];
    [order1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(image1.mas_centerY);
        //        make.left.equalTo(image1.mas_right).with.offset(10*kWidth);
        make.left.mas_equalTo(cityName.mas_left);
    }];
    //outsetTime //  orderTime ←→ dispatchTime
    UILabel * ordertime = [UILabel createUIlabel:[NSString stringWithFormat:@" %@",dic[@"dispatchTime"]] andFont:FontOfSize12 andColor:littleBlackColor];
    [_BackView2 addSubview:ordertime];
    [ordertime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(order1.mas_centerY);
        make.left.equalTo(order1.mas_right);
    }];
    
    
    UIView * viewline2 = [[UIView alloc]init];
    viewline2.backgroundColor = LineGrayColor;
    [_BackView2 addSubview:viewline2];
    [viewline2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(image1.mas_bottom).with.offset(10*kHeight);
        make.left.mas_equalTo(_BackView2.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    
    NSString * outsettime = [NSString stringWithFormat:@"%@",dic[@"departTime"]];
    if (outsettime.length >7) {
        orderTimeYesOrNo = YES;
        contentSizeRatio = 1.60 + isWaybillType;
        scroll.contentSize = CGSizeMake(Main_Width, Main_height*contentSizeRatio);
        _BackView.frame = CGRectMake(0, 0, Main_Width, Main_height * contentSizeRatio);
        
        UIImageView * outsetImage = [[UIImageView alloc]init];
        outsetImage.image = [UIImage imageNamed:@"orderTime"];
        [_BackView2 addSubview:outsetImage];
        [outsetImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(viewline2.mas_bottom).with.offset(15*kHeight);
            make.left.equalTo(_BackView2.mas_left).with.offset(20*kWidth);
        }];
        
        
        UILabel * outsetTime1 = [UILabel createUIlabel:@"交车截止" andFont:FontOfSize12 andColor:littleBlackColor];
        [_BackView2 addSubview:outsetTime1];
        [outsetTime1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(outsetImage.mas_centerY);
            //        make.left.equalTo(image1.mas_right).with.offset(10*kWidth);
            make.left.mas_equalTo(cityName.mas_left);
        }];
        //
        UILabel * outsetTime2 = [UILabel createUIlabel:[NSString stringWithFormat:@" %@",dic[@"departTime"]] andFont:FontOfSize12 andColor:littleBlackColor];
        [_BackView2 addSubview:outsetTime2];
        [outsetTime2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(outsetTime1.mas_centerY);
            make.left.equalTo(outsetTime1.mas_right);
        }];
        
        
        UIView * viewline3 = [[UIView alloc]init];
        viewline3.backgroundColor = LineGrayColor;
        [_BackView2 addSubview:viewline3];
        [viewline3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(outsetImage.mas_bottom).with.offset(10*kHeight);
            make.left.mas_equalTo(_BackView2.mas_left);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];
        
        
        
        
        UIImageView * image2 = [[UIImageView alloc]init];
        image2.image = [UIImage imageNamed:@"icon_infor_Mileage_imges"];
        [_BackView2 addSubview:image2];
        [image2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(viewline3.mas_bottom).with.offset(15*kHeight);
            make.left.equalTo(_BackView2.mas_left).with.offset(20*kWidth);
        }];
        
        UILabel * distance = [UILabel createUIlabel:@"公里数" andFont:FontOfSize12 andColor:littleBlackColor];
        [_BackView2 addSubview:distance];
        [distance mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(image2.mas_centerY);
            //        make.left.equalTo(image2.mas_right).with.offset(10*kWidth);
            make.left.mas_equalTo(order1.mas_left);
            
        }];
        
        NSString *string;
        if ([dic[@"distance"] isEqual:[NSNull null]]) {
            string = @"";
        } else {
            string = [NSString stringWithFormat:@"%.2fkm",[dic[@"distance"] floatValue]];
        }
        
        UILabel * distanceLabel = [UILabel createUIlabel:string andFont:FontOfSize12 andColor:littleBlackColor];
        [_BackView2 addSubview:distanceLabel];
        [distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(image2.mas_centerY);
            make.left.equalTo(distance.mas_right).offset(5);
        }];
        
    }else{
        orderTimeYesOrNo = NO;
        UIView * viewline2 = [[UIView alloc]init];
        viewline2.backgroundColor = LineGrayColor;
        [_BackView2 addSubview:viewline2];
        [viewline2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(image1.mas_bottom).with.offset(10*kHeight);
            make.left.mas_equalTo(_BackView2.mas_left);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
        }];
        
        
        
        UIImageView * image2 = [[UIImageView alloc]init];
        image2.image = [UIImage imageNamed:@"icon_infor_Mileage_imges"];
        [_BackView2 addSubview:image2];
        [image2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(viewline2.mas_bottom).with.offset(15*kHeight);
            make.left.equalTo(_BackView2.mas_left).with.offset(20*kWidth);
        }];
        
        UILabel * distance = [UILabel createUIlabel:@"公里数" andFont:FontOfSize12 andColor:littleBlackColor];
        [_BackView2 addSubview:distance];
        [distance mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(image2.mas_centerY);
            //        make.left.equalTo(image2.mas_right).with.offset(10*kWidth);
            make.left.mas_equalTo(order1.mas_left);
            
        }];
        
        NSString *string;
        if ([dic[@"distance"] isEqual:[NSNull null]]) {
            string = @"";
        } else {
            string = [NSString stringWithFormat:@"%.2fkm",[dic[@"distance"] floatValue]];
        }
        
        UILabel * distanceLabel = [UILabel createUIlabel:string andFont:FontOfSize12 andColor:littleBlackColor];
        [_BackView2 addSubview:distanceLabel];
        [distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(image2.mas_centerY);
            make.left.equalTo(distance.mas_right).offset(5);
        }];
        
        [_BackView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_BackView.mas_left);
            make.top.mas_equalTo(_BackView1.mas_bottom).with.offset(5*kHeight);
            make.size.mas_equalTo(CGSizeMake(Main_Width, 250*kHeight));
        }];
        
    }
    
}

- (void)createBackView3:(NSMutableDictionary*)dic{
    
    _BackView3 = [[UIView alloc]init];
    _BackView3.backgroundColor = WhiteColor;
    [_BackView addSubview:_BackView3];
    [_BackView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView.mas_left);
        make.top.mas_equalTo(_BackView2.mas_bottom).with.offset(5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 235*kHeight));
    }];

    UILabel * CarModel = [UILabel createUIlabel:@"车辆信息" andFont:FontOfSize14 andColor:BlackTitleColor];
    [_BackView3 addSubview:CarModel];
    [CarModel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView3.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_BackView3.mas_top).with.offset(21.5*kHeight);
    }];
    
    UIView * viewline = [[UIView alloc]init];
    viewline.backgroundColor = LineGrayColor;
    [_BackView3 addSubview:viewline];
    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_BackView3.mas_top).with.offset(43*kHeight);
        make.left.mas_equalTo(_BackView3.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    

    UILabel * label1 = [UILabel createUIlabel:@"底盘号：" andFont:FontOfSize12 andColor:BtnTitleColor];

    UILabel * label2 = [UILabel createUIlabel:@"货物说明：" andFont:FontOfSize12 andColor:BtnTitleColor];
    UILabel * label3 = [UILabel createUIlabel:@"分类：" andFont:FontOfSize12 andColor:BtnTitleColor];
    UILabel * label4 = [UILabel createUIlabel:@"车型代码：" andFont:FontOfSize12 andColor:BtnTitleColor];
    label1.textAlignment = NSTextAlignmentRight;
    label2.textAlignment = NSTextAlignmentRight;
    label3.textAlignment = NSTextAlignmentRight;
    label4.textAlignment = NSTextAlignmentRight;

    [_BackView3 addSubview:label1];
    [_BackView3 addSubview:label2];
    [_BackView3 addSubview:label3];
    [_BackView3 addSubview:label4];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(viewline.mas_bottom).with.offset(19*kHeight);
        make.right.mas_equalTo(_BackView3.mas_left).with.offset(80*kWidth);
    }];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(label1.mas_centerY).with.offset(48*kHeight);
        make.right.mas_equalTo(_BackView3.mas_left).with.offset(80*kWidth);
    }];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(label2.mas_centerY).with.offset(48*kHeight);
        make.right.mas_equalTo(_BackView3.mas_left).with.offset(80*kWidth);
    }];
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(label3.mas_centerY).with.offset(48*kHeight);
        make.right.mas_equalTo(_BackView3.mas_left).with.offset(80*kWidth);
    }];
    
    
 
    NSMutableDictionary * newdic = [[NSMutableDictionary alloc]init];
    
    if ([dic[@"dWaybillVehicle"] isEqual:[NSNull null]]) {
        
    } else {
       
        if ([dic[@"dWaybillVehicle"] count]>0) {
            newdic = dic[@"dWaybillVehicle"][0];
            
        }
    }
    
   
    UILabel * rightlabel1 = [UILabel createUIlabel:[NSString stringWithFormat:@"%@",newdic[@"vehicleVin"]] andFont:FontOfSize12 andColor:BtnTitleColor];
    UILabel * rightlabel2 = [[UILabel alloc]init];
    rightlabel2.text = [NSString stringWithFormat:@"%@",newdic[@"vehicleDesc"]];
    rightlabel2.font = Font(FontOfSize12);
    rightlabel2.textColor = BtnTitleColor;
    rightlabel2.numberOfLines = 3;
    UILabel * rightlabel3 = [UILabel createUIlabel:[NSString stringWithFormat:@"%@",newdic[@"vehicleTypeName"]] andFont:FontOfSize12 andColor:BtnTitleColor];
    UILabel * rightlabel4 = [UILabel createUIlabel:[NSString stringWithFormat:@"%@",newdic[@"vehicleTypeCode"]] andFont:FontOfSize12 andColor:BtnTitleColor];
    [_BackView3 addSubview:rightlabel1];
    [_BackView3 addSubview:rightlabel2];
    [_BackView3 addSubview:rightlabel3];
    [_BackView3 addSubview:rightlabel4];
    [rightlabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(label1.mas_centerY);
        make.left.mas_equalTo(label1.mas_right).with.offset(5*kWidth);
    }];
    [rightlabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(label2.mas_centerY);
        make.left.mas_equalTo(label2.mas_right).with.offset(5*kWidth);
        make.width.mas_equalTo(200*kWidth);
    }];
    [rightlabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(label3.mas_centerY);
        make.left.mas_equalTo(label3.mas_right).with.offset(5*kWidth);
    }];
    [rightlabel4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(label4.mas_centerY);
        make.left.mas_equalTo(label4.mas_right).with.offset(5*kWidth);
    }];
    
}

- (void)createBackView4:(NSMutableDictionary*)dic{
    
    
    _BackView4 = [[UIView alloc]init];
    _BackView4.backgroundColor = WhiteColor;
    [_BackView addSubview:_BackView4];
    [_BackView4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView.mas_left);
        make.top.mas_equalTo(_BackView3.mas_bottom).with.offset(5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 105*kHeight));
    }];
    
    UILabel * CarModel = [UILabel createUIlabel:@"备注" andFont:FontOfSize14 andColor:BlackTitleColor];
    [_BackView4 addSubview:CarModel];
    [CarModel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView4.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_BackView4.mas_top).with.offset(21.5*kHeight);
    }];
    
    UIView * viewline = [[UIView alloc]init];
    viewline.backgroundColor = LineGrayColor;
    [_BackView4 addSubview:viewline];
    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_BackView4.mas_top).with.offset(43*kHeight);
        make.left.mas_equalTo(_BackView4.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    
    UILabel * inforlabel = [[UILabel alloc]init];

    inforlabel.text = [NSString stringWithFormat:@"%@",[self backString:dic[@"customerRemark"]]];
    inforlabel.font = Font(FontOfSize12);
    inforlabel.textColor = BtnTitleColor;
    inforlabel.numberOfLines = 3;
    [_BackView4 addSubview:inforlabel];
    [inforlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(viewline.mas_bottom).with.offset(30*kHeight);
        make.left.mas_equalTo(_BackView4.mas_left).with.offset(18*kWidth);
        make.width.mas_equalTo(260*kWidth);
    }];


    
}

- (void)createBackView5:(NSMutableDictionary*)dic{
    
    //1. 容器
    _BackView5 = [[UIView alloc]init];
    _BackView5.backgroundColor = WhiteColor;
    [_BackView addSubview:_BackView5];
    [_BackView5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView.mas_left);
        make.top.mas_equalTo(_BackView4.mas_bottom).with.offset(5*kHeight);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 230*kHeight ));
    }];
    
    //2. 标题(费用）
    NSString *string = nil;
    self.status == 5 ?  (string =  @"预估费用") : (string =  @"费用");
    UILabel *CarModel = [UILabel
                         createUIlabel:string
                         andFont:FontOfSize14
                         andColor:BlackTitleColor];
    [_BackView5 addSubview:CarModel];
    [CarModel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_BackView5.mas_left).with.offset(18*kWidth);
        make.centerY.mas_equalTo(_BackView5.mas_top).with.offset(21.5*kHeight);
    }];
    
    ///3. 横线
    UIView * viewline = [[UIView alloc]init];
    viewline.backgroundColor = LineGrayColor;
    [_BackView5 addSubview:viewline];
    [viewline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_BackView5.mas_top).with.offset(43*kHeight);
        make.left.mas_equalTo(_BackView5.mas_left);
        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    }];
    
    ///4. 合计ImageView
    UIImageView *imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ellipse"]];
    [_BackView5 addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.top.mas_equalTo(viewline.mas_bottom).offset(5 * kHeight);
        make.centerX.equalTo(_BackView5);
        make.size.mas_equalTo(CGSizeMake(138, 58));
        
    }];
    
    NSString *str;
    NSString *str1;

    if ([dic[@"orderPrice"] isEqual:[NSNull null]])
    {
        str = @"";
    }else
    {
        str = [NSString stringWithFormat:@"合计￥%.2f",[dic[@"orderPrice"] floatValue]];
    }
    
    ///运费
    if ([dic[@"shippingFee"] isEqual:[NSNull null]])
    {
        str1 = @"";
    }
    else
    {
        str1 = [NSString stringWithFormat:@"￥%.2f",[dic[@"shippingFee"] floatValue]];
    }
    
 
    UILabel * moneylabel = [UILabel createUIlabel:str
                                          andFont:FontOfSize14
                                         andColor:YellowColor];
    [_BackView5 addSubview:moneylabel];
    [moneylabel mas_makeConstraints:^(MASConstraintMaker *make){
 
        make.center.equalTo(imgV);
    }];
    
    UILabel *price = [UILabel createUIlabel:@"运费" andFont:12 andColor:carScrollColor];
    [_BackView5 addSubview:price];
    [price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(18 * kHeight);
        make.top.mas_equalTo(imgV.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(100, 20 * kHeight));
        
    }];
    price.textAlignment = NSTextAlignmentLeft;
    
    
    UILabel *priceL = [UILabel backLabelFont:12 andTextColor:carScrollColor andString:str1];
    [_BackView5 addSubview:priceL];
    [priceL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-18 * kHeight);
        make.top.equalTo(price);
        make.size.equalTo(price);
        
    }];
    priceL.textAlignment = NSTextAlignmentRight;
    
    UILabel *lineL = [[UILabel alloc]init];
    [_BackView5 addSubview:lineL];
    lineL.backgroundColor = LineGrayColor;
    [lineL mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo (18 * kHeight);
        make.top.mas_equalTo(price.mas_bottom).offset(0);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 36 * kHeight, 0.5));
    }];
 
    
    
///遍历费用清单fees
    NSArray *feesArray = dic[@"fees"];
    
    if (![feesArray isKindOfClass:[NSArray class]])
    {
        NSLog(@"fees格式错误");
        return;
    }
    CGFloat h = 100 * kHeight / feesArray.count;
 
    [feesArray enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSString *leftString = obj[@"item"];
        NSString *rightString = obj[@"text"];
        
        UILabel *leftLabel = [UILabel backLabelFont:12
                                       andTextColor:carScrollColor
                                          andString:leftString];
        [_BackView5 addSubview:leftLabel];
        [leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(18 * kWidth);
            make.top.mas_equalTo(price.mas_bottom).offset(5 * kHeight + h * idx);
            make.size.mas_equalTo(CGSizeMake(100, h));
        }];
        leftLabel.textAlignment = NSTextAlignmentLeft;
        
        
        UILabel *rightLabel = [UILabel backLabelFont:12
                                        andTextColor:carScrollColor
                                           andString:rightString];
        [_BackView5 addSubview:rightLabel];
        rightLabel.textAlignment = NSTextAlignmentRight;
        [rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-18 * kWidth);
            make.top.equalTo(leftLabel);
            make.size.equalTo(leftLabel);
        }];

    }];
}

- (void)initTableView{
    
//    CGFloat h = 0;
//    
//    if ([Common judgeUsertype]) {
//        h = 863 * kHeight;
//    } else {
//        h = 658 * kHeight;
//    }
    
    __weak typeof(self) weakSelf = self;

    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = NO;
    [_BackView addSubview:self.tableView];

    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
//        make.top.mas_equalTo(weakSelf.BackView5.mas_bottom).offset(5 *kHeight);
//        make.top.mas_equalTo(h);
        make.size.mas_equalTo(CGSizeMake(screenWidth, [[self.countDictionry allKeys] count] * 148 *kHeight ) );
    }];
    
    if ([Common judgeUsertype]) {
        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(weakSelf.BackView5.mas_bottom).offset(5 *kHeight);
 
        }];
    } else {
        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(weakSelf.BackView4.mas_bottom).offset(5 *kHeight);
            
        }];
    }
 }

- (void)createBackView6:(NSDictionary *)dic{
    __weak typeof(self) weakSelf = self;

    UIView *view6 = [[UIView alloc]init];
    
    view6.backgroundColor = WhiteColor;
    [_BackView addSubview:view6];
    [view6 mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(weakSelf.tableView.mas_bottom).offset(0);
        make.size.mas_equalTo(CGSizeMake(screenWidth,100 * kHeight + cellHeight ));
        
    }];
    
    UILabel *topLabel = [[UILabel alloc]init];
    [view6 addSubview:topLabel];
    [topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(18);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(200, cellHeight));
        
    }];
    
    topLabel.text = @"事故描述";
    topLabel.textColor = BlackTitleColor;
    topLabel.font = Font(14);
    
    UILabel *lineL = [[UILabel alloc]init];
    [view6 addSubview:lineL];

    [lineL mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(topLabel.mas_bottom).offset(0);
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(screenWidth, 0.5));
        
    }];
    lineL.backgroundColor = LineGrayColor;
    
    UILabel *label = [[UILabel alloc]init];
    [view6 addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(18);
        make.top.mas_equalTo( cellHeight);
        make.size.mas_equalTo(CGSizeMake(screenWidth - 36, 100 * kHeight));
        
    }];
    label.font = Font(12);
    label.text = dic[@"description"];
    label.textColor = carScrollColor;
    label.numberOfLines = 0;
                              
                              
}


#pragma mark -
#pragma mark - 表协议
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    NSArray *arr = [self.countDictionry allKeys];
    return arr.count;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RecordDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecordDetailCell"];
    cell.backgroundColor = UniRandomColor;
    if (!cell) {
        cell = [[RecordDetailCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"RecordDetailCell"];
    }
    
    NSMutableArray *imgArr = [NSMutableArray array];
    NSArray *arr = [self.countDictionry allKeys];
    NSString *str = arr[indexPath.section];

    for (int i = 0; i < [self.countDictionry[str] count]; i ++) {
        
        [imgArr addObject:self.countDictionry[str][i][@"picKey"] ];
                
    }
    
    
    if (imgArr.count > 0) {
        [cell postValueWith:imgArr];

    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100 *kHeight;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    return cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section{
    return 5 * kHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = WhiteColor;
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(18, 0, 200, cellHeight)];
    label.font = Font(14);
    NSArray *arr = [self.countDictionry allKeys];
    label.text = [NSString stringWithFormat:@"%@",arr[section]];
    [view addSubview:label];
    
    
    UILabel *lineL = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight - 0.5, screenWidth, 0.5)];
    lineL.backgroundColor = LineGrayColor;
    [view addSubview:lineL];
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = GrayColor;
    
    return view;
}






- (void)initRightButtonWithDic:(NSDictionary *)dic{
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"事故申报" forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(screenWidth - 80, 20, 80, 44);
    [self.view addSubview:rightButton];
    [rightButton setTitleColor:WhiteColor forState:UIControlStateNormal];
    rightButton.titleLabel.font = Font(14);
    [rightButton addTarget:self action:@selector(rightButton:) forControlEvents:UIControlEventTouchUpInside];
    rightButton.tag = [dic[@"id"] integerValue];

}

- (NSString *)backString:(NSString *)string{
    //    NSString * str = [NSString stringWithFormat:@"%@",string];
    if ([string isEqual:[NSNull null]]) {
        return @"暂无信息";
    } else {
        return string;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    
    [rootVC setTabBarHidden:YES];

    
    if (_isChange || _isSuccess)
    {
        
        for (UIView *view in _BackView.subviews)
        {
            [view removeFromSuperview];
        }
        
        [self initDataSource];
        
    }
}

- (void)rightButton:(UIButton *)sender{
    
    if ([self.dic[@"auditStatus"] integerValue] != 0) {
        ScanVC *scanVC = [[ScanVC alloc]init];
        
        scanVC.orderId = self.dic[@"id"];
        scanVC.statusId = [self.dic[@"auditStatus"] integerValue];
        scanVC.orderNumber = self.dic[@"orderCode"];
        
        scanVC.isChange = ^(BOOL b){
            //判断 是否有图片改变  是否刷新页面
            _isChange = b;
            
        };
        
        
        [self.navigationController pushViewController:scanVC animated:YES];
    } else {
        DeclareViewController *declareVC = [[DeclareViewController alloc]init];
        
        declareVC.orderId = [NSString stringWithFormat:@"%zi",sender.tag];
        declareVC.orderString = self.orderCode;
        
        declareVC.isSuccess = ^(BOOL a){
            //判断 是否事故申报成功 是否刷新页面
            _isSuccess = a;
            //            sender.hidden = YES;
            
        };
        
        [self.navigationController pushViewController:declareVC animated:YES];
    }
    
    
}

@end
