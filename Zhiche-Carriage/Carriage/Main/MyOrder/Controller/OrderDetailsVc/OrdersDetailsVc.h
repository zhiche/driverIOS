//
//  OrdersDetailsVc.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 2017/6/13.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrdersDetailsVc : UIViewController

@property (nonatomic, strong, nonnull, readwrite) NSString * orderID;

@property (nonatomic, assign) BOOL isChange;//scan页面修改图片 刷新页面

@property (nonatomic, assign) BOOL isSuccess;//事故申报成功 刷新页面

@property (nonatomic, assign) NSInteger status;//判断能否事故上报

@property (nonatomic, strong, nullable, readwrite) NSString *specialRequired;

@property (nonatomic, assign, readwrite) NSUInteger waybillType;

@end
