//
//  OrdersDetailsVc.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 2017/6/13.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import "OrdersDetailsVc.h"
#import "RootViewController.h"
#import "TopBackView.h"

#import "DetailComOrdersNum.h"
#import "DetailComOrdersAddress.h"
#import "DetailComOrdersCarInfos.h"
#import "DetailComOrdersNotes.h"

#import "An_ComptitionOrdersModel.h"
#import "VehicleInfoModel.h"

@interface OrdersDetailsVc ()
<
    TopBackViewDelegate,
    UITableViewDelegate,
    UITableViewDataSource
>
@property (nonatomic, strong, readwrite) TopBackView *topBackView;

@property (nonatomic, strong, readwrite) UITableView *tableView;

@property (nonatomic, strong, readwrite) NSMutableArray *tableViewHeight;

@end

@implementation OrdersDetailsVc

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view addSubview:self.topBackView];
    [self.view addSubview:self.tableView];
}


#pragma mark -
#pragma mark - UITableView协议
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return 1;
}
/**
 Cell个数
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tableViewHeight.count;
}
/**
 Cell风格
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case 0:
        {
            DetailComOrdersNum *cell = [DetailComOrdersNum cellWithTableView:tableView reuseIdentifier:@"DetailComOrdersNum"];
            return cell;
        }
            break;
        case 1:
        {
            DetailComOrdersAddress *cell = [DetailComOrdersAddress cellWithTableView:tableView reuseIdentifier:@"DetailComOrdersAddress"];
            return cell;
        }
            break;
        case 2:
        {
            DetailComOrdersCarInfos *cell = [DetailComOrdersCarInfos cellWithTableView:tableView reuseIdentifier:@"DetailComOrdersCarInfos"];
            return cell;
        }
            break;
        default:
        {
            DetailComOrdersNotes *cell = [DetailComOrdersNotes cellWithTableView:tableView reuseIdentifier:@"DetailComOrdersNotes"];

            return cell;
        }
            break;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section{
    return 5 * kHeight;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
     return [self.tableViewHeight[indexPath.section] floatValue];
}
/**
 Cell点击
 */
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
 
}
#pragma mark -
#pragma mark - Lazy
- (NSMutableArray *)tableViewHeight {
    if (!_tableViewHeight)
    {
        _tableViewHeight = @[@100, @280, @240, @110].mutableCopy;
    }
    return _tableViewHeight;
}

- (UITableView *)tableView {
    if (!_tableView)
    {
        
        CGRect frame = CGRectMake(0, 64 , screenWidth, screenHeight - 64);
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = GrayColor;
     
    }
       return _tableView;
}
- (TopBackView *)topBackView {
    if (!_topBackView)
    {
        _topBackView = [[TopBackView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)
                                                    title:@"运单详情"];
        _topBackView.rightButton.hidden = YES;
        _topBackView.delegate = self;
        
    }
    return _topBackView;
}
/**
 导航栏左侧点击
 */
- (void)backViewPopViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 视图即将加载
 */
- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    RootViewController *rootVC = [RootViewController defaultsTabBar];
    
    [rootVC setTabBarHidden:YES];
    
   
    
}

@end
