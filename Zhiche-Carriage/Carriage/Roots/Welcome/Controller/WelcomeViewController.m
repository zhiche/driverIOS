//
//  WelcomeViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "WelcomeViewController.h"
#import <Masonry.h>
@interface WelcomeViewController ()

@property (nonatomic,strong) UIScrollView *scrollView;


@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.scrollView = [[UIScrollView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
    for (int i =0; i<4; i++)
    {
        UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth * i, 0, screenWidth, screenHeight)];
        NSString * nameStr = [[NSString alloc]initWithFormat:@"guidepage_%d.png",i+1];
        imageView.image = [UIImage imageNamed:nameStr];
        if (i == 3)
        {
            UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setTitle:@"" forState:UIControlStateNormal];
            [btn setImage:[UIImage imageNamed:@"guidepage_btn"] forState:UIControlStateNormal];
            btn.tag = 100;
            [btn addTarget:self action:@selector(pressBtn) forControlEvents:UIControlEventTouchUpInside];
            
             imageView.userInteractionEnabled = YES;
            [imageView addSubview:btn];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(imageView.mas_centerX);
                make.bottom.equalTo(imageView.mas_bottom).with.offset(-57*kHeight);
                make.size.mas_equalTo(CGSizeMake(160*kWidth, 38*kHeight));
            }];
        }
        [self.scrollView addSubview:imageView];
    }
    self.scrollView.contentOffset = CGPointMake(0, 0);
    self.scrollView.contentSize = CGSizeMake(screenWidth * 4, screenHeight);
    self.scrollView.bounces = NO;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    //self.scrollView.delegate = self;
    [self.view addSubview:self.scrollView];
}

-(void)pressBtn
{
    //保存为已经进入app
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstIn"];
    [[NSUserDefaults standardUserDefaults]synchronize];//及时保存 不等到按home键就保存内容
    

    if (self.callBack) {
        _callBack();
    };
}




@end
