//
//  RootViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "RootViewController.h"

#import <Masonry.h>
#import "UILabel+Category.h"

///ChildVcs
#import "ReceiveViewController.h" ///Home       (1)
#import "OrderViewController.h"   ///MyOrder    (2)
#import "RecordViewController.h"  ///Reproting  (3)
#import "MineViewController.h"    ///Personal   (4)



///LoginVc
#import "LoginViewController.h"

#define ColumnNum 4.0 

#define TabbarItemW  Main_Width/ColumnNum

@interface RootViewController ()

@property (nonatomic, assign) BOOL tabBarIsShow;

@property (nonatomic, strong) NSMutableArray *buttonArray;

@property (nonatomic, strong) UIImageView *backImageView;

@property (nonatomic, strong) NSMutableArray * labelArray;

@end

static RootViewController *rootVC = nil;

@implementation RootViewController

+ (void)deallocRootVc {
    rootVC = nil;
}

+ (RootViewController *)defaultsTabBar {
    if (rootVC == nil) {
        rootVC = [[RootViewController alloc]init];
    }
    return  rootVC;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    
    
    ///初始化数据
    [self initData];
    
    ///添加子控制器
    [self addChildVcs];
    
    ///设置UI
    [self  setUpUI];

}

#pragma mark -
#pragma mark - 初始化数据
- (void)initData {
    self.buttonArray = @[].mutableCopy;
    self.labelArray = @[].mutableCopy;
    self.tabBarIsShow = YES;
    self.tabBar.hidden = YES;
    _rootString = @"1";
}


#pragma mark -
#pragma mark - 添加子控制器
- (void)addChildVcs {
    
    ReceiveViewController *receiveVC = [[ReceiveViewController alloc]init];
    UINavigationController *receiveNC = [[UINavigationController alloc]initWithRootViewController:receiveVC];
    
    
    OrderViewController *orderVC = [[OrderViewController alloc]init];
    UINavigationController *orderNC = [[UINavigationController alloc]initWithRootViewController:orderVC];
    
    
    RecordViewController *recordVC = [[RecordViewController alloc]init];
    UINavigationController *recordNC = [[UINavigationController alloc]initWithRootViewController:recordVC];
    
    
    MineViewController *mineVC = [[MineViewController alloc]init];
    UINavigationController *mineNC = [[UINavigationController alloc]initWithRootViewController:mineVC];
    
    self.viewControllers = @[receiveNC, orderNC, recordNC, mineNC];

}

#pragma mark -
#pragma mark - 设置UI布局
- (void)setUpUI {
    self.view.backgroundColor = WhiteColor;
    
    ///设置自定义Tabbar
    [self setCustomTabbarItem];
}

#pragma mark -
#pragma mark - 设置Tabbar
- (void)setCustomTabbarItem{
    self.backImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, screenHeight - 48*kHeight, screenWidth, 48*kHeight)];
    UIImageView * imageline = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 0.5)];
    imageline.backgroundColor = AddCarNameBtnColor;
    [self.backImageView addSubview:imageline];
    
    self.backImageView.backgroundColor = [UIColor whiteColor];
    //self.backImageView.image = [UIImage imageNamed:@"wireframe_bottom"];
    
    self.backImageView.userInteractionEnabled = YES;
    [self.view addSubview:self.backImageView];
    
    NSArray *titleArray = @[@"首页",
                            @"我的运单",
                            @"事故申报",
                            @"个人中心"];
    
    for (int i = 0; i < titleArray.count; i++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString * imageNormal = [NSString stringWithFormat:@"new_home_bottom_%d",i+1];
        NSString * imageSelected = [NSString stringWithFormat:@"new_home_bottom_%d_pre",i+1];
        [button setImage:[UIImage imageNamed:imageNormal] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageSelected] forState:UIControlStateSelected];
        button.tag = 1001 + i + 100;
        //        NSString *titleString = [NSString stringWithFormat:@"%@",titleArray[i]];
        //        [button setTitle:titleString forState:UIControlStateNormal];
        //        [button setTitleColor:GrayColor forState:UIControlStateNormal];
        //        button.titleLabel.font = Font(15);
        UIButton * buttonBig = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonBig.tag = 1001 + i;
        
        
        [buttonBig addTarget:self action:@selector(pressButton:)
            forControlEvents:UIControlEventTouchUpInside];
        //        buttonBig.backgroundColor = [UIColor cyanColor];
        //        button.tag = 1001 + i;
        [self.backImageView addSubview:button];
        if (i == 1)
        {
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.backImageView.mas_top).with.offset(8*kHeight);
                //                make.left.mas_equalTo(self.backImageView.mas_left).with.offset(20*kWidth+i*space);
                make.left.mas_equalTo((TabbarItemW - 29*kWidth)/2.0 + i*TabbarItemW);
                make.size.mas_equalTo(CGSizeMake(29*kWidth, 21*kHeight));
            }];
        }
        else
        {
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.backImageView.mas_top).with.offset(8*kHeight);
                //                make.left.mas_equalTo(self.backImageView.mas_left).with.offset(20*kWidth+i*space);
                make.left.mas_equalTo((TabbarItemW - 29*kWidth)/2.0 + i*TabbarItemW);
                
                make.size.mas_equalTo(CGSizeMake(22*kWidth, 21*kHeight));
            }];
            
        }
        [self.backImageView addSubview:buttonBig];
        [buttonBig mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backImageView.mas_top).with.offset(8*kHeight);
            make.left.mas_equalTo(self.backImageView.mas_left).with.offset(TabbarItemW*i);
            make.size.mas_equalTo(CGSizeMake(TabbarItemW, 46*kHeight));
        }];
        
        
        UILabel * label = [UILabel createUIlabel:titleArray[i]
                                         andFont:FontOfSize12
                                        andColor:littleBlackColor];
        label.text = titleArray[i];
        if (i == 0) {
            button.selected = YES;
            [button setTitleColor:BlackColor forState:UIControlStateNormal];
            label.textColor = YellowColor;
        }
        [self.backImageView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(button.mas_bottom).with.offset(2*kHeight);
            make.centerX.mas_equalTo(button.mas_centerX);
        }];
        [self.labelArray addObject:label];
        [self.buttonArray addObject:button];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
     }

- (void)selectButtonWithIndex:(NSInteger)index{
    UIButton *btn = (UIButton *)self.buttonArray[index];
    [self pressButton:btn];
}

- (void)setTabBarHidden:(BOOL)Bool{
    if (Bool) {
        [self hideTabBar];
    } else {
        [self showTabBar];
    }
}

- (void)hideTabBar{
    if (!self.tabBarIsShow) {
        return;
    }
    
    [UIView animateWithDuration:0.35 animations:^{
        self.backImageView.frame = CGRectMake(0, screenHeight, screenWidth, 48*kHeight);
    }];
    
    self.tabBarIsShow = NO;
}

- (void)showTabBar{
    if (self.tabBarIsShow) {
        return;
    }
    [UIView animateWithDuration:0.35 animations:^{
        self.backImageView.frame = CGRectMake(0, screenHeight - 48*kHeight, screenWidth, 48*kHeight);
        
        //        self.tabBarController.tabBar.hidden = NO;
        //        self.hidesBottomBarWhenPushed = NO;
        
        
    }];
    
    self.tabBarIsShow = YES;
}

- (void)pressButton:(UIButton *)sender{
    
    if (sender.tag - 1001 >= 5) {
        return;
    }
    
    
    if (sender.tag - 1001 != 1) {
        _rootString = @"1";
    }
    
    self.selectedIndex = sender.tag - 1001;
    for (UILabel * label in self.labelArray) {
        label.textColor = littleBlackColor;
    }
    UILabel * label = self.labelArray[self.selectedIndex];
    
    label.textColor = YellowColor;
    
    NSInteger smallBtn = sender.tag + 100;
   
    for (UIButton *tempBtn in self.buttonArray) {
       
        if (tempBtn.tag == smallBtn)
        {
           
            tempBtn.selected = YES;
            
            [tempBtn setTitleColor:BlackColor forState:UIControlStateNormal];
        }
        else
        {
            tempBtn.selected = NO;
           
            [tempBtn setTitleColor:GrayColor forState:UIControlStateNormal];
        }
    }
}








@end
