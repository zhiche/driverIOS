//
//  RootViewController.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 RootVC
 */
@interface RootViewController : UITabBarController
/**
 *  返回RootVc单例
 *  @return RootViewController
 */
+ (RootViewController *)defaultsTabBar;
/**
 * 手动销毁 RootViewController
 */
+ (void)deallocRootVc;
/**
 控制Tabbar显示和隐藏
 @param Bool
 */
- (void)setTabBarHidden:(BOOL)Bool;
/**
 设置当前选中的TabbarItem
 @param btn
 */
- (void)pressButton:(UIButton *)btn;
/**
 权限判断
 */
@property (nonatomic, copy) NSString *rootString;

@property (nonatomic, strong) NSString *orderType;
@end
