//
//  OrderTalkingViewController.m
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/4.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "OrderTalkingViewController.h"
#import "UILabel+Category.h"

#import "ActionSheetPicker.h"
#import "area.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "CustomTextField.h"
#define LocationTimeout 10
#define ReGeocodeTimeout 5

#define AreTag 1000
#define HoldingTag 2000
#define MySetValue(dict, value) ((!dict[value] || [dict[value] isKindOfClass:[NSNull class]])?@"":dict[value])

@interface OrderTalkingViewController ()<AMapLocationManagerDelegate,MAMapViewDelegate>
{
    Common * Com;
    area * AREA;
    NSMutableDictionary * dataDic;

}
@property (nonatomic, strong) AMapLocationManager *locationManager;
@property (nonatomic, copy) AMapLocatingCompletionBlock completionBlock;
@property (nonatomic, strong) UILabel *displayLabel;
@property (nonatomic, strong) UIView * backview1;
@property (nonatomic, strong) UIView * backview2;
@property (nonatomic, strong) NSMutableArray *holdingArray;

@property (nonatomic, strong) NSMutableArray *provienceArray;
@property (nonatomic, strong) NSArray *cityArray;
@property (nonatomic, strong) NSArray *areaArray;
//@property (nonatomic, strong) UIButton * destinationBtn;
@property (nonatomic, strong) UIButton * holdBtn;

@property (nonatomic, strong) NSMutableDictionary *dicUrl;


@end

@implementation OrderTalkingViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = WhiteColor;
    Com = [[Common alloc]init];
    AREA = [[area alloc]init];
    dataDic= [[NSMutableDictionary alloc]init];
    
    _dicUrl =[[NSMutableDictionary alloc]init];
    [_dicUrl setObject:@"" forKey:@"currentAddrDesc"];
    [_dicUrl setObject:@"" forKey:@"latitude"];
    [_dicUrl setObject:@"" forKey:@"longitude"];
    [_dicUrl setObject:@"" forKey:@"emptyStore"];
    [_dicUrl setObject:@"" forKey:@"receiptAddress"];
    
    

    [self createData];
    
    [self configLocationManager];
    [self initDisplayLabel];
    [self initCompleteBlock];
    [self reGeocodeAction];
    

}

-(void)createData{
    
    
    [Common requestWithUrlString:onlineinfo contentType:@"application/json" errorShow:YES finished:^(id responseObj){
        
        
        NSLog(@"%@",responseObj);
        dataDic = responseObj[@"data"];
        [self createUI];

        
    } failed:^(NSString *errorMsg) {
        NSLog(@"%@",errorMsg);
    }];

}
-(void)createUI{
    
    
    UIView * nav = [[UIView alloc]init];
    nav.backgroundColor = YellowColor;
    nav.frame = CGRectMake(0, 0, Main_Width, 20);
    [self.view addSubview:nav];
    
    UIImageView *logoImageV = [[UIImageView alloc]initWithFrame:CGRectMake((screenWidth - 108)/2.0, 83 * kHeight, 108, 40)];
    logoImageV.image = [UIImage imageNamed:@"register_logo"];
    
    [self.view addSubview:logoImageV];
    

    _backview1 = [[UIView alloc]init];
    _backview1.layer.borderWidth = 0.5;
    _backview1.layer.borderColor = GrayColor.CGColor;
    _backview1.layer.cornerRadius = 20.0;
    
    _backview2 = [[UIView alloc]init];
    _backview2.layer.borderWidth = 0.5;
    _backview2.layer.borderColor = GrayColor.CGColor;
    _backview2.layer.cornerRadius = 20.0;
    
    
    [self.view addSubview:_backview1];
    [self.view addSubview:_backview2];
    [_backview1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(30*kWidth);
        make.top.equalTo(self.view.mas_top).with.offset(175*kHeight);
        make.size.mas_equalTo(CGSizeMake(260*kWidth, 40*kHeight));
    }];
    [_backview2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(30*kWidth);
        make.top.equalTo(_backview1.mas_bottom).with.offset(20*kHeight);
        make.size.mas_equalTo(CGSizeMake(260*kWidth, 40*kHeight));
    }];
    


    UILabel * label1 = [UILabel createUIlabel:@"目的地" andFont:FontOfSize14 andColor:fontGrayColor];
    [_backview1 addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left).with.offset(20*kWidth);
        make.centerY.equalTo(_backview1.mas_centerY);
    }];
    UILabel * label2 = [UILabel createUIlabel:@"仓位" andFont:FontOfSize14 andColor:fontGrayColor];
    [_backview2 addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview2.mas_left).with.offset(20*kWidth);
        make.centerY.equalTo(_backview2.mas_centerY);
    }];

    CustomTextField * field = [self createField:@"请输入目的地" andTag:100 andFont:FontOfSize14];
    [field addTarget:self action:@selector(FieldWithText:) forControlEvents:UIControlEventEditingChanged];
    
    [_backview1 addSubview:field];
    [field mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview1.mas_left);
        make.top.equalTo(_backview1.mas_top);
        make.bottom.equalTo(_backview1.mas_bottom);
        make.right.equalTo(_backview1.mas_right);
    }];
    CustomTextField * holdField = [self createField:@"请输入空仓位" andTag:200 andFont:FontOfSize14];
    [holdField addTarget:self action:@selector(FieldWithText:) forControlEvents:UIControlEventEditingChanged];
    [_backview2 addSubview:holdField];
    [holdField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_backview2.mas_left);
        make.top.equalTo(_backview2.mas_top);
        make.bottom.equalTo(_backview2.mas_bottom);
        make.right.equalTo(_backview2.mas_right);
    }];

    UILabel * label3 = [UILabel createUIlabel:@"小慧提示您请不要疲劳驾驶呦!" andFont:FontOfSize13 andColor:YellowColor];
    [_backview2 addSubview:label3];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(holdField.mas_bottom).with.offset(28*kHeight);
        make.centerX.equalTo(_backview2.mas_centerX);
    }];
    
    UIImageView * image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"home_prompt"];
    [_backview2 addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(label3.mas_left).with.offset(-3*kWidth);
        make.centerY.equalTo(label3.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(15*kWidth, 15*kHeight));
    }];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.layer.cornerRadius = 63.5*kWidth;
    button.layer.borderWidth = 5;
    button.layer.borderColor = WhiteColor.CGColor;
    button.titleLabel.font = Font(25.0);
    [button setTitle:@"接 单" forState:UIControlStateNormal];
    button.backgroundColor = YellowColor;
    [button setTitleColor:WhiteColor forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(orderTalking:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:button];
    
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_backview2.mas_centerX);
        make.centerY.equalTo(_backview2.mas_centerY).offset(Main_height/2-128*kHeight);
        make.size.mas_equalTo(CGSizeMake(127*kWidth, 127*kHeight));
    }];


    NSString *driver = [NSString stringWithFormat:@"%@",dataDic[@"driver"]];
    NSString *order = [NSString stringWithFormat:@"%@",dataDic[@"order"]];
    NSString * string = [NSString stringWithFormat:@"在线司机%@位  %@运单等您报价",driver,order];

    int driverInt = (int)driver.length;
    int orderInt = (int)order.length;
    int stringInt = (int)string.length;
    NSLog(@"driverInt=%dorderInt=%dstringInt=%d",driverInt,orderInt,stringInt);
    UILabel * label4 = [UILabel createUIlabel:@"" andFont:FontOfSize12 andColor:fontGrayColor];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    //在线司机
    [str addAttribute:NSForegroundColorAttributeName value:fontGrayColor range:NSMakeRange(0,4)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize12] range:NSMakeRange(0, 4)];
   //12345
    [str addAttribute:NSForegroundColorAttributeName value:YellowColor range:NSMakeRange(4,driverInt)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize15] range:NSMakeRange(4, driverInt)];
   // 2+位
    [str addAttribute:NSForegroundColorAttributeName value:fontGrayColor range:NSMakeRange(4+driverInt+2,1)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize12] range:NSMakeRange(4+driverInt+2,1)];
  //12345
    [str addAttribute:NSForegroundColorAttributeName value:YellowColor range:NSMakeRange(4+driverInt+1+2,orderInt)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize15] range:NSMakeRange(4+driverInt+1+2,orderInt)];
   //运单等您报价
    [str addAttribute:NSForegroundColorAttributeName value:fontGrayColor range:NSMakeRange(4+driverInt+1+1+1+orderInt,orderInt)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:FontOfSize12] range:NSMakeRange(4+driverInt+1+1+1+orderInt,orderInt)];
    label4.attributedText = str;
    [_backview2  addSubview:label4];
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(button.mas_bottom).with.offset(25*kHeight);
        make.centerX.equalTo(_backview2.mas_centerX);
    }];
}


-(void)FieldWithText:(CustomTextField*)field{
    
    if (field.tag ==100) {
        [_dicUrl setObject:field.text forKey:@"receiptAddress"];
    }else{
        [_dicUrl setObject:field.text forKey:@"emptyStore"];
        
    }
}


-(void)pressBtnArea{
    
    _provienceArray = nil;
    _cityArray = nil;
    _areaArray = nil;
    
    _provienceArray = [AREA selectAreaWithFArea:nil withTarea:nil withFarea:1];
    _cityArray = [AREA selectAreaWithFArea:@"北京市" withTarea:nil withFarea:2];
    _areaArray = [AREA selectAreaWithFArea:@"北京市" withTarea:@"北京市" withFarea:3];
    
    UIImageView * image = (UIImageView*)[self.view viewWithTag:1000];
    image.transform = CGAffineTransformMakeRotation(M_PI_2);
    UIPickerView *education = [[UIPickerView alloc] init];
    education.tag = AreTag;
    ActionSheetPicker *actionSheet=[[ActionSheetPicker alloc]initWithTarget:self successAction:@selector(selectOK:) cancelAction:nil origin:education];
    
    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
    [education setFrame:pickerFrame];
    education.delegate = self;
    education.dataSource = self;
    //pickview  左选择按钮的文字
    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
    leftBtn.frame=CGRectMake(0, 0, 50, 45);
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    //pickview    右选择按钮的文字
    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    rightBtn.frame=CGRectMake(0, 0, 50, 45);
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    //将按钮添加到pickerview 上
    [actionSheet setDoneButton:rightIte];
    [actionSheet setCancelButton:leftIte];
    actionSheet.pickerView = education;
    actionSheet.title = @"地址选择";
    [actionSheet showActionSheetPicker];
    NSLog(@"点击的是地区选择");
    
}

-(void)pressBtnHold{
    
    _holdingArray = nil;

    _holdingArray = [[NSMutableArray alloc]init];
    for (int i=0; i<16; i++) {
        
        [_holdingArray addObject:[NSString stringWithFormat:@"%d",i]];
        
    }
    UIImageView * image = (UIImageView*)[self.view viewWithTag:1000];
    image.transform = CGAffineTransformMakeRotation(M_PI_2);
    UIPickerView *education = [[UIPickerView alloc] init];
    education.tag = HoldingTag;
    ActionSheetPicker *actionSheet=[[ActionSheetPicker alloc]initWithTarget:self successAction:@selector(selectOK:) cancelAction:nil origin:education];
    CGRect pickerFrame = CGRectMake(0, 40, actionSheet.viewSize.width, 216);
    [education setFrame:pickerFrame];
    education.delegate = self;
    education.dataSource = self;
    //pickview  左选择按钮的文字
    UIButton *leftBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setTitle:@"取消"forState:UIControlStateNormal];
    leftBtn.frame=CGRectMake(0, 0, 50, 45);
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *leftIte=[[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    //pickview    右选择按钮的文字
    UIButton *rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    rightBtn.frame=CGRectMake(0, 0, 50, 45);
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightIte=[[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    //将按钮添加到pickerview 上
    [actionSheet setDoneButton:rightIte];
    [actionSheet setCancelButton:leftIte];
    actionSheet.pickerView = education;
    actionSheet.title = @"空余仓位选择";
    [actionSheet showActionSheetPicker];
    NSLog(@"点击的是地区选择");
    

}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component

{
    
    
    NSInteger ss = [pickerView selectedRowInComponent:0];
    if (pickerView.tag == AreTag) {
        NSInteger dd = [pickerView selectedRowInComponent:1];
        
        switch (component) {
            case 0:
            {
                NSDictionary *dict = [_provienceArray objectAtIndex:row];
                NSString *code = MySetValue(dict, @"provinceName");
                if (code.length > 0)
                {
                    _cityArray = [AREA selectAreaWithFArea:code withTarea:nil withFarea:2];
                    [pickerView reloadComponent:1];
                    dd = 0;
                    NSDictionary *cityDict = [_cityArray objectAtIndex:dd];
                    NSString * cityFcode = MySetValue(cityDict, @"cityName");
                    if (cityFcode.length > 0)
                    {
                        _areaArray = [AREA selectAreaWithFArea:code withTarea:cityFcode withFarea:3];
                        [pickerView reloadComponent:2];
                    }
                }
            }
                break;
            case 1:
            {
                NSDictionary *dict = [_cityArray objectAtIndex:row];
                NSString *code = MySetValue(dict, @"cityName");
                
                NSDictionary * dics = [_provienceArray objectAtIndex:ss];
                NSString * str = MySetValue(dics, @"provinceName");
                NSLog(@"%@",code);
                
                NSLog(@"%@",str);
                if (code.length > 0) {
                    _areaArray = [AREA selectAreaWithFArea:str withTarea:code withFarea:3];
                    [pickerView reloadComponent:2];
                }
            }
                break;
            case 2:{
                
            }
                break;
                
            default:
                break;
        }
    }else{
        
        
        
    }
}


- (void)selectOK:(id)sender{
    
    UIPickerView *pick = sender;
    
    if (pick.tag == AreTag) {
        //NSInteger row = [pick selectedRowInComponent:0];
        //NSDictionary *dict = [_provienceArray objectAtIndex:row];
        //NSString *provinceName = MySetValue(dict, @"provinceName");//省名称
        //NSString *provinceCode = MySetValue(dict, @"provinceCode");//省编码
        
        //NSInteger row1 = [pick selectedRowInComponent:1];
        //NSDictionary *dict1 = [_cityArray objectAtIndex:row1];
        //NSString *cityName = MySetValue(dict1, @"cityName");//市名称
        //NSString *citycode = MySetValue(dict1, @"cityCode");//市编码
        
        //NSInteger row2 = [pick selectedRowInComponent:2];
        //NSDictionary *dict2 = [_areaArray objectAtIndex:row2];
        //NSString *countyName = MySetValue(dict2, @"countyName");//县名称
        //NSString * countycode = MySetValue(dict2, @"countyCode");//县编码
         /*
         [dic setObject:provinceCode forKey:@"provincecode"];//省编码
         [dic setObject:provinceName forKey:@"provincename"]; //省名称
         [dic setObject:citycode forKey:@"citycode"];//市编码
         [dic setObject:cityName forKey:@"cityname"]; //市名称
         [dic setObject:countycode forKey:@"countycode"];//县编码
         [dic setObject:countyName forKey:@"countyname"]; //县名称
         */
        //NSString * btnName = [NSString stringWithFormat:@"%@%@%@",provinceName,cityName,countyName];
        //[_destinationBtn setTitle:btnName forState:UIControlStateNormal];
        UIImageView * image = (UIImageView*)[self.view viewWithTag:1000];
        image.transform = CGAffineTransformMakeRotation(0);
    }else{
        
        NSInteger row = [pick selectedRowInComponent:0];
        NSString * string =[NSString stringWithFormat:@"%@",_holdingArray[row]];
        [_holdBtn setTitle:string forState:UIControlStateNormal];
    }
}

//  跳出的pickerview  显示列数
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView.tag == AreTag) {
        return 3;
    }else{
        return 1;
    }
}

//显示pickerview 每个视图的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component

{
    if (pickerView.tag == AreTag) {
        switch (component) {
            case 0:
                return _provienceArray.count;
                break;
            case 1:
                return _cityArray.count;
                break;
            case 2:
                return _areaArray.count;
                break;
            default:
                break;
        }
    }else{
        
        return _holdingArray.count;
    }
    return 0;
}

//显示每个pickerview  每行的具体内容
-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view

{
    UILabel *lable  = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
    lable.adjustsFontSizeToFitWidth = YES;
    lable.textAlignment = NSTextAlignmentCenter;
    if (pickerView.tag == AreTag) {
        switch (component) {
            case 0:{
                NSDictionary *dict = [_provienceArray objectAtIndex:row];
                lable.text = MySetValue(dict, @"provinceName");
            }
                break;
            case 1:{
                
                NSDictionary *dict = [_cityArray objectAtIndex:row];
                lable.text = MySetValue(dict, @"cityName");
            }
                break;
                
            case 2:{
                
                NSDictionary *dict = [_areaArray objectAtIndex:row];
                lable.text = MySetValue(dict, @"countyName");
            }
                break;
            default:
                break;
        }
    }else{
    
        lable.text = [NSString stringWithFormat:@"%@",_holdingArray[row]];
    }
    return lable;
}
-(void)orderTalking:(UIButton *)sender
{
    
//    journey_Url
    
    NSLog(@"journey_Url==%@",journey_Url);
    NSLog(@"_dicUrl===%@",_dicUrl);
    
    [Common afPostRequestWithUrlString:journey_Url parms:_dicUrl finishedBlock:^(id responseObj) {
        NSMutableDictionary * dicObj =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",dicObj);
        NSString * success = [NSString stringWithFormat:@"%@",dicObj[@"success"]];
        if ([success isEqualToString:@"1"]) {
            [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"dismiss" object:self];
            }];
        }else{
            [self createUIAlertController:[NSString stringWithFormat:@"%@",dicObj[@"message"]]];
        }
        
    } failedBlock:^(NSString *errorMsg) {
        
    }];

    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)initCompleteBlock
{
    __weak OrderTalkingViewController *wSelf = self;
    self.completionBlock = ^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error)
    {
        if (error)
        {
            NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
            
            if (error.code == AMapLocationErrorLocateFailed)
            {
                return;
            }
        }
        
        if (location)
        {
            if (regeocode)
            {
                //                [wSelf.displayLabel setText:[NSString stringWithFormat:@"%@ \n %@-%@-%.2fm", regeocode.formattedAddress,regeocode.citycode, regeocode.adcode, location.horizontalAccuracy]];
                
//                UILabel * label = [[UILabel alloc]init];
//                label.frame = CGRectMake(100, 100, 100, 100);
//                label.textColor = BlackColor;
//                [wSelf.view addSubview:label];
//                label.text = [NSString stringWithFormat:@"%@ \n %@-%@-%.2fm", regeocode.formattedAddress,regeocode.citycode, regeocode.adcode, location.horizontalAccuracy];
//                
                
                [wSelf.dicUrl setObject:[NSString stringWithFormat:@"%f",location.coordinate.latitude] forKey:@"latitude"];
                [wSelf.dicUrl setObject:[NSString stringWithFormat:@"%f",location.coordinate.longitude] forKey:@"longitude"];
                
                
                if (regeocode.city == nil) {
                    
                    regeocode.city = @"";
                }
                if (regeocode.province == nil) {
                    
                    regeocode.province = @"";
                }
                if (regeocode.district == nil) {
                    
                    regeocode.district = @"";
                }

                
                NSString * address = [NSString stringWithFormat:@"%@%@%@",regeocode.province,regeocode.city,regeocode.district];
                [wSelf.dicUrl setObject:address forKey:@"currentAddrDesc"];
            }
            else
            {
                [wSelf.displayLabel setText:[NSString stringWithFormat:@"lat:%f;lon:%f \n accuracy:%.2fm", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy]];
            }
        }
    };
}

- (void)configLocationManager
{
    self.locationManager = [[AMapLocationManager alloc] init];
    
    [self.locationManager setDelegate:self];
    
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    
    [self.locationManager setPausesLocationUpdatesAutomatically:NO];
    
    [self.locationManager setAllowsBackgroundLocationUpdates:YES];
    
    [self.locationManager setLocationTimeout:LocationTimeout];
    
    [self.locationManager setReGeocodeTimeout:ReGeocodeTimeout];
    
}

- (void)initDisplayLabel
{
    _displayLabel = [[UILabel alloc] init];
    _displayLabel.backgroundColor  = [UIColor clearColor];
    _displayLabel.textColor        = [UIColor blackColor];
    _displayLabel.textAlignment = NSTextAlignmentCenter;
    _displayLabel.numberOfLines = 0;
    [_displayLabel setFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:_displayLabel];
}

//- (void)returnAction
//{
//    [self cleanUpAction];
//    
//    self.completionBlock = nil;
//    
//    [self.navigationController popViewControllerAnimated:YES];
//}
//- (void)cleanUpAction
//{
//    [self.locationManager stopUpdatingLocation];
//    
//    [self.locationManager setDelegate:nil];
//}
- (void)reGeocodeAction
{
    [self.locationManager requestLocationWithReGeocode:YES completionBlock:self.completionBlock];
}


-(CustomTextField*)createField:(NSString*)placeholder andTag:(NSInteger)tag andFont:(double)font{
    
    CustomTextField * field =[[CustomTextField alloc]init];
    field.frame = CGRectMake(0, 0, Main_Width-90*kWidth, 40*kWidth);
    field.delegate = self;
    field.userInteractionEnabled = YES;
    field.tag = tag;
    field.textAlignment = NSTextAlignmentCenter;
    field.placeholder = placeholder;
    CGFloat width = field.frame.size.width;
    CGFloat height = field.frame.size.height;
    //控制placeHolder的位置
    [field placeholderRectForBounds:CGRectMake(width/2, 0, width, height)];
    [field editingRectForBounds:CGRectMake(width/2, 0, width, height)];
    [field leftViewRectForBounds:CGRectMake(width/2, 0, width, height)];
    [field textRectForBounds:CGRectMake(width/2, 0, width, height)];
    
    return field;
}
-(void)createUIAlertController:(NSString*)title

{
    UIAlertController * alert =[UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action =[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    UIAlertAction * action1 =[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:action1];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
