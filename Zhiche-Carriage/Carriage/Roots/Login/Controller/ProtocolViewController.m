//
//  ProtocolViewController.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/18.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "ProtocolViewController.h"
#import "TopBackView.h"

@interface ProtocolViewController ()<UIWebViewDelegate>

@end

@implementation ProtocolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    
    TopBackView *topBackView= [[TopBackView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64) title:@"用户协议"];
    
    topBackView.rightButton.hidden = YES;
    [topBackView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:topBackView];
    
    self.view.backgroundColor = WhiteColor;
    
    [self initWebViews];

}

-(void)initWebViews
{
 
    
    NSURL  *url = [NSURL URLWithString:templates_Url];
    
    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 64)];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    webView.delegate = self;
    webView.backgroundColor = [UIColor whiteColor];
    
    [webView loadRequest:request];
    
    [self.view addSubview:webView];
 
}



-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
