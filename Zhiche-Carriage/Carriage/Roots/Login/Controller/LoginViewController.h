//
//  LoginViewController.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

///自定义导航栏
#import "TopBackView.h"

/**
 登录Vc
 */
@interface LoginViewController : UIViewController

@property (nonatomic, strong) TopBackView *topBackView;

@property (nonatomic,copy) NSString *backToFirst;

@end
