//
//  OrderTalkingViewController.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/7/4.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTalkingViewController : UIViewController<UIActionSheetDelegate,UIAlertViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>

@end
