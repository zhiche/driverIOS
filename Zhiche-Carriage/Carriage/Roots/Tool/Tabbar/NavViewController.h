//
//  NavViewController.h
//  GL
//
//  Created by 王亚陆 on 16/2/23.
//  Copyright © 2016年 知车科技. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UMSocial.h"
@interface NavViewController : UIViewController<UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView * navView;
@property (nonatomic, strong) UIImageView * navImage;
@property (nonatomic, strong) UILabel * navTitleLabel;
@property (nonatomic, strong) UIButton * leftBtn;
@property (nonatomic, strong) UIButton * FristRightBtn;
@property (nonatomic, strong) UIButton * TwoRightBtn;
@property (nonatomic, strong) NSString * collect_id;
@property (nonatomic, strong) NSString * collect_type;
@property (nonatomic, strong) NSString * collect_state;
@property (nonatomic, assign) BOOL isTwo;


// 导航栏
- (UIView*)createNavView;
- (void)createNavImage:(NSString *)imageName;
- (void)createReturnButton;
- (void)createNavTitle:(NSString *)text;
- (void)createNavLeftBtn:(NSString *)textName;
- (void)createNavLeftImageBtn:(NSString *)imageName;
- (void)createNavRightBtn:(NSString *)textName;
- (void)createNavRightImageBtn:(NSString *)imageName;
- (void)createNavTwoRightImageBtn:(NSString *)imageName;
- (void)navLeftBtnClick;
- (void)navRightBtnClick;
-(UIImageView*)createNav:(NSString *)title;


@end
