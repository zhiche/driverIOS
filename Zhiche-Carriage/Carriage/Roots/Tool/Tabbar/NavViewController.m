//
//  NavViewController.m
//  GL
//
//  Created by 王亚陆 on 16/2/23.
//  Copyright © 2016年 知车科技. All rights reserved.
//

#import "NavViewController.h"
#import "Header.h"
#import "Masonry.h"
#import "Header.h"
#import "Common.h"
#import "ReceiveViewController.h"
@interface NavViewController ()
{
 
    Common * Com ;
}
@end

@implementation NavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _isTwo = NO;
    [self initBaseData];
    [self addSwipeRecognizer];

}
- (void)addSwipeRecognizer
{
    // 初始化手势并添加执行方法
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(return)];
    
    // 手势方向
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    
    // 响应的手指数
    swipeRecognizer.numberOfTouchesRequired = 1;
    
    // 添加手势
    [[self view] addGestureRecognizer:swipeRecognizer];
}

#pragma mark 返回上一级
- (void)return
{
    // 最低控制器无需返回
    if (self.navigationController.viewControllers.count <= 1) return;
    
    // pop返回上一级
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)initBaseData {
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBar.hidden = YES;
    [self createNavView];
}
// 导航条
- (UIView*)createNavView {
    _navView = [[UIView alloc]init];
    _collect_id = [[NSString alloc]init];
    _collect_type = [[NSString alloc]init];
    _collect_state = [[NSString alloc]init];
    Com = [[Common alloc]init];
    _navView.frame =CGRectMake(0, 20, Main_Width, 44);
//    _navView.backgroundColor = YellowColor;
    return _navView;
}
// 导航条图片
- (void)createNavImage:(NSString *)imageName {
    //    WS(ws)
    UIImage * image = [UIImage imageNamed:imageName];
    if (! _navImage) {
        _navImage = [[UIImageView alloc]initWithImage:image];
        [_navView addSubview:_navImage];
        [_navImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.navView.mas_centerY).with.offset(0);
            make.centerX.equalTo(self.navView);
            make.size.mas_equalTo(image.size);
        }];
    }
}
// 导航条标题
- (void)createNavTitle:(NSString *)title {
    if (! _navTitleLabel) {
        _navTitleLabel = [[UILabel alloc]init];
        _navTitleLabel.text = title;
        _navTitleLabel.textColor = WhiteColor;
        _navTitleLabel.font = Font(15);
        self.navView.backgroundColor = YellowColor;
        
        
        [self.navView addSubview:_navTitleLabel];
        [_navTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.navView.mas_centerX);
            make.top.mas_equalTo(self.navView.mas_top).with.offset(0);
            make.height.mas_equalTo(44);
        }];
        UIView * view = [[UIView alloc]init];
        view.backgroundColor = YellowColor;
        view.frame = CGRectMake(0, 0, Main_Width, 20);
        [self.view addSubview:view];
    }
}
// 返回按钮
- (void)createReturnButton {
    if (! _leftBtn) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _navView.backgroundColor =RGBACOLOR(242, 242, 242, 1);
        [_navView addSubview:_leftBtn];
        [_leftBtn setBackgroundImage:[UIImage imageNamed:@"common_btn_top_back"] forState:UIControlStateNormal];
        
        [_leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.navView).with.offset(14);
            make.centerY.mas_equalTo(_navView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(12, 22));
        }];
        [_leftBtn addTarget:self action:@selector(goBackLast) forControlEvents:UIControlEventTouchUpInside];

    }
    
}
// 导航条左边标题按钮
- (void)createNavLeftBtn:(NSString *)btnName {
    if (! _leftBtn) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        [_leftBtn setTitle:btnName forState:UIControlStateNormal];
        
        [_navView addSubview:_leftBtn];
        [_leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.navView);
            make.top.equalTo(@20);
            make.size.mas_equalTo(CGSizeMake(30, 44));
        }];
        [_leftBtn addTarget:self action:@selector(leftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
}
// 导航条左边图片按钮
- (void)createNavLeftImageBtn:(NSString *)imageName {
    if (! _leftBtn) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftBtn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        _leftBtn.adjustsImageWhenHighlighted = NO;
        [_navView addSubview:_leftBtn];
        [_leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.navView.mas_left).with.offset(14);
        make.top.equalTo(@28);
        make.size.mas_equalTo(CGSizeMake(28, 28));
    }];
        [_leftBtn addTarget:self action:@selector(leftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
}
// 导航条右边标题按钮
- (void)createNavRightBtn:(NSString *)btnName {
    if (! _FristRightBtn) {
        _FristRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_navView addSubview:_FristRightBtn];
        _FristRightBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        
        [_FristRightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.navView);
            make.top.equalTo(@20);
            make.size.mas_equalTo(CGSizeMake(30, 44));
        }];
//        [_FristRightBtn addTarget:self action:@selector(rightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
}
// 导航条右边图片按钮
- (void)createNavRightImageBtn:(NSString *)imageName {
    if (! _FristRightBtn) {
        _FristRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_navView addSubview:_FristRightBtn];
        _FristRightBtn.adjustsImageWhenHighlighted =NO;
        [_FristRightBtn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        
        [_FristRightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.navView).with.offset(- 14);
            make.top.equalTo(@28);
            make.size.mas_equalTo(CGSizeMake(28, 28));
        }];
        [_FristRightBtn addTarget:self action:@selector(FristRightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
}
//导航条右边第二个图片按钮
-(void)createNavTwoRightImageBtn:(NSString *)imageName{
    if (! _TwoRightBtn) {
        _TwoRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _TwoRightBtn.adjustsImageWhenHighlighted = NO;
        if ([_collect_state isEqualToString:@"0"]) {
            _TwoRightBtn.selected = NO;
        }
        else{
            _TwoRightBtn.selected = YES;
        }
        [_navView addSubview:_TwoRightBtn];
        
        [_TwoRightBtn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [_TwoRightBtn setBackgroundImage:[UIImage imageNamed:@"common_list_collect_press"] forState:UIControlStateSelected];
        [_TwoRightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_FristRightBtn.mas_left).with.offset(-15);
            make.top.equalTo(@28);
            make.size.mas_equalTo(CGSizeMake(28, 28));
        }];
        [_TwoRightBtn addTarget:self action:@selector(TwoRightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }

}

-(UIImageView*)createNav:(NSString *)title{
    
    UIView * view = [[UIView alloc]init];
    view.backgroundColor = YellowColor;
    view.frame = CGRectMake(0, 0, Main_Width, 20);
    [self.view addSubview:view];
    
    UIImageView* navImage = [[UIImageView alloc]init];
    navImage.frame = CGRectMake(0, 20, Main_Width, 44);
    navImage.backgroundColor = YellowColor;
    navImage.userInteractionEnabled = YES;
    UILabel * title1 = [self createUIlabel:title andFont:FontOfSize15 andColor:[UIColor blackColor]];
    title1.textColor = WhiteColor;
    [navImage addSubview:title1];
   
    //    UIImageView * imageline =[[UIImageView alloc]init];
    //    imageline.image = [UIImage imageNamed:@"common_list_line1"];
    //    [navImage addSubview:imageline];
    //    [imageline mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.mas_equalTo(navImage).with.offset(0);
    //        make.size.mas_equalTo(CGSizeMake(Main_Width, 0.5));
    //        make.bottom.mas_equalTo(navImage.mas_bottom);
    //    }];
    UIButton * btn =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"common_back_btn"] forState:UIControlStateNormal];
    
    btn.adjustsImageWhenHighlighted = NO;
    //    btn.backgroundColor = [UIColor redColor];
    [navImage addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(navImage.mas_left).with.offset(14);
        make.bottom.mas_equalTo(navImage.mas_bottom).with.offset(-12*kHeight);
        make.size.mas_equalTo(CGSizeMake(10, 18));
    }];
    UIButton * btnBig =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnBig addTarget:self action:@selector(goBackLast) forControlEvents:UIControlEventTouchUpInside];
    [navImage addSubview:btnBig];
    [btnBig mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(navImage.mas_left);
        make.centerY.mas_equalTo(navImage.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(50, 44));
    }];
    
    [title1 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(navImage.mas_centerX);
        make.centerY.mas_equalTo(navImage.mas_centerY);
        make.top.mas_equalTo(navImage.mas_top).with.offset(15);
    }];
    
    
    
    return navImage;
}
-(UILabel*)createUIlabel:(NSString *)title andFont:(CGFloat)font andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}


// 左边按钮点击事件
- (void)leftBtnClick:(UIButton *)btn {
    [self navLeftBtnClick];
}
// 右边按钮点击事件
- (void)FristRightBtnClick:(UIButton *)btn {
    [self navRightBtnClick];
}
- (void)TwoRightBtnClick:(UIButton *)btn {
    
    NSLog(@"点击的是收藏");
    NSString * obj_id = _collect_id;
    NSString * type = _collect_type;
    
    if (btn.selected) {
        btn.selected = NO;
    }
    else{
        btn.selected = YES;
    }
    [self collect:obj_id and:type];
}

-(void)collect:(NSString*)obj_id and:(NSString*)type{
    
//    NSString * stringURL = [NSString stringWithFormat:@"%@&obj_id=%@&type=%@&oauth_token=4df95a191a8cc3cb248d1fedce6abfb2&oauth_token_secret=cfbc73d7d332f1c09f285b6bc4423aa1",Googs_collect_Url,obj_id,type];
//    
//    [Com afPostRequestWithUrlString:stringURL parms:nil finishedBlock:^(id responseObj) {
//        
//        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
//        
//        UIImageView * imageCollect = [Com createUIImage:dic[@"data"][@"success"] andDelay:1];
//        [self.view addSubview:imageCollect];
//        [imageCollect mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.equalTo(self.view.mas_centerX);
//            make.centerY.equalTo(self.view.mas_centerY);
//        }];
//        
//        
//    } failedBlock:^(NSString *errorMsg) {
//        NSLog(@"%@",errorMsg);
//    }];
//
}
- (void)navLeftBtnClick {
    
    NSLog(@"点击的是返回");
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (void)navRightBtnClick {
    
//    [UMSocialSnsService presentSnsIconSheetView:self appKey:@"56dafe3d67e58e97da0022ba" shareText:@"分享" shareImage:[UIImage imageNamed:@"icon"]
// shareToSnsNames:[NSArray arrayWithObjects:UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQQ,nil] delegate:self];
//
//    [UMSocialSnsService presentSnsIconSheetView:self
//                                         appKey:@"507fcab25270157b37000010"
//                                      shareText:@"你要分享的文字"
//                                     shareImage:[UIImage imageNamed:@"icon.png"]
//                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToWechatSession,UMShareToQQ,nil]
//                                       delegate:self];
    
    NSLog(@"点击的是分享");
    
    UIView * backView = [[UIView alloc]init];
    backView.backgroundColor = RGBACOLOR(0, 0, 0, 0.3);
    backView.tag = 100000;
    backView.frame = CGRectMake(0, 0, Main_Width, Main_height+200);
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ShareBackView)];
    tap.delegate = self;
    [backView addGestureRecognizer:tap];
    
    UIImageView * image1 = [[UIImageView alloc]init];
    image1.backgroundColor = [UIColor whiteColor];
    UIImageView * image2 = [[UIImageView alloc]init];
    image2.backgroundColor = [UIColor whiteColor];
    image1.userInteractionEnabled = YES;
    image2.userInteractionEnabled = YES;
    image1.layer.cornerRadius=5.0;
    image1.layer.masksToBounds=YES;
    
    image2.layer.cornerRadius=5.0;
    image2.layer.masksToBounds=YES;
    

    image1.frame = CGRectMake(14, Main_height, Main_Width-28, 110);
    image2.frame = CGRectMake(14, Main_height+115, Main_Width-28, 55);
    [backView addSubview:image1];
    [backView addSubview:image2];
    [UIView animateWithDuration:0.3 animations:^{
        backView.frame = CGRectMake(0, -177, Main_Width, Main_height+200);
    }];


    
    UIButton * btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setBackgroundImage:[UIImage imageNamed:@"aboutus_share_icon_weixin"] forState:UIControlStateNormal];
    btn1.tag = 100;
    [btn1 addTarget:self action:@selector(pressShareBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIButton * btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 setBackgroundImage:[UIImage imageNamed:@"aboutus_share_icon_pengyouquan"] forState:UIControlStateNormal];
    btn2.tag = 200;
    [btn2 addTarget:self action:@selector(pressShareBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton * btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn3 setBackgroundImage:[UIImage imageNamed:@"aboutus_share_icon_qq"] forState:UIControlStateNormal];
    btn3.tag = 300;
    [btn3 addTarget:self action:@selector(pressShareBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton * btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn4 setBackgroundImage:[UIImage imageNamed:@"aboutus_share_icon_weibo"] forState:UIControlStateNormal];
    btn4.tag = 400;
    [btn4 addTarget:self action:@selector(pressShareBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIButton * cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.backgroundColor = [UIColor whiteColor];
    cancelBtn.tag = 500;
    [cancelBtn addTarget:self action:@selector(ShareBackView) forControlEvents:UIControlEventTouchUpInside];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:RGBACOLOR(149, 149, 149, 1) forState:UIControlStateNormal];
    [image2 addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(image2.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(Main_Width-28, 55));
        make.centerY.equalTo(image2.mas_centerY);
    }];

    
    UILabel * label1 = [self createUIlabel:@"微信好友" andFont:FontOfSize12 andColor:RGBACOLOR(149, 149, 149, 1)];
    UILabel * label2 = [self createUIlabel:@"微信朋友圈" andFont:FontOfSize12 andColor:RGBACOLOR(149, 149, 149, 1)];
    UILabel * label3 = [self createUIlabel:@"腾讯QQ" andFont:FontOfSize12 andColor:RGBACOLOR(149, 149, 149, 1)];
    UILabel * label4 = [self createUIlabel:@"新浪微博" andFont:FontOfSize12 andColor:RGBACOLOR(149, 149, 149, 1)];
    

    [image1 addSubview:btn1];
    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(image1.mas_left).with.offset(30*kWidth);
        make.top.equalTo(image1.mas_top).with.offset(27);
        make.size.mas_equalTo(CGSizeMake(41*kWidth, 41*kHeight));
    }];
    [image1 addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btn1.mas_centerX);
        make.top.equalTo(btn1.mas_bottom).with.offset(8);
    }];
    [image1 addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btn1.mas_right).with.offset(41*kWidth);
        make.centerY.equalTo(btn1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(41*kWidth, 41*kHeight));
    }];
    [image1 addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btn2.mas_centerX);
        make.centerY.equalTo(label1.mas_centerY);
    }];
    [image1 addSubview:btn3];
    [btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btn2.mas_right).with.offset(41*kWidth);
        make.centerY.equalTo(btn1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(41*kWidth, 41*kHeight));
    }];
    [image1 addSubview:label3];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btn3.mas_centerX);
        make.centerY.equalTo(label1.mas_centerY);
    }];
    [image1 addSubview:btn4];
    [btn4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btn3.mas_right).with.offset(41*kWidth);
        make.centerY.equalTo(btn1.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(41*kWidth, 41*kHeight));
    }];
    [image1 addSubview:label4];
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(btn4.mas_centerX);
        make.centerY.equalTo(label1.mas_centerY);
    }];
    
    [self.view addSubview:backView];

}

-(void)pressShareBtn:(UIButton*)sender{
    
    UIButton * btn = (UIButton *)sender;
    if (btn.tag == 100) {
        NSLog(@"点击的是微信分享");
    }
    else if (btn.tag == 200){
        NSLog(@"点击的是微信朋友圈分享");
    }
    else if (btn.tag == 300){
        NSLog(@"点击的是qq分享");

    }
    else{
        NSLog(@"点击的是新浪微博分享");
    }

}
-(void)ShareBackView{
    
    UIView * uiview = (UIView*)[self.view viewWithTag:100000];
    [UIView animateWithDuration:0.3 animations:^{
        uiview.frame = CGRectMake(0, 0, Main_Width, Main_height+200);
    }];

    [self performSelector:@selector(dissMiss) withObject:uiview afterDelay:0.3];
}
-(void)dissMiss{
    UIView * uiview = (UIView*)[self.view viewWithTag:100000];

    [uiview removeFromSuperview];
}

/* 解决触摸手势和cell点击的冲突 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    UIView * uiview = (UIView*)[self.view viewWithTag:100000];
    if ([touch.view isEqual:uiview]) {
        return YES;
    }
    return NO;
}

- (void)goBackLast {
    
    if (_isTwo) {
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[ReceiveViewController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
            _isTwo = NO;
        }
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
