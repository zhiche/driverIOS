//
//  Tool_OrderViewController.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/24.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NullView.h"

@interface Tool_OrderViewController : UIViewController

@property (nonatomic, strong, readwrite) UITableView *tableView;

@property (nonatomic, strong, readwrite) NSMutableArray *dataArray;

@property (nonatomic, weak, readwrite) NullView *tableBackgroundView;

@property (nonatomic, assign) NSUInteger pageNo;

@property (nonatomic, assign, readwrite) NSInteger pageTotal;


- (void)downRefresh;
/**
 下拉加载配置
 */
- (void)upRefresh;

@end
