//
//  Tool_OrderViewController.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/5/24.
//  Copyright © 2017年 Code_An. All rights reserved.
//


#import "Tool_OrderViewController.h"

@interface Tool_OrderViewController ()
<
    UITableViewDelegate,
    UITableViewDataSource
>


@end

@implementation Tool_OrderViewController
{
    
    CGFloat threshold;
    CGFloat itemPerPage;

}


#pragma mark -
#pragma mark -  初始化Vc
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    ///0. 初始化本地数据
    [self initData];
    
    ///1. 设置UI
    [self setUpUI];
    
    ///3. 开始刷新
    ///[self downRefresh];
}



#pragma mark -
#pragma mark - 初始化数据
- (void)initData {
    
    self.pageNo = 1;
    
    self.pageTotal = 10;

    threshold = 0.7;

    itemPerPage = 10;
}

#pragma mark -
#pragma mark - 设置UI
- (void)setUpUI {
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;

    
    ///2. 添加TableView
    [self.view addSubview:self.tableView];
    
    ///3. 添加MJRefresh
    [self addMJRefresh];
    

    
}





#pragma mark -
#pragma mark - Lazy
- (UITableView *)tableView {
    if (!_tableView)
    {
        
        CGRect frame = CGRectMake(0, 64 , screenWidth, screenHeight - 64);
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = GrayColor;
        _tableView.tableFooterView = [[UIView alloc] init];
        
        
        
        NullView *nullView = [[NullView  alloc] initWithFrame:frame
                                                     andTitle:@""
                                                     andImage:[UIImage imageNamed:@"no_order"]];
        nullView.backgroundColor = GrayColor;
        self.tableBackgroundView = nullView;
        _tableView.backgroundView = nullView;
        
        
    }
    return _tableView;
}
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = @[].mutableCopy;
    }
    return _dataArray;
}

#pragma mark -
#pragma mark - 表部分
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView
heightForFooterInSection:(NSInteger)section{
    return 5 * kHeight;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90 * kHeight + 73;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark -
#pragma mark - 网络部分
/**
 MJRefresh
 */
- (void)addMJRefresh {
    
    self.tableView.mj_footer.automaticallyChangeAlpha = YES;
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefresh)];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upRefresh)];
}

/**
 上拉刷新配置
 */
- (void)downRefresh{
    self.tableView.mj_footer.hidden = YES;
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    self.pageNo = 1;
    [self getDataToServerDownRefresh:YES];
}

/**
 下拉加载配置
 */
- (void)upRefresh{
    
    [self.tableView.mj_header endRefreshing];
    
    if (self.pageNo <= (int)self.pageTotal)
    {
        [self getDataToServerDownRefresh:NO];
    }
    else
    {
        self.tableView.mj_footer.state = MJRefreshStateNoMoreData;
    }
    
}
/**
 获取所有急发单列表
 ***
 */
- (void)getDataToServerDownRefresh:(BOOL)isDownRefresh {};


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (self.pageNo == 1 || self.pageNo > self.pageTotal) {
        return;
    }
    CGFloat current = scrollView.contentOffset.y + scrollView.frame.size.height;
    CGFloat total = scrollView.contentSize.height;
    CGFloat ratio = current / total;
    
    CGFloat needRead = itemPerPage * threshold + self.pageNo * itemPerPage;
    CGFloat totalItem = itemPerPage * (self.pageNo + 1);
    CGFloat newThreshold = needRead / totalItem;
    
    
    if (ratio >= newThreshold)
    {
    
        [self.tableView.mj_footer beginRefreshing];
    }
}
@end
