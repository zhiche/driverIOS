//
//  TopBackView.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/11.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TopBackView;

@protocol TopBackViewDelegate <NSObject>

@optional

/**
    导航栏左侧点击
 */
- (void)backViewPopViewController;

/**
    导航栏右侧点击
 */
- (void)naviRightItemClick;

/**
    导航栏中间点击
 */
- (void)naviCenterItemClick;


@end

/**
    自定义导航栏
 */
@interface TopBackView : UIView

@property (nonatomic, weak) id<TopBackViewDelegate>delegate;

@property (nonatomic, strong, readwrite) UIButton *rightButton;

@property (nonatomic, strong, readwrite) UIButton *leftButton;

@property (nonatomic, strong, readwrite) UIImageView *phoneImg;

@property (nonatomic, strong) NSString *naviTitle;

@property (nonatomic, weak, readwrite) UIButton *naviTitleBtn;

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)string;

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)string andHaveSmallImage:(BOOL)smallImage;

@end
