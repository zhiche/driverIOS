//
//  PicCollectionViewCell.h
//  LookPhoto
//
//  Created by zhangsong on 2016/10/28.
//  Copyright © 2016年 章松. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PicCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIImageView *heroImageView;
@end
