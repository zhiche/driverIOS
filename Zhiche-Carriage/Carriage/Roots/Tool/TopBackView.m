//
//  TopBackView.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/5/11.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//  Note : 2017-06-01 浅蓝 Modify.

#import "TopBackView.h"

@interface TopBackView ()



@end

@implementation TopBackView{
    CGRect _newsFrames;
    NSString * _naviTitle;
    BOOL  _smallImage;
}


- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)string {
    
  
    
    self = [super initWithFrame:frame];
    
    if (self)
    {
 
        _newsFrames = frame;
        
        _naviTitle = string;
        
        _smallImage = NO;
        
        [self setUpUI];
        
    }
    return self;
    
}

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)string
            andHaveSmallImage:(BOOL)smallImage {

    
    self = [super initWithFrame:frame];
    
    if (self)
    {
        
         _newsFrames = frame;
        
         _naviTitle = string;
        
         _smallImage = YES;
        
         [self setUpUI];
    }
    return self;

}

- (void)setUpUI {
    self.backgroundColor = YellowColor;
    
    
    [self addSubview:({
        
        CGRect lineFrame = CGRectMake(0, 63.5, _newsFrames.size.width, 0.5f);
        
        UIImageView *lineImageV = [[UIImageView alloc] initWithFrame:lineFrame];
        
        lineImageV;})];
    
    
    
    
    
    [self setUpNaviCenter];
    
    //客服
    self.rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self addSubview:self.rightButton];
    [self.rightButton setTitle:@"" forState:UIControlStateNormal];
    [self.rightButton setTintColor:[UIColor blackColor]];
    self.rightButton.titleLabel.font = Font(15);
    self.rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    self.rightButton.frame = CGRectMake(screenWidth - 100 - 18, 30, 100, 25);
    
    __weak typeof(self) weakSelf = self;
    
    
    self.phoneImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noNews"]];
    self.phoneImg.hidden = YES;
    [self.rightButton addSubview:self.phoneImg];
    [self.phoneImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(weakSelf.mas_right).offset(-13*kWidth);
        make.size.mas_equalTo(CGSizeMake(19*kWidth, 26*kHeight));
        make.centerY.equalTo(weakSelf.rightButton);
    }];
    
    
    //返回
    self.leftButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self addSubview:self.leftButton];
    [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(46, 30));
        make.left.mas_equalTo(0);
        make.centerY.equalTo(self.naviTitleBtn);
    }];
    
    
    UIImageView *backImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"common_back_btn"]];
    [self.leftButton addSubview:backImage];
    [backImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(10.5, 17.5));
        make.center.equalTo(_leftButton);
    }];
    
    
    [self.leftButton addTarget:self
                        action:@selector(backAction)
              forControlEvents:UIControlEventTouchUpInside];
    
    [self.rightButton addTarget:self
                         action:@selector(rightClick)
               forControlEvents:UIControlEventTouchUpInside];
    

}
- (void)setNaviTitle:(NSString *)naviTitle {
    _naviTitle = naviTitle;
    [self.naviTitleBtn setTitle:naviTitle forState:UIControlStateNormal];
}
- (void)setUpNaviCenter {
    
    UIButton *naviTitleBtn = [[UIButton alloc] init];
    self.naviTitleBtn = naviTitleBtn;
    [self addSubview:naviTitleBtn];
    
    [naviTitleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(34.0f);
        make.height.mas_equalTo(16.0f);
    }];
   
    [naviTitleBtn setTitle:_naviTitle forState:UIControlStateNormal];
    [naviTitleBtn setTitleColor:WhiteColor forState:UIControlStateNormal];
    naviTitleBtn.titleLabel.font = Font(15);

    
    if (_smallImage != NO)
    {
        UIImageView *smallImage = [[UIImageView alloc] init];
        [self addSubview:smallImage];
        [smallImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(7.50f, 7.50f));
            make.left.equalTo(naviTitleBtn.mas_right);
            make.bottom.equalTo(naviTitleBtn.mas_bottom);
        }];
        smallImage.image = [UIImage imageNamed:@"icon_pull_down"];
        //smallImage.userInteractionEnabled = YES;
        [naviTitleBtn addTarget:self action:@selector(naviCenterClick) forControlEvents:UIControlEventTouchUpInside];
        
        
        smallImage.userInteractionEnabled = YES;
        UITapGestureRecognizer * PrivateLetterTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAvatarView:)];
        PrivateLetterTap.numberOfTouchesRequired = 1; //手指数
        PrivateLetterTap.numberOfTapsRequired = 1; //tap次数
 
        [smallImage addGestureRecognizer:PrivateLetterTap];
        
    }


}
#pragma mark --- 点击触发的方法，完成跳转
- (void)tapAvatarView: (UITapGestureRecognizer *)gesture
{
    [self naviCenterClick];
}
- (void)backAction {
   
    [self.delegate backViewPopViewController];
}

- (void)rightClick {
    
    [self.delegate naviRightItemClick];
}

- (void)naviCenterClick {
    [self.delegate naviCenterItemClick];
}
@end
