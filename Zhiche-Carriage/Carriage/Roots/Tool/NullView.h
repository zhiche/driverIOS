//
//  NullView.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NullView : UIView
-(id)initWithFrame:(CGRect)frame andTitle:(NSString *)string andImage:(UIImage *)image;
@property (nonatomic,strong) UILabel *label;
@end
