//
//  BigPhotoView.m
//  LookPhoto
//
//  Created by zhangsong on 2016/10/28.
//  Copyright © 2016年 章松. All rights reserved.
//

#import "BigPhotoView.h"
#import "PicCollectionViewCell.h"

@interface BigPhotoView ()<UIScrollViewDelegate>
@property (nonatomic, strong) UICollectionView *supviewCollectionView;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) UIScrollView *imageScrollView;
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, assign) NSInteger offIndex;
@property (nonatomic, strong) UILabel *numLabel;
@end
@implementation BigPhotoView

-(id)initWithFrame:(CGRect)frame imageArray:(NSArray *)imageArray selectPathIndex:(NSIndexPath *)indexPath collectionView:(UICollectionView *)collectionView
{
    self = [super initWithFrame:frame];
    if (self) {
        self.supviewCollectionView = collectionView;
        self.indexPath = indexPath;
        self.imageArray = imageArray;
        self.backgroundColor = [UIColor grayColor];
        
        self.imageScrollView = [[UIScrollView alloc]initWithFrame:frame];
        self.imageScrollView.delegate = self;
        self.imageScrollView.pagingEnabled = YES;
        [self addSubview:self.imageScrollView];
        self.imageScrollView.contentSize = CGSizeMake(self.frame.size.width*imageArray.count, self.frame.size.height);
        
        for (int i=0; i<imageArray.count; i++) {
//            UIImage *image = [UIImage imageNamed:imageArray[i]];
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width*i, 0, self.frame.size.width, self.frame.size.height)];
            imageView.image = imageArray[i];
            imageView.userInteractionEnabled = YES;
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.imageScrollView addSubview:imageView];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickImage:)];
            [imageView addGestureRecognizer:tap];
            
        }
        
        self.numLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, frame.size.width, 20)];
        self.numLabel.textColor = [UIColor whiteColor];
        self.numLabel.textAlignment = NSTextAlignmentCenter;
        self.numLabel.text = [NSString stringWithFormat:@"%zi/%zi",indexPath.row+1,imageArray.count];
        [self addSubview:self.numLabel];
        
        [self.imageScrollView setContentOffset:CGPointMake(self.frame.size.width*indexPath.row, 0) animated:NO];
    }
    return self;
}
-(void)didClickImage:(UITapGestureRecognizer *)tapGesture
{
    self.backgroundColor = [UIColor clearColor];
    //先移除子视图
    [self.numLabel removeFromSuperview];
    [self.imageScrollView removeFromSuperview];
    
    //创建一张相同的图片，添加上去
    UIImageView *imageView = (UIImageView *)tapGesture.view;
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:imageView.image];
    tempImageView.frame = self.bounds;
    tempImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:tempImageView];
    
    //得到imageview相对vc的坐标
    NSIndexPath *selectIndexPath = [NSIndexPath indexPathForRow:self.offIndex inSection:self.indexPath.section];
    PicCollectionViewCell *cell = (PicCollectionViewCell *)[self.supviewCollectionView cellForItemAtIndexPath:selectIndexPath];
    CGRect rect = [cell.heroImageView convertRect:cell.heroImageView.frame toView:self.supviewCollectionView.superview];
    NSLog(@"%.2f--%.2f--%.2f--%.2f", rect.origin.x,rect.origin.y,rect.size.width,rect.size.height);
    
    //动画
    [UIView animateWithDuration:0.5 animations:^{
        self.frame = rect;
        tempImageView.frame = self.bounds;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //滑到的几张图片
    self.offIndex = (NSInteger)scrollView.contentOffset.x/self.frame.size.width;
    self.numLabel.text = [NSString stringWithFormat:@"%d/%d",(int)self.offIndex+1, (int)self.imageArray.count];
}
@end
