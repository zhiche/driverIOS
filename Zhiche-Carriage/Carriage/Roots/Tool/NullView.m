//
//  NullView.m
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/19.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "NullView.h"

@implementation NullView

-(id)initWithFrame:(CGRect)frame andTitle:(NSString *)string andImage:(UIImage *)image
{
    if (self = [super init]) {
        
        self.frame = frame;
        
        self.backgroundColor = GrayColor;
        
        UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMidX(self.frame) - 40, 95 , 80, 80)];
        imageV.image = image;
        [self addSubview:imageV];
        
       self.label = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(imageV.frame) + 14, screenWidth, 20)];
        self.label.textColor = littleBlackColor;
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.font = Font(13);
        self.label.text = string;
        [self addSubview:self.label];
        
        
    }
    
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
