//
//  BigPhotoView.h
//  LookPhoto
//
//  Created by zhangsong on 2016/10/28.
//  Copyright © 2016年 章松. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BigPhotoView : UIView
-(id)initWithFrame:(CGRect)frame imageArray:(NSArray *)imageView selectPathIndex:(NSIndexPath *)indexPath collectionView:(UICollectionView *)collectionView;
@end
