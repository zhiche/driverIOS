//
//  DiTuModel.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/2/27.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "DiTuModel.h"

@implementation DiTuModel{
    
    //接收外界的值
    NSString *_title;
    NSString *_subTitle;
    UIImageView *_image;
    CLLocationCoordinate2D _lc2d;

    
}


- (id)initWithTitle:(NSString *)title subTitle:(NSString *)subTitle lc2d:(CLLocationCoordinate2D)lc2d rightimage:(UIImageView *)image{
    self = [super init];
    if (self) {
        _title = title;
        _subTitle=subTitle;
        _lc2d=lc2d;
        _image=image;
    }
    return self;
}

#pragma mark - mkAnnotation delegate
- (CLLocationCoordinate2D)coordinate{
    return _lc2d;
}
- (NSString *)title{
    return _title;
}
- (NSString *)subtitle{
    return _subTitle;
}
- (UIImageView*)image{
    
    return  _image;
}



@end
