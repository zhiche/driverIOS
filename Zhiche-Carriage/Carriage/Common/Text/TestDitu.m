//
//  TestDitu.m
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/2/27.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import "TestDitu.h"
#import "DiTuModel.h"
@interface TestDitu ()<CLLocationManagerDelegate,MKMapViewDelegate>
{
    UIImageView * nav;
    //位置管理类，用于实现定位功能
    //定位需要使用手机的GPS硬件，定位需要网络，定位数据的来源:Wifi网络,3g、4g网络信息号的来源-运营商的蜂窝基站
    CLLocationManager *_locationManager;
    //苹果封装的地图视图
    MKMapView *_mapView;

}
@end

@implementation TestDitu

- (void)viewDidLoad {
    [super viewDidLoad];
    
    nav = [self createNav:@"地图测试"];
    [self.view addSubview:nav];
    
    //初始化
    _locationManager = [[CLLocationManager alloc] init];
    //设置定位的精确度,kCLLocationAccuracyBest 最高精确度
    //GPS定位是一个比较耗电的功能
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //设置每隔多少米定位一次,kCLDistanceFilterNone 不设限制
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    //iOS8之后info中的两个字段与manager中的两个方法相对应
    //获取操作系统的版本号
    //self.automaticallyAdjustsScrollViewInsets
    NSString *systemVersion = [UIDevice currentDevice].systemVersion;
    if ([systemVersion floatValue]>=8.0) {
        //iOS操作系统的版本号在iOS8以上
        //用户授权后，一直使用定位功能
        //与NSLocationAlwaysUsageDescription中的内容相对应
        //[_locationManager requestAlwaysAuthorization];
        [_locationManager requestWhenInUseAuthorization];
    }
    //设置代理
    _locationManager.delegate = self;
    //开始定位
    [_locationManager startUpdatingLocation];
    //[_locationManager stopUpdatingLocation];
    [self createMapView];

    
}


- (void)createMapView{
    _mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    //设置代理
    _mapView.delegate = self;
    [self.view addSubview:_mapView];
    //设定地图的中心点经纬度坐标,还需要设置地图的缩放系数，由中心坐标+缩放系数能确定出地图的一个显示区域
    CLLocationCoordinate2D lc2d = CLLocationCoordinate2DMake(40.035139, 116.311655);
    //MKCoordinateSpan 设置经度和纬度的缩放系数，一般:0.01-0.1之间
    MKCoordinateSpan span = MKCoordinateSpanMake(0.01,0.01);
    //设置地图的显示区域
    MKCoordinateRegion region = MKCoordinateRegionMake(lc2d, span);
    //将区域结构体给到地图
    [_mapView setRegion:region];
    
    UIImageView * image = [[UIImageView alloc]init];
    image.image = [UIImage imageNamed:@"register_capture"];
    //将一组大头针加到地图上
    for (NSInteger i=0; i<10; i++) {
        CLLocationCoordinate2D lc2d = CLLocationCoordinate2DMake(40.035139+0.01*i, 116.311655+0.01*i);
        //创建大头针
        DiTuModel *pin = [[DiTuModel alloc] initWithTitle:@"主标题" subTitle:@"副标题" lc2d:lc2d rightimage:image];

        [_mapView addAnnotation:pin];
    }
    //长按手势
    UILongPressGestureRecognizer *lp = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(lpMapView:)];
    [_mapView addGestureRecognizer:lp];
}

- (void)lpMapView:(UILongPressGestureRecognizer *)lp{
    //一个手势的有效期划分:began-changed
//    if (lp.state==UIGestureRecognizerStateBegan||lp.state==UIGestureRecognizerStateChanged) {
//        //获取手势在地图上的点坐标
//        //利用地图，将点坐标转化成经纬度
//        //添加大头针
//        CGPoint pt = [lp locationInView:_mapView];
//        //toCoordinateFromView 点坐标的来源视图
//        CLLocationCoordinate2D lc2d=[_mapView convertPoint:pt toCoordinateFromView:_mapView];
//        DiTuModel *pin = [[DiTuModel alloc] initWithTitle:@"新添加" subTitle:@"新添加副标题" lc2d:lc2d];
//        [_mapView addAnnotation:pin];
//    }
}


#pragma mark   location manager delegate
//精确度设置最高，此方法会被时时调用
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    //locations 带有最新的CLLocation位置对象
    CLLocation *location = locations[0];
    //获取到带有经度和纬度的结构体
    CLLocationCoordinate2D lc2d = location.coordinate;
    //获取到结构体中的经度和纬度信息
    NSLog(@"current lat:%f  longt:%f",lc2d.latitude,lc2d.longitude);
}

//当用户拒绝授权位置信息，或者定位失败时，触发此方法
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"error:%@",error.localizedDescription);
}

#pragma mark - map view delegate
//定制大头针头顶上的视图的代理方法
//MKAnnotationView 是大头针顶部视图的父类
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    //mapView中有对大头针顶部视图的重用机制
    static NSString *anViewIde = @"annview";
    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:anViewIde];
    if (pinView==nil) {
        //创建
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:anViewIde];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,50,50)];
        view.backgroundColor = [UIColor purpleColor];
        UIImageView * image = [[UIImageView alloc]init];
        image.frame = CGRectMake(0, 0, 50, 50);
        image.image = [UIImage imageNamed:@"register_capture"];
        [view addSubview:image];
        //设置左侧显示的视图
        pinView.leftCalloutAccessoryView =view;
        
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeInfoDark];
        

//        [btn addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
        //设置右侧
        pinView.rightCalloutAccessoryView = btn;
    }
    //是否显示左、右侧的附加视图
    pinView.canShowCallout = YES;
    
    return pinView;
    
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"点击标注");
    
    
    
    
}

//-(void)pressBtn:(UIButton*)sender{
//    
//    NSLog(@"1231231");
//    
//}


-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    
//    // 获得所有MKAnnotationView
//    NSArray *arr = mapView.annotations;
//    // 被点击的MKAnnotationView的标题和副标题
//    NSString *titleStr = view.annotation.title;
//    NSString *subtitleStr = view.annotation.subtitle;

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
