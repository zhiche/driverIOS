//
//  DiTuModel.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/2/27.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <Foundation/Foundation.h>


#import <MapKit/MapKit.h>
//任何一个继承NSObject，并遵守MKAnnotation协议的对象，都可以被MapView处理成一个大头针
@interface DiTuModel : NSObject<MKAnnotation>

//扩展一个init方法，用于设置大头针对象的title,subTitle,lc2d
- (id)initWithTitle:(NSString *)title subTitle:(NSString *)subTitle lc2d:(CLLocationCoordinate2D)lc2d rightimage:(UIImageView *)image;




//设备did
@property (nonatomic,strong) NSString * did;
//最后一次定位时间
@property (nonatomic,strong) NSString * positiondate;
//纬度
@property (nonatomic) double lat;
//经度
@property (nonatomic) double lng;
//位置
@property (nonatomic,strong) NSString * address;




@end
