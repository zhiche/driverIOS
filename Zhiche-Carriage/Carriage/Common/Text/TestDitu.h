//
//  TestDitu.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 17/2/27.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavViewController.h"
#import <CoreLocation/CoreLocation.h>//封装了系统自带的GPS定位功能
#import <MapKit/MapKit.h> //封装了系统自带的地图功能:iOS6之前，苹果系统默认调用的为谷歌地图;iOS6苹果系统开始内置自家的地图,地图在中国的数据由高德提供,地图数据的显示需要网络


@interface TestDitu : NavViewController



@end
