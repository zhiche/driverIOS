//
//  testBaiduVC.h
//  Zhiche-Carriage
//
//  Created by 王亚陆 on 16/11/2.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
@interface testBaiduVC : UIViewController <BMKMapViewDelegate,BMKLocationServiceDelegate>
{
    BMKLocationService* _locService;

}
@end
