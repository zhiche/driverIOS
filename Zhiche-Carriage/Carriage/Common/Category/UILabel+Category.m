//
//  UILabel+Category.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/14.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import "UILabel+Category.h"

@implementation UILabel (Category)
+ (UILabel *)createUIlabel:(NSString *)title
                   andFont:(CGFloat)font
                  andColor:(UIColor*)color {
    UILabel * label =[[UILabel alloc]init];
    label.text = title;
    label.textColor = color;
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:font];CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    CGFloat nameH = size.height;
    CGFloat nameW = size.width;
    label.textAlignment = NSTextAlignmentCenter;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(font);
    return label;
}
+ (UILabel *)backLabelFont:(int)a andTextColor:(UIColor *)color andString:(NSString *)string
{
    UILabel *label = [[UILabel alloc]init];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = Font(a);
    label.text = string;
    label.textColor = color;
    
    return  label;
}
@end
