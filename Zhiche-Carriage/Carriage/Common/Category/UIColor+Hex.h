//
//  UIColor+Hex.h
//  zhiche--delivery
//
//  Created by LeeBruce on 16/7/24.
//  Copyright © 2016年 Code_JM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (instancetype)colorWithHexString:(NSString *)stringToConvert;

+ (instancetype)zj_ColorWithHex:(u_int32_t)hex;

+ (instancetype)zj_ColorWithHex:(u_int32_t)hex alpha:(CGFloat)alpha;

@end
