//
//  NSString+jmCategory.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/18.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (jmCategory)

+ (NSString *)logDic:(NSDictionary *)dic;

@end
