//
//  UILabel+Category.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/14.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Category)
+ (UILabel *)createUIlabel:(NSString *)title
                   andFont:(CGFloat)font
                  andColor:(UIColor*)color;
+ (UILabel *)backLabelFont:(int)a andTextColor:(UIColor *)color andString:(NSString *)string;

@end
