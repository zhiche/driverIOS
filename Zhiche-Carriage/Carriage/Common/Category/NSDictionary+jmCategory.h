//
//  NSDictionary+jmCategory.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/17.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (jmCategory)

- (NSDictionary *)dictionaryByReplacingNullsWithStrings;

@end
