//
//  UIView+jmCategory.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/17.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (jmCategory)
//断网重新加载
+ (UIView*)noInternet:(NSString *)title and:(UIViewController *)target action:(SEL)action andCGreck:(CGRect)fram;
//拨打电话
+ (UIView *)call:(NSString *)iphone and:(UIViewController *)target action:(SEL)action;

+ (UIView*)customNavigationItem:(UIViewController *)target action:(SEL)action;
- (CABasicAnimation *)opacityForever_Animation:(float)time;
@end
