//
//  UIView+Extension.h
//  Uni-Desgin
//
//  Created by 深圳华尔兹科技 on 15/10/15.
//  Copyright © 2015年 华尔兹r.Jr All rights reserved.
//
#import <UIKit/UIKit.h>
@interface UIView (Extension)
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize size;
- (void)settingLayerForView:(UIView *)imageView borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)color cornerRadius:(CGFloat)cornerRadius;
@end




