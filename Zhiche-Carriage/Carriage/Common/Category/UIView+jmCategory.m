//
//  UIView+jmCategory.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/17.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import "UIView+jmCategory.h"
#import <Masonry.h>
@implementation UIView (jmCategory)

- (CABasicAnimation *)opacityForever_Animation:(float)time
{
    CABasicAnimation  *animation = [CABasicAnimation animationWithKeyPath  : @"opacity" ]; // 必须写 opacity 才行。
    
    animation.fromValue = [NSNumber numberWithFloat : 1.0f];
    
    animation.toValue = [NSNumber numberWithFloat : 0.0f]; // 这是透明度。
    
    animation.autoreverses =  YES ;
    
    animation.duration = time;
    
    animation.repeatCount =  MAXFLOAT ;
    
    animation.removedOnCompletion  = NO ;
    
    animation.fillMode  = kCAFillModeForwards  ;
    
    animation.timingFunction = [CAMediaTimingFunction functionWithName  : kCAMediaTimingFunctionEaseIn]; /// 没有的话是均匀的动画。
    
    return animation;
    
}
+ (UIView*)customNavigationItem:(UIViewController *)target action:(SEL)action{
    
    UIView * NaView =[[UIView alloc]initWithFrame:CGRectMake(0, 20, Main_Width, 36)];
    UIButton * btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(14, 8, 28, 28);
    [btn1 setBackgroundImage:[UIImage imageNamed:@"detials_project_top_back"] forState:UIControlStateNormal];
    btn1.tag = 1;
    [btn1 addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [NaView addSubview:btn1];
    UIButton * btn2 =[UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(Main_Width-14-28, 8, 28, 28);
    [btn2 setBackgroundImage:[UIImage imageNamed:@"detials_project_top_share"] forState:UIControlStateNormal];
    btn2.tag = 2;
    [btn2 addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [NaView addSubview:btn2];
    UIButton * btn3 =[UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(Main_Width-14-28-28-15, 8, 28, 28);
    [btn3 setBackgroundImage:[UIImage imageNamed:@"detials_project_top_collect"] forState:UIControlStateNormal];
    btn3.tag = 3;
    [btn3 addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [NaView addSubview:btn3];
    
    return NaView;
}

+ (UIView*)noInternet:(NSString *)title and:(UIViewController *)target action:(SEL)action andCGreck:(CGRect)frame
{
    
    UIView * backview = [[UIView alloc]initWithFrame:frame];
    backview.backgroundColor = [UIColor whiteColor];
    UILabel * label = [[UILabel alloc]init];
    label.text = @"当前网络不可用，请稍后尝试";
    UIFont *fnt = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    CGSize size = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:fnt,NSFontAttributeName, nil]];
    label.textColor = RGBACOLOR(149, 149, 149, 1);
    // 名字的H
    CGFloat nameH = size.height;
    // 名字的W
    CGFloat nameW = size.width;
    label.frame =CGRectMake(0, 0, nameW, nameH);
    label.font = Font(12.0f);
    [backview addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo (backview.mas_centerX);
        make.centerY.mas_equalTo (backview.mas_centerY);
    }];
    
    UIImageView * imageBack = [[UIImageView alloc]init];
    imageBack.image = [UIImage imageNamed:@"jiaohu_duanwang_pic"];
    [backview addSubview:imageBack];
    [imageBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo (label.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(76, 75));
        make.bottom.mas_equalTo(label.mas_top).with.offset(-10);
    }];
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"jiaohu_duanwang_juxing"] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor whiteColor];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"刷新重试" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backview addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo (label.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(100, 35));
        make.top.mas_equalTo(label.mas_bottom).with.offset(10);
    }];
    
    return backview;
}

+ (UIView*)call:(NSString *)iphone and:(UIViewController *)target action:(SEL)action{
    
    UIView * backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Width, Main_height)];
    backView.tag = 1000;
    backView.backgroundColor = RGBACOLOR(0, 0, 0, 0.3);
    
    UIImageView * imageIphone = [[UIImageView alloc]initWithFrame:CGRectMake(14, 271, Main_Width-28, 125)];
    imageIphone.image = [UIImage imageNamed:@"common_400call_background"];
    imageIphone.tag = 2000;
    imageIphone.userInteractionEnabled = YES;
    imageIphone.hidden = YES;
    imageIphone.layer.cornerRadius=20/3.0;
    imageIphone.layer.masksToBounds=YES;
    imageIphone.backgroundColor = [UIColor whiteColor];
    UILabel * title1 = [[UILabel alloc]init];
    title1.text = @"咨询投诉、手艺人入驻可拨打客服电话。";
    title1.textColor = RGBACOLOR(149, 149, 149, 1);
    title1.textAlignment = NSTextAlignmentCenter;
    UILabel * title2 = [[UILabel alloc]init];
    title2.textColor = RGBACOLOR(149, 149, 149, 1);
    title2.text = @"客服时间：7X24小时";
    title2.textAlignment = NSTextAlignmentCenter;
    title2.font = Font(FontOfSize12);
    title1.font = Font(FontOfSize12);
    [imageIphone addSubview:title1];
    [imageIphone addSubview:title2];
    
    [title1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageIphone.mas_left).with.offset(65);
        make.right.equalTo(imageIphone.mas_right).with.offset(-65);
        make.top.equalTo(imageIphone.mas_top).with.offset(20);
    }];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageIphone.mas_left).with.offset(100);
        make.right.equalTo(imageIphone.mas_right).with.offset(-100);
        make.top.equalTo(title1.mas_bottom).with.offset(8);
    }];
    //    UIImageView * image1 = [[UIImageView alloc]init];
    //    image1.image = [UIImage imageNamed:@"common_list_line1"];
    //    [backView addSubview:image1];
    //    [image1 mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.equalTo(imageIphone.mas_left).with.offset(117);
    //        make.right.equalTo(imageIphone.mas_right).with.offset(-117);
    //        make.top.equalTo(title1.mas_bottom).with.offset(15);
    //    }];
    //
    UIButton * cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelBtn.tag = 1000;
    [cancelBtn setTitleColor:RGBACOLOR(149, 149, 149, 1) forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = Font(17);
    [cancelBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    UIButton * dialBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [dialBtn setTitle:@"确定" forState:UIControlStateNormal];
    [dialBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    dialBtn.tag = 2000;
    dialBtn.titleLabel.font = Font(17);
    [dialBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [imageIphone addSubview:cancelBtn];
    [imageIphone addSubview:dialBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageIphone.mas_left).with.offset(66);
        make.size.mas_equalTo(CGSizeMake(40, 30));
        make.bottom.equalTo(imageIphone.mas_bottom).with.offset(-15);
    }];
    [dialBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(imageIphone.mas_right).with.offset(-66);
        make.size.mas_equalTo(CGSizeMake(40, 30));
        make.bottom.equalTo(imageIphone.mas_bottom).with.offset(-15);
    }];
    
    
    
    imageIphone.hidden = NO;
    [backView addSubview:imageIphone];
    
    return backView;
}

@end
