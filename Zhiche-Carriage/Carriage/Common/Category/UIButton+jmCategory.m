//
//  UIButton+jmCategory.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/17.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import "UIButton+jmCategory.h"

@implementation UIButton (jmCategory)
//点击button
+ (UIButton*)createBtn:(NSString *)title andFont:(long)font andTag:(NSInteger)tag and:(UIViewController *)target action:(SEL)action andTitleColor:(UIColor *)color andBorderColor:(UIColor*)borderColor andBackgroundColor:(UIColor*)backgroundColor{
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.layer.borderWidth = 0.5;
    btn.layer.borderColor = borderColor.CGColor;
    btn.titleLabel.font = Font(font);
    btn.backgroundColor = backgroundColor;
    if (color) {
        [btn setTitleColor:color forState:UIControlStateNormal];
    }else{
        [btn setTitleColor:BlackColor forState:UIControlStateNormal];
    }
    btn.tag = tag;
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return btn;
    
}
@end
