//
//  NSDictionary+jmCategory.m
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/17.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import "NSDictionary+jmCategory.h"

@implementation NSDictionary (jmCategory)
- (NSDictionary *)dictionaryByReplacingNullsWithStrings {
    
    const NSMutableDictionary *replaced = [self mutableCopy];
    
    const id nul = [NSNull null];
    
    const NSString *blank = @"";
    
    for(NSString *key in self) {
        const id object = [self objectForKey:key];
        if(object == nul) {
            //pointer comparison is way faster than -isKindOfClass:
            //since [NSNull null] is a singleton, they'll all point to the same
            //location in memory.
            [replaced setObject:blank
                         forKey:key];
        }
    }
    
    return [replaced copy];
    
}
@end
