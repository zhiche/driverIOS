//
//  UIButton+jmCategory.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/17.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (jmCategory)
//button点击
+ (UIButton*)createBtn:(NSString *)title andFont:(long)font andTag:(NSInteger)tag and:(UIViewController *)target action:(SEL)action andTitleColor:(UIColor *)color andBorderColor:(UIColor*)borderColor andBackgroundColor:(UIColor*)backgroundColor;

@end
