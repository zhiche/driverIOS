//
//  CustomTextField.m
//  zhiche--delivery
//
//  Created by 王亚陆 on 16/7/1.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

//控制清除按钮的位置

-(CGRect)clearButtonRectForBounds:(CGRect)bounds

{

    return CGRectMake(bounds.origin.x + bounds.size.width - 50, bounds.origin.y + bounds.size.height -20, 16, 16);
    
}


//控制placeHolder的位置，左右缩20
-(CGRect)placeholderRectForBounds:(CGRect)bounds

{
    //return CGRectInset(bounds, 20, 0);
    UILabel * label = [[UILabel alloc]init];
    label.text = @"必填";
    label.font = Font(FontOfSize12);
    [label sizeToFit];
    
    CGFloat heigth = bounds.size.height;
    CGFloat width = bounds.size.width;
    CGRect inset = CGRectMake(width/3,heigth/2.0-label.frame.size.height/2.0, bounds.size.width, bounds.size.height);//更好理解些

    return inset;
}

//控制显示文本的位置
-(CGRect)textRectForBounds:(CGRect)bounds

{
    
    //return CGRectInset(bounds, 50, 0);
    CGRect inset = CGRectMake(bounds.origin.x+10, bounds.origin.y, bounds.size.width -10, bounds.size.height);//更好理解些
    return inset;
}

//控制编辑文本的位置 (输入之后文本的位置)
-(CGRect)editingRectForBounds:(CGRect)bounds
{
    
    //return CGRectInset( bounds, 10 , 0 );
    CGRect inset = CGRectMake(bounds.origin.x , bounds.origin.y, bounds.size.width -10, bounds.size.height);
    
    return inset;
}

//控制左视图位置
- (CGRect)leftViewRectForBounds:(CGRect)bounds
{
    
    CGRect inset = CGRectMake(bounds.origin.x +100, bounds.origin.y, bounds.size.width-250, bounds.size.height);
    
    return inset;
    //return CGRectInset(bounds,50,0);
}

//控制placeHolder的颜色、字体
-(void)drawPlaceholderInRect:(CGRect)rect

{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, YellowColor.CGColor);
//    [[UIColor orangeColor] setFill];
    [self.placeholder drawInRect:rect withFont:[UIFont systemFontOfSize:12]];
}

@end
