//
//  Header.h
//  Zhiche-Carriage
//
//  Created by LeeBruce on 16/6/6.
//  Copyright © 2016年 LeeBruce. All rights reserved.
//




#ifndef Header_h
#define Header_h

#ifdef DEBUG

#define NSLog(fmt, ...) NSLog((@"%s [Line %d]" fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#else

#define NSLog(...)

#endif


#import <SDWebImage/UIImageView+WebCache.h>
#import <Masonry/Masonry.h>
#import <MJRefresh/MJRefresh.h>
#import <MJExtension.h>
#import "WKProgressHUD.h"
#import "UIView+Extension.h"
#import "HTTPHeader.h"

#import "DriverConstant.h" ///司机版本APP 定义的常量

#import "Common.h"

#import "NSDictionary+jmCategory.h"

#define screenWidth     [UIScreen mainScreen].bounds.size.width
#define screenHeight    [UIScreen mainScreen].bounds.size.height
#define Main_Width      [UIScreen mainScreen].bounds.size.width
#define Main_height     [UIScreen mainScreen].bounds.size.height


#define NaviStatusH  64.0f

#define Font(a)     [UIFont systemFontOfSize:a * kWidth];

#pragma mark 宽高比cc

#define kWidth  screenWidth/320.0
#define kHeight screenHeight/568.0

#define cellHeight  43 * kHeight

#define phoneNumber @"400-880-9009"

#define UniColor(r, g, b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1.0]

#define UnColor(r, g, b, a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

#define UniRandomColor [UIColor colorWithRed:(arc4random_uniform(256))/255.0f green:(arc4random_uniform(256))/255.0f blue:(arc4random_uniform(256))/255.0f alpha:1.0]



//颜色
#define Color_RGB(a,b,c,d) [UIColor colorWithRed:(a)/255.0 green:(b)/255.0 blue:(c)/255.0 alpha:(d)]

#define WhiteColor [UIColor whiteColor]
#define BlackColor [UIColor blackColor]

#define YellowColor  Color_RGB(253,131,42,1)//主题黄

#define GreenColor  Color_RGB(132, 189, 110,1)//已读
#define RedColor  Color_RGB(254,81,62,1)//状态红
#define GrayColor  Color_RGB(236,236,236,1)//headerView 灰
#define LineGrayColor  Color_RGB(210,210,210,1)//分割线 灰
#define fontGrayColor  Color_RGB(157,157,157,1)// 字体浅灰
#define littleBlackColor  Color_RGB(79,79,79,1)// 字体浅黑
#define carScrollColor   Color_RGB(119, 119, 119, 1)//宝马－2辆
#define rearrangeBtnColor   Color_RGB(254, 242, 227, 1)//重新发布
#define BlackTitleColor Color_RGB(14, 14, 14, 1)//发布信息 黑色体 地址、时间、车型
#define BtnBorderColor  Color_RGB(253,217,185,1)// 按钮外环
//80, 80, 80
#define BtnTitleColor  Color_RGB(80,80,80,1)// 下单发运时间按钮
#define AddressSCtitleColor Color_RGB(109, 109, 109,1)//地址删除按钮
#define FieldPlacerColor  Color_RGB(181, 181,188,1)//下单发运 汽车数量背景字体
#define AddCarColor  Color_RGB(34,34,34,1)//增加车辆
#define AddCarNameBtnColor  Color_RGB(199,199,199,1)//车名称
#define headerBottomColor  Color_RGB(252,252,252,1) //运单上下颜色
#define RGBACOLOR(a,b,c,d) Color_RGB(a,b,c,d)
#define headerTitleColor Color_RGB(255, 79, 61,1)//查看报价上面的分钟
#define headerBackViewColor Color_RGB(252, 252, 252,1)//查看报价上面背景颜色
#define RGBACOLOR(a,b,c,d) Color_RGB(a,b,c,d)

//字体大小
#define FontOfSize9  9.0
#define FontOfSize10  10.0
#define FontOfSize11  11.0
#define FontOfSize12  12.0
#define FontOfSize13  13.0
#define FontOfSize14  14.0
#define FontOfSize15  15.0
#define FontOfSize16  16.0
#define FontOfSize17  17.0
#define FontOfSize18  18.0
#define FontOfSize19  19.0

//用户类型
#define userType @"50"
#define Button_height 40 * kHeight




#endif /* Header_h */
