//
//  HTTPHeader.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/18.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#ifndef HTTPHeader_h

#define HTTPHeader_h

/**
    测试环境
 */
#if CeShi
/**
    Servers
 */
//#define Main_interface  @"http://119.57.140.26/sendbydriveruat/"

//#define Main_interface  @"http://10.2.7.180:8080/"

#define Main_interface  @"http://sd.unlcn.com/sendbydriver/"

/******************************************************  开发环境*/

#elif KaiFa

/** Servers */
#define Main_interface  @"http://119.57.140.26/sendbydriver/"

/** Unlcn 5G */
//#define Main_interface  @"http://10.2.6.206:8080/"

/** shenzhen 5G */
//#define Main_interface @"http://192.168.2.199:8080/"

/** LiGuanglu */
//#define Main_interface @"http://192.168.199.157:8080/"

#else

/**
    生产发布环境
 */

#define Main_interface  @"http://sendbydriver.unlcn.com/"

//#define Main_interface @"http://sendbydriver.huiyunche.cn/"

//#define Main_interface @"http://192.168.199.199:8090/driver-personnel/"

//#define Main_interface @"http://192.168.199.217:8080/"

//#define Main_interface @"http://119.57.140.26/sendbydriveruat/"

//#define Main_interface @"http://sendbydrivernew.huiyunche.cn/"


#endif

#define STR_F2(p1, p2)           [NSString stringWithFormat:@"%@%@", p1, p2]

#define STR_F3(p1, p2, p3)       [NSString stringWithFormat:@"%@%@%@", p1, p2, p3]

#define STR_F4(p1, p2, p3, p4)   [NSString stringWithFormat:@"%@%@%@%@", p1, p2, p3, p4]

/**
    首页轮播图
 */
#define banner_list_Url STR_F2(Main_interface,@"banner/list")

/**
    首页用户状态waybill/pageinfo
 */
#define waybill_pageinfo_Url STR_F2(Main_interface,@"waybill/pageinfo")


//上线排队
#define driver_online_Url STR_F2(Main_interface,@"user/loginOnline")

//取消排队
#define user_offline_Url STR_F2(Main_interface,@"user/offline")

//获取排队情况
#define driver_queue_Url STR_F2(Main_interface,@"user/queue")

//出车记录
#define waybill_done_Url STR_F2(Main_interface,@"waybill/done")



//账单列表和时间列表
#define Find_summaryList_Url STR_F2(Main_interface,@"/billSummary/summaryList")
//账单详情
#define Find_myBillDetail_Url STR_F2(Main_interface,@"/billSummary/billDetailList")


//消息列表
#define News_List_Url STR_F2(Main_interface,@"/notice/list")
//消息详情
#define News_detail_Url STR_F2(Main_interface,@"/notice/")



/*
 我的运单
 */

//运单详情
#define mission_detail STR_F2(Main_interface,@"waybill/")
//运单列表
#define waybill_list STR_F2(Main_interface,@"waybill/list")

//急发单列表
#define quickDispatchList STR_F2(Main_interface,@"waybill/quickDispatchList")

///抢单
#define competeQuickOrderList STR_F2(Main_interface,@"waybill/competeQuickOrderList")

/*
 事故申报
 */
//事故上报
#define accident_modify STR_F2(Main_interface,@"accident/modify")

//事故列表
#define accident_list STR_F2(Main_interface,@"accident/list")

//带伤发运

#define attach_modify STR_F2(Main_interface,@"attach/modify")
//查询带伤照片
#define attach_listpic STR_F2(Main_interface,@"attach/listpic")

//上报交车照片
#define stan_waybill_delivery STR_F2(Main_interface,@"waybill/delivery")

/*
 个人中心
 */
//用户个人信息
#define  user_info STR_F2(Main_interface,@"user")

//修改个人信息
#define user_modify   STR_F2(Main_interface,@"user/modify")
//意向方向
#define addr_modify   STR_F2(Main_interface,@"addr/modify")
//价格说明
#define price_show STR_F2(Main_interface,@"templates/cms/price-explain.html")
//客服中心

#define about_contact STR_F2(Main_interface,@"about/contact")


/*
 common
 */
//七牛token
#define qiniu_token  STR_F2(Main_interface,@"qiniu/upload/ticket")

//七牛头像token
#define picture_qiniu_token  STR_F2(Main_interface,@"qiniu//uploadlogo/ticket")

//登录
#define login_codeLogin STR_F2(Main_interface,@"login/userlogin")
//注册
#define registers STR_F2(Main_interface,@"sign")

//忘记密码
#define forgetpassword STR_F2(Main_interface,@"login/resetpwd")
//注册获取验证吗
#define  get_capture STR_F2(Main_interface,@"sign/captcha")



//检测token是否过期
#define token_check  STR_F2(Main_interface,@"user/check")

// 登录成功返回token
#define  login_token @"login_token"

#define  application_json @"application/json"

//gps地址跟踪
#define trail_Url STR_F2(Main_interface,@"user/position")


/*
 钱包
 */

//我的银行卡列表
#define bankcard_list STR_F2(Main_interface,@"cash/list")
//添加银行卡
#define bind_bankcard STR_F2(Main_interface,@"cash/bind")
//解绑银行卡
#define unbind_bankcard STR_F2(Main_interface,@"cash/unbind")

//提现
#define cost_cash STR_F2(Main_interface,@"cash/cost")

//检测银行卡类型
#define check_bankcard STR_F2(Main_interface,@"cash/check")
//判断用户是否已经设置提现密码
#define exist_pwd STR_F2(Main_interface,@"accntLedger/exist/pwd")

//交易记录
#define user_trade STR_F2(Main_interface,@"accntDetail/details")

/**
 *
 *  提现密码
 */

//验证原始密码
#define check_pwd STR_F2(Main_interface,@"accntLedger/check/pwd")

//输入新密码
#define update_pwd STR_F2(Main_interface,@"accntLedger/update/pwd")
//忘记密码
#define set_password STR_F2(Main_interface,@"accntLedger/set/password")



//行程上报
#define journey_Url STR_F2(Main_interface,@"journey/add")



//司机活跃度 行程onlineinfo
#define onlineinfo STR_F2(Main_interface,@"complex/onlineinfo")

//用户注册 服务协议
#define templates_Url STR_F2(Main_interface,@"templates/cms/user-protocol-view.html")

//钱包说明
#define  wallet_explain  STR_F2(Main_interface,@"templates/cms/wallet-explain.html")



//下线
#define user_offLine STR_F2(Main_interface,@"user/modifyuserstatus")
//余额信息
#define user_balances STR_F2(Main_interface,@"accntLedger/balances")






//长跑线路列表
#define often_line STR_F2(Main_interface,@"addr/list")


//确认交车
#define waybill_delivery STR_F2(Main_interface,@"waybill/delivery")




//积分查询
#define score_details STR_F2(Main_interface,@"scores/details")

//查询手续费
#define with_drawfee STR_F2(Main_interface,@"accntLedger/withdrawfee")

 

#endif /* HTTPHeader_h */
