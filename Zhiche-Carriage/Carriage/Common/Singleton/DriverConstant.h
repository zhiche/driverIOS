//
//  DriverConstant.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/14.
//  Copyright © 2017年 LeeBruce. All rights reserved.
//

#ifndef DriverConstant_h
#define DriverConstant_h

///APP进入前台的通知
#define AppIntoForeground @"foreground"

///APP进入后台的通知
#define AppIntoBackground @"background"


#endif /* DriverConstant_h */
