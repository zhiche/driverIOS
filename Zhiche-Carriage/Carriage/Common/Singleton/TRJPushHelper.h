//
//  TRJPushHelper.h
//  欣窝互联网(深圳)有限公司
//
//  Created by User on 16/5/20.
//  Copyright © 2016年 张 杰. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

/*
 * @brief 极光推送相关API封装
 *
 */
@interface TRJPushHelper : NSObject
/**
 *
 *  在应用启动的时候调用
 *
 ***/
+ (void)setupWithOptions:(NSDictionary *)launchOptions;

- (void)setupWithOptions:(NSDictionary *)launchOptions uuidString:(NSString *)uuidString;
/**
 *
 *  在AppDelegate注册设备处调用
 *
 ***/
+ (void)registerDeviceToken:(NSData *)deviceToken;
/**
 *
 * iOS7以后，才有Completion，否则传nil
 *
 ***/
+ (void)handleRemoteNotification:(NSDictionary *)userInfo completion:(void (^)(UIBackgroundFetchResult))completion;
/**
 *
 * 显示本地通知在最前面
 *
 ***/
+ (void)showLocalNotificationAtFront:(UILocalNotification *)notification;
/**
 *
 * 上传别名到极光推送
 *
 ***/
+ (void)uploadToJpushAlias:(NSString *)alias andToServier:(BOOL)isServer;
/**
 *
 *   给服务器发别名
 *
 * 1 日志发送  你有新的日志点评
 * 2 点评日志  你的日志已被点评
 * 3 日志回复  你有新的日志回复
 *
 * 4 审批发送  你有新的审批复
 * 5 点评审批  你的审批已被批复
 * 6 审批回复  你有新的审批回复
 *
 * 7 销售流程进入下一阶段时发送  你有新的销售流程确认
 * 8 销售流程被确认时发送  你的销售流程已被确认
 *
 * 10 新建客户审核     你有新的报备客户审核
 * 11 被审核提醒新建人  新的报备客户已审核
 *
 *
 ***/
+ (void)uploadToServerAlias:(NSString *)alias push_status:(NSString *)push_status;

+ (TRJPushHelper *)sharedHelper;

@end
