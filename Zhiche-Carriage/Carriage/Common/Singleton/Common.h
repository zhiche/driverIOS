//
//  Common.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 17/4/18.
//  Copyright © 2017年 Code_JM. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

typedef void(^DownloadFinishedBlock)(id responseObj);

typedef void(^DownloadFailedBlock)(NSString *errorMsg);

typedef NS_ENUM(NSInteger, item_position) {
    item_left = 0,
    item_right
};

@interface Common : NSObject

UIKIT_EXTERN  NSString * const UUIDAccount;


+ (Common *)shared;

///HTTP GET
+ (void)requestWithUrlString:(NSString *)urlString
                 contentType:(NSString *)type
                  errorShow:(BOOL)errorShow
                    finished:(DownloadFinishedBlock)finished
                      failed:(DownloadFailedBlock)failedBlock;

///HTTP Post
+ (void)afPostRequestWithUrlString:(NSString *)urlString
                             parms:(NSDictionary *)dic
                     finishedBlock:(DownloadFinishedBlock)finished
                       failedBlock:(DownloadFailedBlock)failed;

///HTTP Post
- (void)afPostRequestWithUrlString:(NSString *)urlString
                             parms:(NSDictionary *)dic
                     finishedBlock:(DownloadFinishedBlock)finished
                       failedBlock:(DownloadFailedBlock)failed;


///HTTP 上传图片
- (void)afUploadImageWithUrlString:(NSString *)urlString
                             parms:(NSDictionary *)dic
                         imageData:(NSData *)data
                          imageKey:(NSString *)key
                     finishedBlock:(DownloadFinishedBlock)finished
                       failedBlock:(DownloadFailedBlock)failed;





///判断是否登录
+ (void)isLogin:(UIViewController *)target
        andPush:(UIViewController *)next;



///判断用户类型
+ (BOOL)judgeUsertype;



///创建一个消息提示
- (UIImageView *)createUIImage:(NSString *)title
                      andDelay:(float)delay;


///创建一个消息提示框
+ (void)showAlertViewAndAnimated:(BOOL)animated
                       andString:(NSString *)string;

 


+ (float)contentText:(NSString *)content
                fonts:(UIFont *)fonts
            maxWidth:(float)maxWidth
           maxHeight:(float)maxHeight;

+ (float)contentBackWidthText:(NSString *)content
                        fonts:(UIFont *)fonts
                     maxWidth:(float)maxWidth
                    maxHeight:(float)maxHeight;


+ (UIAlertController *)sureAlertsControllerWithMessage:(NSString *)message
                                                action:(void (^)(UIAlertAction *action))handler;

+ (UIAlertController *)zj_QueueAlertsControllerWithMessage:(NSString *)message
                                             continueTitle:(NSString *)continueTitle
                                               cancelTitle:(NSString *)cancelTitle
                                                cancelAction:(void (^)(UIAlertAction *action))handler;

+ (UIAlertController *)zj_alertsControllerWithMessage:(NSString *)message
                                             title:(NSString *)title
                                            sureItem:(NSString *)sureItem
                                              sureAction:(void (^)(UIAlertAction *action))handler;








@end
