//
//  AnAdapterNib.h
//  Zhiche-Carriage
//
//  Created by Code_浅蓝 on 2017/6/13.
//  Copyright © 2017年 Code_An. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  button
 */
@interface UIButton (MyFont)

@end


/**
 *  Label
 */
@interface UILabel (myFont)

@end

/**
 *  TextField
 */

@interface UITextField (myFont)

@end

/**
 *  TextView
 */
@interface UITextView (myFont)

@end





